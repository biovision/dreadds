/** Analysis of Gerrit Hilgen experiments.
    We fit his data with the model for the cells population he gave us.
    We make the stats on models parameters in CTL and CNO conditions
    
    Bruno Cessac, 21-09-2022
***/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include "Defs.h"
#include "Utils.h"
#include "Plot.h"
#include "Spectrum.h"
#include "Dynamics.h"
#include "Fitting.h"
#include <unistd.h>

int main(void){
  printf("\n\nAnalysis of Gerrit Hilgen experiments\n");

  /** Initializations of the main variables ***/
  double convert_s_to_ms=1000; //Conversion s to ms. 1s = 1000 ms
  double convert_ms_to_s=0.001; //Conversion ms to s. 1 ms = 0.001 s
  double weight_factor=1E-3;
  
  void (*f)(double *X,double *Xp,double t,params p,double (*input)(double x,double t, params p))=NULL;
  void (*fS)(double x,double *X,double *Xp,double t,params p,double (*input)(int beta,double x,double t, params p))=NULL;

  /*** Initialization of the parameters variable p ***/
  params p;//Cells parameters

  //Lattice
  p.N=60; //Number of visible neurons (excluding boundaries)
  if (p.N%2) {printf("N must be even. Exiting \n");exit(0);} //Used only for spectral computing, avoiding a zero eigenvalue in the Laplacian spectrum
  p.offset=0;//(int)(5.0*sigmabg/p.dx);//Number of invisible neurons (on the left of the neuron 0 and on the right to neuron N-1) to avoid boundaries effects. The offset is computed so that the visible neurons at the boundaries are at a distance 5 sigma from the offset.
  if (p.offset!=0) {printf("The case p.N \neq p.L (non zero offset) may not work properly\n");exit(0);}
  p.L=p.N+2*p.offset; //Total number of neurons. Be careful if L\neq N parts of the code might not work.
  //printf("offset=%d\n",p.offset); exit(0);
  p.dt=0.1; //Integration time step (ms)
  p.dx=0.03; //x Cells spacing (mm)
  p.dy=0.03; //y Cells spacing (mm)
  
  //Cells
  p.tauL=C/gL;//  printf("tauL=%g ms\n",p.tauL);exit(0);
  
  //Bipolars
  p.thetaB=0;//Threshold for bipolar rectification (mV)
  p.tauBref=p.tauB=50;//Baseline bipolar voltage integration characteristic time (ms). This is the leak characteristic time.
  p.CB=C;//Bipolar membrane capacity (pF)
  p.gLB=gL;//p.CB/p.tauB; //printf("gLB=%lg\n",p.gLB);exit(0);//Bipolar leak conductance (nS) 
  p.ELB=0;
  p.unsurtauB=1.0/p.tauB;//,unsurtaua=1.0/p.taua;
  p.KB=p.gLB*p.ELB/p.CB; //Constant fixed by the leak potential (mV/ms)

 //Gain control
  p.taua=10;//Bipolar activity characteristic time (ms)
  p.h=1E-2;//Activity rate (mV^-1 ms^-1)

  //Time RF of BCells(second order kernel) (fit from Gerrit Hilgen's data)
  p.A0=1;//Amplitude of the BCell response to a pulse - alpha profile (mV)
  p.t0=0; //Starting time of the alpha profile (ms)
  p.tauRF=50;//Characteristic time of the BCell RF (ms)
  p.tauRF3= pow(p.tauRF,3); 
  p.b0=0; //Constant value in  the BCell response to a pulse (mV)

  //Spatial RF of BCells
  p.Ac=2;//Amplitude of the spatial RF center (mV.mm)
  p.sigma_c=2*p.dx;//Variance of  the spatial RF center (mm)
  p.As=1.5;//Amplitude of  the spatial RF surround (mV.mm) 
  p.sigma_s=4*p.dx;//Variance of  the spatial RF surround (mm)

  //Amacrines
  p.thetaA=0; //Threshold for Amacrine rectification (mV)
  p.tauA=p.tauAref=100;//Modifi� le 09/09/2022
  p.gCNOA=0;//Amacrine CNO conductance (nS)
  p.lambdaA=p.gCNOA/C;//Amacrine CNO  ratio g/C (kHz)
  p.ECNOA=ENa-EL; //Amacrine CNO reversal potential in mV (Sodium -rest state)
  p.zetaA=p.lambdaA*p.ECNOA; //CNO influence for Amacrines (mV/ms)
  //p.tauA=p.tauAref/(1+p.lambdaA*p.tauAref);//Amacrine characteristic time in the presence of CNO (ms)
  //p.tauA=50;//Modifi� le 09/09/2022
  p.unsurtauA=1.0/p.tauA;//1/tauA (kHz)
  p.CA=C; //Amacrine membrane capacity (pF)
  p.gLA=gL;//p.CA/p.tauA; //printf("gLA=%lg nS\n",p.gLA);exit(0);//Amacrine leak conductance (nS) 
  p.ELA=0;
  p.KA=p.gLA*p.ELA/p.CA; //Constant fixed by the leak potential (mV/ms)

  //Ganglions
  p.gCNOG=0;//Ganglion CNO conductance (nS)
  p.lambdaG=p.gCNOG/C;//GCell CNO conductance (nS)
  p.ECNOG=ENa-EL; //GCell CNO reversal potential in mV 
  p.zetaG=p.lambdaG*p.ECNOG; //CNO influence for GCells (mV/ms)
  p.tauG=p.tauGref=1.1*p.tauB;//GCell leak characteristic time (ms) 
  //p.tauG=p.tauGref/(1+p.lambdaG*p.tauGref);//GCell characteristic time in the presence of CNO and with membrane polarisation (ms)
  //p.tauG=1.1*p.tauB;//Modifi� le 09/09/2022
  p.unsurtauG=1.0/p.tauG;
  p.CG=C; //GCell membrane capacity (in microF/cm2)
  p.gLG=gL;//p.CG/p.tauG;// printf("gLG=%lg nS\n",p.gLG);exit(0);//GCell leak conductance (nS) 
  p.ELG=0;
  p.KG=p.gLG*p.ELG/p.CG; //Constant fixed by the leak potential (mV/ms)

//printf("p.lambdaG*p.tauGref=%lg\n",p.lambdaG*p.tauGref);
  //printf("tauGref=%lg\ttauG=%lg\tzetaG=%lg\n",p.tauGref,p.tauG,p.zetaG);

  //Connections
  p.wba=6*weight_factor; //Bipolar Amacrine  weight (kHz)
  p.wab=-p.wba; //Amacrine Bipolar weight (kHz)
  p.wbg=3*weight_factor;//Bipolar Ganglion weight in the Gaussian pooling (kHz)
  p.wag=-p.wbg;//Amacrine Ganglion weight. Must be negative. (kHz)
  double sigmabg=5*p.dx; //Size of the GCell RF (after pooling) (mm)
  p.sigmabg2=pow(sigmabg,2);//Bipolar Ganglion variance in the Gaussian pooling (mm^2)
  p.BtoA=1;//If 0 one BCell connects a unique ACell, if 1 WAB=WBA
  p.IsPooling=true; //Pooling if true

  //Global
  p.unsurtau=p.unsurtauA-p.unsurtauB;//1/tauA-1/tauB (kHz)
  p.unsurtauAB=p.unsurtauA+p.unsurtauB;//1/tauA+1/tauB (kHz)
  if ((fabs(p.unsurtau)>1E-10)) {
    p.tau=1.0/p.unsurtau;
  }
  else {
    printf("Case tau^-1=0 not implemented yet, exiting\n");
    exit(0);
  }
  p.tau2=pow(p.tau,2);
  p.tauAB=1.0/p.unsurtauAB;
  p.mu=-p.wab*p.wba*p.tau2;

 //Pulse
  p.AmpStim=0.0; //Amplitude of the pulse (mV)
  p.speed=1; //GaussianPulse speed (mm/ms) (propagation along the x axis)
  p.mean=0;//p.N*p.dx/2.0;
  p.sigma=1; //GaussianPulse width (mm)
  p.tmin=-500;//initial time (ms)
  p.tmax=100; //Final time (ms)
  //p.strength=1.0; //Strength of the stimulus

  int beta=0;
  int iGC=p.N/2;//Index of the GCell on the lattice
  
  int alphaBC=iGC;//Index of the BCell above this GCell in the extended phase space
  int alphaAC=iGC+p.L;//Index of the ACell above this GCell in the extended phase space
  int alphaGC=iGC+2*p.L;//Index of the GCell in the extended phase space
  int alphaAc=iGC+3*p.L;//Index of the BCell activity in the extended phase space
  double xGC=iGC*p.dx,yGC=0;//Physical coordinates of the GCell (mm)

  // Dynamics variables
  // State memory allocation and initialization at 0
  int dim_ext=4*p.L;// Dimension of the phase space
  int dim=3*p.L;// Dimension of the phase space without activities
  double *X=(double*)calloc(dim_ext,sizeof(double));//State vector time t: Variables 0... N-1 are bipolar voltages,Variables N... 2N-1 are amacrine voltages, Variables 2N... 3N-1 are ganglion voltages
  // double *Xp=(double*)calloc(dim_ext,sizeof(double));//State vector time t+dt
  //double *Xx=(double*)calloc(dim_ext,sizeof(double)); //X(x,t)
  //double* temp;//swap variable
 
  p.b0=-0.001;
  p.tmax=600;

  int dyn_type=0; //0 if linear, 1 if rectified without gain control, 2 if rectified with gain control
  string chain_type=Select_dyn_type(dyn_type,&f,&fS);
  
  init_Network_Laplacian(&p,1);   
  double VB_rest=0,VA_rest=0,VG_rest=0,A_rest=0;
  ComputeRestState_FixedCharacteristicTimes(iGC,p,f,X,&VB_rest,&VA_rest,&VG_rest,&A_rest);
  ComputeReferenceTimes(&p,VB_rest, VA_rest,VG_rest);

  //std::string Gene="Grik4",ExpCond="CTL",datafile=Gene+"_temp_STAs_"+ExpCond+".txt";
  //std::string Gene="Grik4",ExpCond="CNO",datafile=Gene+"_temp_STAs_"+ExpCond+".txt";
  std::string Gene="Scnn1a",ExpCond="CTL",datafile=Gene+"_temp_STAs_"+ExpCond+".txt";
  //std::string Gene="Scnn1a",ExpCond="CNO",datafile=Gene+"_temp_STAs_"+ExpCond+".txt";
  std::vector <Experiment> experiences;
  char chaineDir[1000],chaineParams[1000],chaine[1000];
  sprintf(chaineDir,"%s/",Gene.c_str());//printf("%s\n",chaineDir);exit(0);
  fit_switch fit_variables;

  if (ExpCond=="CTL"){  // Fitting variables in control conditions
    fit_variables={
      1,//wba
      1,//wab
      1,//wbg
      1,//wag
      1,//tauB
      1,//tauA
      1,//tauG
      1,//tauRF
      1,//A0
      1,//b0
      0,//gCNOA
      0,//gCNOG
      0,//ECNOA
      0//ECNOG
    };
  }//Fitting  variables in control conditions
  else  if (ExpCond=="CNO"){  // Fitting variables in CNO conditions
    fit_variables={
      0,//wba
      1,//wab
      0,//wbg
      1,//wag
      1,//tauB
      1,//tauA
      1,//tauG
      0,//tauRF
      1,//A0
      1,//b0
      0,//gCNOA
      0,//gCNOG
      0,//zetaA
      0,//zetaG
      0,//ECNOA //This variable ought to be set to 1 when fitting CNO as it affords having a non zero rest state, thereby triggering the change in conductance (when CNO increases characteristic times)
      0,//ECNOG
    };   
  }//End of Fitting variables in CNO conditions
  printf("\n\t\t\033[0;35mThe file %s will be analysed\n\n\033[0m",datafile.c_str());
  double sample_rate=0.033/4.0;//s
  GetData(datafile,Gene,&experiences,sample_rate);
  
  int data_size=experiences.size();
  char chaine_corres[1000]; sprintf(chaine_corres,"%sCells_correspondence_%s",chaineDir,ExpCond.c_str());
  FILE *fcor;//=fopen(chaine_corres,"w");  fclose(fcor);
 // exit(0);

  double tmin_fit=0,tmax_fit=600;
  char InFile[1000];//Suffix of the trajectory file indicating the model's parameters
  char ISCNO[1000];//Suffix of the trajectory file indicating if there is CNO or not
  char chaineS[1000],chaineT[1000],chaineTh[1000],chaineExp[1000];
  char Prefix[1000]="",GnuName[1000]="",PlotName[1000]="",Movie[1000]=""; 
  char FileDistance[1000];
  sprintf(FileDistance,"%sFinalDistanceFit_%s",chaineDir,ExpCond.c_str());
  FILE* fdist=NULL;//=fopen(FileDistance,"w");fclose(fdist);
  params pp;
  //CopyParams(p,&pp);
  string Cell_Type="";

  int label=36;

  if (0){//Plot experimental traces and power spectrum only
    fcor=fopen(chaine_corres,"r"); 
    int i1=0,i2=0,i3=0;
    char type[3],bid;
    do{
      fscanf(fcor,"%d%d%c%d%s",&i1,&i2,&bid,&i3,&type);
//      printf("i1=%d, i2=%d, bid=%c, i3=%d, type=%s\n",i1,i2,bid,i3,type);
  //    exit(0);
    }
    while (i1!=label);
    Cell_Type=type;
    sprintf(InFile,"_%d_%d_%s",experiences[label].Cell_id,experiences[label].Exp_id,ExpCond.c_str());//printf("%s\n",InFile);exit(0);
    fclose(fcor);    
    
    sprintf(chaineExp,"Trajectories/Empirical_Cell%s",InFile);
    FILE *fexp=fopen(chaineExp,"w");
    
    for (int i=2;i<experiences[label].data.size();i++)
      fprintf(fexp,"%lg\t%lg\n",experiences[label].data[i].t,experiences[label].data[i].Amp);
    fclose(fexp);
    
    sprintf(GnuName,"Exp_Cell_%d_%d_%s.gnu",experiences[label].Cell_id,experiences[label].Exp_id,ExpCond.c_str());//printf("%s\n",GnuName);//exit(0);
    char LocExec[1000];
    sprintf(LocExec,"%sRFs/Exp_Cell_%d_%d_%s.gnu",chaineDir,experiences[label].Cell_id,experiences[label].Exp_id,ExpCond.c_str());//printf("%s\n",GnuName);//exit(0);
    sprintf(PlotName,"Exp_Cell_%d_%d_%s",experiences[label].Cell_id,experiences[label].Exp_id,ExpCond.c_str());//printf("%s\n",PlotName);exit(0);
    
      //FFT
    char PowerSpectrum[1000]="";
    sprintf(PowerSpectrum,"%sRFs/Exp_PowerSpectrum_RF%s",chaineDir,InFile);//printf("%s\n",PowerSpectrum);//exit(0);
    int n_peaks=0;
    double max_range=0;//max_range is the maximal frequency range i.e. the maximal value where the amplitude of the normalized spectrum is larger than zero_PS.
    
    //sprintf(InFile,"_%d_%d_%s",experiences[label].Cell_id,experiences[label].Exp_id,ExpCond.c_str());//printf("%s\n",InFile);exit(0);
    sample_rate=0.033*1000/4.0;//Convert sample rate in ms
    double t_RF=600;//ms

    std::vector<Peaks_fft> periods=getPeriod(experiences[label],sample_rate,&n_peaks,PowerSpectrum,&max_range); 
    //exit(0);
    printf("\n\t\tThe %d first peaks (periods) in the power spectrum are:\n",n_peaks);
    for (int k=0;k<n_peaks;k++) {
      if (periods[k].f>0) printf("\t\t\t(f=%lg (Hz), T=%lg (ms), Amp=%lg, width=%lg (Hz) => %lg (ms))\n",1000*periods[k].f,1.0/periods[k].f,periods[k].Amp,1000*periods[k].width,1.0/periods[k].width);
      else
	printf("\t\t\t(f=%lg (Hz), int, Amp=%lg, width=%lg (Hz) => %lg (ms)) \n",1000*periods[k].f,periods[k].Amp,1000*periods[k].width,1.0/periods[k].width);
    }
    char chaineFFT_Th[1000]="";
    sprintf(PowerSpectrum,"Exp_PowerSpectrum_RF%s",InFile);//printf("%s\n",PowerSpectrum);//exit(0);//Label case

    plot_Experimental_Traces(chaineExp,ExpCond,PowerSpectrum,chaineFFT_Th,periods,p,LocExec,PlotName,max_range,t_RF,Cell_Type);

    if (Gene=="Grik4") chdir("Grik4/RFs/");
    if (Gene=="Scnn1a") chdir("Scnn1a/RFs/");
    //system("pwd\n");
    sprintf(chaine,"gnuplot %s\n",GnuName);
    printf("\n\n\tExecuting %s",chaine);
    system(chaine);
    chdir("../..");

    exit(0);
  }//End of plot experimental traces and power spectrum only
  
  bool FineTuned=true;
  //printf("CTL\n");ReadParams(&p,"params_52_1_CTL");WriteParams(p);
  //printf("\nCTL Osc\n");ReadParams(&p,"params_52_1_CTL_Osc");WriteParams(p);
  //exit(0);

  if (1){//Fit
    int labels[]={33};
    //int labels[] = {22,23,26,30,33,5,64};//Grik4 CTL
    //int labels[] = {22,23,29,33,4,35,36,5,6,8,37,9,38,10,11,15,41,44,18,19,57,26,64};//Grik4 CNO
    //int labels[] = {36,4,38,40,0,41,13,18,43,45,46,33,34,48}; //Scnn1a CTL
    //int labels[] = {7,12,14,15,43,20,22,44,27,45,28,33,48};//Scnn1a CNO
    for (int line : labels) {//Loop on cells
    //for (int line=3;line<data_size;line++){//Loop on cells
    //for (int line=label;line<=label;line++){//Loop on cells
      if (ExpCond=="CTL"){
	if (FineTuned==false){
	  CopyParams(p,&pp);
	  preFit(experiences[line],&pp,&Cell_Type,ExpCond,chaineDir,1000*sample_rate);//sample_rate is expressed in ms here.
	  printf("\n\t\t\tCell _%d_%d_%s (index %d) is a %s Cell\n",experiences[line].Cell_id,experiences[line].Exp_id,ExpCond.c_str(),line,Cell_Type.c_str());    
	  //pp.b0=experiences[line].data[2].Amp;
	}
	else { //Fine tuning
	  sprintf(chaineParams,"%sParameters/params_%d_%d_CTL",chaineDir,experiences[line].Cell_id,experiences[line].Exp_id);printf("Reading %s\n",chaineParams);//exit(0);
	  if (ReadParams(&pp,chaineParams)==false) exit(0);
	  //preFit(experiences[line],&pp,&Cell_Type,ExpCond,chaineDir,1000*sample_rate);//sample_rate is expressed in ms here.
	  double r=pp.tauA/pp.tauB;
	  double s=-pp.wab/pp.wba;
	  printf("\t\t\tBefore renor: r=%g, s=%g\n",r,s);//exit(0);
	  double wba=0.0085,tauB=30;
	  double renorm=(pow(pp.wba*pp.tauB,2)/pow(wba*tauB,2));
	 // s*=2;
	  //r*=2;
	  printf("\t\t\tAfter renor: r=%g, s=%g\n",r,s/renorm);//exit(0);
	  //s=60;
	  //s/=renorm;
	  //r=13;
	  //pp.tauA/=2;pp.tauB/=2;//pp.tauRF/=2;pp.tauG/=2;
	  //s*=1.1;
	  //r*=1.1;
	  //printf("\t\t\tNew values: r=%g, s=%g\n",r,s);//exit(0);
	  // pp.wab/=2;
	  //pp.wba*=1.5;
	  //pp.wag*=1.5;	
	  //pp.wbg*=1.2;
	  // pp.tauB=151.939;
	  //pp.tauA=205.807;
	  //pp.wba=0.00168149;
	  //pp.wab=-0.400905;
	  //pp.wbg=0.0193379;
	  //pp.wba/=2;
	  //pp.wab=-0.0836958;
	  //pp.wbg=0.223568;

	  // pp.tauA=r*pp.tauB;
	  //pp.wab=-s*pp.wba;
	  //pp.tauG*=1.1;
	  //pp.tauRF*=1.1;
	  //pp.b0=0.05;
	 //pp.tauA/=2;
	  //pp.A0/=2.;
	 // pp.wbg*=1.5;
	  //pp.wag=-0.01;
	  //pp.wab/=4;
	  //pp.tauRF*=0.9;
	  fit_variables={
	    0,//wba
	    0,//wab
	    1,//wbg
	    1,//wag
	    0,//tauB
	    0,//tauA
	    1,//tauG
	    1,//tauRF
	    1,//A0
	    1,//b0
	    0,//gCNOA
	    0,//gCNOG
	    0,//ECNOA
	    0//ECNOG
	  };UpdateParams(&pp);
	}
      }
      else if (ExpCond=="CNO"){
	//To be removed
	/*CopyParams(p,&pp);
	preFit(experiences[line],&pp,&Cell_Type,ExpCond,chaineDir,1000*sample_rate);//sample_rate is expressed in ms here.
	printf("\n\t\t\tCell _%d_%d_%s (index %d) is a %s Cell\n",experiences[line].Cell_id,experiences[line].Exp_id,ExpCond.c_str(),line,Cell_Type.c_str()); */
	//Up to here
	sprintf(chaineParams,"%sParameters/params_%d_%d_CTL",chaineDir,experiences[line].Cell_id,experiences[line].Exp_id);printf("Reading %s\n",chaineParams);//exit(0);
	if (ReadParams(&pp,chaineParams)==false) exit(0);
	//preFit(experiences[line],&pp,&Cell_Type,ExpCond,chaineDir,1000*sample_rate);//sample_rate is expressed in ms here.
	printf("\n\t\t\tCell _%d_%d_%s (index %d) is a %s Cell\n",experiences[line].Cell_id,experiences[line].Exp_id,ExpCond.c_str(),line,Cell_Type.c_str()); 
	  
	if (FineTuned==true){//Fine tuning
	  sprintf(chaineParams,"%sParameters/params_%d_%d_CNO",chaineDir,experiences[line].Cell_id,experiences[line].Exp_id);printf("Reading %s\n",chaineParams);//exit(0);
	  if (ReadParams(&pp,chaineParams)==false) exit(0);
	  //double s=64;
	  //double r=32;
	  //pp.tauA=r*pp.tauB;
	  //pp.wab=-s*pp.wba;
	  //pp.wab*=1.1;
	  //pp.b0=-0.08;
	  //pp.A0*=-1;
	  // pp.wbg*=2;
	  //pp.wag*=2;
	  //pp.wab=-pp.wba;
	  //pp.tauRF*=1.1;
	  //pp.tauB*=0.5;
	  // pp.tauA*=0.5;
	  //pp.wab*=2;
	  fit_variables={
	    1,//wba
	    1,//wab
	    1,//wbg
	    1,//wag
	    1,//tauB
	    1,//tauA
	    0,//tauG
	    1,//tauRF
	    1,//A0
	    1,//b0
	    0,//gCNOA
	    0,//gCNOG
	    0,//ECNOA
	    0//ECNOG
	  };
	  UpdateParams(&pp);
	}
      }
      //pp.wba=8.5*weight_factor; UpdateParams(&pp);//Reference value for the  RFs map
     // sprintf(chaineParams,"%sParameters/params_%d_%d_CTL",chaineDir,experiences[line].Cell_id,experiences[line].Exp_id);//Test to be removed
      //if (ReadParams(&pp,chaineParams)==false) exit(0);//Test to be removed

      //printf("\t\t\ttauRF=%lg, tauB_ref=%lg, tauA_ref=%lg, tauG_ref=%lg\n",pp.tauRF,pp.tauBref,pp.tauAref,pp.tauGref);

      //exit(0);
      //WriteParams(pp);exit(0);
      
      /*fcor=fopen(chaine_corres,"w"); 
      fprintf(fcor,"%d\t%d_%d\t%s\n",line,experiences[line].Cell_id,experiences[line].Exp_id,Cell_Type.c_str());
      fclose(fcor);    */
      
      init_Network_Laplacian(&pp,1);   	     
      double dist=FitCells(experiences[line],&pp,f,tmin_fit,tmax_fit,fit_variables,100,1E-3);//Fit the model to the cell
      fdist=fopen(FileDistance,"a");
      fprintf(fdist,"%d\t%lg\t%d_%d\t%s\n",line,dist,experiences[line].Cell_id,experiences[line].Exp_id,Cell_Type.c_str());
      fclose(fdist);
      
      sprintf(chaineParams,"%sParameters/params_%d_%d_%s",chaineDir,experiences[line].Cell_id,experiences[line].Exp_id,ExpCond.c_str());//printf("%s\n",chaineParams);exit(0);
      SaveParams(pp,chaineParams);
      delete_params(&pp);//Freeing pointers in pp
      //break;
      //exit(0);
      //}
    }//End of loop on cells
     // exit(0);
  }//End of fit

  if (1){//Plotting RFS
    // int line=0;
    gsl_vector_complex* SumPinvbgKSg=gsl_vector_complex_calloc(dim);//Auxilary used for computation
    gsl_vector_complex* SumPinvbgKSg_CNOA=gsl_vector_complex_calloc(dim);//Auxilary used for computation

    int labels[]={33};
    //int labels[] = {22,23,26,30,33,5,64};//Grik4 CTL
   // int labels[] = {22,23,26,29,33,4,35,36,5,6,8,37,9,38,10,11,15,41,64,44,18,19,57};//Grik4 CNO
    //int labels[] = {48,36,4,38,40,0,41,13,18,43,45,46,33,34}; //Scnn1a CTL
    // int labels[] = {48,35,36,4,38,40,0,7,10,12,14,15,43,20,22,44,27,45,28,33};//Scnn1a CNO

     for (int line : labels) {//Loop on cells
       //    for (int line=label;line<data_size;line++){//Loop on cells
    //for (int line=label;line<=label;line++){//Loop on cells
      //for (int line=0;line<=0;line++){//Loop on cells
 
      //Utile seulement pour le print
      int i1=0,i2=0,i3=0;
      char type[3],bid;
     fcor=fopen(chaine_corres,"r"); 
     do{
	fscanf(fcor,"%d%d%c%d%s",&i1,&i2,&bid,&i3,&type);
      }
      while (i1!=line);
     fclose(fcor);   

      Cell_Type=type;
      sprintf(InFile,"_%d_%d_%s",experiences[line].Cell_id,experiences[line].Exp_id,ExpCond.c_str());//printf("%s\n",InFile);exit(0);
      //sprintf(InFile,"_%d_%d_%s",experiences[line].Cell_id,experiences[line].Exp_id,experiences[line].Gene.c_str());
      printf("\n\tSimulating the Gcell%s's response, of %s type, to a step stimulus\n",InFile,Cell_Type.c_str());   
      //Utile seulement pour le print
  
      sprintf(chaineParams,"%sParameters/params_%d_%d_%s",chaineDir,experiences[line].Cell_id,experiences[line].Exp_id,ExpCond.c_str());//printf("%s\n",chaineParams);exit(0);
      if (ReadParams(&pp,chaineParams)==false) break;
      //WriteParams(pp);exit(0);
      
      init_Network_Laplacian(&pp,1);   
      
      sprintf(ISCNO,"%s",ExpCond.c_str());  
      sprintf(chaineT,"Trajectories/Cell_Time%s",InFile);
      sprintf(chaineS,"Trajectories/Cell_Space%s",InFile);
      sprintf(chaineTh,"Trajectories/Theoretical_Cell%s",InFile);
      sprintf(chaineExp,"Trajectories/Empirical_Cell%s",InFile);
      FILE *fexp=fopen(chaineExp,"w");
      for (int i=2;i<experiences[line].data.size();i++)
	fprintf(fexp,"%lg\t%lg\n",experiences[line].data[i].t,experiences[line].data[i].Amp);
      fclose(fexp);
      
      ComputeAndStoreSpaceRF(iGC,pp,chaineT,chaineS,chaineTh,f,fS,SumPinvbgKSg,SumPinvbgKSg_CNOA);
      //exit(0);
      sprintf(GnuName,"Cell_%d_%d_%s.gnu",experiences[line].Cell_id,experiences[line].Exp_id,ExpCond.c_str());//printf("%s\n",GnuName);//exit(0);
      char LocExec[1000];
      sprintf(LocExec,"%sRFs/Cell_%d_%d_%s.gnu",chaineDir,experiences[line].Cell_id,experiences[line].Exp_id,ExpCond.c_str());//printf("%s\n",GnuName);//exit(0);
      sprintf(PlotName,"Cell_%d_%d_%s.png",experiences[line].Cell_id,experiences[line].Exp_id,ExpCond.c_str());//printf("%s\n",PlotName);exit(0);
     
      //FFT
      char PowerSpectrum[1000]="";
      sprintf(PowerSpectrum,"%sRFs/PowerSpectrum_RF%s",chaineDir,InFile);//printf("%s\n",PowerSpectrum);//exit(0);
       int n_peaks=0;
      double max_range=0;//max_range is the maximal frequency range i.e. the maximal value where the amplitude of the normalized spectrum is larger than zero_PS.
      
      //sprintf(InFile,"_%d_%d_%s",experiences[line].Cell_id,experiences[line].Exp_id,ExpCond.c_str());//printf("%s\n",InFile);exit(0);
      sample_rate=0.033*1000/4.0;//Convert sample rate in ms
      std::vector<Peaks_fft> periods=getPeriod(experiences[line],sample_rate,&n_peaks,PowerSpectrum,&max_range); 
      //exit(0);
      printf("\n\t\tThe %d first peaks (periods) in the power spectrum are:\n",n_peaks);
      for (int k=0;k<n_peaks;k++) {
	if (periods[k].f>0) printf("\t\t\t(f=%lg (Hz), T=%lg (ms), Amp=%lg, width=%lg (Hz) => %lg (ms))\n",1000*periods[k].f,1.0/periods[k].f,periods[k].Amp,1000*periods[k].width,1.0/periods[k].width);
	else
	  printf("\t\t\t(f=%lg (Hz), int, Amp=%lg, width=%lg (Hz) => %lg (ms)) \n",1000*periods[k].f,periods[k].Amp,1000*periods[k].width,1.0/periods[k].width);
      }
      
      string Title="";
      string PlotMod="Panel";
      //string PlotMod="Separated";//This can be Panel or Separated. If Separated one plots different figures for the timeRF, the space RF, and the power spectrum. If Panel, all figures are collapsed on a unique panel. 
     
      if (strcmp(PlotMod.c_str(),"Separated")==0){//If Separated one plots different figures for the timeRF, the space RF, and the power spectrum. If Panel, all figures are collapsed on a unique panel. 
	printf("\t\t\tPlotting separated Figures\n");
	
	plot_RFs_2Figs(chaineT,chaineS,chaineTh,chaineExp,ExpCond,pp,LocExec,PlotName,Title,pp.tmax);
	//sprintf(chaine,"%s/RFs/\n",Gene.c_str());
	//chdir(chaine);
	
	if (Gene=="Grik4") chdir("Grik4/RFs/");
	if (Gene=="Scnn1a") chdir("Scnn1a/RFs/");
	//system("pwd\n");
	sprintf(chaine,"gnuplot %s\n",GnuName);
	printf("\n\n\tExecuting %s",chaine);
	system(chaine);
	chdir("../..");
	
	//Plot the PowerSpectrum
	sprintf(GnuName,"FFT%s.gnu",InFile);//printf("%s\n",GnuName);exit(0);
	sprintf(LocExec,"%sRFs/FFT%s.gnu",chaineDir,InFile);//printf("%s\n",LocExec);exit(0);    
	sprintf(PlotName,"PowerSpectrum%s.png",InFile);//printf("%s\n",PlotName);exit(0);
	
	sprintf(PowerSpectrum,"PowerSpectrum_RF%s",InFile);//printf("%s\n",PowerSpectrum);//exit(0);//Label case
	plot_Power_Spectrum(PowerSpectrum,periods,LocExec,PlotName,Title,max_range);
	if (Gene=="Grik4") chdir("Grik4/RFs/");
	if (Gene=="Scnn1a") chdir("Scnn1a/RFs/");
	//system("pwd\n");
	sprintf(chaine,"gnuplot %s\n",GnuName);
	printf("\n\t\tExecuting %s",chaine);
	system(chaine);
	chdir("../..");
	
	//Cleaning
	printf("\n\t\tcleaning\n");
	sprintf(chaine,"rm -f %s\n",LocExec);
	printf("\n\t\tExecuting %s",chaine);
	//system(chaine);
	sprintf(chaine,"rm -f %sRFs/%s\n",chaineDir,PowerSpectrum);
	printf("\n\t\tExecuting %s",chaine);
	//system(chaine);
      }//End of i Separated one plots different figures for the timeRF, the space RF, and the power spectrum. If Panel, all figures are collapsed on a unique panel. 
      else if (strcmp(PlotMod.c_str(),"Panel")==0){// If Panel, all figures are collapsed on a unique panel. 
	printf("\t\t\tPlotting a summary panel\n");
	gsl_complex kg={0,0};//Computes the RF of a GCell alpha embedded in the BCell, ACell, GCell network
	int NumPoint=1<<13,n=NumPoint;//printf("NumPoint=%d\n",NumPoint);exit(0);//Pour FFT.
	pp.tmax=n*pp.dt;
	pp.tmin=0;
	double sample_rate=pp.dt;
	char chaineFFT_Th[1000]="";
	double df=0.00005;
	gsl_matrix *Psi=gsl_matrix_calloc(pp.L,pp.L),*Psi_transpose=gsl_matrix_calloc(pp.L,pp.L);
	Spectrum kappa_th;//Theoretical eigenvalues of the connectivity matrix amacrine bipolar
	Spectrum sigma_th;//Theoretical eigenvalues of the connectivity matrix bipolar amacrine
	ComputeTheoreticalModesLaplacian1D(pp,&kappa_th,&sigma_th,Psi,Psi_transpose);
	ComputeTheoreticalSpectrumL(kappa_th,sigma_th,Psi,Psi_transpose,&pp);
 	
	KG_CNOA(pp,SumPinvbgKSg_CNOA);//CNO A contribution
    	KSG(xGC,yGC,pp, SumPinvbgKSg);//KSG contains the value of the spatial RF, of each eigenmode beta, at point x,y.

	double maxi=0;
	vector <Event> Spectre;
	
	//Storing the spectrum in Spectre
	for (double nu=-0.2;nu<0.2;nu+=df){
	  gsl_complex z=KG_hat(alphaGC,xGC,0,nu,pp,SumPinvbgKSg,SumPinvbgKSg_CNOA);
	  if (abs(nu)<df){
	    double omega=dpi*nu;
	    gsl_complex Dirac=gsl_complex_div(gsl_complex_add_real(gsl_complex_exp({0,dpi*n*sample_rate}),-1),gsl_complex_add_real(gsl_complex_exp({0,dpi*sample_rate}),-1));
	    //printf("Dirac=(%g,%g) \n",pp.b0*GSL_REAL(Dirac),pp.b0*GSL_IMAG(Dirac));
	    z=gsl_complex_add(z,Dirac); //Add Discrete Dirac in the power spectrum
	  }
	  Event point={nu,gsl_complex_abs(z)};
	  maxi=max(maxi,point.Amp);
	  Spectre.push_back(point);
	}

	//Storing the normalized spectrum in a file
	sprintf(chaineFFT_Th,"%sRFs/FFT_Th_RF%s",chaineDir,InFile);//printf("%s\n",FFT_Th_RF);//exit(0);
	FILE *fp1=fopen(chaineFFT_Th,"w");
	for (int ii=0;ii<Spectre.size();ii++){	  
	  fprintf(fp1,"%lg\t%lg\n",1000*Spectre[ii].t,Spectre[ii].Amp/maxi);
	}
	fclose(fp1);
	double t_RF=600;//ms
	sprintf(PowerSpectrum,"PowerSpectrum_RF%s",InFile);//printf("%s\n",PowerSpectrum);//exit(0);
	sprintf(chaineFFT_Th,"FFT_Th_RF%s",InFile);//printf("%s\n",FFT_Th_RF);//exit(0);

	//Plot the panel
	sprintf(GnuName,"Panel%s.gnu",InFile);//printf("%s\n",GnuName);//exit(0);
	sprintf(LocExec,"%sRFs/Panel%s.gnu",chaineDir,InFile);//printf("%s\n",LocExec);//exit(0);
	sprintf(PlotName,"Panel%s.png",InFile);//printf("%s\n",PlotName);//exit(0);	

	plot_Summary_Panel(chaineT,chaineS,chaineTh,chaineExp,ExpCond,PowerSpectrum,chaineFFT_Th,periods,p,LocExec,PlotName,Title,max_range,t_RF,Cell_Type);
	if (Gene=="Grik4") chdir("Grik4/RFs/");
	if (Gene=="Scnn1a") chdir("Scnn1a/RFs/");

	system("pwd\n");
	sprintf(chaine,"gnuplot %s\n",GnuName);
	printf("\n\t\tExecuting %s",chaine);
	system(chaine);
	sprintf(chaine,"rm -f %s\n",GnuName);
	printf("\n\t\tExecuting %s",chaine);
	//system(chaine);
	chdir("../..");
	sprintf(chaine,"rm -f %s\n\t\trm -f %s\n\t\trm -f %s\n\t\trm -f %s\n\n",chaineT,chaineS,chaineTh,chaineExp);
	printf("\n\t\tExecuting %s",chaine);
	//system(chaine);
	My_gsl_matrix_free(Psi);My_gsl_matrix_free(Psi_transpose);
      }//End of "Panel"
      else {
	printf("The plot modality %s is not defined. Aborting\n",PlotMod.c_str());
      }
      //exit(0);
    }//End of loop on cells
    
    My_gsl_vector_complex_free(SumPinvbgKSg);
    My_gsl_vector_complex_free(SumPinvbgKSg_CNOA);
     exit(0); 
  }//End of plotting RFS

  if (1){//Statistics separating Grik and Scnn1
    printf("\t\tStatistics separating Grik and Scnn1\n");
    stats data;
    sprintf(chaine_corres,"%sCells_correspondence_%s",chaineDir,ExpCond.c_str());
    if ((fcor=fopen(chaine_corres,"r"))==NULL) {printf("File %s couldn't be opened, aborting\n",chaine_corres);exit(0);}
    FILE *fdist=NULL;
    char chaine_dist[1000];sprintf(chaine_dist,"%sFinalDistanceFit_%s",chaineDir,ExpCond.c_str());
    if ((fdist=fopen(chaine_dist,"r"))==NULL) {printf("File %s couldn't be opened, aborting\n",chaine_corres);exit(0);}
    int label=0,id_cell=0,id_exp;
    char bid,type[3],comment[10000];
    char chaine_data[1000];sprintf(chaine_data,"%sSummary_Fit_%s",chaineDir,ExpCond.c_str());
    //printf("\t\t%s\n",chaine_data);//exit(0);
    
    FILE* fdat=fopen(chaine_data,"w");fclose(fdat);
    fdat=fopen(chaine_data,"a");
    int id_field_max=13;
    Histo histo[id_field_max];   /*tauB->0
			 tauA->1
			 tauRF->2
			 tauG->3
			 wba->4
			 wab->5
			 wbg->6
			 wag->7
			 s->8
			 r->9
			 A0->10
			 b0->11
			 dist->12*/
    Histo aux; //Used to get from log to linear (see below).
    moments moments[id_field_max];
    double d_hist[id_field_max];//Bin size in the histogram
    Cell_id_type cell;
    double val=0; 
    /*double tauBmax=500,tauAmax=500,tauRFmax=500,tauGmax=500;//ms
    double wbamax=500,wabmin=-500,wbgmax=500,wagmin=-500;//Hz
    double smax=512,rmax=512;*/
    double dist=0;
    int size=0;
    if (Gene=="Grik4") size=66;
    else if (Gene=="Scnn1a") size =50;
    
    //Tuning histo bins
    d_hist[0]=5; //dtauB (ms)
    d_hist[1]=20; //dtauA (ms)
    d_hist[2]=5; //dtauRF (ms)
    d_hist[3]=5; //dtauG (ms)
    d_hist[4]=1; //dwba (Hz)
    d_hist[5]=1; //dwab (Hz)
    d_hist[6]=1; //dwbg (Hz)
    d_hist[7]=1; //dwag (Hz)
    double dr=1.0/2.0,ds=1.0/2.0;
    d_hist[8]=dr; //ds (adim)
    d_hist[9]=ds; //dr (adim)
    d_hist[10]=0.1; //A0
    d_hist[11]=0.01; //b0
    d_hist[12]=0.002; //dist
 
    for (int line=0;line<=size;line++){//Loop on cells
      sprintf(chaineParams,"%sParameters/params_%d_%d_%s",chaineDir,experiences[line].Cell_id,experiences[line].Exp_id,ExpCond.c_str());//printf("Reading %s\n",chaineParams);
      if (ReadParams(&pp,chaineParams)!=false){//The parameters have been found
	if (fscanf(fdist,"%d%lg%d%c%d%[^\n]",&label,&dist,&id_cell,&bid,&id_exp,&comment)!=EOF);
	//printf("%d\t%lg\t%d%c%d\t%s\n",label,dist,id_cell,bid,id_exp,comment);
	if (label!=line) {printf("Mismatch between data structure and cells correspondence data label=%d != correspondence label =%d\n",line,label);exit(0);}
	fscanf(fcor,"%d%d%c%d%s\n",&label,&id_cell,&bid,&id_exp,&type);
	if (label!=line) {printf("Mismatch between data structure and cells correspondence data label=%d != correspondence label =%d\n",line,label);exit(0);}
	//exit(0);
	
	if ((pp.tauA<2000)&&(pp.tauB<2000)){//Times are reasonable
	  //WriteParams(pp);exit(0);	  
	  //Load tauB, tauA, tauRF, tauG, wab, wba, wag, wbg, mu,A0,b0
	  data.label=line;data.type=type;
	  data.field[0]=pp.tauB;
	  data.field[1]=pp.tauA;
	  data.field[2]=pp.tauRF;
	  data.field[3]=pp.tauG;
	  data.field[4]=pp.wba*1000;
	  data.field[5]=pp.wab*1000;
	  data.field[6]=pp.wbg*1000;
	  data.field[7]=pp.wag*1000;
	  data.field[8]=log(-pp.wab/pp.wba);//s. Parsing the log for histogram. We get back to linear below.
	  data.field[9]=log(pp.tauA/pp.tauB);//r. Parsing the log for histogram. We get back to linear below.
	  data.field[10]=pp.A0;
	  data.field[11]=pp.b0;
	  data.field[12]=dist;
	  char  id[100]; sprintf(id,"%d_%d",experiences[line].Cell_id,experiences[line].Exp_id);
	  //printf("%d\t%s\t%s\t%6f\t%6f\t%6f\t%6f\t%6f\t%6f\t%6f\t%6f\t%6f\t%6f\t%6f\t%6f\n",line,id,type,data.tauB,data.tauA,data.tauRF,data.tauG,data.wba,data.wab,data.wbg,data.wag,data.mu,data.r,data.A0,data.b0);
	  fprintf(fdat,"%d\t%s\t%s\t%6f\t%6f\t%6f\t%6f\t%6f\t%6f\t%6f\t%6f\t%6f\t%6f\t%6f\t%6f\n",line,id,type,pp.tauB,pp.tauA,pp.tauRF,pp.tauG,pp.wba,pp.wab,pp.wbg,pp.wag,exp(data.field[8]),exp(data.field[9]),pp.A0,pp.b0);
	  
	  //Filling histograms
	  for (int id_field=0;id_field<id_field_max;id_field++){//Loop on fields
	    val=(int)(data.field[id_field]/d_hist[id_field])*d_hist[id_field]; //printf("val=%g\ttype=%s\n",val,type);
	    cell={val,type};
	    Update_Histo(cell,&histo[id_field]);
	    //printf("");
	  }//End of loop on fields
	}//End of times are reasonable
	else{
	  printf("Cell%d: Characteristic times are too large: tauB=%g, tauA=%g, tauRF=%g, tauG=%g. \n",line,pp.tauB,pp.tauA,pp.tauRF,pp.tauG);
	}
      }//End of the parameters have been found
      else {
	printf("Parameters %s not found\n",chaineParams);
      }
    }//End of loop on cells  
     fclose(fcor);
     fclose(fdat);
     
     char chaine_hist[id_field_max][1000],Names[id_field_max][1000],GnuNames[id_field_max][1000],PlotNames[id_field_max][1000],labels[id_field_max][1000],unit[id_field_max][100],mode[id_field_max][100];
     char chaine_Sum_Stats[id_field_max][1000];
     
     //Filling file names
     //tauB->0
     sprintf(Names[0],"Histogram_tauB_%s",ExpCond.c_str());
     sprintf(chaine_Sum_Stats[0],"Moments_tauB");//printf("%s\n",chaine_Sum_Stats[0]);exit(0);
     sprintf(chaine_hist[0],"%sStats/Histogram_tauB_%s",chaineDir,ExpCond.c_str());
     sprintf(GnuNames[0],"%s.gnu",chaine_hist[0]);
     sprintf(PlotNames[0],"%s.png",Names[0]);
     sprintf(labels[0],"{/Symbol t}_B");//printf("label=%s\n",labels[0]);
     sprintf(unit[0],"ms");//printf("unit=%s\n",unit[0]);
     sprintf(mode[0],"");//printf("mode=%s\n",mode[0]);
    //tauA->1
     sprintf(Names[1],"Histogram_tauA_%s",ExpCond.c_str());
     sprintf(chaine_Sum_Stats[1],"Moments_tauA");
     sprintf(chaine_hist[1],"%sStats/Histogram_tauA_%s",chaineDir,ExpCond.c_str());
     sprintf(GnuNames[1],"%s.gnu",chaine_hist[1]);
     sprintf(PlotNames[1],"%s.png",Names[1]);
     sprintf(labels[1],"{/Symbol t}_A");//printf("label=%s\n",labels[1]);
     sprintf(unit[1],"ms");//printf("unit=%s\n",unit[1]);
     sprintf(mode[1],"");//printf("mode=%s\n",mode[1]);
    //tauRF->2
     sprintf(Names[2],"Histogram_tauRF_%s",ExpCond.c_str());
     sprintf(chaine_Sum_Stats[2],"Moments_tauRF");
     sprintf(chaine_hist[2],"%sStats/Histogram_tauRF_%s",chaineDir,ExpCond.c_str());
     sprintf(GnuNames[2],"%s.gnu",chaine_hist[2]);
     sprintf(PlotNames[2],"%s.png",Names[2]);
     sprintf(labels[2],"{/Symbol t}_{RF}");//printf("label=%s\n",labels[2]);
     sprintf(unit[2],"ms");//printf("unit=%s\n",unit[2]);
     sprintf(mode[2],"");//printf("mode=%s\n",mode[2]);
    //tauG->3
     sprintf(Names[3],"Histogram_tauG_%s",ExpCond.c_str());
     sprintf(chaine_Sum_Stats[3],"Moments_tauG");
     sprintf(chaine_hist[3],"%sStats/Histogram_tauG_%s",chaineDir,ExpCond.c_str());
     sprintf(GnuNames[3],"%s.gnu",chaine_hist[3]);
     sprintf(PlotNames[3],"%s.png",Names[3]);
     sprintf(labels[3],"{/Symbol t}_{G}");//printf("label=%s\n",labels[3]);
     sprintf(unit[3],"ms");//printf("unit=%s\n",unit[3]);
     sprintf(mode[3],"");//printf("mode=%s\n",mode[3]);
     //wba->4
     sprintf(Names[4],"Histogram_wba_%s",ExpCond.c_str());
     sprintf(chaine_Sum_Stats[4],"Moments_wba");
     sprintf(chaine_hist[4],"%sStats/Histogram_wba_%s",chaineDir,ExpCond.c_str());
     sprintf(GnuNames[4],"%s.gnu",chaine_hist[4]);
     sprintf(PlotNames[4],"%s.png",Names[4]);
     sprintf(labels[4],"w_{ba}");//printf("label=%s\n",labels[4]);
     sprintf(unit[4],"Hz");//printf("unit=%s\n",unit[4]);
     sprintf(mode[4],"");//printf("mode=%s\n",mode[4]);
     //wab->5
     sprintf(Names[5],"Histogram_wab_%s",ExpCond.c_str());
     sprintf(chaine_Sum_Stats[5],"Moments_wab");
     sprintf(chaine_hist[5],"%sStats/Histogram_wab_%s",chaineDir,ExpCond.c_str());
     sprintf(GnuNames[5],"%s.gnu",chaine_hist[5]);
     sprintf(PlotNames[5],"%s.png",Names[5]);
     sprintf(labels[5],"w_{ab}");//printf("label=%s\n",labels[5]);
     sprintf(unit[5],"Hz");//printf("unit=%s\n",unit[5]);
     sprintf(mode[5],"");//printf("mode=%s\n",mode[5]);
     //wbg->6
     sprintf(Names[6],"Histogram_wbg_%s",ExpCond.c_str());
     sprintf(chaine_Sum_Stats[6],"Moments_wbg");
     sprintf(chaine_hist[6],"%sStats/Histogram_wbg_%s",chaineDir,ExpCond.c_str());
     sprintf(GnuNames[6],"%s.gnu",chaine_hist[6]);
     sprintf(PlotNames[6],"%s.png",Names[6]);
     sprintf(labels[6],"w_{bg}");//printf("label=%s\n",labels[6]);
     sprintf(unit[6],"Hz");//printf("unit=%s\n",unit[6]);
     sprintf(mode[6],"");//printf("mode=%s\n",mode[6]);
   //wag->7
     sprintf(Names[7],"Histogram_wag_%s",ExpCond.c_str());
     sprintf(chaine_Sum_Stats[7],"Moments_wag");
     sprintf(chaine_hist[7],"%sStats/Histogram_wag_%s",chaineDir,ExpCond.c_str());
     sprintf(GnuNames[7],"%s.gnu",chaine_hist[7]);
     sprintf(PlotNames[7],"%s.png",Names[7]);
     sprintf(labels[7],"w_{ag}");//printf("label=%s\n",labels[7]);
     sprintf(unit[7],"Hz");//printf("unit=%s\n",unit[7]);
     sprintf(mode[7],"");//printf("mode=%s\n",mode[7]);
     //s->8
     sprintf(Names[8],"Histogram_s_%s",ExpCond.c_str());
     sprintf(chaine_Sum_Stats[8],"Moments_s");
     sprintf(chaine_hist[8],"%sStats/Histogram_s_%s",chaineDir,ExpCond.c_str());
     sprintf(GnuNames[8],"%s.gnu",chaine_hist[8]);
     sprintf(PlotNames[8],"%s.png",Names[8]);
     sprintf(labels[8],"s");//printf("label=%s\n",labels[8]);
     sprintf(unit[8],"");//printf("unit=%s\n",unit[8]);
     sprintf(mode[8],"");//printf("mode=%s\n",mode[8]);
     for (Histo::iterator it=histo[8].begin();it!=histo[8].end();++it){//From log back to linear scale
       aux.insert({{exp(it->first.first),it->first.second},it->second});
       //aux.insert(*it);
     }
     histo[8].clear();histo[8]=aux;aux.clear();
     
    //r->9
     sprintf(Names[9],"Histogram_r_%s",ExpCond.c_str());
     sprintf(chaine_Sum_Stats[9],"Moments_r");
    sprintf(chaine_hist[9],"%sStats/Histogram_r_%s",chaineDir,ExpCond.c_str());
     sprintf(GnuNames[9],"%s.gnu",chaine_hist[9]);
     sprintf(PlotNames[9],"%s.png",Names[9]);
     sprintf(labels[9],"r");//printf("label=%s\n",labels[9]);
     sprintf(unit[9],"");//printf("unit=%s\n",unit[9]);
     sprintf(mode[9],"");//printf("mode=%s\n",mode[9]);
     for (Histo::iterator it=histo[9].begin();it!=histo[9].end();++it){//From log back to linear scale
       aux.insert({{exp(it->first.first),it->first.second},it->second});
       //aux.insert(*it);
     }
     histo[9].clear();histo[9]=aux;aux.clear();

    //A0->10
     sprintf(Names[10],"Histogram_A0_%s",ExpCond.c_str());
     sprintf(chaine_Sum_Stats[10],"Moments_A0");
     sprintf(chaine_hist[10],"%sStats/Histogram_A0_%s",chaineDir,ExpCond.c_str());
     sprintf(GnuNames[10],"%s.gnu",chaine_hist[10]);
     sprintf(PlotNames[10],"%s.png",Names[10]);
     sprintf(labels[10],"A_0");//printf("label=%s\n",labels[10]);
     sprintf(unit[10],"");//printf("unit=%s\n",unit[10]);
     sprintf(mode[10],"");//printf("mode=%s\n",mode[10]);
    //b0->11
     sprintf(Names[11],"Histogram_b0_%s",ExpCond.c_str());
     sprintf(chaine_Sum_Stats[11],"Moments_b0");
     sprintf(chaine_hist[11],"%sStats/Histogram_b0_%s",chaineDir,ExpCond.c_str());
     sprintf(GnuNames[11],"%s.gnu",chaine_hist[11]);
     sprintf(PlotNames[11],"%s.png",Names[11]);
     sprintf(labels[11],"b_0");//printf("label=%s\n",labels[11]);
     sprintf(unit[11],"");//printf("unit=%s\n",unit[11]);
     sprintf(mode[11],"");//printf("mode=%s\n",mode[11]);
     //dist->12
     sprintf(Names[12],"Histogram_dist_%s",ExpCond.c_str());
     sprintf(chaine_Sum_Stats[12],"Moments_dist");
     sprintf(chaine_hist[12],"%sStats/Histogram_dist_%s",chaineDir,ExpCond.c_str());
     sprintf(GnuNames[12],"%s.gnu",chaine_hist[12]);
     sprintf(PlotNames[12],"%s.png",Names[12]);
     sprintf(labels[12],"d");//printf("label=%s\n",labels[12]);
     sprintf(unit[12],"");//printf("unit=%s\n",unit[12]);
     sprintf(mode[12],"");//printf("mode=%s\n",mode[12]);

    //Storing and plotting histograms, saving moments
     for (int id_field=0;id_field<id_field_max;id_field++){//Loop on fields
      printf("\t\tProcessing %s\n",labels[id_field]);
      //printf("\t\tHistogram will be loaded in %s\n",chaine_hist[id_field]);//exit(0);
      PlotStats(histo[id_field],chaine_hist[id_field],Names[id_field],GnuNames[id_field],PlotNames[id_field],labels[id_field],Gene,ExpCond,unit[id_field],mode[id_field],chaine_Sum_Stats[id_field]);
    }//End of loop on fields
    //fclose(fMoments);
     exit(0);
   }//End of statistics separating Grik and Scnn1


  if (1){//Full statistics
    ExpCond="CTL";
    printf("\t\tFull statistics in %s conditions\n",ExpCond.c_str());

    stats data;
    int id_field_max=13;
    Histo histo[id_field_max];   /*tauB->0
				   tauA->1
				   tauRF->2
				   tauG->3
				   wba->4
				   wab->5
				   wbg->6
				   wag->7
				   s->8
				   r->9
				   A0->10
				   b0->11
				   dist->12*/
    //Tuning histo bins
    double d_hist[id_field_max];//Bin size in the histogram
    d_hist[0]=5; //dtauB (ms)
    d_hist[1]=20; //dtauA (ms)
    d_hist[2]=5; //dtauRF (ms)
    d_hist[3]=5; //dtauG (ms)
    d_hist[4]=1; //dwba (Hz)
    d_hist[5]=1; //dwab (Hz)
    d_hist[6]=1; //dwbg (Hz)
    d_hist[7]=1; //dwag (Hz)
    double dr=1.0/2.0,ds=1.0/2.0;
    d_hist[8]=dr; //ds (adim)
    d_hist[9]=ds; //dr (adim)
    d_hist[10]=0.1; //A0
    d_hist[11]=0.01; //b0
    d_hist[12]=0.002; //dist

    Histo aux; //Used to get from log to linear (see below).
    moments moments[id_field_max];
    Cell_id_type cell;
    std::string Genes[]={{"Grik4"},{"Scnn1a"}};

    for (std::string gene : Genes) {//Loop on Genes
      datafile=gene+"_temp_STAs_"+ExpCond+".txt";
      printf("\n\t\t\033[0;35mThe file %s will be analysed\n\n\033[0m",datafile.c_str());
      std::vector <Experiment> Experience;
      GetData(datafile,gene,&Experience,sample_rate);

      sprintf(chaineDir,"%s/",gene.c_str());//printf("%s\n",chaineDir);exit(0);

      sprintf(chaine_corres,"%sCells_correspondence_%s",chaineDir,ExpCond.c_str());
      if ((fcor=fopen(chaine_corres,"r"))==NULL) {printf("File %s couldn't be opened, aborting\n",chaine_corres);exit(0);}
      FILE *fdist=NULL;
      char chaine_dist[1000];sprintf(chaine_dist,"%sFinalDistanceFit_%s",chaineDir,ExpCond.c_str());
      if ((fdist=fopen(chaine_dist,"r"))==NULL) {printf("File %s couldn't be opened, aborting\n",chaine_corres);exit(0);}
      int label=0,id_cell=0,id_exp;
      char bid,type[3],comment[10000];
      char chaine_data[1000];sprintf(chaine_data,"%sSummary_Fit_%s",chaineDir,ExpCond.c_str());
      //printf("\t\t%s\n",chaine_data);//exit(0);
      
      FILE* fdat=fopen(chaine_data,"w");fclose(fdat);
      fdat=fopen(chaine_data,"a");
      
      double val=0; 
      /*double tauBmax=500,tauAmax=500,tauRFmax=500,tauGmax=500;//ms
	double wbamax=500,wabmin=-500,wbgmax=500,wagmin=-500;//Hz
	double smax=512,rmax=512;*/
      double dist=0;
      int size=Experience.size();
      /*if (Gene=="Grik4") size=66;
	else if (Gene=="Scnn1a") size =50;*/
      
      for (int line=0;line<size;line++){//Loop on cells
	sprintf(chaineParams,"%sParameters/params_%d_%d_%s",chaineDir,Experience[line].Cell_id,Experience[line].Exp_id,ExpCond.c_str());//printf("Reading %s\n",chaineParams);
	if (ReadParams(&pp,chaineParams)!=false){//The parameters have been found
	  if (fscanf(fdist,"%d%lg%d%c%d%[^\n]",&label,&dist,&id_cell,&bid,&id_exp,&comment)!=EOF);
	  //printf("%d\t%lg\t%d%c%d\t%s\n",label,dist,id_cell,bid,id_exp,comment);
	  if (label!=line) {printf("Mismatch between data structure and cells correspondence data label=%d != correspondence label =%d\n",line,label);exit(0);}
	  fscanf(fcor,"%d%d%c%d%s\n",&label,&id_cell,&bid,&id_exp,&type);
	  if (label!=line) {printf("Mismatch between data structure and cells correspondence data label=%d != correspondence label =%d\n",line,label);exit(0);}
	  //exit(0);
	  
	  if ((pp.tauA<2000)&&(pp.tauB<2000)&&(fabs(pp.wab)<0.2)&&(fabs(pp.wba)<0.2)){//Values are reasonable
	    //WriteParams(pp);exit(0);	  
	    //Load tauB, tauA, tauRF, tauG, wab, wba, wag, wbg, mu,A0,b0
	    data.label=line;data.type=type;
	    data.field[0]=pp.tauB;
	    data.field[1]=pp.tauA;
	    data.field[2]=pp.tauRF;
	    data.field[3]=pp.tauG;
	    data.field[4]=pp.wba*1000;
	    data.field[5]=pp.wab*1000;
	    data.field[6]=pp.wbg*1000;
	    data.field[7]=pp.wag*1000;
	    data.field[8]=log(-pp.wab/pp.wba);//s. Parsing the log for histogram. We get back to linear below.
	    data.field[9]=log(pp.tauA/pp.tauB);//r. Parsing the log for histogram. We get back to linear below.
	    data.field[10]=pp.A0;
	    data.field[11]=pp.b0;
	    data.field[12]=dist;
	    char  id[100]; sprintf(id,"%d_%d",Experience[line].Cell_id,Experience[line].Exp_id);
	    //printf("%d\t%s\t%s\t%6f\t%6f\t%6f\t%6f\t%6f\t%6f\t%6f\t%6f\t%6f\t%6f\t%6f\t%6f\n",line,id,type,data.tauB,data.tauA,data.tauRF,data.tauG,data.wba,data.wab,data.wbg,data.wag,data.mu,data.r,data.A0,data.b0);
	    fprintf(fdat,"%d\t%s\t%s\t%6f\t%6f\t%6f\t%6f\t%6f\t%6f\t%6f\t%6f\t%6f\t%6f\t%6f\t%6f\n",line,id,type,pp.tauB,pp.tauA,pp.tauRF,pp.tauG,pp.wba,pp.wab,pp.wbg,pp.wag,exp(data.field[8]),exp(data.field[9]),pp.A0,pp.b0);
	    
	    //Filling histograms
	    for (int id_field=0;id_field<id_field_max;id_field++){//Loop on fields
	      val=(int)(data.field[id_field]/d_hist[id_field])*d_hist[id_field]; //printf("val=%g\ttype=%s\n",val,type);
	      cell={val,type};
	      Update_Histo(cell,&histo[id_field]);
	      //printf("");
	    }//End of loop on fields
	  }//End of times are reasonable
	  else{
	    printf("Cell%d: Characteristic times are too large: tauB=%g, tauA=%g, tauRF=%g, tauG=%g. \n",line,pp.tauB,pp.tauA,pp.tauRF,pp.tauG);
	  }
	}//End of the parameters have been found
	else {
	  printf("Parameters %s not found\n",chaineParams);
	}
      }//End of loop on cells  
      fclose(fcor);
      fclose(fdat);
      
      //Restoring r in normal scale
      for (Histo::iterator it=histo[8].begin();it!=histo[8].end();++it){//From log back to linear scale
	aux.insert({{exp(it->first.first),it->first.second},it->second});
	//aux.insert(*it);
      }
      histo[8].clear();histo[8]=aux;aux.clear();

      //Restoring s in normal scale
      for (Histo::iterator it=histo[9].begin();it!=histo[9].end();++it){//From log back to linear scale
	aux.insert({{exp(it->first.first),it->first.second},it->second});
	//aux.insert(*it);
      }
      histo[9].clear();histo[9]=aux;aux.clear();

    }//End of loop on Genes

      char chaine_hist[id_field_max][1000],Names[id_field_max][1000],GnuNames[id_field_max][1000],PlotNames[id_field_max][1000],labels[id_field_max][1000],unit[id_field_max][100],mode[id_field_max][100];
      char chaine_Sum_Stats[id_field_max][1000];

      //Filling file names
      //tauB->0
      sprintf(Names[0],"Histogram_tauB_%s",ExpCond.c_str());
      sprintf(chaine_Sum_Stats[0],"Moments_tauB");//printf("%s\n",chaine_Sum_Stats[0]);exit(0);
      sprintf(chaine_hist[0],"Histogram_tauB_%s",ExpCond.c_str());
      sprintf(GnuNames[0],"%s.gnu",chaine_hist[0]);
      sprintf(PlotNames[0],"%s.png",Names[0]);
      sprintf(labels[0],"{/Symbol t}_B");//printf("label=%s\n",labels[0]);
      sprintf(unit[0],"ms");//printf("unit=%s\n",unit[0]);
      sprintf(mode[0],"");//printf("mode=%s\n",mode[0]);
      //tauA->1
      sprintf(Names[1],"Histogram_tauA_%s",ExpCond.c_str());
      sprintf(chaine_Sum_Stats[1],"Moments_tauA");
      sprintf(chaine_hist[1],"Histogram_tauA_%s",ExpCond.c_str());
      sprintf(GnuNames[1],"%s.gnu",chaine_hist[1]);
      sprintf(PlotNames[1],"%s.png",Names[1]);
      sprintf(labels[1],"{/Symbol t}_A");//printf("label=%s\n",labels[1]);
      sprintf(unit[1],"ms");//printf("unit=%s\n",unit[1]);
      sprintf(mode[1],"");//printf("mode=%s\n",mode[1]);
      //tauRF->2
      sprintf(Names[2],"Histogram_tauRF_%s",ExpCond.c_str());
      sprintf(chaine_Sum_Stats[2],"Moments_tauRF");
      sprintf(chaine_hist[2],"Histogram_tauRF_%s",ExpCond.c_str());
      sprintf(GnuNames[2],"%s.gnu",chaine_hist[2]);
      sprintf(PlotNames[2],"%s.png",Names[2]);
      sprintf(labels[2],"{/Symbol t}_{RF}");//printf("label=%s\n",labels[2]);
      sprintf(unit[2],"ms");//printf("unit=%s\n",unit[2]);
      sprintf(mode[2],"");//printf("mode=%s\n",mode[2]);
      //tauG->3
      sprintf(Names[3],"Histogram_tauG_%s",ExpCond.c_str());
      sprintf(chaine_Sum_Stats[3],"Moments_tauG");
      sprintf(chaine_hist[3],"Histogram_tauG_%s",ExpCond.c_str());
      sprintf(GnuNames[3],"%s.gnu",chaine_hist[3]);
      sprintf(PlotNames[3],"%s.png",Names[3]);
      sprintf(labels[3],"{/Symbol t}_{G}");//printf("label=%s\n",labels[3]);
      sprintf(unit[3],"ms");//printf("unit=%s\n",unit[3]);
      sprintf(mode[3],"");//printf("mode=%s\n",mode[3]);
      //wba->4
      sprintf(Names[4],"Histogram_wba_%s",ExpCond.c_str());
      sprintf(chaine_Sum_Stats[4],"Moments_wba");
      sprintf(chaine_hist[4],"Histogram_wba_%s",ExpCond.c_str());
      sprintf(GnuNames[4],"%s.gnu",chaine_hist[4]);
      sprintf(PlotNames[4],"%s.png",Names[4]);
      sprintf(labels[4],"w_{ba}");//printf("label=%s\n",labels[4]);
      sprintf(unit[4],"Hz");//printf("unit=%s\n",unit[4]);
      sprintf(mode[4],"");//printf("mode=%s\n",mode[4]);
      //wab->5
      sprintf(Names[5],"Histogram_wab_%s",ExpCond.c_str());
      sprintf(chaine_Sum_Stats[5],"Moments_wab");
      sprintf(chaine_hist[5],"Histogram_wab_%s",ExpCond.c_str());
      sprintf(GnuNames[5],"%s.gnu",chaine_hist[5]);
      sprintf(PlotNames[5],"%s.png",Names[5]);
      sprintf(labels[5],"w_{ab}");//printf("label=%s\n",labels[5]);
      sprintf(unit[5],"Hz");//printf("unit=%s\n",unit[5]);
      sprintf(mode[5],"");//printf("mode=%s\n",mode[5]);
      //wbg->6
      sprintf(Names[6],"Histogram_wbg_%s",ExpCond.c_str());
      sprintf(chaine_Sum_Stats[6],"Moments_wbg");
      sprintf(chaine_hist[6],"Histogram_wbg_%s",ExpCond.c_str());
      sprintf(GnuNames[6],"%s.gnu",chaine_hist[6]);
      sprintf(PlotNames[6],"%s.png",Names[6]);
      sprintf(labels[6],"w_{bg}");//printf("label=%s\n",labels[6]);
      sprintf(unit[6],"Hz");//printf("unit=%s\n",unit[6]);
      sprintf(mode[6],"");//printf("mode=%s\n",mode[6]);
      //wag->7
      sprintf(Names[7],"Histogram_wag_%s",ExpCond.c_str());
      sprintf(chaine_Sum_Stats[7],"Moments_wag");
      sprintf(chaine_hist[7],"Histogram_wag_%s",ExpCond.c_str());
      sprintf(GnuNames[7],"%s.gnu",chaine_hist[7]);
      sprintf(PlotNames[7],"%s.png",Names[7]);
      sprintf(labels[7],"w_{ag}");//printf("label=%s\n",labels[7]);
      sprintf(unit[7],"Hz");//printf("unit=%s\n",unit[7]);
      sprintf(mode[7],"");//printf("mode=%s\n",mode[7]);
      //s->8
      sprintf(Names[8],"Histogram_s_%s",ExpCond.c_str());
      sprintf(chaine_Sum_Stats[8],"Moments_s");
      sprintf(chaine_hist[8],"Histogram_s_%s",ExpCond.c_str());
      sprintf(GnuNames[8],"%s.gnu",chaine_hist[8]);
      sprintf(PlotNames[8],"%s.png",Names[8]);
      sprintf(labels[8],"s");//printf("label=%s\n",labels[8]);
      sprintf(unit[8],"");//printf("unit=%s\n",unit[8]);
      sprintf(mode[8],"");//printf("mode=%s\n",mode[8]);      
      //r->9
      sprintf(Names[9],"Histogram_r_%s",ExpCond.c_str());
      sprintf(chaine_Sum_Stats[9],"Moments_r");
      sprintf(chaine_hist[9],"Histogram_r_%s",ExpCond.c_str());
      sprintf(GnuNames[9],"%s.gnu",chaine_hist[9]);
      sprintf(PlotNames[9],"%s.png",Names[9]);
      sprintf(labels[9],"r");//printf("label=%s\n",labels[9]);
      sprintf(unit[9],"");//printf("unit=%s\n",unit[9]);
      sprintf(mode[9],"");//printf("mode=%s\n",mode[9]);
      //A0->10
      sprintf(Names[10],"Histogram_A0_%s",ExpCond.c_str());
      sprintf(chaine_Sum_Stats[10],"Moments_A0");
      sprintf(chaine_hist[10],"Histogram_A0_%s",ExpCond.c_str());
      sprintf(GnuNames[10],"%s.gnu",chaine_hist[10]);
      sprintf(PlotNames[10],"%s.png",Names[10]);
      sprintf(labels[10],"A_0");//printf("label=%s\n",labels[10]);
      sprintf(unit[10],"");//printf("unit=%s\n",unit[10]);
      sprintf(mode[10],"");//printf("mode=%s\n",mode[10]);
      //b0->11
      sprintf(Names[11],"Histogram_b0_%s",ExpCond.c_str());
      sprintf(chaine_Sum_Stats[11],"Moments_b0");
      sprintf(chaine_hist[11],"Histogram_b0_%s",ExpCond.c_str());
      sprintf(GnuNames[11],"%s.gnu",chaine_hist[11]);
      sprintf(PlotNames[11],"%s.png",Names[11]);
      sprintf(labels[11],"b_0");//printf("label=%s\n",labels[11]);
      sprintf(unit[11],"");//printf("unit=%s\n",unit[11]);
      sprintf(mode[11],"");//printf("mode=%s\n",mode[11]);
      //dist->12
      sprintf(Names[12],"Histogram_dist_%s",ExpCond.c_str());
      sprintf(chaine_Sum_Stats[12],"Moments_dist");
      sprintf(chaine_hist[12],"Histogram_dist_%s",ExpCond.c_str());
      sprintf(GnuNames[12],"%s.gnu",chaine_hist[12]);
      sprintf(PlotNames[12],"%s.png",Names[12]);
      sprintf(labels[12],"d");//printf("label=%s\n",labels[12]);
      sprintf(unit[12],"");//printf("unit=%s\n",unit[12]);
      sprintf(mode[12],"");//printf("mode=%s\n",mode[12]);
      
      //Storing and plotting histograms, saving moments
       for (int id_field=0;id_field<id_field_max;id_field++){//Loop on fields
	// for (int id_field=12;id_field<13;id_field++){//Loop on fields
	printf("\t\tProcessing %s\n",labels[id_field]);
	//printf("\t\tHistogram will be loaded in %s\n",chaine_hist[id_field]);//exit(0);
	PlotStats(histo[id_field],chaine_hist[id_field],Names[id_field],GnuNames[id_field],PlotNames[id_field],labels[id_field],Gene,ExpCond,unit[id_field],mode[id_field],chaine_Sum_Stats[id_field]);
      }//End of loop on fields
      
  }//End of full statistics

  //Free the pointers
  free(X);
  delete_params(&p);
  return EXIT_SUCCESS;
}
