/**  Functions for dynamic evolution **/

#ifndef Dynamics_h
#define Dynamics_h

#include "Defs.h"
#include "Utils.h"

//Bipolar rectification
double NB(double V,params p);

//Amacrine Rectification
double NA(double V,params p);

//Bipolar Gain control
double G(double A);

//Non Linear Bipolar correction
double R(double V,double A,params p);

//Vector field with rectification and gain control
void vector_field(double *X,double *Xp,double t,params p,double (*input)(double x,double t, params p));

//Vector field with rectification without gain control
void vector_field_Rect(double *X,double *Xp,double t,params p,double (*input)(double x,double t, params p));

//Linear vector field, without rectification
void vector_field_lin(double *X,double *Xp,double t,params p,double (*input)(double x,double t, params p));

//Vector field, similar as above,  with an additional variable, the space location x, the point of space where GCells RFs are computed.
void vector_field_Space(double x,double *X,double *Xp,double t,params p,double (*input)(int beta,double x,double t, params p));

//Vector field, similar as above. Rectified without Gain control
void vector_field_Space_Rect(double x,double *X,double *Xp,double t,params p,double (*input)(int ic,double x,double t, params p));

//Vector field, similar as above,  but without rectification. 
void vector_field_Space_lin(double x,double *X,double *Xp,double t,params p,double (*input)(int ic,double x,double t, params p));

//Computes the reference times tauBref,tauAref, tauGref, from the prescribed value of tauB, tauA, tauG. Used to initialise data from experiments.
//These times are computed for the center cell
//params contains the parameters and the prescribed value of  tauB, tauA, tauG.
//V* rest are the rest states (to be computed before)
void ComputeReferenceTimes(params*p,double VB_rest, double VA_rest,double VG_rest);

//Computes the characteristic times as a function of weights and rest state.
//These times are computed for the center cell
//params contains the parameters
//V* rest are the rest states (to be computed before)
 void ComputeCharacteristicTimes_FixedRestState(params*p,double VB_rest, double VA_rest,double VG_rest);

//Compute the rest state of the model for a fixed value of characteristic times
//p is the set of parameters
//f is the vector field
//Xrest is the network rest state
//VB_rest, VA_rest, VG_rest are the rest state of BCells, ACells, GCells used later to compute characteristic times
//Arest is the rest activity for a model with gain control 
void ComputeRestState_FixedCharacteristicTimes(int iGC,params p,void (*f)(double *X,double *Xp,double t,params p,double (*input)(double x,double t, params p)),double*Xrest,double *VB_rest,double *VA_rest,double *VG_rest,double *A_rest);

//Computes the rest state characteristics, Vrest and times
//p is the set of parameters
//f is the vector field
//X is the network rest state
//VB_rest, VA_rest, VG_rest are the rest state of BCells, ACells, GCells used later to compute characteristic times
//Arest is the rest activity for a model with gain control 
void ComputeRestState_Full(int iGC,params *p,void (*f)(double *X,double *Xp,double t,params p,double (*input)(double x,double t, params p)),double *Xrest,double *VB_rest,double *VA_rest,double *VG_rest,double *A_rest);

#endif //Dynamics_h

//Checks the linear stability 
bool IsStable(params p);
