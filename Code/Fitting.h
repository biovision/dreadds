/*** Library to fit model to data ***/

#ifndef Fitting_h
#define Fitting_h

#include <string.h>
#include "Defs.h"
#include "Utils.h"
#include "Plot.h"
#include "Spectrum.h"
#include "Dynamics.h"

#define MAXCHAR 2000
#define MaxTime 3000 //Maximal allowed time for characteristic times

typedef struct fit_switch{
  bool fitwba=false;
  bool fitwab=false;
  bool fitwbg=false;
  bool fitwag=false;
  bool fittauB=false;
  bool fittauA=false;
  bool fittauG=false;
  bool fittauRF=false;
  bool fitA0=false;
  bool fitb0=false;
  bool fitgCNOA=false;
  bool fitgCNOG=false;
  bool fitzetaA=false;
  bool fitzetaG=false;
  bool fitECNOA=false;
  bool fitECNOG=false;
}Fit_switch;

/*** Gets the data.
     Each row corresponds to the temporal trace of each cell, where 
     the first column is the id of the cell and 
     the second column is the id of the experiment. 
     cell_id + experiment_id + gene.
     Data is the string naming the data file
     Gene is the name of the gene.
     experiences contains the whole set of experiences in the appropriate format for the function FitCells
 **/
void GetData(std::string Data,std::string Gene,std::vector <Experiment> *experiences,double sample_rate);

/*** Fits a power spectrum in the r,s plane 
p contains the parameters;
PowerSpectrum is the name of the file where the power spectrum is stored
max_iter is the maximal number of iterations for the fit
precision is the precision required
sample_rate is the rate at which the power spectrum has been sampled.
On output r,s,p are modified 
***/
void Fit_FT(double *r,double* s,params *p,char* PowerSpectrum,int max_iter,double precision,double sample_rate);

/*** Initializes the params p using a pre fit of the rising and decaying part of the RF. Also generates the power spectrum of the experimental trace having a sample rate sample_rate (expressed in ms, WARNING of the unit). Cell_Type returns an  estimation of the cell type (ON or OFF). ExpCond contains the experimental conditions CTL or CNO. chainDir contains the name of the directory where the power spectrum is stored**/
void preFit(Experiment exper,params *p,string *Cell_Type,string ExpCond,char* chaineDir,double sample_rate);

/** Fit the model from a experimental cell
    The cell data are stored in exp_file (format time (ms), RF)
    The model vector field is f
    tmin_fit,tmax_fit are the lower and upper bound for the time interval of fit
    fit_variables is a vector of booleans selecting the parameters to fit.
    max_iter is the maximum number of iterations 
    precision is the required precision
**/
void FitCell(std::string exp_file,params *p,void (*f)(double *X,double *Xp,double t,params p,double (*input)(double x,double t, params p)),double tmin_fit,double tmax_fit,fit_switch fit_variables,int max_iter,double precision);

/** Fit the model from a experimental cell by minimising the distance between the temporal RF and the model. Spatial RF is not taken into account.
    The cell data are stored in the structure exper, of type Experiment
    The model vector field is f
    tmin_fit,tmax_fit are the lower and upper bound for the time interval of fit
    fit_variables is a vector of booleans selecting the parameters to fit.
    max_iter is the maximum number of iterations 
    precision is the required precisions
    return the final distance between the fit and data
**/
double FitCells(Experiment exper,params *p,void (*f)(double *X,double *Xp,double t,params p,double (*input)(double x,double t, params p)),double tmin_fit,double tmax_fit,fit_switch fit_variables,int max_iter,double precision);

//Perturbs the parameters in one direction and computes the linear response
//On output the params pdp is modified and the linear response vector  dThRFd is computed
//pdp: params to be modified
//tmax_fit: maximal time for computing the response
//iGC: index of the GCell 
//data: vector of data
//ThRF: theoretical RF without perturbation
//kappa_th, sigma_th,Psi, Psi_transpose, SumPinvbgKSg, SumPinvbgKSg_CNOA: auxilary vectors. Must be allocated first
// View, checks: used to debug.
void ComputeLinearResponse(params *pdp,double tmax_fit,int iGC,std::vector <Event> data,std::vector <Event> ThRF,std::vector <Event> *dThRFd,Spectrum kappa_th,Spectrum sigma_th,gsl_matrix *Psi,gsl_matrix *Psi_transpose,gsl_vector_complex* SumPinvbgKSg,gsl_vector_complex* SumPinvbgKSg_CNOA,bool View,bool checks);

//Perturbs the parameters in one direction and computes the normalized power spectrum of the linear response
//On output the params pdp is modified and the linear response vector  dThRFd is computed
//pdp: params to be modified
//tmax_fit: maximal time for computing the response
//iGC: index of the GCell 
//data: vector of data
//ThRF: theoretical RF without perturbation
//kappa_th, sigma_th,Psi, Psi_transpose, SumPinvbgKSg, SumPinvbgKSg_CNOA: auxilary vectors. Must be allocated first
//View, checks: used to debug.
//scaling. Scaling factor to normalise kg
void ComputeLinearResponse_hat(params *pdp,double tmax_fit,int iGC,std::vector <Event> data,std::vector <Event> ThRF,std::vector <Event> *dThRFd,Spectrum kappa_th,Spectrum sigma_th,gsl_matrix *Psi,gsl_matrix *Psi_transpose,gsl_vector_complex* SumPinvbgKSg,gsl_vector_complex* SumPinvbgKSg_CNOA,bool View,bool checks,double scaling);

double Distance(std::vector <Event> data1,std::vector <Event> data2,double tmax_fit);//Computes the L2 distance between two sets of data over a time horizon tmax

//Compute and store numerical and theoretical RF of GCell with index iGC
//string Cell_Type distinguishes between a ON and OFF Cell
//chaineT is the name of the file where OPL,BCell,ACell,GCell voltage and BCell activity vs time are stored
//chaineS contains the spatial receptive field
//chainTh contains the theoretical spatio temporal RF
//f is the space independent vector field
//fS is the space dependent vector field
//SumPinvbgKSg is an auxilary vector used for computations (must be allocated first)
void ComputeAndStoreSpaceRF(int iGC,params p,char *chaineT,char* chaineS,char* chaineTh,void (*f)(double *X,double *Xp,double t,params p,double (*input)(double x,double t, params p)),void (*fS)(double x,double *X,double *Xp,double t,params p,double (*input)(int beta,double x,double t, params p)),gsl_vector_complex* SumPinvbgKSg,gsl_vector_complex* SumPinvbgKSg_CNOA);

//Compute and store numerical and theoretical temporal RF of GCell with index iGC
//chaineT is the name of the file where OPL,BCell,ACell,GCell voltage and BCell activity vs time are stored
//chainTh contains the theoretical spatio temporal RF
//f is the space independent vector field
//fS is the space dependent vector field
//SumPinvbgKSg is an auxilary vector used for computations (must be allocated first)
void ComputeAndStoreRF(int iGC,params p,char *chaineT,char* chaineTh,void (*f)(double *X,double *Xp,double t,params p,double (*input)(double x,double t, params p)),void (*fS)(double x,double *X,double *Xp,double t,params p,double (*input)(int beta,double x,double t, params p)),gsl_vector_complex* SumPinvbgKSg,gsl_vector_complex* SumPinvbgKSg_CNOA);

//Rescale the increment epsilon used in FitCell whenever the computed derivative is outside the range [d_min,d_max].
//Returns 1 when rescaling, 2 when aborting because derivative becomes too large
int Rescale(double derivative,double *epsilon,double d_min,double d_max);

#endif //Fitting_h
