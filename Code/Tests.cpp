/***
Tests for the code utilities

Bruno Cessac, 07-11-2022
 ***/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include "Defs.h"
#include "Utils.h"
#include "Plot.h"
#include "Spectrum.h"
#include "Dynamics.h"
#include "Fitting.h"
#include <unistd.h>
#include <cmath>


int main(void){
    /** Initializations of the main variables ***/
  double convert_s_to_ms=1000; //Conversion s to ms. 1s = 1000 ms
  double convert_ms_to_s=0.001; //Conversion ms to s. 1 ms = 0.001 s
  double weight_factor=1E-3;
  
  void (*f)(double *X,double *Xp,double t,params p,double (*input)(double x,double t, params p))=NULL;
  void (*fS)(double x,double *X,double *Xp,double t,params p,double (*input)(int beta,double x,double t, params p))=NULL;

  /*** Initialization of the parameters variable p ***/
  params p;//Cells parameters

  //Lattice
  p.N=60; //Number of visible neurons (excluding boundaries)
  if (p.N%2) {printf("N must be even. Exiting \n");exit(0);} //Used only for spectral computing, avoiding a zero eigenvalue in the Laplacian spectrum
  p.offset=0;//(int)(5.0*sigmabg/p.dx);//Number of invisible neurons (on the left of the neuron 0 and on the right to neuron N-1) to avoid boundaries effects. The offset is computed so that the visible neurons at the boundaries are at a distance 5 sigma from the offset.
  if (p.offset!=0) {printf("The case p.N \neq p.L (non zero offset) may not work properly\n");exit(0);}
  p.L=p.N+2*p.offset; //Total number of neurons. Be careful if L\neq N parts of the code might not work.
  //printf("offset=%d\n",p.offset); exit(0);
  p.dt=0.1; //Integration time step (ms)
  p.dx=0.03; //x Cells spacing (mm)
  p.dy=0.03; //y Cells spacing (mm)
  
  //Cells
  p.tauL=C/gL;//  printf("tauL=%g ms\n",p.tauL);exit(0);
  
  //Bipolars
  p.thetaB=0;//Threshold for bipolar rectification (mV)
  p.tauBref=p.tauB=50;//Baseline bipolar voltage integration characteristic time (ms). This is the leak characteristic time.
  p.CB=C;//Bipolar membrane capacity (pF)
  p.gLB=gL;//p.CB/p.tauB; //printf("gLB=%lg\n",p.gLB);exit(0);//Bipolar leak conductance (nS) 
  p.ELB=0;
  p.unsurtauB=1.0/p.tauB;//,unsurtaua=1.0/p.taua;
  p.KB=p.gLB*p.ELB/p.CB; //Constant fixed by the leak potential (mV/ms)

 //Gain control
  p.taua=10;//Bipolar activity characteristic time (ms)
  p.h=1E-2;//Activity rate (mV^-1 ms^-1)

  //Time RF of BCells(second order kernel) (fit from Gerrit Hilgen's data)
  p.A0=1;//Amplitude of the BCell response to a pulse - alpha profile (mV)
  p.t0=0; //Starting time of the alpha profile (ms)
  p.tauRF=50;//Characteristic time of the BCell RF (ms)
  p.tauRF3= pow(p.tauRF,3); 
  p.b0=0; //Constant value in  the BCell response to a pulse (mV)

  //Spatial RF of BCells
  p.Ac=2;//Amplitude of the spatial RF center (mV.mm)
  p.sigma_c=2*p.dx;//Variance of  the spatial RF center (mm)
  p.As=1.5;//Amplitude of  the spatial RF surround (mV.mm) 
  p.sigma_s=4*p.dx;//Variance of  the spatial RF surround (mm)

  //Amacrines
  p.thetaA=0; //Threshold for Amacrine rectification (mV)
  p.tauA=p.tauAref=100;//Modifi� le 09/09/2022
  p.gCNOA=0;//Amacrine CNO conductance (nS)
  p.lambdaA=p.gCNOA/C;//Amacrine CNO  ratio g/C (kHz)
  p.ECNOA=ENa-EL; //Amacrine CNO reversal potential in mV (Sodium -rest state)
  p.zetaA=p.lambdaA*p.ECNOA; //CNO influence for Amacrines (mV/ms)
  //p.tauA=p.tauAref/(1+p.lambdaA*p.tauAref);//Amacrine characteristic time in the presence of CNO (ms)
  //p.tauA=50;//Modifi� le 09/09/2022
  p.unsurtauA=1.0/p.tauA;//1/tauA (kHz)
  p.CA=C; //Amacrine membrane capacity (pF)
  p.gLA=gL;//p.CA/p.tauA; //printf("gLA=%lg nS\n",p.gLA);exit(0);//Amacrine leak conductance (nS) 
  p.ELA=0;
  p.KA=p.gLA*p.ELA/p.CA; //Constant fixed by the leak potential (mV/ms)

  //Ganglions
  p.gCNOG=0;//Ganglion CNO conductance (nS)
  p.lambdaG=p.gCNOG/C;//GCell CNO conductance (nS)
  p.ECNOG=ENa-EL; //GCell CNO reversal potential in mV 
  p.zetaG=p.lambdaG*p.ECNOG; //CNO influence for GCells (mV/ms)
  p.tauG=p.tauGref=1.1*p.tauB;//GCell leak characteristic time (ms) 
  //p.tauG=p.tauGref/(1+p.lambdaG*p.tauGref);//GCell characteristic time in the presence of CNO and with membrane polarisation (ms)
  //p.tauG=1.1*p.tauB;//Modifi� le 09/09/2022
  p.unsurtauG=1.0/p.tauG;
  p.CG=C; //GCell membrane capacity (in microF/cm2)
  p.gLG=gL;//p.CG/p.tauG;// printf("gLG=%lg nS\n",p.gLG);exit(0);//GCell leak conductance (nS) 
  p.ELG=0;
  p.KG=p.gLG*p.ELG/p.CG; //Constant fixed by the leak potential (mV/ms)

//printf("p.lambdaG*p.tauGref=%lg\n",p.lambdaG*p.tauGref);
  //printf("tauGref=%lg\ttauG=%lg\tzetaG=%lg\n",p.tauGref,p.tauG,p.zetaG);

  //Connections
  p.wba=6*weight_factor; //Bipolar Amacrine  weight (kHz)
  p.wab=-p.wba; //Amacrine Bipolar weight (kHz)
  p.wbg=3*weight_factor;//Bipolar Ganglion weight in the Gaussian pooling (kHz)
  p.wag=0;//Amacrine Ganglion weight. Must be negative. (kHz)
  double sigmabg=5*p.dx; //Size of the GCell RF (after pooling) (mm)
  p.sigmabg2=pow(sigmabg,2);//Bipolar Ganglion variance in the Gaussian pooling (mm^2)
  p.BtoA=1;//If 0 one BCell connects a unique ACell, if 1 WAB=WBA
  p.IsPooling=true; //Pooling if true

  //Global
  p.unsurtau=p.unsurtauA-p.unsurtauB;//1/tauA-1/tauB (kHz)
  p.unsurtauAB=p.unsurtauA+p.unsurtauB;//1/tauA+1/tauB (kHz)
  if ((fabs(p.unsurtau)>1E-10)) {
    p.tau=1.0/p.unsurtau;
  }
  else {
    printf("Case tau^-1=0 not implemented yet, exiting\n");
    exit(0);
  }
  p.tau2=pow(p.tau,2);
  p.tauAB=1.0/p.unsurtauAB;
  p.mu=-p.wab*p.wba*p.tau2;

 //Pulse
  p.AmpStim=0.0; //Amplitude of the pulse (mV)
  p.speed=1; //GaussianPulse speed (mm/ms) (propagation along the x axis)
  p.mean=0;//p.N*p.dx/2.0;
  p.sigma=1; //GaussianPulse width (mm)
  p.tmin=-500;//initial time (ms)
  p.tmax=100; //Final time (ms)
  //p.strength=1.0; //Strength of the stimulus

  int beta=0;
  int iGC=p.N/2;//Index of the GCell on the lattice
  
  int alphaBC=iGC;//Index of the BCell above this GCell in the extended phase space
  int alphaAC=iGC+p.L;//Index of the ACell above this GCell in the extended phase space
  int alphaGC=iGC+2*p.L;//Index of the GCell in the extended phase space
  int alphaAc=iGC+3*p.L;//Index of the BCell activity in the extended phase space
  double xGC=iGC*p.dx,yGC=0;//Physical coordinates of the GCell (mm)

  // Dynamics variables
  // State memory allocation and initialization at 0
  int dim_ext=4*p.L;// Dimension of the phase space
  int dim=3*p.L;// Dimension of the phase space without activities
  double *X=(double*)calloc(dim_ext,sizeof(double));//State vector time t: Variables 0... N-1 are bipolar voltages,Variables N... 2N-1 are amacrine voltages, Variables 2N... 3N-1 are ganglion voltages
  // double *Xp=(double*)calloc(dim_ext,sizeof(double));//State vector time t+dt
  //double *Xx=(double*)calloc(dim_ext,sizeof(double)); //X(x,t)
  //double* temp;//swap variable
  int dyn_type=0; //0 if linear, 1 if rectified without gain control, 2 if rectified with gain control
  string chain_type=Select_dyn_type(dyn_type,&f,&fS);

  //Experiments variable
  std::string Gene="Grik4",ExpCond="CTL",datafile=Gene+"_temp_STAs_"+ExpCond+".txt";
  //std::string Gene="Scnn1a",ExpCond="CTL",datafile=Gene+"_temp_STAs_"+ExpCond+".txt";
  std::vector <Experiment> experiences;
  char chaineDir[1000],chaineParams[1000],chaine[1000];
  sprintf(chaineDir,"%s/",Gene.c_str());//printf("%s\n",chaineDir);exit(0);
  double sample_rate=0.033/4.0;//s
  GetData(datafile,Gene,&experiences,sample_rate);

                                                    /**** Tests section ***/
  
  //(0) Tests My_..._free
  if (0){
    init_Network_Laplacian(&p,1);
    delete_params(&p);
    exit(0);
  }

  //(1) Tests the FT of an experimental trace
  if (0){//Tests the FT of an experimental trace and the fit of the FT in the r,s plane
    printf("\n\t\tTests the FT of an experimental trace\n");
    char InFile[1000]="",PowerSpectrum[1000]="",GnuName[1000]="",PlotName[1000]="",LocExec[1000]="",Title[1000]="";
    int n_peaks=0;
    int line=56;
    double max_range=0;//max_range is the maximal frequency range i.e. the maximal value where the amplitude of the normalized spectrum is larger than zero_PS.
    
    sprintf(InFile,"_%d_%d_%s",experiences[line].Cell_id,experiences[line].Exp_id,ExpCond.c_str());//printf("%s\n",InFile);exit(0);
    sprintf(GnuName,"FFT%s.gnu",InFile);//printf("%s\n",GnuName);exit(0);
    sprintf(LocExec,"%sRFs/FFT%s.gnu",chaineDir,InFile);//printf("%s\n",LocExec);exit(0);    
    sprintf(PlotName,"PowerSpectrum%s.png",InFile);//printf("%s\n",PlotName);exit(0);
    sprintf(PowerSpectrum,"%sRFs/PowerSpectrum_RF%s",chaineDir,InFile);//printf("%s\n",PowerSpectrum);//exit(0);
    
    //FFT
    sample_rate=0.033*1000/4.0;//Convert sample rate in ms
    std::vector<Peaks_fft> periods=getPeriod(experiences[line],sample_rate,&n_peaks,PowerSpectrum,&max_range); 
    //exit(0);
    printf("\n\t\tThe %d first peaks (periods) in the power spectrum are:\n",n_peaks);
    for (int k=0;k<n_peaks;k++) {
      if (periods[k].f>0) printf("\t\t\t(f=%lg (Hz), T=%lg (ms), Amp=%lg, width=%lg (Hz) => %lg (ms))\n",1000*periods[k].f,1.0/periods[k].f,periods[k].Amp,1000*periods[k].width,1.0/periods[k].width);
      else
	printf("\t\t\t(f=%lg (Hz), int, Amp=%lg, width=%lg (Hz) => %lg (ms)) \n",1000*periods[k].f,periods[k].Amp,1000*periods[k].width,1.0/periods[k].width);
    }
    
    //Fit of the FT
     printf("\n\t\tTests the fit of the FT in the r,s plane\n");
     params pp;
     sprintf(chaineParams,"%sParameters/params_%d_%d_%s",chaineDir,experiences[line].Cell_id,experiences[line].Exp_id,ExpCond.c_str());//printf("%s\n",chaineParams);exit(0);
    if (ReadParams(&pp,chaineParams)==false) {printf("Parameters file %s does not exist\n",chaineParams);exit(0);};

    double r=pp.tauA/pp.tauB,s=-pp.wab/pp.wba;
    // pp.wab=-s*pp.wba;
    //UpdateParams(&pp);
    //printf("r=%g, s=%g\n",r,s);
    //Fit_FT(&r,&s,&pp,PowerSpectrum,10000,1E-3,sample_rate);
    //exit(0);
    //Plot
    sprintf(PowerSpectrum,"PowerSpectrum_RF%s",InFile);//printf("%s\n",PowerSpectrum);//exit(0);//Label case
    
    plot_Power_Spectrum(PowerSpectrum,periods,LocExec,PlotName,Title,max_range);
    if (Gene=="Grik4") chdir("Grik4/RFs/");
    if (Gene=="Scnn1a") chdir("Scnn1a/RFs/");
    //system("pwd\n");
    sprintf(chaine,"gnuplot %s\n",GnuName);
    printf("\n\t\tExecuting %s",chaine);
    system(chaine);
    chdir("../..");
    
    //Cleaning
     printf("\n\t\tcleaning\n");
    sprintf(chaine,"rm -f %s\n",LocExec);
    printf("\n\t\tExecuting %s",chaine);
    //system(chaine);
    sprintf(chaine,"rm -f %sRFs/%s\n",chaineDir,PowerSpectrum);
    printf("\n\t\tExecuting %s",chaine);
    //system(chaine);
    
    exit(0);
  }//End of tests the FT of an experimental trace
  
  // (2) Test. FFT of UT.
  if (0){//Test. FFT of UT. Compares the results of the function UT and UTc in Utils.ccp. Computes UT_hat the analytic FT of UT and compare with the FFT of the temporal trace. Results are compared with a plot file in TestUT.gnu

    printf("\t\tTest. FFT of UT.\n");
    
    int NumPoint=1<<11,n=NumPoint;//printf("NumPoint=%d\n",NumPoint);exit(0);//Pour FFT.
    params p2=p;
    p2.tmax=1<<14;
    p2.tmin=0;
    p2.dt=(float)(p2.tmax/(double)NumPoint);//Pour FFT.
    double sample_rate=p2.dt;
    double ArrayF1[2*n],PowSpect[2*n];
    int index=0;
    gsl_complex lambda_beta={-1.0/60,60};
    // gsl_complex lambda_beta={0,0};
    // p2.b0=0.01;
    p2.tauRF=30;
    
    FILE *fUT=fopen("UT_Test","w"),*fUTc=fopen("UT_Test_c","w");
    
    for (double t=0;t<p2.tmax;t+=sample_rate){
      gsl_complex z=UT_c(lambda_beta,t,p2);
      REAL(ArrayF1,index) = GSL_REAL(z); 
      IMAG(ArrayF1,index) = GSL_IMAG(z);
      fprintf(fUTc,"%lg\t%lg\t%lg\t%lg\t%lg\n",t,GSL_REAL(z),GSL_IMAG(z),gsl_complex_abs(z),gsl_complex_arg(z));
      z=UT(lambda_beta,t,p2);
      fprintf(fUT,"%lg\t%lg\t%lg\t%lg\t%lg\n",t,GSL_REAL(z),GSL_IMAG(z),gsl_complex_abs(z),gsl_complex_arg(z));
      index++;
    }
    
    fclose(fUT);fclose(fUTc);
    
    //Computes the Fourier transform and stores it in a complex vector
    int err1=gsl_fft_complex_radix2_forward(ArrayF1,1,n);   
    if (err1==GSL_EDOM) {printf("In getPeriod, n=%d must be a power of 2\n",n);exit(0);}
    if (err1!=GSL_SUCCESS) {printf("In getPeriod, GSL error in fft computation\n",n);exit(0);}
    
    double delta_f=1.0/(n*sample_rate);//Frequency sampling in Hz
    
    //Resymetrize the spectrum
    //for (index=1;index<n/2;index++) REAL(ArrayF1,n/2+index)=REAL(ArrayF1,n/2);
    FILE *fp=fopen("FFT_UT_Test","w");
    for (index=1;index<n/2;index++){
      gsl_complex z={REAL(ArrayF1,n/2+index)*sample_rate,IMAG(ArrayF1,n/2+index)*sample_rate};
      fprintf(fp,"%lg\t%lg\t%lg\t%lg\t%lg\n",-(n/2-index)*delta_f,REAL(ArrayF1,n/2+index)*sample_rate,IMAG(ArrayF1,n/2+index)*sample_rate,gsl_complex_abs(z),gsl_complex_arg(z));
    }
    
    for (index=0;index<n/2;index++){
      gsl_complex z={REAL(ArrayF1,index)*sample_rate,IMAG(ArrayF1,index)*sample_rate};
      fprintf(fp,"%lg\t%lg\t%lg\t%lg\t%lg\n",index*delta_f,REAL(ArrayF1,index)*sample_rate,IMAG(ArrayF1,index)*sample_rate,gsl_complex_abs(z),gsl_complex_arg(z));
    }
    fclose(fp);
    
    //   printf("A0=%g, tauRF=%g, b0=%g\n",p2.A0,p2.tauRF,p2.b0);
    fp=fopen("FFT_UT_An","w");
    
    double df=0.0001;
    for (double f=-0.2;f<0.2;f+=df){
      gsl_complex z=UT_hat(lambda_beta,f,p2);
      fprintf(fp,"%lg\t%lg\t%lg\t%lg\t%lg\n",f,GSL_REAL(z),GSL_IMAG(z),gsl_complex_abs(z),gsl_complex_arg(z));
    }
    
    fclose(fp);
    
    exit(0);
  }//End of FFT of UT

  // (3) Testing the analytic Fourier Transform of the RF
  if (0){//Testing the analytic Fourier Transform of the RF. Loads a param file, computes the temporal RF (File Test_RF_for_FFT_time), and its FFT  (Test_RF_for_FFT_FFT). Than computes the analytic FFT (KG_CNOA_hat in Utils.cpp) loaded in the file Test_RF_for_FFT_An. The comparison is done by plot (gnuplot file Test_FFT_RF.gnu).

    printf("\n\t\t\tTesting the analytic Fourier Transform of the RF\n");
    params pp;
    int line=56;
    sprintf(chaineParams,"%sParameters/params_%d_%d_%s",chaineDir,experiences[line].Cell_id,experiences[line].Exp_id,ExpCond.c_str());//printf("%s\n",chaineParams);exit(0);
    if (ReadParams(&pp,chaineParams)==false) {printf("Parameters file %s does not exist\n",chaineParams);exit(0);};
    //WriteParams(pp);exit(0);
    
    FILE *fexp=fopen("Test_RF_for_FFT_Exp_Trace","w");
    for (int i=2;i<experiences[line].data.size();i++)
      fprintf(fexp,"%lg\t%lg\n",experiences[line].data[i].t,experiences[line].data[i].Amp);
    fclose(fexp);
    
    init_Network_Laplacian(&pp,1);
    
    gsl_matrix *Psi=gsl_matrix_calloc(pp.L,pp.L),*Psi_transpose=gsl_matrix_calloc(pp.L,pp.L);
    Spectrum kappa_th;//Theoretical eigenvalues of the connectivity matrix amacrine bipolar
    Spectrum sigma_th;//Theoretical eigenvalues of the connectivity matrix bipolar amacrine
    bool View=false; //If true print the matrices involved in the model
    bool checks=true; //If true checks spectrum and P^-1
    
    ComputeTheoreticalModesLaplacian1D(pp,&kappa_th,&sigma_th,Psi,Psi_transpose);
    ComputeTheoreticalSpectrumL(kappa_th,sigma_th,Psi,Psi_transpose,&pp);
    
    gsl_vector_complex* SumPinvbgKSg=gsl_vector_complex_calloc(dim);//Auxilary used for computation
    gsl_vector_complex* SumPinvbgKSg_CNOA=gsl_vector_complex_calloc(dim);//Auxilary used for computation
    KG_CNOA(pp,SumPinvbgKSg_CNOA);//CNO A contribution
    
    KSG(xGC,yGC,pp, SumPinvbgKSg);//KSG contains the value of the spatial RF, of each eigenmode beta, at point x,y.
    gsl_complex kg={0,0};//Computes the RF of a GCell alpha embedded in the BCell, ACell, GCell network
    int NumPoint=1<<13,n=NumPoint;//printf("NumPoint=%d\n",NumPoint);exit(0);//Pour FFT.
    pp.tmax=n*pp.dt;
    pp.tmin=0;
    double sample_rate=pp.dt;
    double ArrayF1[2*n],PowSpect[2*n];
    int index=0;

    char chaineTh[1000]; sprintf(chaineTh,"Test_RF_for_FFT_time");
    FILE* fp1=fopen(chaineTh,"w");	
    
    //Computes and stores the experimental trace
    for (double t=0;(t<pp.tmax)&&(index<n);t+=sample_rate){
      kg=KG(alphaGC,xGC,0,t-pp.t0,pp,SumPinvbgKSg,SumPinvbgKSg_CNOA);
      if (fabs(GSL_IMAG(kg)>1E-8)) {printf("Problem. The RF is not real. Imaginary part at (%lg,%lg,%lg)=%lg",xGC,yGC,t,kg);exit(0);}
      //fprintf(fp1,"%lg\t%lg\t%lg\n",x,t,p.A0*GSL_REAL(kg)+p.b0+VG_rest);
      fprintf(fp1,"%lg\t%lg\t%lg\n",xGC,t,GSL_REAL(kg));
      REAL(ArrayF1,index) = GSL_REAL(kg); 
      IMAG(ArrayF1,index) = GSL_IMAG(kg);
      index++;
    }
    fclose(fp1);
    
    //Computes and stores the FFT of the experimental trace
    char PowerSpectrum[1000];sprintf(PowerSpectrum,"Test_RF_for_FFT_FFT");
    int n_peaks=0;
    double max_range=0;
    
    //Computes the Fourier transform and stores it in a complex vector
    int err1=gsl_fft_complex_radix2_forward(ArrayF1,1,n);   
    if (err1==GSL_EDOM) {printf("In getPeriod, n=%d must be a power of 2\n",n);exit(0);}
    if (err1!=GSL_SUCCESS) {printf("In getPeriod, GSL error in fft computation\n",n);exit(0);}
    
    double delta_f=1.0/(n*sample_rate);//Frequency sampling in Hz
    fp1=fopen(PowerSpectrum,"w");
    for (index=1;index<n/2;index++){
      gsl_complex z={REAL(ArrayF1,n/2+index)*sample_rate,IMAG(ArrayF1,n/2+index)*sample_rate};
      fprintf(fp1,"%lg\t%lg\t%lg\t%lg\t%lg\n",-(n/2-index)*delta_f,REAL(ArrayF1,n/2+index)*sample_rate,IMAG(ArrayF1,n/2+index)*sample_rate,gsl_complex_abs(z),gsl_complex_arg(z));
    }
    
    for (index=0;index<n/2;index++){
      gsl_complex z={REAL(ArrayF1,index)*sample_rate,IMAG(ArrayF1,index)*sample_rate};
      fprintf(fp1,"%lg\t%lg\t%lg\t%lg\t%lg\n",index*delta_f,REAL(ArrayF1,index)*sample_rate,IMAG(ArrayF1,index)*sample_rate,gsl_complex_abs(z),gsl_complex_arg(z));
    }
    fclose(fp1);
    
    //Computes and stores analytic form
    sprintf(PowerSpectrum,"Test_RF_for_FFT_An");
    fp1=fopen(PowerSpectrum,"w");
    double df=0.00005;
    for (double f=-0.2;f<0.2;f+=df){
      gsl_complex z=KG_hat(alphaGC,xGC,0,f,pp,SumPinvbgKSg,SumPinvbgKSg_CNOA);
      if (abs(f)<df){
	double omega=dpi*f;
	gsl_complex Dirac=gsl_complex_div(gsl_complex_add_real(gsl_complex_exp({0,dpi*n*sample_rate}),-1),gsl_complex_add_real(gsl_complex_exp({0,dpi*sample_rate}),-1));
	//printf("Dirac=(%g,%g) \n",pp.b0*GSL_REAL(Dirac),pp.b0*GSL_IMAG(Dirac));
	z=gsl_complex_add(z,Dirac); //Add Discrete Dirac in the power spectrum
     }
      fprintf(fp1,"%lg\t%lg\t%lg\t%lg\t%lg\n",f,GSL_REAL(z),GSL_IMAG(z),gsl_complex_abs(z),gsl_complex_arg(z));
    }
    fclose(fp1);
    
    My_gsl_vector_complex_free(SumPinvbgKSg);
    My_gsl_vector_complex_free(SumPinvbgKSg_CNOA);
    My_gsl_matrix_free(Psi);My_gsl_matrix_free(Psi_transpose);
    
  }//End of testing the analytic Fourier Transform

  if (0){//Testing the method std::vector<Peaks_fft> getPeriod(int alpha,double x,double y,params p)
    printf("\n\t\tTesting the method std::vector<Peaks_fft> getPeriod(int alpha,double x,double y,params p)\n");
     params pp;
     int line=29;
     sprintf(chaineParams,"%sParameters/params_%d_%d_%s",chaineDir,experiences[line].Cell_id,experiences[line].Exp_id,ExpCond.c_str());//printf("%s\n",chaineParams);exit(0);
    if (ReadParams(&pp,chaineParams)==false) {printf("Parameters file %s does not exist\n",chaineParams);exit(0);};
   double r=64,s=32;
    pp.tauA=r*pp.tauB;
    pp.wab=-s*pp.wba;
    UpdateParams(&pp);
    std::vector<Peaks_fft> periods=getPeriod(alphaGC,xGC,0,pp);   
  }
  
  if (0){//Testing the FFT by a periodic signal
    Experiment expe;
    Event point;  
    int NumPoint=1<<17,n=NumPoint;//printf("NumPoint=%d\n",NumPoint);exit(0);//Pour FFT.
    double tmax=1<<15;
    double sample_rate=(float)(tmax/(double)NumPoint);//kHz
    printf("sample_rate=%g, delta_f=%g\n",sample_rate, 1.0/tmax);

    FILE *fp=fopen("signal2","w");
    double period1=10,period2=23,period3=39,period4=14.5; //ms
    double tau1=20,tau2=130,tau3=50,tau4=100;//ms
    for (double t=0;t<tmax;t+=sample_rate){
      point.t=t;
      point.Amp=10*cos(2*M_PI*t/period1)*exp(-t/tau1)+7*cos(2*M_PI*t/period2)*exp(-t/tau2)-10*cos(2*M_PI*t/period3)*exp(-t/tau3)-3*cos(2*M_PI*t/period4)*exp(-t/tau4);
      //point.Amp=2*cos(2*M_PI*t/period1)*exp(-t/tau1);
      fprintf(fp,"%g\t%g\n",point.t,point.Amp);
      expe.data.push_back(point);
    }
    fclose(fp);
    int n_peaks=5;
    char PowerSpectrum[1000]="FFT_Test_signal2";
    double max_range=0;
    std::vector<Peaks_fft> periods=getPeriod(expe,sample_rate,&n_peaks,PowerSpectrum,&max_range);    printf("\t\tThe %d first peaks in the power spectrum are:\n",n_peaks);
    for (int k=0;k<n_peaks;k++) printf("\t\t\t(peak=%g Hz=>%g ms, Amp=%g, width=%g Hz=>%g ms)\n",1000*periods[k].f,1.0/periods[k].f,periods[k].Amp,1000*periods[k].width,1.0/(dpi*periods[k].width));
  }

  if (1){//Tests the Stats method by generating a double Gaussian: one corresponding to ON the other to OFF 
    printf("Tests the Stats method by generating a double Gaussian: one corresponding to ON the other to OFF\n");
    Histo histo;
    double mON=20.0,sigma2ON=2.0,mOFF=-15,sigma2OFF=2,dx=1;
    srand48(0);
   Cell_id_type cell;
   char type[3];

    for (int i=0;i<100000;i++){
      if (drand48()>=0.5){
	cell.second="ON";
	cell.first=(int)(Gauss(mON,sigma2ON)/dx)*dx;
      }
      else{
	cell.second="OFF";
	cell.first=(int)(Gauss(mOFF,sigma2OFF)/dx)*dx;
      }
      Update_Histo(cell,&histo);
    }
    char chaine_hist[1000],Name[1000],GnuName[1000],PlotName[1000],label[1000],unit[100],mode[100];
    sprintf(Name,"Histogram_Test");
    sprintf(chaine_hist,"Histogram_Test");;
    sprintf(GnuName,"%s.gnu",chaine_hist);
    sprintf(PlotName,"%s.png",Name);
    sprintf(label,"Test");//printf("label=%s\n",labels[0]);
    sprintf(unit,"");//printf("unit=%s\n",unit[0]);
    sprintf(mode,"");//printf("mode=%s\n",mode[0]);
    std::string Gene="";
    PlotStats(histo,chaine_hist,Name,GnuName,PlotName,label,Gene,"",unit,mode,"");
  }//End of tests the Stats method
  
  return EXIT_SUCCESS;
}
