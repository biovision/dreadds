/*** Gnuplot routines **/

#include "Plot.h"
#include "Utils.h"
#include <string.h>

//Plot the experimental traces only (temporal and power spectrum)
//chaineExp contains the experimental trace. The time length of the plot is t_RF. It can differ from the sampling time in the FFT (Power spectrum defined by p.tmax). ExpCond contains the experimental condition (CTL or CNO). The gnuplot commands are copied in the file GnuName and 2 png files. Title contains the title to give to the figure. Cell_Type contains the type of the figure ON, or OFF. Used to display the legend properly. 
//chaineFFT is the file containing the spectrum; periods contains the periods. The gnuplot commands are copied in the file GnuName and the png  are generated. max_range is the maximal frequency range i.e. the maximal value where the amplitude of the normalized spectrum is larger than zero_PS.
void plot_Experimental_Traces(char* chaineExp,string ExpCond,char* chaineFFT,char* chaineFFT_Th,std::vector<Peaks_fft> periods,params p,string GnuName,char* PlotName,double max_range,double t_RF,string Cell_Type){

  printf("\n-------------------\n\tPlot a summary panel of network response \n\n-------------------\n");
  printf("\t\tGnuplot file will be stored in %s\n",GnuName.c_str());
  char TempTrace[1000]; sprintf(TempTrace,"%s_Temp.png",PlotName);
  char PowerTrace[1000]; sprintf(PowerTrace,"%s_FFT.png",PlotName);
  printf("\t\tpng files will be stored in %s (Temporal trace) and %s (Power Spectrum) \n",TempTrace,PowerTrace);

  std::string str = "";
  char chaine[1000];
  std::string cote="'";//Used in gnuplot to define the symbol '
  std::ofstream cout;//ofstream used to handle strings
  cout.open(GnuName);

  cout<<"#Plot experimental response\n\n";
  cout<<"\n#General settings\n";
  cout << "reset\n";
  str="load" +cote+"../../jet.pal"+cote+"\n";cout<<str;
  //cout << "set key right outside\n";
  cout << "set sample 10000\n";
  cout << "unset arrow\nunset label\nunset object\n";
  cout << "set size square 1,1\n";
  cout<<"\n#Line styles \n";
  cout << "set style line 1 lt rgb " + cote+"black" + cote + "lw 2.1\n";
  cout << "set style line 2 lt rgb " + cote+"red" + cote +" dt "+cote+"-"+cote+ " lw 3\n";
  cout << "set style line 3 lt rgb " + cote+"blue" + cote + " lw 3\n";
  cout << "set style line 4 lt rgb " + cote+"purple" + cote + "  lw 3\n";
  cout << "set style line 5 lt rgb " + cote+"green" + cote + "  lw 3\n";
  cout << "set style line 6 lt rgb " + cote+"orange" + cote +" dt "+cote+"-"+cote+ " lw 3\n";

  cout<<"\n#Experimental trace\n";
  cout<<"\n#Initializing variables\n";
  sprintf(chaine,"N=%d\n",p.N);str=chaine;cout << str;
  cout<<"ic=N/2\n";
  sprintf(chaine,"dx=%f\n",p.dx);str=chaine;cout << str;
  cout<<"xc=ic*dx\n";

  sprintf(chaine,"tmin=0\n");str=chaine;cout << str;
  sprintf(chaine,"tmax=%f\n",t_RF);str=chaine;cout << str;
  cout <<"xmin=0*dx\n";  cout <<"xmax=N*dx\n";
  cout << "set xlabel offset 0,0\n";
  cout << "set ylabel offset 0,0\n";
  cout << "set zlabel offset 0,0\n";


  //Power spectrum 
  cout<<"\n\n#Drawing the power spectrum of a GCell\n\n";
  cout<<"\n#General settings\n";
  cout << "reset\nunset label\nunset arrow\n";
  //str="load" +cote+"../../jet.pal"+cote+"\n";cout<<str;
  cout << "set key right outside\n";
  cout << "set sample 10000\n";
  
  cout<<"\n#Axes\n";
  cout << "set key top\n";
  cout << "set xlabel 'f (Hz)'\n";
  cout << "set ylabel 'P(f)'\n";
  
  sprintf(chaine,"fmax=%g\n",max_range);str=chaine;cout << str;
  cout << "fmin=-1\n";
  //cout << "set xrange [-fmax:fmax]\n";
  //cout << "set xtics -fmax,int(2*fmax/5.0),fmax\n";
  cout << "set xrange [0:fmax]\n";
  cout << "set xtics 0,int(fmax/5.0),fmax\n";
  //cout << "set xtics 40\n";
  sprintf(chaine,"zero=%g\n",zero_PS);str=chaine;cout << str;
  cout << "set yrange [zero:1.1]\n";
  //cout << "set size 0.45,0.5\n";
  //cout << "set logscale y\n";
  
  //Peaks
  cout<<"\n#Peaks\n";
  char aux[1000];
  for (int n=0;n<periods.size();n++){//Loop on peaks
    sprintf(chaine,"f%d=%g\n",n,1000*periods[n].f);str=chaine;cout << str;
    sprintf(chaine,"A%d=%g\n",n,periods[n].Amp);str=chaine;cout << str;
    //cout<<"\n#Vertical arrow and label\n";
    sprintf(chaine,"set arrow from f%d,zero to f%d,A%d size 2,45\n",n,n,n);str=chaine;cout << str;
    if (periods[n].f>0){
      sprintf(chaine,"{/Symbol t}_r=%g ms ",1.0/periods[n].f);
      sprintf(aux,"at 1.1*f%d,A%d",n,n);
      str="set label "+cote+chaine+cote+aux+"\n";cout << str;
    }
    cout<<"\n \t#Peak width arrow and label\n";
    if (periods[n].width>0){
      sprintf(chaine,"w%d=%g\n",n,1000*periods[n].width);str=chaine;cout << str;
      sprintf(chaine,"set arrow heads from f%d,A%d/sqrt(2.0) to f%d+w%d,A%d/sqrt(2.0)  size 2,45 lc ",n,n,n,n,n,n);str=chaine+cote+"red"+cote+"\n";cout << str;
      sprintf(chaine,"{/Symbol t}_i=%g ms",1.0/periods[n].width);
      sprintf(aux," at 1.3*(f%d+w%d),1.1*A%d/sqrt(2.0)",n,n,n);
      str="set label "+cote+chaine+cote+aux+"\n";cout << str;
    }
  }//End of loop on peaks
  
  str="\n\n#Setting output\nset terminal pngcairo  enhanced font "+cote+"Verdana,8"+cote+"\n";cout<<str;
  str="set output "+cote+PowerTrace+cote+"\n";cout<<str;
  
  str="\nplot "+cote+chaineFFT+cote+" u ($1*1000):2 w lp  ls 6 t "+cote+cote+"\n";cout<<str;

  cout<<"\n\n#******** Temporal RFs *********\n";
  //cout << "set title "+cote+ExpCond.c_str()+cote+"\n"; 
  
  if (Cell_Type=="OFF") cout << "set key bottom\n";
  cout<<"unset label\n";
  cout<<"unset arrow\n";
  cout<<"\n#Positioning arrows of time scales\n";
  if (periods[0].f>0) {
    cout<<"tr=1000.0/f0\n";
    sprintf(chaine,"set arrow heads from 0,0 to tr,0  size 20,20 lc ");str=chaine+cote+"black"+cote+"\n";cout << str;
    sprintf(chaine,"{/Symbol t}_r=%g ms",1.0/periods[0].f);
    sprintf(aux," at tr/2.0,-0.03");
    str="set label "+cote+chaine+cote+aux+"\n";cout << str;
  }

  cout<<"\n#x axis\n";
  cout << "set xlabel 't (ms)'\n";
  cout <<"set xrange [tmin:tmax]\n";
  cout << "set xtics int((tmax-tmin)/5)\n";
  //cout << "set xtics auto\n";
  cout << "\nset format x"+cote+"%2.1f"+cote+"\n";
  
  cout<<"\n#y axis\n";
  cout << "set autoscale y\n";
  //cout << "set yrange [-0.3:0.8]\n";
  cout << "set ylabel ''\n";

  str="\n\n#Setting output\nset terminal pngcairo  enhanced font "+cote+"Verdana,8"+cote+"\n";cout<<str;
  str="set output "+cote+TempTrace+cote+"\n";cout<<str;
  
  cout<<"\n\t#Time plot of Cells located at xc\n";
  str="\nplot "+cote+"../../"+chaineExp+cote+" u 1:2  w lp ls 6 t  "+cote+"Exp"+cote+"\n";cout<<str;
  
  cout.close();
}


//Plot a summary panel which encompasses:
//1) The time response of BCs, ACs, OPL
//2) The time response of RGCs
//3) The spatio-temporal RF
//4) The power spectrum experimental (chaineFFT) and theoretical (chaineFFT_Th). If chaineFFT_Th="" the theoretical power spectrum is not computed.
//chaineT contains the temporal trace of the model, chaineS the spatiotemporal, chaineTh the theoretical (analytic) one, chaineExp contains the experimental trace. If this chain is equal to "void", then it is not plotted.  The time length of the plot is t_RF. It can differ from the sampling time in the FFT (Power spectrum defined by p.tmax). ExpCond contains the experimental condition (CTL or CNO). The gnuplot commands are copied in the file GnuName and 2 png file Space_RF_PlotName  and Time_RF_PlotName are generated. Title contains the title to give to the figure. Cell_Type contains the type of the figure ON, or OFF. Used to display the legend properly. 
//chaineFFT is the file containing the spectrum; periods contains the periods. The gnuplot commands are copied in the file GnuName and the png  PlotName is generated. max_range is the maximal frequency range i.e. the maximal value where the amplitude of the normalized spectrum is larger than zero_PS.
void plot_Summary_Panel(char* chaineT,char* chaineS,char* chaineTh,char* chaineExp,string ExpCond,char* chaineFFT,char* chaineFFT_Th,std::vector<Peaks_fft> periods,params p,string GnuName,char* PlotName,string Title,double max_range,double t_RF,string Cell_Type){

  printf("\n-------------------\n\tPlot a summary panel of network response \n\n-------------------\n");
  printf("\t\tGnuplot file will be stored in %s\n",GnuName.c_str());
  printf("\t\tpng file will be stored in %s\n",PlotName);

  std::string str = "";
  char chaine[1000];
  std::string cote="'";//Used in gnuplot to define the symbol '
  std::ofstream cout;//ofstream used to handle strings
  cout.open(GnuName);

  cout<<"#Plot a summary panel of network response\n\n";
  cout<<"\n#General settings\n";
  cout << "reset\n";
  str="load" +cote+"../../jet.pal"+cote+"\n";cout<<str;
  //cout << "set key right outside\n";
  cout << "set sample 10000\n";
  cout << "unset arrow\nunset label\nunset object\n";
  cout << "set size square 1,1\n";
  cout<<"\n#Line styles \n";
  cout << "set style line 1 lt rgb " + cote+"black" + cote + "lw 2.1\n";
  cout << "set style line 2 lt rgb " + cote+"red" + cote +" dt "+cote+"-"+cote+ " lw 3\n";
  cout << "set style line 3 lt rgb " + cote+"blue" + cote + " lw 3\n";
  cout << "set style line 4 lt rgb " + cote+"purple" + cote + "  lw 3\n";
  cout << "set style line 5 lt rgb " + cote+"green" + cote + "  lw 3\n";
  cout << "set style line 6 lt rgb " + cote+"orange" + cote +" dt "+cote+"-"+cote+ " lw 3\n";

  str="\n\n#Setting output\nset terminal pngcairo  enhanced font "+cote+"Verdana,8"+cote+"\n";cout<<str;
  str="set output "+cote+PlotName+cote+"\n";cout<<str;

  //cout << "set autoscale y\nset xrange [tmin:tmax]\n";
  if (strcmp(Title.c_str(),"")){//There is a title
    str="set multiplot layout 2,2 columns title "+cote+Title+cote+"font "+cote+"Verdana,12"+cote+"\n";cout<<str;
  }
  else
    cout<<"set multiplot layout 2,2 columns\n";

  cout<<"\n#Initializing variables\n";
  sprintf(chaine,"N=%d\n",p.N);str=chaine;cout << str;
  cout<<"ic=N/2\n";
  sprintf(chaine,"dx=%f\n",p.dx);str=chaine;cout << str;
  cout<<"xc=ic*dx\n";

  sprintf(chaine,"tmin=0\n");str=chaine;cout << str;
  sprintf(chaine,"tmax=%f\n",t_RF);str=chaine;cout << str;
  cout <<"xmin=0*dx\n";  cout <<"xmax=N*dx\n";
  cout << "set xlabel offset 0,0\n";
  cout << "set ylabel offset 0,0\n";
  cout << "set zlabel offset 0,0\n";

  cout<<"\n\n#******** Temporal RFs *********\n";
  //cout << "set title "+cote+ExpCond.c_str()+cote+"\n"; 

  if (Cell_Type=="OFF") cout << "set key bottom\n";
  cout<<"\n#x axis\n";
  cout << "set xlabel 't (ms)'\n";
  cout <<"set xrange [tmin:tmax]\n";
  cout << "set xtics int((tmax-tmin)/5)\n";
  //cout << "set xtics auto\n";
  cout << "\nset format x"+cote+"%2.1f"+cote+"\n";

  cout<<"\n#y axis\n";
  cout << "set autoscale y\n";
  //cout << "set yrange [-0.3:0.8]\n";
  cout << "set ylabel ''\n";

  cout<<"\n\t#step 1: Time plot of Cells located at xc\n";
  str="\nplot "+cote+"../../"+chaineT+cote+" u 1:2 w l ls 1 t "+cote+"OPL"+cote+","+cote+"../../"+chaineT+cote+" u 1:3  w l ls 2 t "+cote+"BC"+cote+","+cote+"../../"+chaineT+cote+" u 1:4 w l ls 3 t "+cote+"AC"+cote+
"\n";cout<<str;

  cout<<"\n\n\t#step 2: Comparison with theoretical RF\n";
  //cout<<"set autoscale y\n";
  if (strcmp(chaineExp,"void")==0){
    // printf("The experimental trace is void\n");
    str="\nplot "+cote+"../../"+chaineT+cote+" u 1:5 w l ls 5 t "+cote+"Sim"+cote+","+cote+"../../"+chaineTh+cote+" u (abs($1-xc)<0.01 ? ($2):1/0):($3) w l ls 1 t "+cote+"Th"+cote+"\n";cout<<str;
  }
  else{
    str="\nplot "+cote+"../../"+chaineT+cote+" u 1:5 w l ls 5 t "+cote+"Sim"+cote+","+cote+"../../"+chaineTh+cote+" u (abs($1-xc)<0.01 ? ($2):1/0):($3) w l ls 1 t "+cote+"Th"+cote+","+cote+"../../"+chaineExp+cote+" u 1:2  w lp ls 6 t  "+cote+"Exp"+cote+"\n";cout<<str;
  }

  cout<<"\n\n#******** Spatial RFs *********\n";
  cout<<"set ylabel 'x (mm)'\n";
  cout<<"set xtics int((tmax-tmin)/5)\n";
  cout<<"set xlabel 't (ms)'\n";
  cout<<"set xrange [tmin:tmax]\n";
  cout<<"set cblabel 'V '\n";
  //cout<<"set cbrange [-0.3:0.8]\n";
  cout<<"set autoscale cb\n";
  cout<<"#set colorbox horiz user origin .1,.1 size .8,.04\n";
  cout<<"set style line 2604 linetype -1 linewidth .4\n";
  cout << "set cntrlabel font  "+cote+",7"+cote+"\n"; 
  cout<<"set view 60,62,1\n";
  cout<<"set pm3d map\n";
  
  //cout<<"set size square 1,1\n";
  cout<<"set autoscale y\n";

   //cout << "set title "+cote+""+cote+"\n"; 
  str="\nplot "+cote+"../../"+chaineTh+cote+" u 2:1:3 w p lw 2 palette t "+cote+""+cote+"\n";cout<<str;

  //Power spectrum 
  cout<<"\n\n#Drawing the power spectrum of a GCell\n\n";
  cout<<"\n#General settings\n";
  //cout << "reset\nunset label\nunset arrow\n";
  //str="load" +cote+"../../jet.pal"+cote+"\n";cout<<str;
  //cout << "set key right outside\n";
  //cout << "set sample 10000\n";

  cout<<"\n#Axes\n";
  cout << "set key top\n";
  cout << "set xlabel 'f (Hz)'\n";
  cout << "set ylabel 'P(f)'\n";
  
  sprintf(chaine,"fmax=%g\n",max_range);str=chaine;cout << str;
  cout << "fmin=-1\n";
  //cout << "set xrange [-fmax:fmax]\n";
  //cout << "set xtics -fmax,int(2*fmax/5.0),fmax\n";
  cout << "set xrange [0:fmax]\n";
  cout << "set xtics 0,int(fmax/5.0),fmax\n";
  //cout << "set xtics 40\n";
  sprintf(chaine,"zero=%g\n",zero_PS);str=chaine;cout << str;
  cout << "set yrange [zero:1.1]\n";
  cout << "set size 0.45,0.5\n";
  //cout << "set logscale y\n";
  
  //Peaks
  cout<<"\n#Peaks\n";
  char aux[1000];
  for (int n=0;n<periods.size();n++){//Loop on peaks
    sprintf(chaine,"f%d=%g\n",n,1000*periods[n].f);str=chaine;cout << str;
    sprintf(chaine,"A%d=%g\n",n,periods[n].Amp);str=chaine;cout << str;
    //cout<<"\n#Vertical arrow and label\n";
    //sprintf(chaine,"set arrow from f%d,zero to f%d,A%d size 2,45\n",n,n,n);str=chaine;cout << str;
    if (periods[n].f>0){
      sprintf(chaine,"{/Symbol t}_r=%g ms ",1.0/periods[n].f);
      sprintf(aux,"at 1.1*f%d,A%d",n,n);
     // str="set label "+cote+chaine+cote+aux+"\n";cout << str;
    }
    //cout<<"\n \t#Peak width arrow and label\n";
    if (periods[n].width>0){
      sprintf(chaine,"w%d=%g\n",n,1000*periods[n].width);str=chaine;cout << str;
      //sprintf(chaine,"set arrow heads from f%d,A%d/sqrt(2.0) to f%d+w%d,A%d/sqrt(2.0)  size 2,45 lc ",n,n,n,n,n,n);str=chaine+cote+"red"+cote+"\n";cout << str;
      sprintf(chaine,"{/Symbol t}_i=%g ms",1.0/periods[n].width);
      sprintf(aux," at 1.3*(f%d+w%d),1.1*A%d/sqrt(2.0)",n,n,n);
      //str="set label "+cote+chaine+cote+aux+"\n";cout << str;
    }
  }//End of loop on peaks

  if (chaineFFT_Th==""){//We don't want to plot the theoretical spectrum
    str="\nplot "+cote+chaineFFT+cote+" u ($1*1000):2 w lp  ls 6 t "+cote+cote+"\n";cout<<str;
  }
  else{//We want to plot the theoretical spectrum in addition to the experimental one
    str="\nplot "+cote+chaineFFT_Th+cote+" u 1:2 w l ls 1  t "+cote+"Th"+cote+","+cote+chaineFFT+cote+" u ($1*1000):2 w lp ls 6  t "+cote+"Exp"+cote+"\n";cout<<str;
  }
  cout<<"\nunset multiplot\n";
  cout.close();
}

//Plot the power spectrum and places the peaks in frequency
//chaineFFT is the file containing the spectrum; periods contains the periods. The gnuplot commands are copied in the file GnuName and the png  PlotName is generated. Title contains the title to give to the figure. max_range is the maximal frequency range i.e. the maximal value where the amplitude of the normalized spectrum is larger than zero_PS.
void plot_Power_Spectrum(char* chaineFFT,std::vector<Peaks_fft> periods,string GnuName,char* PlotName,string Title,double max_range){

  printf("\n\t\t\t-------------------\n\t\t\tDrawing the power spectrum of a GCell\n\t\t\t-------------------\n\n");
  printf("\t\tGnuplot file will be stored in %s\n",GnuName.c_str());
  printf("\t\tpng file will be stored in %s\n",PlotName);
  
  std::string str = "";
  char chaine[1000];
  std::string cote="'";//Used in gnuplot to define the symbol '
  std::ofstream cout;//ofstream used to handle strings
  cout.open(GnuName);

  cout<<"#Drawing the power spectrum of a GCell\n\n";
  cout<<"\n#General settings\n";
  cout << "reset\nunset label\nunset arrow\n";
  str="load" +cote+"../../jet.pal"+cote+"\n";cout<<str;
  //cout << "set key right outside\n";
  cout << "set sample 10000\n";

  cout<<"\n#Axes\n";
  cout << "set xlabel 'f (Hz)'\n";
  cout << "set ylabel 'P(f)'\n";

  /* double fmax=0;
  for (int n=0;n<periods.size();n++){
    fmax=max(fmax,1000*periods[n].t);
    }*/

  sprintf(chaine,"fmax=%g\n",max_range);str=chaine;cout << str;
  cout << "fmin=-1\n";
  //cout << "set xrange [-fmax:fmax]\n";
  cout << "set xrange [0:fmax]\n";
  //cout << "set xtics fmin,int((fmax-fmin)/10.0),fmax\n";
  cout << "set xtics 5\n";
  sprintf(chaine,"zero=%g\n",zero_PS);str=chaine;cout << str;
  cout << "set yrange [zero:1.2]\n";
  //cout << "set logscale y\n";

  //Peaks
  cout<<"\n#Peaks\n";
  char aux[1000];
  for (int n=0;n<periods.size();n++){//Loop on peaks
    sprintf(chaine,"f%d=%g\n",n,1000*periods[n].f);str=chaine;cout << str;
    sprintf(chaine,"A%d=%g\n",n,periods[n].Amp);str=chaine;cout << str;
    sprintf(chaine,"w%d=%g\n",n,1000*periods[n].width);str=chaine;cout << str;
    //cout<<"\n#Vertical arrow and label\n";
    sprintf(chaine,"set arrow from f%d,zero to f%d,A%d size 2,45\n",n,n,n);str=chaine;cout << str;
    if (periods[n].f>0){
      sprintf(chaine,"{/Symbol t}_r=%g ms ",1.0/periods[n].f);
      sprintf(aux,"at 1.1*f%d,1.05*A%d",n,n);
      str="set label "+cote+chaine+cote+aux+"\n";cout << str;
    }
    //cout<<"\n \t#Peak width arrow and label\n";
    if (periods[n].width>0){
      sprintf(chaine,"set arrow heads from f%d,A%d/sqrt(2.0) to f%d+w%d,A%d/sqrt(2.0)  size 2,45 lc ",n,n,n,n,n,n);str=chaine+cote+"red"+cote+"\n";cout << str;
      sprintf(chaine,"{/Symbol t}_i=%g ms",1.0/periods[n].width);
      sprintf(aux," at 1.1*(f%d+w%d),1.05*A%d/sqrt(2.0)",n,n,n);
      str="set label "+cote+chaine+cote+aux+"\n";cout << str;
    }
  }//End of loop on peaks


  str="\nset terminal pngcairo  enhanced font "+cote+"Verdana,8"+cote+"\n";cout<<str;
  str="set output "+cote+PlotName+cote+"\n";cout<<str;

  str="\nplot "+cote+chaineFFT+cote+" u ($1*1000):2 w lp  t "+cote+Title+cote+"\n";cout<<str;
  cout.close();

}

//Plot the 3D net activity - chainBCells, chainACells, chainGCells  - and the RF. chaineT contains the temporal trace of the model, chaineS the spatiotemporal, chaineTh the theoretical (analytic) one, chaineExp contains the experimental trace. If this chain is equal to "void", then it is not plotted. The time length of the plot is t_RF. ExpCond contains the experimental condition (CTL or CNO). One generates the time plots in one png file and the space plots in another png file. The gnuplot commands are copied in the file GnuName and a png file PlotName is generated. Title contains the title to give to the figure.
void plot_RFs_2Figs(char* chaineT,char* chaineS,char* chaineTh,char* chaineExp,string ExpCond,params p,string GnuName,char* PlotName,string Title,double t_RF){

  printf("\n-------------------\n\tDrawing the RF of a GCell\n\n-------------------\n");
  printf("\t\tGnuplot file will be stored in %s\n",GnuName.c_str());
  printf("\t\tpng file will be stored in %s\n",PlotName);
  
  std::string str = "";
  char chaine[1000];
  std::string cote="'";//Used in gnuplot to define the symbol '
  std::ofstream cout;//ofstream used to handle strings
  cout.open(GnuName);

  cout<<"#Drawing the RF of a GCell\n\n";
  cout<<"\n#General settings\n";
  cout << "reset\n";
  str="load" +cote+"../../jet.pal"+cote+"\n";cout<<str;
  cout << "set key right outside\n";
  cout << "set sample 10000\n";
  cout << "unset arrow\nunset label\nunset object\n";
  cout << "set size 1,1\n";
  //cout << "set autoscale y\nset xrange [tmin:tmax]\n";

  cout<<"\n#Line styles \n";
  cout << "set style line 1 lt rgb " + cote+"black" + cote + " lw 2.1\n";
  cout << "set style line 2 lt rgb " + cote+"red" + cote + " dashtype 3 lw 5\n";
  cout << "set style line 3 lt rgb " + cote+"blue" + cote + " dashtype 2 lw 4\n";
  cout << "set style line 4 lt rgb " + cote+"purple" + cote + " dashtype 4 lw 5\n";
  cout << "set style line 5 lt rgb " + cote+"green" + cote + " dashtype 5 lw 4\n";
  cout << "set style line 6 lt rgb " + cote+"orange" + cote + " dashtype 2 lw 4\n";

  cout<<"\n#Initializing variables\n";
  sprintf(chaine,"N=%d\n",p.N);str=chaine;cout << str;
  cout<<"ic=N/2\n";
  sprintf(chaine,"dx=%f\n",p.dx);str=chaine;cout << str;
  cout<<"xc=ic*dx\n";

  sprintf(chaine,"tmin=0\n");str=chaine;cout << str;
  sprintf(chaine,"tmax=%f\n",t_RF);str=chaine;cout << str;
  //sprintf(chaine,"tmax=%f\n",p.tmax);str=chaine;cout << str;
  cout <<"xmin=0*dx\n";  cout <<"xmax=N*dx\n";
  cout << "set xlabel offset 0,0\n";
  cout << "set ylabel offset 0,0\n";
  cout << "set zlabel offset 0,0\n";

  cout<<"\n\n#******** Temporal RFs *********\n";
  cout << "set title "+cote+ExpCond.c_str()+cote+"\n"; 

  cout<<"\n#x axis\n";
  cout << "set xlabel 't (ms)'\n";
  cout <<"set xrange [tmin:tmax]\n";
   cout << "set xtics tmax/10\n";

  cout<<"\n#y axis\n";
  cout << "set autoscale y\n";
  //cout << "set yrange [-0.3:0.8]\n";
  cout << "set ylabel 'V ''\n";

  str="\nset terminal pngcairo  enhanced font "+cote+"Verdana,8"+cote+"\n";cout<<str;
  str="set output "+cote+"TimeRF_"+PlotName+cote+"\n";cout<<str;

  if (strcmp(Title.c_str(),"")){//There is a title
    str="set multiplot layout 2,1 title "+cote+Title+cote+"font "+cote+"Verdana,12"+cote+"\n";cout<<str;
  }
  else
    cout<<"set multiplot layout 2,1\n";

  cout<<"\n\t#step 1: Time plot of Cells located at xc\n";
  str="\nplot "+cote+"../../"+chaineT+cote+" u 1:2 w l ls 1 t "+cote+"OPL"+cote+","+cote+"../../"+chaineT+cote+" u 1:3  w l ls 2 t "+cote+"BC"+cote+","+cote+"../../"+chaineT+cote+" u 1:4 w l ls 3 t "+cote+"AC"+cote+
"\n";cout<<str;

  cout<<"\n\n\t#step 2: Comparison with theoretical RF\n";
  //cout<<"set autoscale y\n";
  if (strcmp(chaineExp,"void")==0){
    // printf("The experimental trace is void\n");
    str="\nplot "+cote+"../../"+chaineT+cote+" u 1:5 w l ls 5 t "+cote+"Sim"+cote+","+cote+"../../"+chaineTh+cote+" u (abs($1-xc)<0.01 ? ($2):1/0):($3) w l ls 1 t "+cote+"Th"+cote+"\n";cout<<str;
  }
  else{
    str="\nplot "+cote+"../../"+chaineT+cote+" u 1:5 w l ls 5 t "+cote+"Sim"+cote+","+cote+"../../"+chaineTh+cote+" u (abs($1-xc)<0.01 ? ($2):1/0):($3) w l ls 1 t "+cote+"Th"+cote+","+cote+"../../"+chaineExp+cote+" u 1:2  w p ls 6 t  "+cote+"Exp"+cote+"\n";cout<<str;
  }
  cout<<"\nunset multiplot\n";

  cout<<"set ylabel 'x (mm)'\n";
  cout<<"set xtics int((tmax-tmin)/5)\n";
  cout<<"set xlabel 't (ms)'\n";
  cout<<"set xrange [tmin:tmax]\n";
  cout<<"set cblabel 'V '\n";
  //cout<<"set cbrange [-0.3:0.8]\n";
  cout<<"set autoscale cb\n";
  cout<<"#set colorbox horiz user origin .1,.1 size .8,.04\n";
  cout<<"set style line 2604 linetype -1 linewidth .4\n";
  cout << "set cntrlabel font  "+cote+",7"+cote+"\n"; 
  cout<<"set view 60,62,1\n";
  cout<<"set pm3d map\n";
  
  cout<<"\n\n#******** Spatial RFs *********\n";
  cout<<"set size square\n";
  cout<<"set autoscale y\n";
  str="\nset terminal pngcairo  enhanced font "+cote+"Verdana,8"+cote+"\n";cout<<str;
  str="set output "+cote+"SpaceRF_"+PlotName+cote+"\n";cout<<str;

  if (strcmp(Title.c_str(),"")){//There is a title
    str="set multiplot layout 1,2 title "+cote+Title+cote+"font "+cote+"Verdana,12"+cote+"\n";cout<<str;
  }
  else
    cout<<"set multiplot layout 1,2\n";

  cout << "set title "+cote+"Sim"+cote+"\n"; 
  str="\nplot "+cote+"../../"+chaineS+cote+" u 2:1:3 w p lw 2 palette t "+cote+""+cote+"\n";cout<<str;
  cout << "set title "+cote+"Th"+cote+"\n"; 
  str="\nplot "+cote+"../../"+chaineTh+cote+" u 2:1:3 w p lw 2 palette t "+cote+""+cote+"\n";cout<<str;
  
  cout<<"unset multiplot\n";
  cout.close();
}

void plot_Net_RF(char* RF_Time,char* RF_Full,char* chainBCells,char* chainACells,char* chainGCells,char* Plotted_RF,params p,char* GnuName,char* PlotName){//Plot the 3D net activity - files chainBCells, chainACells, chainGCells  - and the RF - file RF. BCells, ACells and RGCells are plotted in separated plots (old version). The gnuplot commands are copied in the file GnuName and a png file PlotName is generated. 

  printf("\n-------------------\n\tGenerating the dynamics views\n\n-------------------\n");
  printf("\t\tGnuplot file will be stored in %s\n",GnuName);
  printf("\t\tPng file will be stored in %s\n",PlotName);
  
  std::string str = "";
  char chaine[1000];
  std::string cote="'";//Used in gnuplot to define the symbol '
  std::ofstream cout;//ofstream used to handle strings
  cout.open(GnuName);

  cout<<"#Computes the RF of GCells in the different regions on the stability map\n\n";
  cout<<"\n#General settings\n";
  cout << "reset\n";
  str="load" +cote+"jet.pal"+cote+"\n";cout<<str;
  cout << "set sample 10000\n";
  cout << "set key right inside\n";
  cout << "set size square 1,1\n";
  cout << "set xlabel offset 0,0\n";
  cout << "set ylabel offset 0,0\n";
  cout << "set xtics 0.3 scale 1\n";
   cout << "set zlabel"+cote+" V (mV)"+cote+"\n"; 
   cout <<"set autoscale z\n";
   //cout << "set cblabel"+cote+"V (mV)"+cote+"\n"; 
   //cout <<"set cbtics 2\n";
   cout <<"set autoscale cb\n";
   cout << "set pm3d\n";

  cout << "\nset terminal pngcairo  enhanced font 'Verdana,8'\n";
  str="set output"+cote+PlotName+cote+"\n\n";cout<<str;

  //cout <<"set style line 2604 linetype -1 linewidth .4\n";
  cout << "set style line 1 lt rgb " + cote+"black" + cote + " lw 2.1\n";
  cout << "set style line 2 lt rgb " + cote+"red" + cote + " dashtype 3 lw 5\n";
  cout << "set style line 3 lt rgb " + cote+"blue" + cote + " dashtype 2 lw 4\n";
  cout << "set style line 4 lt rgb " + cote+"purple" + cote + " dashtype 4 lw 5\n";
  cout << "set style line 5 lt rgb " + cote+"green" + cote + " dashtype 5 lw 4\n";

  cout<<"\n#Initializing variables\n\n";
  sprintf(chaine,"N=%d\n",p.N);str=chaine;cout << str;
  sprintf(chaine,"\ndx=%f\n",p.dx);str=chaine;cout << str;
  sprintf(chaine,"tmin=0\n");str=chaine;cout << str;
  //sprintf(chaine,"tmin=%f\n",p.tmin);str=chaine;cout << str;
  sprintf(chaine,"tmax=%f\n",p.tmax);str=chaine;cout << str;

  cout <<"set xrange [tmin:tmax]\n";
  cout << "set xlabel 't (ms)'\n";
  cout << "set xtics tmax/5\n";
  //cout <<"\nxmin=10*dx\n";  cout <<"xmax=(N-10)*dx\n";
  cout <<"\nxmin=0*dx\n";  cout <<"xmax=N*dx\n";
  cout <<"set yrange [xmin:xmax]\n";
  cout << "set ylabel 'x (mm)'\n";
  cout << "set ytics xmax*10/N\n";
  //cout <<"set cbrange [Vmin:Vmax] \n";
  // cout <<"set cbtics (Vmax-Vmin)/10\n";
  //cout <<"set zrange [Vmin:Vmax] \n";
   //cout <<"set ztics (Vmax-Vmin)/10\n";


  char title[1000];sprintf(title,"w=%3.2f, {/Symbol t}_A=%3.2f",p.wba*1000,p.tauA);//printf("Title=%s\n",title);
  cout << "\nset multiplot layout 2,2 title "+cote+title+cote+" font 'Verdana,12'\n\n";
  cout << str;

  cout<<"\n#Plotting  Network activity\n\n";
  //str="set title"+cote+"Net"+cote+"\n";cout<<str;
  cout<<"set xlabel offset 0,-0.4\n\n";
  cout<<"set view 65,345,1,1\n";
  cout<<"set dgrid3d 50,50 gauss 0.1\n";
  cout<<"set hidden3d back offset 1 trianglepattern 3 undefined 1 altdiagonal bentover\n";
  //cout<<"set xtics offset 0,-0.2\n";
  cout<<"set xtics scale 3\n";
  //cout<<"set xlabel offset 14,3\n";
  cout<<"set ylabel offset 0,-0.5\n";

  //cout << "set colorbox horiz user origin .1,0.1 size .35,.02\n";
  //BCells
  cout<<"\n#BCells\n";
  str="set zlabel "+cote+"V_B"+cote+"\n";cout<<str;
  sprintf(chaine,"Vmin=%g\nVmax=%g\n",p.VBmin,p.VBmax);str=chaine;cout << str;
  cout <<"set cbrange [Vmin:Vmax] \n";
  // cout <<"set cbtics (Vmax-Vmin)/10\n";
  cout <<"set zrange [Vmin:Vmax] \n";
   //cout <<"set ztics (Vmax-Vmin)/10\n";
  str="splot "+cote+chainBCells+cote+" u 1:2:3 t "+cote+""+cote+" w l \n";cout<<str;

  //ACells
  cout<<"\n#ACells\n";
  str="set zlabel "+cote+" V_A"+cote+"\n";cout<<str;
  sprintf(chaine,"Vmin=%g\nVmax=%g\n",p.VAmin,p.VAmax);str=chaine;cout << str;
  cout <<"set cbrange [Vmin:Vmax] \n";
  // cout <<"set cbtics (Vmax-Vmin)/10\n";
  cout <<"set zrange [Vmin:Vmax] \n";
   //cout <<"set ztics (Vmax-Vmin)/10\n";
  str="splot "+cote+chainACells+cote+" u 1:2:3 t "+cote+""+cote+" w l \n";cout<<str;

  //GCells
  cout<<"\n#GCells\n";
  str="set zlabel "+cote+"V_G"+cote+"\n";cout<<str;
  sprintf(chaine,"Vmin=%g\nVmax=%g\n",p.VGmin,p.VGmax);str=chaine;cout << str;
  cout <<"set cbrange [Vmin:Vmax] \n";
  // cout <<"set cbtics (Vmax-Vmin)/10\n";
  cout <<"set zrange [Vmin:Vmax] \n";
   //cout <<"set ztics (Vmax-Vmin)/10\n";
  str="splot "+cote+chainGCells+cote+" u 1:2:3 t "+cote+""+cote+" w l \n";cout<<str;

  cout<<"\n#Plotting  RF\n\n";
   cout<<"set xlabel offset 0,0\n\n";
 //cout<<"set colorbox horiz user origin .6,0.1 size .35,.02\n";
  //cout<<"set xlabel offset 0,0\n";
  //cout<<"set ylabel offset 0,0\n";
  cout<<"set xtics scale 1\n";
  cout <<"set yrange [xmin:xmax]\n";
  //cout << "set autoscale cb\n";
  str="plot"+cote+Plotted_RF+cote+" u 2:1:3 t "+cote+""+cote+" w p lw 2 palette\n";cout<<str;
  cout<<"\nunset multiplot\n";

  cout.close();

  sprintf(chaine,"gnuplot %s\n",GnuName);
  //exit(system(str.c_str()));
  printf("\n\n\tExecuting %s",chaine);
  system(chaine);
  
  sprintf(chaine,"rm -f %s\n",RF_Time);
  printf("\tExecuting %s",chaine);
  system(chaine);
  
  sprintf(chaine,"rm -f %s\n",RF_Full);
  printf("\tExecuting %s",chaine);
  system(chaine);
  
  sprintf(chaine,"rm -f %s\n",chainBCells);
  printf("\t\tExecuting %s",chaine);
  system(chaine);
  
  sprintf(chaine,"rm -f %s\n",chainACells);
  printf("\t\tExecuting %s",chaine);
  system(chaine);

  sprintf(chaine,"rm -f %s\n",chainGCells);
  printf("\t\tExecuting %s",chaine);
  system(chaine);

  sprintf(chaine,"rm -f %s\n",Plotted_RF);
  printf("\tExecuting %s",chaine);
  system(chaine);

}

void plot_Mode_Evolution(int n, char* RF_Time,char* RF_Full,char* chainBCells,char* chainACells,char* chainGCells,char* Plotted_RF,params p,char* GnuName,char* PlotName,double rho_r,double rho_i,double pi_r,double pi_i){//Plot the 3D net activity when the initial state is prepared in the eigenmode n - files chainBCells, chainACells, chainGCells  - and the RF - file RF. The gnuplot commands are copied in the file GnuName and a png file PlotName is generated. 

  gsl_complex z=gsl_vector_complex_get(p.lambda_th,n);
  double zr=GSL_REAL(z),zi=GSL_IMAG(z),tr=(fabs(zr)>1E-4 ? 1/fabs(zr):0),ti=(fabs(zi)>1E-4 ? 1/fabs(zi):0);

  printf("\n-------------------\n\tGenerating the dynamics views of an eigenmode\n\n-------------------\n");
  printf("\t\tGnuplot file will be stored in %s\n",GnuName);
  printf("\t\tPng file will be stored in %s\n",PlotName);
  
  std::string str = "";
  char chaine[1000];
  std::string cote="'";//Used in gnuplot to define the symbol '
  std::ofstream cout;//ofstream used to handle strings
  cout.open(GnuName);

  cout<<"#Computes the RF of GCells in the different regions on the stability map\n\n";
cout<<"\n#General settings\n";
  cout << "reset\n";
   str="load" +cote+"jet.pal"+cote+"\n";cout<<str;
  cout << "set sample 10000\n";
  cout << "set key right inside\n";
  cout << "set size square 1,1\n";
  cout << "set xlabel offset 0,0\n";
  cout << "set ylabel offset 0,0\n";
  //cout << "set xtics 0.3 scale 1\n";
   cout << "set zlabel"+cote+" V (mV)"+cote+"\n"; 
   cout <<"set autoscale z\n";
   //cout << "set cblabel"+cote+"V (mV)"+cote+"\n"; 
   cout <<"set autoscale cb\n";
   cout << "set pm3d\n";

  cout << "\nset terminal pngcairo  enhanced font 'Verdana,8'\n";
  str="set output"+cote+PlotName+cote+"\n\n";cout<<str;

  //cout <<"set style line 2604 linetype -1 linewidth .4\n";
  cout << "set style line 1 lt rgb " + cote+"black" + cote + " lw 2.1\n";
  cout << "set style line 2 lt rgb " + cote+"red" + cote + " dashtype 3 lw 5\n";
  cout << "set style line 3 lt rgb " + cote+"blue" + cote + " dashtype 2 lw 4\n";
  cout << "set style line 4 lt rgb " + cote+"purple" + cote + " dashtype 4 lw 5\n";
  cout << "set style line 5 lt rgb " + cote+"green" + cote + " dashtype 5 lw 4\n";

  cout<<"\n#Initializing variables\n\n";
  sprintf(chaine,"N=%d\n",p.N);str=chaine;cout << str;
  sprintf(chaine,"\ndx=%f\n",p.dx);str=chaine;cout << str;
  sprintf(chaine,"tmin=-10\n");str=chaine;cout << str;
  //sprintf(chaine,"tmin=%f\n",p.tmin);str=chaine;cout << str;
  sprintf(chaine,"tmax=%f\n",p.tmax);str=chaine;cout << str;

  //cout <<"set xrange [tmin:tmax]\n";
  cout << "set xlabel 't (ms)'\n";
  cout << "set xtics tmax/5\n";
  //cout <<"\nxmin=10*dx\n";  cout <<"xmax=(N-10)*dx\n";
  cout <<"\nxmin=0*dx\n";  cout <<"xmax=N*dx\n";
  cout <<"set yrange [xmin:xmax]\n";
  cout << "set ylabel 'x (mm)'\n";
  cout << "set ytics xmax*10/N\n";


  char title[1000];sprintf(title,"w=%2.2g, {/Symbol t}_A=%2.2g, {/Symbol l}_r=%2.2g, {/Symbol l}_i=%2.2g, {/Symbol t}_r=%2.2g, {/Symbol t}_i=%2.2g,{/Symbol r}_r=%2.2g, {/Symbol r}_i=%2.2g,{/Symbol p}_r=%2.2g, {/Symbol p}_i=%2.2g",p.wba*1000,p.tauA,zr,zi,tr,ti,rho_r,rho_i,pi_r,pi_i);//printf("Title=%s\n",title);
  cout << "\nset multiplot layout 2,2 title "+cote+title+cote+" font 'Verdana,10'\n\n";
  cout << str;

  cout<<"\n#Plotting  Network activity\n\n";
  //str="set title"+cote+"Net"+cote+"\n";cout<<str;
  cout<<"set xlabel offset 0,-0.4\n\n";
  cout<<"set view 75,320,1,1\n";
  //cout<<"set dgrid3d 100,100 gauss 0.1\n";
  cout<<"set dgrid3d 4*N,4*N,16\n";
  cout<<"set hidden3d back offset 1 trianglepattern 3 undefined 1 altdiagonal bentover\n";
  //cout<<"set xtics offset 0,-0.2\n";
  cout<<"set xtics scale 3\n";
  //cout<<"set xlabel offset 14,3\n";
  cout<<"set ylabel offset 0,-0.5\n";

  //cout << "set colorbox horiz user origin .1,0.1 size .35,.02\n";
  //BCells
  cout<<"\n#BCells\n";
  str="set zlabel "+cote+"V_B"+cote+"\n";cout<<str;
  sprintf(chaine,"Vmin=%g\nVmax=%g\n",p.VBmin,p.VBmax);str=chaine;cout << str;
  /* cout <<"set cbrange [Vmin:Vmax] \n";
  cout << "tics_space=int((Vmax-Vmin)/5) \n";
  cout << "print tics_space\n";
  cout <<"set cbtics tics_space\n";
  cout <<"set zrange [Vmin:Vmax] \n";
  cout <<"set ztics tics_space\n";*/
  str="splot "+cote+chainBCells+cote+" u 1:2:3 t "+cote+""+cote+" w l \n";cout<<str;

  //ACells
  cout<<"\n#ACells\n";
  str="set zlabel "+cote+" V_A"+cote+"\n";cout<<str;
  sprintf(chaine,"Vmin=%g\nVmax=%g\n",p.VAmin,p.VAmax);str=chaine;cout << str;
  /*cout <<"set cbrange [Vmin:Vmax] \n";
  cout << "tics_space=int((Vmax-Vmin)/5) \n";
  cout << "print tics_space\n";
  cout <<"set cbtics tics_space\n";
  cout <<"set zrange [Vmin:Vmax] \n";
  cout <<"set ztics tics_space\n";*/
  str="splot "+cote+chainACells+cote+" u 1:2:3 t "+cote+""+cote+" w l \n";cout<<str;

  //GCells
  cout<<"\n#GCells\n";
  str="set zlabel "+cote+"V_G"+cote+"\n";cout<<str;
  sprintf(chaine,"Vmin=%g\nVmax=%g\n",p.VGmin,p.VGmax);str=chaine;cout << str;
  //cout <<"set cbrange [Vmin:Vmax] \n";
  cout << "tics_space=int((Vmax-Vmin)/5) \n";
  cout << "print tics_space\n";
  //cout <<"set cbtics tics_space\n";
  //cout <<"set zrange [Vmin:Vmax] \n";
  //cout <<"set ztics tics_space\n";
  str="splot "+cote+chainGCells+cote+" u 1:2:3 t "+cote+""+cote+" w l \n";cout<<str;

  cout<<"\n#Plotting  RF\n\n";
   cout<<"set xlabel offset 0,0\n\n";
 //cout<<"set colorbox horiz user origin .6,0.1 size .35,.02\n";
  //cout<<"set xlabel offset 0,0\n";
  //cout<<"set ylabel offset 0,0\n";
  cout<<"set xtics 40 scale 1\n";
  cout <<"set yrange [xmin:xmax]\n";
  cout << "set autoscale cb\n";
  cout << "set autoscale z\n";

  str="plot"+cote+Plotted_RF+cote+" u 2:1:3 t "+cote+""+cote+" w p lw 2 palette\n";cout<<str;
  cout<<"\nunset multiplot\n";

  cout.close();

  sprintf(chaine,"gnuplot %s\n",GnuName);
  //exit(system(str.c_str()));
  printf("\n\n\tExecuting %s",chaine);
  system(chaine);

  sprintf(chaine,"rm -f %s\n",RF_Time);
  printf("\tExecuting %s",chaine);
  system(chaine);
  
  sprintf(chaine,"rm -f %s\n",RF_Full);
  printf("\tExecuting %s",chaine);
  system(chaine);
  
  sprintf(chaine,"rm -f %s\n",chainBCells);
  printf("\t\tExecuting %s",chaine);
  system(chaine);
   
  sprintf(chaine,"rm -f %s\n",chainACells);
  printf("\t\tExecuting %s",chaine);
  system(chaine);
 
  sprintf(chaine,"rm -f %s\n",chainGCells);
  printf("\t\tExecuting %s",chaine);
  system(chaine);
  
  sprintf(chaine,"rm -f %s\n",Plotted_RF);
  printf("\tExecuting %s",chaine);
  system(chaine);
}


//Plot the stability map in the plane w,tau. a and b parametrize the path
void plot_Stability_Map(char* MapName,params p,char* GnuName,char* PlotName,double a, double b,int Pathway){

  printf("\n-------------------\n\tGenerating the dynamics views\n\n-------------------\n");
  printf("\t\tGnuplot file will be stored in %s\n",GnuName);
  printf("\t\tPng file will be stored in %s\n",PlotName);
  
  std::string str = "";
  char chaine[1000];
  std::string cote="'";//Used in gnuplot to define the symbol '
  std::ofstream cout;//ofstream used to handle strings
  cout.open(GnuName);

  cout<<"\n#General settings\n\n";
  cout << "reset\n";
  cout << "set sample 10000\n";
  cout << "set key right inside\n";

   str="\nset terminal pngcairo  enhanced font"+cote+"Verdana,10"+cote+"\n";cout<<str;
  str="set output" + cote + PlotName +cote+"\n";cout<<str;

  cout << "\n#Complex lower branch inf\n";
  str="t1="+cote+ " {/Symbol t}_{A_{c_^+}(1)}"+cote;cout<<str;
  cout << "\n#Complex lower branch sup\n";
  str="t2="+cote+ " {/Symbol t}_{A_{c_^+}(L/2)}"+cote;cout<<str;
  cout << "\n#Complex upper branch inf\n";
  str="t3="+cote+ " {/Symbol t}_{A_{c_^-}(1)}"+cote;cout<<str;
  cout << "\n#Complex upper branch sup\n";
  str="t4="+cote+ " {/Symbol t}_{A_{c_^-}(L/2)}"+cote;cout<<str;
  cout << "\n#Unstable lower branch\n";
  str="t5="+cote+ " {/Symbol t}_{A_{s}(L/2+1)}"+cote;cout<<str;
  cout << "\n##Unstable upper branch\n";
  str="t6="+cote+ " {/Symbol t}_{A_{c_{sup}}}"+cote;cout<<str;
  
  cout<<"\n\n#Initializing variables\n";
  sprintf(chaine,"N=%d\nL=N\ntauA=%f\ntauB=%f\nw=%f\na=%f\nb=%f\n",p.N,p.tauA,p.tauB,-p.wab,a,b);str=chaine;cout << str;
  cout << "wmin=1E-3\n";
  cout << "wmax=2048E-3\n";
  cout << "tauAmin=0.25\n";
  cout << "tauAmax=2048\n";
  cout <<"set xrange [wmin:wmax]\n";
  cout <<"set xtics wmin,2,wmax\n";
  str="set xlabel"+cote+"w (KHz)"+cote+"\n";cout<<str;
  cout <<"set yrange [tauAmin:tauAmax]\n";
  cout <<"set ytics tauAmin,2,tauAmax\n";
  str="set ylabel"+cote+" {/Symbol t}_A (ms)"+cote+"\n";cout<<str;
  cout << "set logscale xy\n";

  cout << "\n#Color box\nset cbrange [0:9]\n";
  cout << "set palette maxcolors 9\n";
  str="set cbtics ("+cote+"sR-fS"+cote+"0,"+cote+"sR-fM"+cote+"1,"+cote+"sR-fU" +cote+"2,"+cote+"sC-fS"+cote+ "3, "+cote+"sC-fM"+cote+ "4, "+cote+"sC-fU"+cote+ "5,"+cote+"sM-fS"+cote+ "6,"+cote+"sM-fM"+cote+ "7, "+cote+"sM-fU"+cote+ "8)\n";cout<<str;
  str="load" +cote+"jet.pal"+cote+"\n";cout<<str;

  //cout << "\nset terminal pngcairo  enhanced font 'Verdana,8'\n";
  //str="set output"+cote+PlotName+cote+"\n\n";cout<<str;

  cout << "\n\n#Functions\n";
  cout << "kappa(n)=2.0*cos(n*pi/(L+1)) #Eigenvalue n\n";
  cout << "tauA_cp(w,n)=(kappa(n) > 0 ? tauB/(1+2*w*tauB*sqrt(kappa(n))):1/0)\n";
  cout << "tauA_cm(w,n)=(kappa(n) > 0 ? (2*w*tauB*sqrt(kappa(n)) < 1 ? tauB/(1-2*w*tauB*sqrt(kappa(n))):1/0):1/0)\n";
  cout << "tauS(w,n)=(kappa(n) < 0 ? -1.0/(tauB*w**2*kappa(n)):1/0)\n";
  cout << "dr(w,a,b)=b*w**a\n";
  
  cout << "\n\n#Plot\n";

  cout << "set style line 1 lc rgb " + cote+"black" + cote + "dashtype 1 lw 2\n";
  cout << "set style line 2 lc rgb " + cote+"black" + cote + "dashtype 2 lw 2\n";
  cout << "set style line 3 lc rgb " + cote+"black" + cote + "dashtype 5 lw 2\n";
  cout << "set style line 4 lc rgb " + cote+"dark-gray" + cote + "dashtype 4 lw 3\n";
  cout << "set style line 5 lc rgb " + cote+"light-red" + cote + "dashtype 7 lw 3\n"; 
  cout << "set style line 6 lw 4 lc rgb " + cote+"dark-grey" + cote + "ps 3 pt 7 pi 4\n";

  str="\nset label at w,tauA "+cote+cote+ " point  ls 6  pointsize 2 front\n";cout<<str;

  char title[1000];sprintf(title,"w=%lg, {/Symbol t}_A=%lg",p.wba*1000,p.tauA);//printf("Title=%s\n",title);
  str="\nset title"+cote+title+cote+"\n";
  cout<<str;

  str="\nplot "+cote+MapName+cote+" u ($1):($2):($4) w p lw 2 palette  t "+cote+""+cote+", [w=wmin:wmax] tauA_cp(w,L/2) t t2  w l ls 1, [w=wmin:wmax]  tauA_cm(w,1) w l  ls 2 t t3, [w=wmin:wmax]  tauA_cm(w,L/2) w l  ls 2 t t4, [w=wmin:wmax] tauS(w,L/2+1)  w l ls 3  t t5, [w=wmin:wmax] tauS(w,L)  w l ls 3  t t6, [w=wmin:wmax] dr(w,a,b) w l ls 5 t" + cote +"Pathway 2\n";
  //+cote+","+cote+"-"+cote+ " w p ls 6  t \n w tauA\n";
  cout<<str;

  cout.close();

  sprintf(chaine,"gnuplot %s\n",GnuName);
  //exit(system(str.c_str()));
  printf("\n\n\tExecuting %s",chaine);
  system(chaine);
 }

void plot_Net_RF_Periodic(char* RF_Time,char* RF_Full,char* chainBCells,char* chainACells,char* chainGCells,char* Plotted_RF,params p,char* GnuName,char* PlotName,double tau,double tinf,double tsup){//Plot the 3D net activity - files chainBCells, chainACells, chainGCells  - and the RF - file RF in response to a periodic stimulus. The gnuplot commands are copied in the file GnuName and a png file PlotName is generated. 

  printf("\n-------------------\n\tGenerating the dynamics views\n\n-------------------\n");
  printf("\t\tGnuplot file will be stored in %s\n",GnuName);
  printf("\t\tPng file will be stored in %s\n",PlotName);
  
  std::string str = "";
  char chaine[1000];
  std::string cote="'";//Used in gnuplot to define the symbol '
  std::ofstream cout;//ofstream used to handle strings
  cout.open(GnuName);

  cout<<"#Computes the RF of GCells in the different regions on the stability map\n\n";
  cout<<"\n#General settings\n";
  cout << "reset\n";
  str="load" +cote+"jet.pal"+cote+"\n";cout<<str;
  cout << "set sample 10000\n";
  cout << "set key right inside\n";
  cout << "set size square 1,1\n";
  cout << "set xlabel offset 0,0\n";
  cout << "set ylabel offset 0,0\n";
  cout << "set xtics 0.3 scale 1\n";
   cout << "set zlabel"+cote+" V (mV)"+cote+"\n"; 
   cout <<"set autoscale z\n";
   //cout << "set cblabel"+cote+"V (mV)"+cote+"\n"; 
   //cout <<"set cbtics 2\n";
   cout <<"set autoscale cb\n";
   cout << "set pm3d\n";

  cout << "\nset terminal pngcairo  enhanced font 'Verdana,8'\n";
  str="set output"+cote+PlotName+cote+"\n\n";cout<<str;

  //cout <<"set style line 2604 linetype -1 linewidth .4\n";
  cout << "set style line 1 lt rgb " + cote+"black" + cote + " lw 2.1\n";
  cout << "set style line 2 lt rgb " + cote+"red" + cote + " dashtype 3 lw 5\n";
  cout << "set style line 3 lt rgb " + cote+"blue" + cote + " dashtype 2 lw 4\n";
  cout << "set style line 4 lt rgb " + cote+"purple" + cote + " dashtype 4 lw 5\n";
  cout << "set style line 5 lt rgb " + cote+"green" + cote + " dashtype 5 lw 4\n";

  cout<<"\n#Initializing variables\n\n";
  sprintf(chaine,"N=%d\n",p.N);str=chaine;cout << str;
  sprintf(chaine,"\ndx=%f\n",p.dx);str=chaine;cout << str;
  sprintf(chaine,"tmin=-10\n");str=chaine;cout << str;
  //sprintf(chaine,"tmin=%f\n",p.tmin);str=chaine;cout << str;
  sprintf(chaine,"tmax=%f\n",p.tmax);str=chaine;cout << str;
  sprintf(chaine,"tinf=%f\n",tinf);str=chaine;cout << str;
   sprintf(chaine,"tsup=%f\n",tsup);str=chaine;cout << str;

  cout <<"set xrange [tinf:tsup]\n";
  cout << "set xlabel 't (ms)'\n";
  cout << "dt=(tsup-tinf)/5\n";
  cout << "set xtics dt\n";
  cout << "\nset format x"+cote+"%2.0f"+cote+"\n";

  //cout <<"\nxmin=10*dx\n";  cout <<"xmax=(N-10)*dx\n";
  cout <<"\nxmin=3*dx\n";  cout <<"xmax=(N-4)*dx\n";
  cout <<"set yrange [xmin:xmax]\n";
  cout << "set ylabel 'x (mm)'\n";
  cout << "set ytics xmax*10/N\n";

  char title[1000];sprintf(title,"w=%3.2f Hz, {/Symbol t}_A=%3.2f ms,{/Symbol n}=%3.2f Hz,T=%3.2f ms,{/Symbol t}_r=%3.2f ms",p.wba*1000,p.tauA,p.omega*1000/dpi,p.T,tau);//printf("Title=%s\n",title);
  cout << "\nset multiplot layout 2,2 title "+cote+title+cote+" font 'Verdana,10'\n\n";
  //cout << str;

  cout<<"\n#Plotting  Network activity\n\n";
  //str="set title"+cote+"Net"+cote+"\n";cout<<str;
  cout<<"set xlabel offset 0,-0.4\n\n";
  cout<<"set view 65,345,1,1\n";
  cout<<"set dgrid3d 2*N,2*N,16\n";
  cout<<"set hidden3d back offset 1 trianglepattern 3 undefined 1 altdiagonal bentover\n";
  //cout<<"set xtics offset 0,-0.2\n";
  cout<<"set xtics scale 3\n";
  //cout<<"set xlabel offset 14,3\n";
  cout<<"set ylabel offset 0,-0.5\n";

  //cout << "set colorbox horiz user origin .1,0.1 size .35,.02\n";
  //BCells
  cout<<"\n#BCells\n";
  str="set zlabel "+cote+"V_B"+cote+"\n";cout<<str;
  sprintf(chaine,"Vmin=%g\nVmax=%g\n",p.VBmin,p.VBmax);str=chaine;cout << str;
  cout <<"set cbrange [Vmin:Vmax] \n";
  //cout <<"set cbtics int((Vmax-Vmin)/5)\n";
  cout <<"set zrange [Vmin:Vmax] \n";
  //cout <<"set ztics int((Vmax-Vmin)/5)\n";
   str="splot "+cote+chainBCells+cote+" u 1:2:3 t "+cote+""+cote+" w l \n";cout<<str;

  //ACells
  cout<<"\n#ACells\n";
  str="set zlabel "+cote+" V_A"+cote+"\n";cout<<str;
  sprintf(chaine,"Vmin=%g\nVmax=%g\n",p.VAmin,p.VAmax);str=chaine;cout << str;
  cout <<"set cbrange [Vmin:Vmax] \n";
  //cout <<"set cbtics int((Vmax-Vmin)/5)\n";
  cout <<"set zrange [Vmin:Vmax] \n";
  //cout <<"set ztics int((Vmax-Vmin)/5)\n";
  str="splot "+cote+chainACells+cote+" u 1:2:3 t "+cote+""+cote+" w l \n";cout<<str;

  //GCells
  cout<<"\n#GCells\n";
  str="set zlabel "+cote+"V_G"+cote+"\n";cout<<str;
  sprintf(chaine,"Vmin=%g\nVmax=%g\n",p.VGmin,p.VGmax);str=chaine;cout << str;
  cout <<"set cbrange [Vmin:Vmax] \n";
  //cout <<"set cbtics int((Vmax-Vmin)/5)\n";
  cout <<"set zrange [Vmin:Vmax] \n";
  //cout <<"set ztics int((Vmax-Vmin)/5)\n";
  str="splot "+cote+chainGCells+cote+" u 1:2:3 t "+cote+""+cote+" w l \n";cout<<str;

  cout<<"\n#Plotting  RF\n\n";
   cout<<"set xlabel offset 0,0\n\n";
 //cout<<"set colorbox horiz user origin .6,0.1 size .35,.02\n";
  //cout<<"set xlabel offset 0,0\n";
  //cout<<"set ylabel offset 0,0\n";
  cout<<"set xtics scale 1\n";
  cout <<"set yrange [xmin:xmax]\n";
 
  str="plot"+cote+Plotted_RF+cote+" u 2:1:3 t "+cote+""+cote+" w p lw 2 palette\n";cout<<str;
  cout<<"\nunset multiplot\n";

  cout.close();

  sprintf(chaine,"gnuplot %s\n",GnuName);
  //exit(system(str.c_str()));
  printf("\n\n\tExecuting %s",chaine);
  system(chaine);
  
  //return;

  sprintf(chaine,"rm -f %s\n",RF_Time);
  printf("\tExecuting %s",chaine);
  system(chaine);
  
  sprintf(chaine,"rm -f %s\n",RF_Full);
  printf("\tExecuting %s",chaine);
  system(chaine);
  
  sprintf(chaine,"rm -f %s\n",chainBCells);
  printf("\t\tExecuting %s",chaine);
  system(chaine);
  
  sprintf(chaine,"rm -f %s\n",chainACells);
  printf("\t\tExecuting %s",chaine);
  system(chaine);

  sprintf(chaine,"rm -f %s\n",chainGCells);
  printf("\t\tExecuting %s",chaine);
  system(chaine);

  sprintf(chaine,"rm -f %s\n",Plotted_RF);
  printf("\tExecuting %s",chaine);
  system(chaine);
}


//Plot the resonance curve in the plane w,tau. a and b parametrize the path. Lambdai_min is the slowest peak
void plot_Resonance_curve(char* Name,params p,char* GnuName,char* PlotName,double lambdai_min,double lambdar_slow,double max_BC,double max_AC,double max_GC){

  printf("\n-------------------\n\tGenerating the resonance curve plot\n\n-------------------\n");
  printf("\t\tGnuplot file will be stored in %s\n",GnuName);
  printf("\t\tPng file will be stored in %s\n",PlotName);
  
  std::string str = "";
  char chaine[1000];
  std::string cote="'";//Used in gnuplot to define the symbol '
  std::ofstream cout;//ofstream used to handle strings
  cout.open(GnuName);


  cout << "#Resonance curve\n";
  cout << "\nreset\n";
  //str="set format"+cote+"%2.2f"+cote+"\n";cout<<str;
  cout << "dpi=2*pi\n";
  cout << "set sample 10000\n";
  cout << "set xrange [-40:40]\n";
  cout << "set xtics 10\n";
  //cout << "set ytics 1\n";
  str="set xlabel"+cote+cote+"\n";cout<<str;

  cout << "\n\n########Functions\n";
  cout << "den(lambdar,lambdai,omega)=sqrt(lambdar**2+(lambdai+omega)**2)\n";
  cout << "Repr(lambdar,lambdai,omega)=lambdar/den(lambdar,lambdai,omega)\n";
  cout << "Repi(lambdar,lambdai,omega)=-(lambdai+omega)/den(lambdar,lambdai,omega)\n";
  cout << "Mod(lambdar,lambdai,omega)=1.0/den(lambdar,lambdai,omega)\n";
  cout << "module(x,y)=sqrt(x**2+y**2)\n";
  cout << "f(lambdar,lambdai,x)=A*Mod(lambdar,lambdai,x)\n";

  cout<<"\n\n#Initializing variables\n\n";
  sprintf(chaine,"peak1=%lg*1000\n",lambdai_min);str=chaine;cout << str;
  sprintf(chaine,"Delta_BC=%lg*1000\n",lambdar_slow);str=chaine;cout << str;
  sprintf(chaine,"Delta_AC=%lg*1000\n",p.unsurtauA);str=chaine;cout << str;
  sprintf(chaine,"Delta_GC=%lg*1000\n",p.unsurtauG);str=chaine;cout << str;
  sprintf(chaine,"max_BC=%lg\n",max_BC);str=chaine;cout << str;
  sprintf(chaine,"max_AC=%lg\n",max_AC);str=chaine;cout << str;
  sprintf(chaine,"max_GC=%lg\n",max_GC);str=chaine;cout << str;
  char title[1000];//sprintf(title,"w=%3.2f Hz, {/Symbol t}_A=%3.2f ms, {/Symbol l}_r=%3.2f Hz, {/Symbol l}_i=%3.2f Hz",p.wba*1000,p.tauA,fabs(lambdar_slow)*1000,lambdai_min*1000);//printf("Title=%s\n",title);

  cout << "\nset terminal pngcairo  enhanced font 'Verdana,10'\n";
  str="set output"+cote+PlotName+cote+"\n\n";cout<<str;

  cout << "\nset multiplot layout 3,1 title "+cote+title+cote+" font 'Verdana,10'\n\n";

    cout << "\n############BCells\n";
    cout << "unset arrow\n";
    cout << "val=1.1*max_BC\n";
    cout << "set yrange [0:val]\n";
    
    cout << "unset arrow\n";
    /*str="set  arrow from 0,0 to 0,val nohead dt"+cote+"-"+cote+"\n";cout<<str;
    str="set arrow from peak1,0 to peak1,val nohead dt"+cote+"-"+cote+"\n";cout<<str;
    str="set arrow from -peak1,0 to -peak1,val nohead dt"+cote+"-"+cote+"\n";cout<<str;*/
    //str="set arrow from peak1-Delta_BC,val/2 to peak1+Delta_BC,val/2 heads\n";cout<<str;

    cout << "unset label\n";
    //str="set label "+cote+"p_1"+cote+ "at peak1,val*1.1\n";cout<<str;
    //str="set label "+cote+"p_1"+cote+ "at -peak1,val*1.1\n";cout<<str;

    str="set ylabel "+cote+"|{/Symbol c}_{BC}|"+cote+"\n";cout<<str;

    cout<<"\n\t########## Fitting resonance curve\n";
    cout << "lambdar=abs(Delta_BC)\n";
    cout << "lambdai=peak1\n";
    cout << "A=max_BC*lambdar\n";

    //str="fit f(lambdar,lambdai,x) "+cote+Name+cote+" u ($1*1000/dpi):(module($2,$3)) via lambdar,A\n";cout<<str;

    str="\nplot "+cote+Name+cote+" u ($1*1000/dpi):(module($2,$3)) t "+cote+""+cote+"w p lc 7 ps 1.5#, f(lambdar,lambdai,x) t"+cote+cote+"\n";cout<<str;

    cout << "\n############ACells\n";
    cout << "unset arrow\n";
    cout << "val=1.1*max_AC\n";
    cout << "set yrange [0:val]\n";
    
    cout << "unset arrow\n";
    /*str="set  arrow from 0,0 to 0,val nohead dt"+cote+"-"+cote+"\n";cout<<str;
    str="set arrow from peak1,0 to peak1,val nohead dt"+cote+"-"+cote+"\n";cout<<str;
    str="set arrow from -peak1,0 to -peak1,val nohead dt"+cote+"-"+cote+"\n";cout<<str;*/
    //str="set arrow from peak1-Delta_AC,val/2 to peak1+Delta_AC,val/2 heads\n";cout<<str;

   // cout << "unset label\n";
    //str="set label "+cote+"p_1"+cote+ "at peak1,val*1.1\n";cout<<str;
    //str="set label "+cote+"p_1"+cote+ "at -peak1,val*1.1\n";cout<<str;

    str="set ylabel "+cote+"|{/Symbol c}_{AC}|"+cote+"\n";cout<<str;
    str="\nplot "+cote+Name+cote+" u ($1*1000/dpi):(module($4,$5)) t "+cote+""+cote+"w p lc 7 ps 1.5\n";cout<<str;

    cout << "\n############GCells\n";

    str="set xlabel "+cote+"{/Symbol n} (Hz)"+cote+"\n";cout<<str;

    cout << "unset arrow\n";
    cout << "val=1.1*max_GC\n";
    cout << "set yrange [0:val]\n";
    
    cout << "unset arrow\n";
    /*str="set  arrow from 0,0 to 0,val nohead dt"+cote+"-"+cote+"\n";cout<<str;
    str="set arrow from peak1,0 to peak1,val nohead dt"+cote+"-"+cote+"\n";cout<<str;
    str="set arrow from -peak1,0 to -peak1,val nohead dt"+cote+"-"+cote+"\n";cout<<str;*/
    //str="set arrow from peak1-Delta_GC,val/2 to peak1+Delta_GC,val/2 heads\n";cout<<str;

    cout << "unset label\n";
    //str="set label "+cote+"p_1"+cote+ "at peak1,val*1.1\n";cout<<str;
    //str="set label "+cote+"p_1"+cote+ "at -peak1,val*1.1\n";cout<<str;

    str="set ylabel "+cote+"|{/Symbol c}_{GC}|"+cote+"\n";cout<<str;
    str="\nplot "+cote+Name+cote+" u ($1*1000/dpi):(module($6,$7)) t "+cote+""+cote+"w p lc 7 ps 1.5\n";cout<<str;

    cout << "\nunset multiplot\n";

  cout.close();

  sprintf(chaine,"gnuplot %s\n",GnuName);
  //exit(system(str.c_str()));
  printf("\n\n\tExecuting %s",chaine);
  system(chaine);
  
  return;

  sprintf(chaine,"rm -f %s\n",Name);
  printf("\tExecuting %s",chaine);
  system(chaine);
 
}

//Plot statistics: Histo contains the histogram (must be initialized first with Update_Histo). It is normalized here and moments are computed. The histogram is stored in chaine_hist while a gnuplot script generates the plot and plot the file Name. The gnuplot commands are copied in the file GnuName and  png file PlotName is generated. Label contains the name of the variable. Unit the unit. Mode can be void or exp. In the last case one plots the exponential of the variable. OutMoments contains a file where the moments are loaded. 
void PlotStats(Histo histo,char *chaine_hist,string Name,string GnuName,char* PlotName,char* label,std::string Gene,std::string ExpCond,std::string unit,std::string mode,std::string OutMoments){

    moments moments;
    Cell_id_type cell;
    double val=0; 

    FILE* fhist=NULL;
    
    //Storing and plotting histograms
    compute_Moments(&histo,&moments);
    if (OutMoments!=""){
      FILE *fp=fopen(OutMoments.c_str(),"a");
      fprintf(fp,"%s,%s\t%g\t%g\t%g\t%g\t%g\t%g\n",Gene.c_str(),ExpCond.c_str(),moments.mean,moments.std,moments.mean_ON,moments.std_ON,moments.mean_OFF,moments.std_OFF);
      fclose(fp);
   }
    double xmin=10000,xmax=-10000,Pmin=10000,Pmax=-10000;
    printf("\t\t\tFull (mean =%lg, std=%lg)\t ON (mean =%lg, std=%lg)\t OFF (mean =%lg, std=%lg)\n",moments.mean,moments.std,moments.mean_ON,moments.std_ON,moments.mean_OFF,moments.std_OFF);

    fhist=fopen(chaine_hist,"w");
    for (Histo::iterator it=histo.begin();it!=histo.end();++it){
      double val=it->first.first,P=it->second;
      string type=it->first.second;
      if ((type=="OFF")&&(it!=histo.end())){
	Histo::iterator itp=it;
	++itp;
	if ((itp!=histo.end())&&(itp->first.second=="ON")&&(itp->first.first==val)){
	  P+=itp->second;//P(val) is the sum of P(val,OFF)+P(val,ON) 
	  ++it;
 	}
     }
      xmax=max(val,xmax);
      xmin=min(val,xmin);
      Pmax=max(P,Pmax);
      Pmin=min(P,Pmin);
      fprintf(fhist,"%lg\t%lg\n",val,P);
    }
    fclose(fhist);
    
    //Generating the plot files
    printf("\t\t\tGnuplot file will be stored in %s\n",GnuName.c_str());
    printf("\t\t\tpng file will be stored in %s\n",PlotName);
    
    char chaine[1000];
    std::string str="";
    std::string cote="'";//Used in gnuplot to define the symbol '
    std::ofstream cout;//ofstream used to handle strings
    cout.open(GnuName);
  
    cout<<"#Drawing the histogram\n";
    cout<<"\n#General settings\n";
    cout << "reset\n";
    str="load" +cote+"~/jet.pal"+cote+"\n";cout<<str;
    cout << "set sample 10000\n";
    cout << "unset arrow\nunset label\nunset object\n";
    cout<<"\n\t#Histogram style\n";
    cout << "set boxwidth 3 absolute\n";
    cout << "set style fill   solid 1.00 border lt -1\n";
    cout << "set key  right top vertical Right noreverse noenhanced autotitle nobox\n";
    cout << "set style histogram clustered gap 5 title textcolor lt -1\n";
    cout << "set datafile missing '-'\n";
    cout << "set style data histograms\n";
    //cout << "set style data fillsteps\n";
    
    cout<<"\n#Line styles \n";
    cout << "set style line 1 lt rgb " + cote+"black" + cote + " lw 2.1\n";
    cout << "set style line 2 lt rgb " + cote+"red" + cote + " dashtype 3 lw 5\n";
    cout << "set style line 3 lt rgb " + cote+"blue" + cote + " dashtype 2 lw 4\n";
    cout << "set style line 4 lt rgb " + cote+"purple" + cote + " dashtype 4 lw 5\n";
    cout << "set style line 5 lt rgb " + cote+"green" + cote + " dashtype 5 lw 4\n";
    cout << "set style line 6 lt rgb " + cote+"orange" + cote + " dashtype 2 lw 4\n";
    
    cout<<"\n#Initializing variables\n";
    sprintf(chaine,"xmin=%f\n",xmin);str=chaine;cout << str;
    sprintf(chaine,"xmax=%f\n",xmax);str=chaine;cout << str;
    sprintf(chaine,"Pmin=%f\n",Pmin);str=chaine;cout << str;
    sprintf(chaine,"Pmax=%f\n",Pmax);str=chaine;cout << str;
    sprintf(chaine,"mean=%lg\n",moments.mean);str=chaine;cout << str;
    sprintf(chaine,"mean_ON=%lg\n",moments.mean_ON);str=chaine;cout << str;
    sprintf(chaine,"mean_OFF=%lg\n",moments.mean_OFF);str=chaine;cout << str;
    sprintf(chaine,"std=%lg\n",moments.std);str=chaine;cout << str;
    sprintf(chaine,"std_ON=%lg\n",moments.std_ON);str=chaine;cout << str;
    sprintf(chaine,"std_OFF=%lg\n",moments.std_OFF);str=chaine;cout << str;

    cout<<"\n#x axis\n";
    if (unit!="") {
      str="\nset xlabel "+cote+label+" ("+unit+") "+cote+"\n";cout<<str;
    }
    else {
      str="\nset xlabel "+cote+label+cote+"\n";cout<<str;
    }
    cout <<"set xtics border in scale 0,0 nomirror rotate by -45  autojustify\n";
    cout <<"set xtics  norangelimit \n";
    cout <<"set xtics   ()\n";
    //cout <<"set xrange [xmin:xmax]\n";
   // cout << "set xtics ((xmax-xmin)/10.0)\n";

    cout<<"\n#y axis\n";
    str="\nset ylabel "+cote+"P_{"+label+"}"+cote+"\n";cout<<str;
    cout <<"set yrange [0:1.1*Pmax]\n";
    //cout << "set ytics ((Pmax-Pmin)/10.0)\n";

    cout<<"\n#Arrows and labels\n";
    cout << "unset arrow\nunset label\n";
    /*str="set arrow from mean_ON,0 to mean_ON,Pmax nohead ls 2 front\n";cout<<str;
    str="set arrow from mean_OFF,0 to mean_OFF,Pmax nohead ls 3 front\n";cout<<str;
    str="set label "+cote+"ON"+cote+ " at mean_ON,1.05*Pmax\n";cout<<str;
    str="set label "+cote+"OFF"+cote+ " at mean_OFF,1.05*Pmax\n";cout<<str;*/

    cout<<"\n#Plot\n";
    cout << "\nset terminal pngcairo  enhanced font 'Verdana,10'\n";
    str="set output "+cote+PlotName+cote+"\n\n";cout<<str;
    if (mode=="exp"){
      str="\nplot "+cote+Name+cote+"u 2:xtic(log($1)) t " +cote+"\n";cout<<str;
    }
    else{
      str="\nplot "+cote+Name+cote+"u 2:xtic(1) t " +cote+cote+"\n";cout<<str;
    }
    //str="\nplot "+cote+Name+cote+"t " +cote+cote+"\n";cout<<str;

    cout.close();
    
    if (Gene=="Grik4") chdir("Grik4/Stats/");
    if (Gene=="Scnn1a") chdir("Scnn1a/Stats/");
    //system("pwd\n");
    
    sprintf(chaine,"gnuplot %s.gnu",Name.c_str());
    printf("\t\t\tExecuting %s\n\n",chaine);
    system(chaine);
    chdir("../..");
  

}
