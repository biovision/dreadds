/***
    Draw the map of eigenvalues (stability, complexity) in the plane (r=tauA/tauB,s=wab/wba).
    The idea is to analyse the spectrum of L in the map and display the typical shape of the RGC RF.
    One starts from a prototypical set of parameters (e.g. typical_params_Grik4) and then let mu and r evolve. 
    Note that this requires to fix at least tauB (or tauA), wab (or wba) as well as tauG, wbg and wag. 
    Hence the need of having a prototypical set of parameters.
    
    B. Cessac, 12/10/2022
 ***/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include "Defs.h"
#include "Utils.h"
#include "Plot.h"
#include "Spectrum.h"
#include "Dynamics.h"
#include "Fitting.h"
#include <unistd.h>

typedef struct r_s{
  double r=0;
  double s=0;
}RS;

int main(void){
  printf("\n\n\tEigenvalue map\n");
  
  /** Initializations of the main variables ***/
  double convert_s_to_ms=1000; //Conversion s to ms. 1s = 1000 ms
  double convert_ms_to_s=0.001; //Conversion ms to s. 1 ms = 0.001 s
  double weight_factor=1E-3;
  char chaineSp[1000],chaineMap[1000];
  FILE *fpMap=NULL;
  params p;//Cells parameters
  std::string Gene="Grik4",ExpCond="CTL",datafile=Gene+"_temp_STAs_"+ExpCond+".txt";
  //std::string Gene="Scnn1a",ExpCond="CTL",datafile=Gene+"_temp_STAs_"+ExpCond+".txt";
  char chaineDir[1000],chaineParams[1000],chaine[1000];
  sprintf(chaineDir,"%s/",Gene.c_str());//printf("%s\n",chaineDir);exit(0);
  char SaveFile[1000];
  char InFile[1000];//Suffix of the trajectory file indicating the model's parameters
  char chaineT[1000],chaineTh[1000],chaineExp[1000],GnuName[1000]="",PlotName[1000]="";
  char LocExec[1000],Title[1000];
  
  //Loading data
  if (Gene=="Grik4"){//Grik4
    sprintf(SaveFile,"%sParameters/typical_params_Grik4",chaineDir);printf("\t\tParameters loaded from %s\n",SaveFile);//exit(0);
    ReadParams(&p,SaveFile);
    //WriteParams(p);exit(0);
    p.b0=0; //To remove the continuous component in the power spectrum.
    p.tauB=p.tauBref=30;
    //p.unsurtauB=1.0/p.tauB;//,unsurtaua=1.0/p.taua;
    p.tauRF=30;
    //p.tauRF3= pow(p.tauRF,3); 
    p.tauG=p.tauGref=30;
    //p.unsurtauG=1.0/p.tauG;
    p.wba=0.0085;
    p.wag=-0.003;
    p.wbg=0.0095;
    UpdateParams(&p);
    init_Network_Laplacian(&p,1);  
    SaveParams(p,SaveFile);
    //exit(0);
  }//End of Grik4
  else if (Gene=="Scnn1a"){//Scnn1a
    printf("Case Scnn1a\n");
    sprintf(SaveFile,"%sParameters/typical_params_Scnn1a",chaineDir);printf("\t\tParameters loaded from %s\n",SaveFile);//exit(0);
    ReadParams(&p,SaveFile);
    // exit(0);
  }
  
  int beta=0;
  int iGC=p.N/2;//Index of the GCell on the lattice
  
  int alphaBC=iGC;//Index of the BCell above this GCell in the extended phase space
  int alphaAC=iGC+p.L;//Index of the ACell above this GCell in the extended phase space
  int alphaGC=iGC+2*p.L;//Index of the GCell in the extended phase space
  int alphaAc=iGC+3*p.L;//Index of the BCell activity in the extended phase space
  double xGC=iGC*p.dx,yGC=0;//Physical coordinates of the GCell (mm)

  void (*f)(double *X,double *Xp,double t,params p,double (*input)(double x,double t, params p))=NULL;
  void (*fS)(double x,double *X,double *Xp,double t,params p,double (*input)(int beta,double x,double t, params p))=NULL;
  string chain_type=Select_dyn_type(0,&f,&fS);
  int dim_ext=4*p.L;// Dimension of the phase space
  int dim=3*p.L;// Dimension of the phase space without activities


  bool ComputeTimes=true;
  if (0){ //Drawing the map including characteristic times when ComputeTimes=true
    if (p.BtoA==1){//GammaAB=GammaBA
      int nslot=16;//=> Each step is multiplied by $\sqrt[nslot]{2}$
      double r=0,rmin=0.999/(1<<2),rmax=1<<7,dr=exp(log(2.)/nslot);//0.999 to avoid having r=1 => \tau = +infinity
      double dr_dec=exp(log(2.)/(2*nslot));
      double mu=0,mumin=1.0/(1<<9),mumax=1<<10,dmu=exp(log(2.)/nslot);
      double s=0,smin=1.0/(1<<6),smax=1<<8,ds=exp(log(2.)/nslot);
      double ds_dec=exp(log(2.)/(2*nslot));
      sprintf(InFile,"_N%d",p.N);
      sprintf(chaineMap,"%sMap/StabilityMap_LaplacianConnection_vs_r%s_An",chaineDir,InFile);
      //printf("%s\n",chaineMap); exit(0);
      //      fpMap=fopen(chaineMap,"w");fclose(fpMap);
      
      //int nb_points=0;for (r=rmin;r<=rmax;r*=dr)  for (s=smin;s<=smax;s*=ds) nb_points++;printf("Number of points in the map=%d\n",nb_points);exit(0);
      std::vector<Peaks_fft> periods;
      // for (r=rmin;r<=rmax;r*=dr){//Loop on r = tauA/tauB. delta r increases with iterations
      //rmax=2.65048/dr_dec;
      for (r=rmax;r>=1;r/=dr_dec){//Loop on r = tauA/tauB. delta r decreases with iterations starting from large r
	printf("r=%lg\n",r);
	/** Tuning parameters **/
	p.tauA=p.tauAref=r*p.tauB;
	
	//for (s=smin;s<=smax;s*=ds){//Loop on s=-wab/wba. delta s increases with iterations
	for (s=smax;s>=1;s/=ds_dec){//Loop on s=-wab/wba. delta s decreases with iterations starting from large s
	  //printf("\ts=%lg\n",s);
	  p.wab=-s*p.wba;
	  UpdateParams(&p);
	  
	  /** Generating the network and computing the transfert operator **/
	  //printf("\n--------------------\n\t\tGenerating the network and computing the transfert operator\n--------------------\n");
	  init_Network_Laplacian(&p,1);  
	
	  /*** Computing characteristic times **/
	  if (ComputeTimes){// Computing characteristic times

	    //Old method: computing the RF then its FFT
	    /*params pp=p;
	    int n_peaks=0;
	    char PowerSpectrum[1000];
	    double max_range=0;

	    gsl_vector_complex* SumPinvbgKSg=gsl_vector_complex_calloc(dim);//Auxilary used for computation
	    gsl_vector_complex* SumPinvbgKSg_CNOA=gsl_vector_complex_calloc(dim);//Auxilary used for computation
	    // sprintf(InFile,"_N%d_%s",p.N,labels[n]);//printf("InFile=%s\n",InFile);
	    sprintf(InFile,"_N%d_r%3.3f_s%3.3f",p.N,r,s);//printf("InFile=%s\n",InFile);
	    sprintf(chaineT,"Trajectories/Cell_Time%s",InFile);//printf("chaineT=%s\n",chaineT);
	    sprintf(chaineSp,"Trajectories/Cell_Space%s",InFile);//printf("chaineSp=%s\n",chaineSp);
	    sprintf(chaineTh,"Trajectories/Theoretical_Cell%s",InFile);
	    sprintf(chaineExp,"void");//printf("chaineExp=%s\n",chaineExp);exit(0);
	    int NumPoint=1<<11;//printf("NumPoint=%d\n",NumPoint);exit(0);//Pour FFT.
	    pp.tmax=1<<11;
	    pp.tmin=0;
	    pp.dt=(float)(pp.tmax/(double)NumPoint);//Pour FFT.
	    double sample_rate=pp.dt;
	    ComputeAndStoreSpaceRF(iGC,pp,chaineT,chaineSp,chaineTh,f,fS,SumPinvbgKSg,SumPinvbgKSg_CNOA);
	    periods=getPeriod(chaineTh,xGC,sample_rate,&n_peaks,PowerSpectrum,&max_range); 
	    printf("\n\t\tThe %d first peaks (periods) in the power spectrum are:\n",n_peaks);
	    for (int k=0;k<n_peaks;k++) {
	      if (periods[k].f>0) printf("\t\t\t(f=%lg (Hz), T=%lg (ms), Amp=%lg, width=%lg (Hz) => %lg (ms))\n",1000*periods[k].f,1.0/periods[k].f,periods[k].Amp,1000*periods[k].width,1.0/periods[k].width);
	      else{
		if (periods[k].width>0)
		  printf("\t\t\t(f=%lg (Hz),T= inf, Amp=%lg, width=%lg (Hz) => %lg (ms))\n",1000*periods[k].f,periods[k].Amp,1000*periods[k].width,1.0/periods[k].width);
		else 
		  printf("\t\t\t(f=%lg (Hz), T=Inf, Amp=%lg, width=%lg (Hz) => Inf)\n",1000*periods[k].f,periods[k].Amp,1000*periods[k].width);
	      }
	    }
	    sprintf(chaine,"rm -f %s\n\t\trm -f %s\n\t\trm -f %s\t\trm -f %s\n\n",chaineT,chaineSp,chaineTh,PowerSpectrum);
	    printf("\n\t\tExecuting %s",chaine);
	    system(chaine);
	    My_gsl_vector_complex_free(SumPinvbgKSg);
	    My_gsl_vector_complex_free(SumPinvbgKSg_CNOA);
	    */
	  }//End of computing characteristic times

	  //New method. Compute directly the analytic form
	  periods=getPeriod(alphaGC,xGC,0,p);   
	  
	  /*** Computing eigenmodes and eigenvalues **/
	  //printf("\n\tComputing the theoretical RF of cell %d \n",alphaBC);
	  //printf("\n\t\t--------------------\n\t\tComputing eigenmodes and eigenvalues\n\t\t--------------------\n");
	  gsl_matrix *Psi=gsl_matrix_calloc(p.L,p.L),*Psi_transpose=gsl_matrix_calloc(p.L,p.L);
	  Spectrum kappa_th;//Theoretical eigenvalues of the connectivity matrix amacrine bipolar
	  Spectrum sigma_th;//Theoretical eigenvalues of the connectivity matrix bipolar amacrine
	  
	  ComputeTheoreticalModesLaplacian1D(p,&kappa_th,&sigma_th,Psi,Psi_transpose);
	  ComputeTheoreticalSpectrumL(kappa_th,sigma_th,Psi,Psi_transpose,&p);
	  
	  //Checking unstability and oscillations.
	  //Code
	  //  Code 0. All eigenvalues are stable real  (Stable Real, S.R.)
	  //  Code n. Mixed stable regime: there are n complex eigenvalues (Stable Mixed, S.M.)
	  //  Code L. All eigenvalues are complex ((Stable Complex, S.C.)
	  
	  gsl_complex lambdab={0,0};
	  double lambdab_r=0,lambdab_i=0;
	  int code=0;
	  
	  std::list <int> idx_real; //List of real modes indices. 
	  std::list <int> idx_complex; //List of  complex modes indices 
	  int nc=idx_complex.size();//Number of complex eigenvalues
	  
	  for (int n=1;n<=p.L;n++){//Loop on modes
	    //modes rhop
	    beta=n-1;
	    lambdab=gsl_vector_complex_get(p.lambda_th,beta);
	    lambdab_r=GSL_REAL(lambdab);
	    lambdab_i=GSL_IMAG(lambdab);
	    if (lambdab_r<0){//Slow mode is stable
	      if (fabs(lambdab_i)<1E-6){//Slow mode is stable not oscillating 
		idx_real.push_back(beta);
	      }
	      else{//Slow mode is stable and oscillating
		idx_complex.push_back(beta);
	      }
	    }//End of slow mode is stable
	    else{//Slow mode is unstable. This shouldn't happen in the GammaAB=GammaBA case.
	      printf("Mode %d is unstable. This shouldn't happen in the GammaAB=GammaBA case. Aborting \n",n);
	      exit(0);
	    }
	    
	    //modes rhom
	    beta=n-1+p.L;
	    lambdab=gsl_vector_complex_get(p.lambda_th,beta);
	    lambdab_r=GSL_REAL(lambdab);
	    lambdab_i=GSL_IMAG(lambdab);
	    if (lambdab_r<0){//Slow mode is stable
	      if (fabs(lambdab_i)<1E-6){//Slow mode is stable not oscillating 
		idx_real.push_back(beta);
	      }
	      else{//Slow mode is stable and oscillating
		idx_complex.push_back(beta);
	      }
	    }//End of slow mode is stable
	    else{//Slow mode is unstable. This shouldn't happen in the Laplacian case.
	      printf("Mode %d is unstable. This shouldn't happen. Aborting \n",n);
	      exit(0);
	    }
	  }//End of loop on modes
	  
	  
	  //Identifying the code map in the plane w,tau
	  int dsR=idx_real.size(),dsC=idx_complex.size();	  
	  code =idx_complex.size();
	  fpMap=fopen(chaineMap,"a");//Save line by line
	  fprintf(fpMap,"%lg\t%lg\t%d\t%d",r,s,code,periods.size());
	  for (int k=0;k<periods.size();k++){
	    fprintf(fpMap,"\t%lg\t%lg",1.0/periods[k].f,1.0/periods[k].width);
	  }
	  fprintf(fpMap,"\n");
	  fclose(fpMap);
	  //exit(0);
	  My_gsl_matrix_free(Psi);My_gsl_matrix_free(Psi_transpose);
	}//End of loop on s=-wab/wba
	//exit(0);
      }//End of loop on r = tauA/tauB    
    }//End of GammaAB=GammaBA
    else if (p.BtoA==0){//one BCell connects a unique ACell
      int nslot=9;//=> Each step is multiplied by $\sqrt[nslot]{2}$
      double r=0,rmin=0.999/(1<<4),rmax=1<<7,dr=exp(log(2.)/nslot);//0.999 to avoid having r=1 => \tau = +infinity
      double mu=0,mumin=1.0/(1<<9),mumax=1<<12,dmu=exp(log(2.)/nslot);
      sprintf(InFile,"_N%d",p.N);
      sprintf(chaineMap,"Trajectories/StabilityMap_MonoConnection_vs_r%s",InFile);
      fpMap=fopen(chaineMap,"w");fclose(fpMap);
      
      for (r=rmin;r<=rmax;r*=dr){//Loop on r = tauA/tauB
	printf("r=%lg\n",r);
	/** Tuning parameters **/
	p.tauA=r*p.tauB;
	p.unsurtauA=1.0/p.tauA;
	p.unsurtau=p.unsurtauA-p.unsurtauB;
	p.unsurtauAB=p.unsurtauA+p.unsurtauB;
	if ((fabs(p.unsurtau)>1E-10)) p.tau=1.0/p.unsurtau;//printf("tau=%lg\n",p.tau);//exit(0);
	p.tau2=pow(p.tau,2);
	p.tauAB=1.0/p.unsurtauAB;//printf("tauAB=%lg\n",p.tauAB);//exit(0);
	
	fpMap=fopen(chaineMap,"a");//Save line by line
	for (mu=mumin;mu<=mumax;mu*=dmu){//Loop on mu
	  //printf("\tmu=%lg\n",mu);
	  p.mu=mu;
	  p.wab=-mu/(p.tau2*p.wba);
	  
	  /** Generating the network and computing the transfert operator **/
	  //printf("\n--------------------\n\t\tGenerating the network and computing the transfert operator\n--------------------\n");
	  init_Network_Laplacian(&p,1);  
	  
	  /*** Computing eigenmodes and eigenvalues **/
	  //printf("\n\tComputing the theoretical RF of cell %d \n",alphaBC);
	  //printf("\n\t\t--------------------\n\t\tComputing eigenmodes and eigenvalues\n\t\t--------------------\n");
	  gsl_matrix *Psi=gsl_matrix_calloc(p.L,p.L),*Psi_transpose=gsl_matrix_calloc(p.L,p.L);
	  Spectrum kappa_th;//Theoretical eigenvalues of the connectivity matrix amacrine bipolar
	  Spectrum sigma_th;//Theoretical eigenvalues of the connectivity matrix bipolar amacrine
	  
	  ComputeTheoreticalModesLaplacian1D(p,&kappa_th,&sigma_th,Psi,Psi_transpose);
	  ComputeTheoreticalSpectrumL(kappa_th,sigma_th,Psi,Psi_transpose,&p);
	  
	  //Checking unstability and oscillations.
	  //Code
	  //  Code 0. All eigenvalues are stable real  (Stable Real, S.R.)
	  //  Code 1. All potentially complex stable eigenvalues are complex (Stable Complex, S.C.)
	  //  Code 2. Mixed stable regime (Stable Mixed, S.M.)
	  //  Code 3. There are some stable eigenvalues (U.M.)
	  //  Code 4. All potentially unstable eigenvalues are unstable (U.R.)
	  
	  gsl_complex lambdab={0,0};
	  double lambdab_r=0,lambdab_i=0;
	  int code=0;
	  
	  std::list <int> idx_slow_real; //List of slow real modes indices. Slow modes are the modes n=1 ..L/2 in the Laplacian case.
	  std::list <int> idx_slow_complex; //List of slow complex modes indices 
	  std::list <int> idx_fast_stable; //List of fast stable  modes indices. Fast modes are the modes ,=L/2+1 ... L in the Laplacian case
	  std::list <int> idx_fast_unstable; //List of fast unstable  modes indices. 
	  
	  for (int n=1;n<=p.L/2;n++){//Loop on slow modes
	    //modes rhop
	    beta=n-1;
	    lambdab=gsl_vector_complex_get(p.lambda_th,beta);
	    lambdab_r=GSL_REAL(lambdab);
	    lambdab_i=GSL_IMAG(lambdab);
	    if (lambdab_r<0){//Slow mode is stable
	      if (fabs(lambdab_i)<1E-6){//Slow mode is stable not oscillating 
		idx_slow_real.push_back(beta);
	      }
	      else{//Slow mode is stable and oscillating
		idx_slow_complex.push_back(beta);
	      }
	    }//End of slow mode is stable
	    else{//Slow mode is unstable. This shouldn't happen in the Laplacian case.
	      printf("Mode %d is unstable. This shouldn't happen in the Laplacian case. Aborting \n",n);
	      exit(0);
	    }
	    
	    //modes rhom
	    beta=n-1+p.L;
	    lambdab=gsl_vector_complex_get(p.lambda_th,beta);
	    lambdab_r=GSL_REAL(lambdab);
	    lambdab_i=GSL_IMAG(lambdab);
	    if (lambdab_r<0){//Slow mode is stable
	      if (fabs(lambdab_i)<1E-6){//Slow mode is stable not oscillating 
		idx_slow_real.push_back(beta);
	      }
	      else{//Slow mode is stable and oscillating
		idx_slow_complex.push_back(beta);
	      }
	    }//End of slow mode is stable
	    else{//Slow mode is unstable. This shouldn't happen in the Laplacian case
	      printf("Mode %d is unstable. This shouldn't happen in the Laplacian case. Aborting \n",n);
	      exit(0);
	    }
	  }//End of loop on slow modes
	  
	  for (int n=p.L/2+1;n<=p.L;n++){//Loop on fast modes
	    //modes rhop
	    beta=n-1;
	    lambdab=gsl_vector_complex_get(p.lambda_th,beta);
	    lambdab_r=GSL_REAL(lambdab);
	    lambdab_i=GSL_IMAG(lambdab);
	    if (fabs(lambdab_i)>1E-6) {//Fast mode is complex. This shouldn't happen in the Laplacian case.
	      printf("Mode %d is complex. This shouldn't happen in the Laplacian case. Aborting \n",n);
	      exit(0);
	    }
	    else{//Fast mode is real
	      if (lambdab_r<0){//Fast mode is stable
		idx_fast_stable.push_back(beta);
	      }
	      else{//Fast mode is unstable
		idx_fast_unstable.push_back(beta);
	      }
	    }//End of fast mode is real
	    
	    //modes rhom
	    beta=n-1+p.N;
	    lambdab=gsl_vector_complex_get(p.lambda_th,beta);
	    lambdab_r=GSL_REAL(lambdab);
	    lambdab_i=GSL_IMAG(lambdab);
	    if (fabs(lambdab_i)>1E-6) {//Fast mode is complex. This shouldn't happen in the Laplacian case.
	      printf("Mode %d is complex. This shouldn't happen in the Laplacian case. Aborting \n",n);
	      exit(0);
	    }
	    else{//Fast mode is real
	      if (lambdab_r<0){//Fast mode is stable
		idx_fast_stable.push_back(beta);
	      }
	      else{//Fast mode is unstable
		idx_fast_unstable.push_back(beta);
	      }
	    }//End of fast mode is real
	    
	  } //End of loop on fast modes
	  
	  //Identifying the code map in the plane w,tau
	  int dsR=idx_slow_real.size(),dsC=idx_slow_complex.size(),dfS=idx_fast_stable.size(),dfU=idx_fast_unstable.size();
	  //printf("\t\t\tdsR=%d\tdsC=%d\tdfS=%d\tdfU%d\n",dsR,dsC,dfS,dfU);
	  
	  if (dsR==p.L){//All slow modes are real
	    if (dfS==p.L){//All fast modes are stable => Code 0 (sRfS)
	      code=0;
	      fprintf(fpMap,"%lg\t%lg\t%d\n",r,p.mu,code);
	    }//End of all fast modes are stable
	    else if (dfU==p.L/2){//All possible unstable fast modes are unstable=> Code 2 (sRfU)
	      code=2;
	      fprintf(fpMap,"%lg\t%lg\t%d\n",r,p.mu,code);
	    }//End of all fast modes are unstable
	    else {//Mixed regime => Code 1 (sRfm)
	      code =1;
	      fprintf(fpMap,"%lg\t%lg\t%d\n",r,p.mu,code);
	      // fprintf(fpMap,"%lg\t%lg\t%lg\t%d\t%d\n",w*weight_factor,p.tauA,p.tau,code,dfU);
	    }
	  }//End of all slow modes are real
	  else if (dsC==p.L){//All slow modes are complex
	    if (dfS==p.L){//All fast modes are stable => Code 3 (sCfS)
	      code=3;
	      fprintf(fpMap,"%lg\t%lg\t%d\n",r,p.mu,code);
	    }//End of all fast modes are stable
	    else if (dfU==p.L/2){//All possible unstable fast modes are unstable=> Code 5 (sCfU)
	      code=5;
	      fprintf(fpMap,"%lg\t%lg\t%d\n",r,p.mu,code);
	    }//End of all fast modes are unstable
	    else {//Mixed regime => Code 5 (sCfm)
	      code =4;
	      fprintf(fpMap,"%lg\t%lg\t%d\n",r,p.mu,code);
	      //    fprintf(fpMap,"%lg\t%lg\t%lg\t%d\t%d\n",w*weight_factor,p.tauA,p.tau,code,dfU);
	    }
	  }//End of all slow modes are complex
	  else{//Mixed Slow case
	    if (dfS==p.L){//All fast modes are stable => Code 6 (smfS)
	      code=6;
	      fprintf(fpMap,"%lg\t%lg\t%d\n",r,p.mu,code);
	    }//End of all fast modes are stable
	    else if (dfU==p.L/2){//All possible unstable fast modes are unstable=> Code 8 (smfU)
	      code=8;
	      fprintf(fpMap,"%lg\t%lg\t%d\n",r,p.mu,code);
	    }//End of all fast modes are unstable
	    else {//Mixed regime => Code 7 (smfm)
	      code =7;
	      fprintf(fpMap,"%lg\t%lg\t%d\n",r,p.mu,code);
	      //  fprintf(fpMap,"%lg\t%lg\t%lg\t%d\n",w*weight_factor,p.tauA,p.tau,code);
	      //	    fprintf(fpMap,"%lg\t%lg\t%lg\t%d\t%d\n",w*weight_factor,p.tauA,p.tau,code,dfU);
	    }
	  }//End of mixed slow case
	  My_gsl_matrix_free(Psi);My_gsl_matrix_free(Psi_transpose);
	}//End of loop on mu
	fclose(fpMap);
	exit(0);
      }//End of loop on r = tauA/tauB
    }//one BCell connects a unique ACell
  }//End of drawing the map


  if (1){//Plotting the RF of selected places in the map
    //string PlotMod="Panel";
    string PlotMod="Separated";//This can be Panel or Separated. If Separated one plots different figures for the timeRF, the space RF, and the power spectrum. If Panel, all figures are collapsed on a unique panel. 
    char GnuName[1000]="",PlotName[1000]="",LocExec[1000],Title[1000];
    
    
    if (1){//Points or pathways in the map
      bool LabelsOrPath=false;//True when moving along a pathway case,  false in the Label case

      int n=0;
      int nslot=4;//=> Each step is multiplied by $\sqrt[nslot]{2}$
      double r=0,rmin=0.999/(1<<2),rmax=1<<7,dr=exp(log(2.)/nslot);//0.999 to avoid having r=1 => \tau = +infinity
      double ssmax=0.0,ssmin=0.0,rrmax=0.0,rrmin=0.0,a1=0,b1=0;
      int Pathway=1;
      
      switch(Pathway){
      case 1:{
	//Pathway 1
	ssmax=8.0;ssmin=0.0625;rrmax=16;rrmin=0.25;
	a1=log(ssmax/ssmin)/log(rrmax/rrmin);
	b1=log(ssmax)-a1*log(rrmax);
      }
	break;
      default:{
	printf("This pathway is not allowed\n");
	exit(0);
      }
      }
      
      int count=1000;
      int n_peaks=0;
      char PowerSpectrum[1000];
      double max_range=0,s=0;
      //char StorePeriods[1000];
      //sprintf(StorePeriods,"%s/Map/Periods_Pathway%d",Gene.c_str(),Pathway);//printf("%s\n",StorePeriods);exit(0);
      //FILE *fstore;//=fopen(StorePeriods,"w");fclose(fstore);//exit(0);
      //RS RSvalues[] = {{0.25,4},{12,1.21},{32,60},{1.3,100}}; //List of values for (r,s)
      //char labels[][15]={"A","B","C","D"};
      //RS RSvalues[] = {{32,60}}; //List of values for (r,s)
      //char labels[][15]={"C"};
      //RS RSvalues[] = {{1.3,100}}; //List of values for (r,s)
      //char labels[][15]={"D"};
      RS RSvalues[] = {{0.25,4}}; //List of values for (r,s)
      char labels[][15]={"A"};
       
      for (RS rs : RSvalues) {//Loop on RS value. Labels case. Must be commented in the moving case.
     // for (r=rmin;r<=rmax;r*=dr){//Loop on r. Case moving along a pathway in the map. Must be commented in the label case.
	if (LabelsOrPath==true){//Case moving along a pathway in the map
	  s=exp(a1*log(r)+b1);
	  printf("\n\n\t\tPlotting the RF at place (%g,%g)\n",r,s);//Moving case
	  sprintf(InFile,"_N%d_r%3.3f_s%3.3f",p.N,r,s);//printf("InFile=%s\n",InFile);//Moving case
	}
	else{//Labels case
	  r=rs.r; 
	  s=rs.s;
	  printf("\n\n\t\tPlotting the RF at place %s=(%g,%g)\n",labels[n],rs.r,rs.s);//exit(0);//Label case
	  sprintf(InFile,"_N%d_Label_%s",p.N,labels[n]);//printf("InFile=%s\n",InFile);//Label case
	}
	
	p.tauA=p.tauAref=r*p.tauB;
	p.wab=-s*p.wba;printf("\t\t\twab=%g\n",p.wab);
	if (strcmp(labels[n],"C")==0) {p.wbg*=4;}
	if (strcmp(labels[n],"D")==0) {p.wbg*=10;}

	UpdateParams(&p);
	init_Network_Laplacian(&p,1);  
	
	gsl_vector_complex* SumPinvbgKSg=gsl_vector_complex_calloc(dim);//Auxilary used for computation
	gsl_vector_complex* SumPinvbgKSg_CNOA=gsl_vector_complex_calloc(dim);//Auxilary used for computation
	sprintf(chaineT,"Trajectories/Cell_Time%s",InFile);//printf("chaineT=%s\n",chaineT);
	sprintf(chaineSp,"Trajectories/Cell_Space%s",InFile);//printf("chaineS=%s\n",chaineS);
	sprintf(chaineTh,"Trajectories/Theoretical_Cell%s",InFile);//printf("chaineTh=%s\n",chaineTh);//exit(0);
	
	sprintf(chaineExp,"void");//printf("chaineExp=%s\n",chaineExp);exit(0);
	int      NumPoint=1<<15;//printf("NumPoint=%d\n",NumPoint);exit(0);//Pour FFT.
	p.tmax=1<<10;
	p.tmin=0;
	p.dt=(float)(p.tmax/(double)NumPoint);//Pour FFT.
	//printf("p.dt=%g\n",p.dt);exit(0);
	double sample_rate=8*p.dt;
	double t_RF=600;
	ComputeAndStoreSpaceRF(iGC,p,chaineT,chaineSp,chaineTh,f,fS,SumPinvbgKSg,SumPinvbgKSg_CNOA);
	
	//Computing the FFT and the period of the time RF
	std::vector<Peaks_fft> periods;
	if (LabelsOrPath==true){//Case moving along a pathway in the map
	  sprintf(PowerSpectrum,"%sMap/%d_PowerSpectrum_RF%s",chaineDir,count,InFile);//printf("%s\n",PowerSpectrum);//exit(0);
	  periods=getPeriod(chaineTh,xGC,sample_rate,&n_peaks,PowerSpectrum,&max_range); 
	  sprintf(PowerSpectrum,"%d_PowerSpectrum_RF%s",count,InFile);//printf("%s\n",PowerSpectrum);//exit(0);//Moving case
	}
	else{//Labels case
	  sprintf(PowerSpectrum,"%sMap/PowerSpectrum_RF%s",chaineDir,InFile);//printf("%s\n",PowerSpectrum);//exit(0);//Label case
	  periods=getPeriod(chaineTh,xGC,sample_rate,&n_peaks,PowerSpectrum,&max_range); 
	  sprintf(PowerSpectrum,"PowerSpectrum_RF%s",InFile);//printf("%s\n",PowerSpectrum);//exit(0);//Label case
	}	
	/*fstore=fopen(StorePeriods,"a");
	  printf("\n\t\tThe %d first peaks (periods) in the power spectrum are:\n",n_peaks);
	  fprintf(fstore,"%g\t%g\t",r,s);
	  for (int k=0;k<n_peaks;k++) {
	  fprintf(fstore,"%g\t%g\t",1000*periods[k].f,1000*periods[k].width);
	  if (periods[k].f>0) printf("\t\t\t(f=%lg (Hz), T=%lg (ms), Amp=%lg, width=%lg (Hz) => %lg (ms))\n",1000*periods[k].f,1.0/periods[k].f,periods[k].Amp,1000*periods[k].width,1.0/periods[k].width);
	  else{
	  if (periods[k].width>0)
	  printf("\t\t\t(f=%lg (Hz),T= inf, Amp=%lg, width=%lg (Hz) => %lg (ms))\n",1000*periods[k].f,periods[k].Amp,1000*periods[k].width,1.0/periods[k].width);
	  else 
	  printf("\t\t\t(f=%lg (Hz), T=Inf, Amp=%lg, width=%lg (Hz) => Inf)\n",1000*periods[k].f,periods[k].Amp,1000*periods[k].width);
	  }
	  }
	  fprintf(fstore,"\n");
	  fclose(fstore);
	*/
	
	if (strcmp(PlotMod.c_str(),"Separated")==0){//If Separated one plots different figures for the timeRF, the space RF, and the power spectrum. If Panel, all figures are collapsed on a unique panel. 
	  printf("\t\t\tPlotting separated Figures\n");
	  /* sprintf(GnuName,"RF_Label_%s.gnu",labels[n]);//printf("%s\n",GnuName);//exit(0);
	     char LocExec[1000];
	     sprintf(LocExec,"%sMap/RF_Label_%s.gnu",chaineDir,labels[n]);//printf("%s\n",LocExec);exit(0);
	     sprintf(PlotName,"RF_Label_%s.png",labels[n]);//printf("%s\n",PlotName);exit(0);*/
	     sprintf(GnuName,"RF%s.gnu",InFile);//printf("%s\n",GnuName);//exit(0);
	     sprintf(LocExec,"%sMap/RF%s.gnu",chaineDir,InFile);//printf("%s\n",LocExec);//exit(0);
	     if (LabelsOrPath==true){//Case moving along a pathway in the map
	       sprintf(PlotName,"%d_RF%s.png",count,InFile);//printf("%s\n",PlotName);//exit(0);	//Moving case
	     }
	     else{//Labels case
	       sprintf(PlotName,"%s.png",InFile);//printf("%s\n",PlotName);//exit(0);	//Label case
	     }
	     char Title[1000];sprintf(Title,"r=%3.3f,s=%3.3f",r,s);
	     plot_RFs_2Figs(chaineT,chaineSp,chaineTh,chaineExp,ExpCond,p,LocExec,PlotName,Title,t_RF);
	     if (Gene=="Grik4") chdir("Grik4/Map/");
	     if (Gene=="Scnn1a") chdir("Scnn1a/Map/");
	     //system("pwd\n");
	     sprintf(chaine,"gnuplot %s\n",GnuName);
	     printf("\n\t\tExecuting %s",chaine);
	     system(chaine);
	     /*sprintf(chaine,"rm -f %s\n",GnuName);
	     printf("\n\t\tExecuting %s",chaine);
	     system(chaine);*/
	     chdir("../..");
	     
	     sprintf(GnuName,"FFT%s.gnu",InFile);//printf("%s\n",GnuName);//exit(0);
	     sprintf(LocExec,"%sMap/FFT%s.gnu",chaineDir,InFile);//printf("%s\n",LocExec);//exit(0);    
	     if (LabelsOrPath==true){//Case moving along a pathway in the map
	       sprintf(PlotName,"%d_PowerSpectrum%s.png",count,InFile);//printf("%s\n",PlotName);//exit(0);//Moving case	
	     }
	     else{//Label case
	       sprintf(PlotName,"PowerSpectrum%s.png",InFile);//printf("%s\n",PlotName);//exit(0);//Label case
	     }	
	     
	     plot_Power_Spectrum(PowerSpectrum,periods,LocExec,PlotName,Title,max_range);
	     if (Gene=="Grik4") chdir("Grik4/Map/");
	     if (Gene=="Scnn1a") chdir("Scnn1a/Map/");
	     system("pwd\n");
	     sprintf(chaine,"gnuplot %s\n",GnuName);
	     printf("\n\t\tExecuting %s",chaine);
	     system(chaine);
	     chdir("../..");
	     sprintf(chaine,"rm -f %s\n",LocExec);
	     printf("\n\t\tExecuting %s",chaine);
	     system(chaine);
	     /*sprintf(chaine,"rm -f %sMap/%s\n",chaineDir,PowerSpectrum);
	     printf("\n\t\tExecuting %s",chaine);
	     system(chaine);*/
	}//End of "Separated"
	else if (strcmp(PlotMod.c_str(),"Panel")==0){// If Panel, all figures are collapsed on a unique panel. 
	  printf("\t\t\tPlotting a summary panel\n");
	  sprintf(GnuName,"Panel%s.gnu",InFile);//printf("%s\n",GnuName);//exit(0);
	  sprintf(LocExec,"%sMap/Panel%s.gnu",chaineDir,InFile);//printf("%s\n",LocExec);//exit(0);
	  sprintf(PlotName,"%d_Panel%s.png",count,InFile);//printf("%s\n",PlotName);//exit(0);	
	  sprintf(Title,"r=%3.3f,s=%3.3f",r,s);
	  p.tmax=1<<11;//ms//Pour FFT.
	  char chaineFFT_Th[1000]="";
	  string Cell_Type="ON";
	  plot_Summary_Panel(chaineT,chaineSp,chaineTh,chaineExp,ExpCond,PowerSpectrum,chaineFFT_Th,periods,p,LocExec,PlotName,Title,max_range,t_RF,Cell_Type);
	  if (Gene=="Grik4") chdir("Grik4/Map/");
	  if (Gene=="Scnn1a") chdir("Scnn1a/Map/");
	  //system("pwd\n");
	  sprintf(chaine,"gnuplot %s\n",GnuName);
	  printf("\n\t\tExecuting %s",chaine);
	  system(chaine);
	  sprintf(chaine,"rm -f %s\n",GnuName);
	  printf("\n\t\tExecuting %s",chaine);
	  system(chaine);
	  chdir("../..");
	}//End of "Panel"
	else {
	  printf("The plot modality %s is not defined. Aborting\n",PlotMod.c_str());
	}
	
	if (LabelsOrPath==true){
	  sprintf(chaine,"rm -f %s\n\t\trm -f %s\n\t\trm -f %s\n\n",chaineT,chaineSp,chaineTh);
	  printf("\n\t\tExecuting %s",chaine);
	  system(chaine);
	}
	My_gsl_vector_complex_free(SumPinvbgKSg);
	My_gsl_vector_complex_free(SumPinvbgKSg_CNOA);
	n++;//Label case
	count++;//Moving case
	//exit(0); 
      }//End of loop on RS values
    }//End of points or pathways in the map
  }//End of plotting the RF of selected places in the map

  if (0){//Drawing the map corresponding to the parameters of a specific cell
    int label=23;
    Gene="Scnn1a";
    ExpCond="CTL";
    datafile=Gene+"_temp_STAs_"+ExpCond+".txt";
    printf("Drawing the map corresponding to the parameters of the cell %d => ",label);
    
    //Loading data
    std::vector <Experiment> experiences;
    char chaine_corres[1000]; sprintf(chaine_corres,"%sCells_correspondence_%s",chaineDir,ExpCond.c_str());
    
    double sample_rate=0.033/4.0;//s
    // printf("\n\t\t\033[0;35mThe file %s will be analysed\n\n\033[0m",datafile.c_str());
    GetData(datafile,Gene,&experiences,sample_rate);
    int data_size=experiences.size();
    
    FILE* fcor=fopen(chaine_corres,"r"); 
    int i1=0,i2=0,i3=0;
    char type[3],bid;
    do{
      fscanf(fcor,"%d%d%c%d%s",&i1,&i2,&bid,&i3,&type);
      //printf("i1=%d, i2=%d, bid=%c, i3=%d, type=%s\n",i1,i2,bid,i3,type);
      //    exit(0);
    }
    while (i1!=label);
    string Cell_Type=type;
    sprintf(InFile,"_%d_%d_%s",experiences[label].Cell_id,experiences[label].Exp_id,ExpCond.c_str());//printf("%s\n",InFile);exit(0);
    printf("%s of %s type\n",InFile,Cell_Type.c_str());
    
    //Loading parameters
    params pp=p;
    sprintf(chaineParams,"%sParameters/params_%d_%d_%s",chaineDir,experiences[label].Cell_id,experiences[label].Exp_id,ExpCond.c_str());printf("Reading %s\n",chaineParams);//exit(0);
    if (ReadParams(&pp,chaineParams)==false) exit(0);
    //WriteParams(pp);exit(0);
    UpdateParams(&pp);
    init_Network_Laplacian(&pp,1);  
    
    //Drawing experimental trajectory
    sprintf(chaineExp,"Trajectories/Empirical_Cell%s",InFile);
    printf("\t\tStoring experimental trace in %s\n",chaineExp);
    FILE *fexp=NULL;
    if ((fexp=fopen(chaineExp,"w"))==NULL){printf("The file %s could not be open\n",chaineExp);exit(0);}    
    for (int i=2;i<experiences[label].data.size();i++){
      //printf("%lg\t%lg\n",experiences[label].data[i].t,experiences[label].data[i].Amp);
      fprintf(fexp,"%lg\t%lg\n",experiences[label].data[i].t,experiences[label].data[i].Amp);
    }
    printf("\t\tdone\n");
    fclose(fexp);

    //FFT of the experimental trace
    char PowerSpectrum[1000]="";
    sprintf(PowerSpectrum,"%sMap/Exp_PowerSpectrum_RF%s",chaineDir,InFile);//printf("%s\n",PowerSpectrum);//exit(0);
    int n_peaks=0;
    double max_range=0;//max_range is the maximal frequency range i.e. the maximal value where the amplitude of the normalized spectrum is larger than zero_PS.
    
    //sprintf(InFile,"_%d_%d_%s",experiences[label].Cell_id,experiences[label].Exp_id,ExpCond.c_str());//printf("%s\n",InFile);exit(0);
    sample_rate=0.033*1000/4.0;//Convert sample rate in ms
    double t_RF=600;//ms
    
    std::vector<Peaks_fft> periods=getPeriod(experiences[label],sample_rate,&n_peaks,PowerSpectrum,&max_range); 
    //exit(0);
    printf("\n\t\tThe %d first peaks (periods) in the power spectrum are:\n",n_peaks);
    for (int k=0;k<n_peaks;k++) {
      if (periods[k].f>0) printf("\t\t\t(f=%lg (Hz), T=%lg (ms), Amp=%lg, width=%lg (Hz) => %lg (ms))\n",1000*periods[k].f,1.0/periods[k].f,periods[k].Amp,1000*periods[k].width,1.0/periods[k].width);
      else
	printf("\t\t\t(f=%lg (Hz), int, Amp=%lg, width=%lg (Hz) => %lg (ms)) \n",1000*periods[k].f,periods[k].Amp,1000*periods[k].width,1.0/periods[k].width);
    }
    char chaineFFT_Th[1000]="";
    sprintf(PowerSpectrum,"Exp_PowerSpectrum_RF%s",InFile);//printf("%s\n",PowerSpectrum);//exit(0);//Label case
    
    //Plot Experimental traces
    sprintf(GnuName,"Exp_Cell_%d_%d_%s.gnu",experiences[label].Cell_id,experiences[label].Exp_id,ExpCond.c_str());//printf("%s\n",GnuName);//exit(0);
    char LocExec[1000];
    sprintf(LocExec,"%sMap/Exp_Cell_%d_%d_%s.gnu",chaineDir,experiences[label].Cell_id,experiences[label].Exp_id,ExpCond.c_str());//printf("%s\n",GnuName);//exit(0);
    sprintf(PlotName,"Exp_Cell_%d_%d_%s",experiences[label].Cell_id,experiences[label].Exp_id,ExpCond.c_str());//printf("%s\n",PlotName);exit(0);

    plot_Experimental_Traces(chaineExp,ExpCond,PowerSpectrum,chaineFFT_Th,periods,pp,LocExec,PlotName,max_range,t_RF,Cell_Type);
    if (Gene=="Grik4") chdir("Grik4/Map/");
    if (Gene=="Scnn1a") chdir("Scnn1a/Map/");
    system("pwd\n");
    sprintf(chaine,"gnuplot %s\n",GnuName);
    printf("\n\n\tExecuting %s",chaine);
    system(chaine);
    chdir("../..");
 
    //RFs in the map
    double ssmax=0.0,ssmin=0.0,rrmax=0.0,rrmin=0.0,a1=0,b1=0;
    int nslot=16;//=> Each step is multiplied by $\sqrt[nslot]{2}$
    double dr=exp(log(2.)/nslot);//0.999 to avoid having r=1 => \tau = +infinity
    
    int Pathway=1;
    
    switch(Pathway){
    case 1:{
      //Pathway 1
      //ssmax=64.0;ssmin=1;rrmax=128;rrmin=0.99;//rrmin=pp.tauA/pp.tauB;//
      ssmax=256.0;ssmin=-pp.wab/pp.wba;rrmax=256;rrmin=pp.tauA/pp.tauB;
      printf("rmin=%g, rmax=%g, smin=%g, smax=%g\n",rrmin,rrmax,ssmin,ssmax);
     a1=log(ssmax/ssmin)/log(rrmax/rrmin);
      b1=log(ssmax)-a1*log(rrmax);
    }
      break;
    default:{
      printf("This pathway is not allowed\n");
      exit(0);
    }
    }
    int count=1000;
    string PlotMod="Panel";
    int      NumPoint=1<<10;//printf("NumPoint=%d\n",NumPoint);exit(0);//Pour FFT.
    pp.tmax=sample_rate*NumPoint;
    pp.tmin=0;
    pp.dt=sample_rate/8;
    double df=1.0/pp.tmax;
    // rrmin=pp.tauA/pp.tauB;

    for (double r=rrmin;r<=rrmax;r*=dr){//Loop on r. Case moving along a pathway in the map. Must be commented in the label case.
      double s=exp(a1*log(r)+b1);
      printf("\n\n\t\tPlotting the RF at place (%g,%g)\n",r,s);//Moving case
      //      sprintf(InFile,"_N%d_r%3.3f_s%3.3f",p.N,r,s);//printf("InFile=%s\n",InFile);//Moving case
      pp.tauA=pp.tauAref=r*pp.tauB;
      pp.wab=-s*pp.wba;printf("\t\t\twab=%g\n",pp.wab);    
      UpdateParams(&pp);
      init_Network_Laplacian(&pp,1);  

      gsl_vector_complex* SumPinvbgKSg=gsl_vector_complex_calloc(dim);//Auxilary used for computation
      gsl_vector_complex* SumPinvbgKSg_CNOA=gsl_vector_complex_calloc(dim);//Auxilary used for computation
      sprintf(chaineT,"Trajectories/Cell_Time%s",InFile);//printf("chaineT=%s\n",chaineT);
      sprintf(chaineSp,"Trajectories/Cell_Space%s",InFile);//printf("chaineS=%s\n",chaineS);
      sprintf(chaineTh,"Trajectories/Theoretical_Cell%s",InFile);//printf("chaineTh=%s\n",chaineTh);//exit(0);
      
      //sprintf(chaineExp,"void");//printf("chaineExp=%s\n",chaineExp);exit(0);

      //Computes the theoretical RF
      ComputeAndStoreSpaceRF(iGC,pp,chaineT,chaineSp,chaineTh,f,fS,SumPinvbgKSg,SumPinvbgKSg_CNOA);
 
     //Computes the theoretical power spectrum
      gsl_complex kg={0,0};//Computes the RF of a GCell alpha embedded in the BCell, ACell, GCell network
      int NumPoint=1<<13,n=NumPoint;//printf("NumPoint=%d\n",NumPoint);exit(0);//Pour FFT.
      pp.tmax=n*pp.dt;
      pp.tmin=0;
      double sample_rate=pp.dt;
      char chaineFFT_Th[1000]="";
      double df=0.00005;
      gsl_matrix *Psi=gsl_matrix_calloc(pp.L,pp.L),*Psi_transpose=gsl_matrix_calloc(pp.L,pp.L);
      Spectrum kappa_th;//Theoretical eigenvalues of the connectivity matrix amacrine bipolar
      Spectrum sigma_th;//Theoretical eigenvalues of the connectivity matrix bipolar amacrine
      ComputeTheoreticalModesLaplacian1D(pp,&kappa_th,&sigma_th,Psi,Psi_transpose);
      ComputeTheoreticalSpectrumL(kappa_th,sigma_th,Psi,Psi_transpose,&pp);
      
      KG_CNOA(pp,SumPinvbgKSg_CNOA);//CNO A contribution
      KSG(xGC,yGC,pp, SumPinvbgKSg);//KSG contains the value of the spatial RF, of each eigenmode beta, at point x,y.
      
      double maxi=0;
      vector <Event> Spectre;
      
      //Storing the spectrum in Spectre
      for (double nu=-0.2;nu<0.2;nu+=df){
	gsl_complex z=KG_hat(alphaGC,xGC,0,nu,pp,SumPinvbgKSg,SumPinvbgKSg_CNOA);
	if (abs(nu)<df){
	  double omega=dpi*nu;
	  gsl_complex Dirac=gsl_complex_div(gsl_complex_add_real(gsl_complex_exp({0,dpi*n*sample_rate}),-1),gsl_complex_add_real(gsl_complex_exp({0,dpi*sample_rate}),-1));
	  //printf("Dirac=(%g,%g) \n",pp.b0*GSL_REAL(Dirac),pp.b0*GSL_IMAG(Dirac));
	  z=gsl_complex_add(z,Dirac); //Add Discrete Dirac in the power spectrum
	}
	Event point={nu,gsl_complex_abs(z)};
	maxi=max(maxi,point.Amp);
	Spectre.push_back(point);
      }
      
      //Storing the normalized spectrum in a file
      sprintf(chaineFFT_Th,"%sMap/FFT_Th_RF%s",chaineDir,InFile);//printf("%s\n",FFT_Th_RF);//exit(0);
      FILE *fp1=fopen(chaineFFT_Th,"w");
      for (int ii=0;ii<Spectre.size();ii++){	  
	fprintf(fp1,"%lg\t%lg\n",1000*Spectre[ii].t,Spectre[ii].Amp/maxi);
      }
      fclose(fp1);
      sprintf(chaineFFT_Th,"FFT_Th_RF%s",InFile);//printf("%s\n",FFT_Th_RF);//exit(0);

      //Plots the panel
      sprintf(GnuName,"Panel%s.gnu",InFile);//printf("%s\n",GnuName);//exit(0);
      sprintf(LocExec,"%sMap/Panel%s.gnu",chaineDir,InFile);//printf("%s\n",LocExec);//exit(0);
      sprintf(PlotName,"%d_Panel%s.png",count,InFile);//printf("%s\n",PlotName);//exit(0);	
      sprintf(Title,"r=%3.3f,s=%3.3f",r,s);
      plot_Summary_Panel(chaineT,chaineSp,chaineTh,chaineExp,ExpCond,PowerSpectrum,chaineFFT_Th,periods,p,LocExec,PlotName,Title,max_range,t_RF,Cell_Type);
      if (Gene=="Grik4") chdir("Grik4/Map/");
      if (Gene=="Scnn1a") chdir("Scnn1a/Map/");
      system("pwd\n");
      sprintf(chaine,"gnuplot %s\n",GnuName);
      printf("\n\t\tExecuting %s",chaine);
      system(chaine);
      sprintf(chaine,"rm -f %s\n",GnuName);
      printf("\n\t\tExecuting %s",chaine);
      system(chaine);
      sprintf(chaine,"rm -f %s\n\t\trm -f %s\n\t\trm -f %s\n\n",chaineT,chaineSp,chaineTh);
      printf("\n\t\tExecuting %s",chaine);
      system(chaine);
      
      chdir("../..");

      count++;
      My_gsl_vector_complex_free(SumPinvbgKSg);
      My_gsl_vector_complex_free(SumPinvbgKSg_CNOA);
      //exit(0);
}

  }//End of drawing the map corresponding to the parameters of a specific cell
  

 return EXIT_SUCCESS;
}
