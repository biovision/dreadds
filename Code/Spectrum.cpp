/** Spectral computations **/

#include "Spectrum.h"

//Compute the eigenmodes of a Laplacian in 1D with a lattice of size L, 0 boundary conditions
//Psi and Psi_transpose must be allocated before
void ComputeTheoreticalModesLaplacian1D( params p,Spectrum *kappa_th,Spectrum *sigma_th,gsl_matrix *Psi,gsl_matrix *Psi_transpose){

  //printf("\n\tComputing the theoretical spectrum of the 1D Laplacian\n");
  if ((Psi==NULL)||(Psi_transpose==NULL)) {
    printf("Psi and Psi_transpose must be allocated first\n");
    exit(0);
  }

    //Theoretical eigenvectors of WAB in the Laplacian case

  int L=p.L;
  int n=0;
  while (n<L){
    double kappa=2*cos(M_PI*(double)(n+1)/(L+1));
    kappa_th->push_back(kappa);
      n++;
    }  

    //Theoretical eigenvectors of WBA 
    n=0;
    if (p.BtoA==0){
      while (n<L){
	sigma_th->push_back(1.0); //Works only if WBA=Id
	n++;
      }
    }
    else {
      while (n<L){
	sigma_th->push_back((*kappa_th)[n]); //Works only if WBA=Id
	n++;
      }
    }
  double normepsi=sqrt(2.0/(L+1)),pisurL=M_PI/(double)(L+1);
  
  for (int i=1;i<=L;i++){
    for (int n=1;n<=L;n++){
      double psi_in=normepsi*sin(n*i*pisurL);
      gsl_matrix_set(Psi,i-1,n-1,psi_in);
      gsl_matrix_set(Psi_transpose,n-1,i-1,psi_in);
    }
  }
  
  //Checking the orthonormality of Psi.
  bool check_orthog=false;
  
  for (int i=0;i<L;i++){
    for (int j=0;j<L;j++){
      double prod_ij=0;
      for (int k=0;k<L;k++){
	double psi_ik=gsl_matrix_get(Psi,i,k);
	double psi_transpose_kj=gsl_matrix_get(Psi_transpose,k,j);
	prod_ij+=psi_ik*psi_transpose_kj;
      }
      if (i!=j)
	check_orthog=(fabs(prod_ij)<1E-10);
      else
	check_orthog=(fabs(prod_ij-1)<1E-10);
      if (check_orthog==false){
	printf("\tOrthogonality violated for i=%d, j=%d prod_ij=%lg\n",i,j,prod_ij);
	exit(0);
      }
      // printf("%3.2lg\t",prod_ij);
    }
    //printf("\n");
  }
  //printf("\n\tOrthogonality of psi checked\n");
}

#define resol 1E-8

/**Computing the theoretical spectrum and eigenvectors of cL (without the activities). Eigenvalues are stored in p.lambda_th. Eigenvectors are stored in p.P. The inverse of P, p.Pinv, is computed too.
***/
void  ComputeTheoreticalSpectrumL(Spectrum kappa_th,Spectrum sigma_th,gsl_matrix *Psi,gsl_matrix *Psi_transpose,params *p){
 
  //printf("\n----------\nComputing the theoretical spectrum and eigenvectors of cL\n-----------\n\n");

  //Checks  
  if (kappa_th.size()==0) {printf("kappa_th must be computed first, aborting\n");exit(0);}
  if (sigma_th.size()==0) {printf("sigma_th must be computed first, aborting\n");exit(0);}
  if (Psi==NULL)  {printf("Psi must be initialized first, aborting\n");exit(0);}
  if (Psi_transpose==NULL)  {printf("Psi_transpose must be initialized first, aborting\n");exit(0);}
  /*if (p->lambda_th==NULL)  {printf("lambda_th must be initialized first, aborting\n");exit(0);}
  if (p->P==NULL)  {printf("P must be initialized first, aborting\n");exit(0);}
  if (p->Pinv==NULL)  {printf("P must be initialized first, aborting\n");exit(0);}*/

  //Auxilary variables
  gsl_complex Pab={0,0}, PSIin={0,0},rhop={0,0},rhom={0,0},polyp={0,0},polym={0,0},sigmanp={0,0},sigmanm={0,0},aux1={0,0};//Complex auxilaries
  double re_lambda_th_p=0,im_lambda_th_p=0,re_lambda_th_m=0,im_lambda_th_m=0,kappan=0,sigman=0,Delta=0,psi_ab=0;//Real auxilaries
  gsl_complex lambda_th_n={0,0};
  int N=p->N;
  int dim=3*N;//Activities are not considered here
  double normePp_n=0,normePm_n=0;

  //Allocating eigenvectors and eigenvalues
  /*p->lambda_th=gsl_vector_complex_calloc(dim); //Theoretical eigenvalues of the full linear operator cL
    p->P=gsl_matrix_complex_calloc(dim,dim);//Theoretical eigenvectors
    p->Pinv=gsl_matrix_complex_calloc(dim,dim);//Inverse of P*/
 
 //Allocating auxilary vectors and matrices 
 gsl_matrix_complex *J=gsl_matrix_complex_calloc(N,N);//Matrix \Phi^+Q-\Phi^-S
  gsl_matrix_complex *K=gsl_matrix_complex_calloc(N,N);//Matrix (\Phi^+-\Phi^-)R
   gsl_matrix_complex *Mp=gsl_matrix_complex_calloc(N,N);//Matrix (WBG +WAG \rho^+)/(lambdap+1/tauG)
  gsl_matrix_complex *Mm=gsl_matrix_complex_calloc(N,N);//Matrix (WBG +WAG \rho^-)/(lambdam+1/tauG)
  gsl_matrix_complex *N_beta=gsl_matrix_complex_calloc(N,N);
  gsl_matrix_complex *U_beta=gsl_matrix_complex_calloc(N,N);
  gsl_vector_complex *Phip=gsl_vector_complex_calloc(N);//Vector $\Phi^+$
  gsl_vector_complex *Phim=gsl_vector_complex_calloc(N);//Vector $\Phi^_$
  //gsl_vector_complex *Rhop=gsl_vector_complex_calloc(N);//Vector of $\rho^+_n$
  //gsl_vector_complex *Rhom=gsl_vector_complex_calloc(N);//Vector of $\rho^-_n$
  gsl_vector_complex *nu=gsl_vector_complex_calloc(N);//Vector of $\rho^+_n- \rho^-_n$
  gsl_vector *normePp=gsl_vector_calloc(N);
  gsl_vector *normePm=gsl_vector_calloc(N);

  //Computing theoretical eigenvalues
  int n=0;
  int alpha=0,beta=0;

  while (n<N){//Scaning the eigenvalues and eigenmodes of WBA to compute the eigenvalues and eigenmodes of L

    kappan=kappa_th[n];
    sigman=sigma_th[n];
    if (fabs(p->unsurtau)> resol){//Beginning of 1/tau neq 0
      Delta=1.0-4*p->mu*kappan*sigman;
      
      if (Delta>0){//Real eigenvalue
	re_lambda_th_m=(-p->unsurtauAB+p->unsurtau*sqrt(Delta))/2.0;
	im_lambda_th_m=0;
	re_lambda_th_p=(-p->unsurtauAB-p->unsurtau*sqrt(Delta))/2.0;
	im_lambda_th_p=0;
        
	//Auxilaries for eigenvectors
	if (fabs(kappan)>resol){//kappan is non zero
	  rhom=gsl_complex_rect((1-sqrt(Delta))/(2*p->tau*(-p->wab)*kappan),0);
	  rhop=gsl_complex_rect((1+sqrt(Delta))/(2*p->tau*(-p->wab)*kappan),0);
	}
	else{//kappan is zero
	  rhom=gsl_complex_rect(p->wba*p->tau*sigman,0);
	  rhop=rhom;
	}
        
      }//End of real eigenvalue
      else{//Complex eigenvalue
	// printf("Eigenvalue %d is complex\t",n);
	re_lambda_th_m=-p->unsurtauAB/2.0;
	im_lambda_th_m=p->unsurtau*sqrt(-Delta)/2.0;
	re_lambda_th_p=-p->unsurtauAB/2.0;
	im_lambda_th_p=-p->unsurtau*sqrt(-Delta)/2.0;
        
	//Auxilaries for eigenvectors
	if (fabs(kappan)>resol){//kappan is non zero
	  rhom=gsl_complex_rect(1.0/(2*p->tau*(-p->wab)*kappan),-sqrt(-Delta)/(2*p->tau*(-p->wab)*kappan));
	  rhop=gsl_complex_rect(1.0/(2*p->tau*(-p->wab)*kappan),sqrt(-Delta)/(2*p->tau*(-p->wab)*kappan));
	}
	else{//kappan is zero
	  rhom=gsl_complex_rect(p->wba*p->tau*sigman,0);
	  rhop=rhom;
	}
      }//End of complex eigenvalue
    }//End of 1/tau neq 0
    else{//Beginning of 1//tau =0
      if (sigman==0) {printf("In ComputeTheoretical Spectrum: there is something wrong here, sigman=%lg, aborting\n",sigman);exit(0);}
      Delta=-(-p->wab)*(-p->wab)*kappan*sigman;
      
      if (Delta>0){//Real eigenvalue
	re_lambda_th_m=-p->unsurtauA+sqrt(Delta);
	im_lambda_th_m=0;
	re_lambda_th_p=-p->unsurtauA-sqrt(Delta);
	im_lambda_th_p=0;
      }//End of real eigenvalue
      else{//Complex eigenvalue
	re_lambda_th_m=-p->unsurtauA;
	im_lambda_th_m=sqrt(-Delta);
	re_lambda_th_p=-p->unsurtauA;
	im_lambda_th_p=-sqrt(-Delta);
      }//End of complex eigenvalue
    }//End of 1//tau =0

    //Eigenvalues lamdabp-> Storage and test
    GSL_REAL(lambda_th_n)=re_lambda_th_p;//lambda+
    GSL_IMAG(lambda_th_n)=im_lambda_th_p;
    gsl_vector_complex_set(p->lambda_th,n,lambda_th_n);
    gsl_vector_complex_set(p->rhop,n,rhop);
    gsl_vector_complex_set(p->rhom,n,rhom);
    gsl_vector_complex_set(nu,n,gsl_complex_sub(rhop,rhom));

    bool test=true,checks=true;
    gsl_complex lhs,rhs;
    
    if (checks){//check lambdap eigenvalues
      //Check if rhop is a zero of the characteristic polynomial.
      polyp=gsl_complex_add_real(gsl_complex_add(gsl_complex_mul_real(gsl_complex_pow_real(rhop,2),(-p->wab)*kappan),gsl_complex_mul_real(rhop,-p->unsurtau)),p->wba*sigman);
      if ((gsl_complex_abs(polyp)>resol)) {
	printf("Solution rhop for n=%d is not OK\n kappan=%lg,rhop=%lg+i%lg,polyp=%lg+i%lg\n",n,kappan,GSL_REAL(rhop),GSL_IMAG(rhop),GSL_REAL(polyp),GSL_IMAG(polyp));
	exit(0);
      }
      else {//printf("Solution rhop for n=%d is OK\n",n);
      }
      
      //Checking -1/tauB - wm rhonp kappan=lambdabn
      lhs=rhop;//printf("lhs=%lg + i %lg\n",GSL_REAL(lhs),GSL_IMAG(lhs)); printf("p->wm*kappan=%lg\n",(-p->wab)*kappan);
      lhs=gsl_complex_mul_real(lhs,(-p->wab)*kappan);//printf("lhs=%lg + i %lg\n",GSL_REAL(lhs),GSL_IMAG(lhs));
      GSL_REAL(lhs)=-p->unsurtauB-GSL_REAL(lhs);
      GSL_IMAG(lhs)=-GSL_IMAG(lhs);
      
      rhs=lambda_th_n;
      
      test=(gsl_complex_abs(gsl_complex_sub(lhs,rhs))<resol);
      if (test==false) {printf("\nEquation -1/tauB - w- rhonp kappan=lambdabn failed at n=%d: lhs=%lg + i%lg != rhs=%lg+i%lg\n",n,GSL_REAL(lhs),GSL_IMAG(lhs),GSL_REAL(rhs),GSL_IMAG(rhs));
	exit(0);
      }
      
      //Checking w^+*sigman - rhop tauA=rhop*lambdabn
      lhs=rhop;
      lhs=gsl_complex_mul_real(lhs,-p->unsurtauA);
      GSL_REAL(lhs)=p->wba*sigman+GSL_REAL(lhs);
      
      rhs=gsl_complex_mul(rhop,lambda_th_n);
      test=(gsl_complex_abs(gsl_complex_sub(lhs,rhs))<resol);
      if (test==false) {
	printf("\nEquation w+ - rhop tauA=rhop*lambdabn failed at n=%d: lhs=%lg + i%lg != rhs=%lg+i%lg\n",n,GSL_REAL(lhs),GSL_IMAG(lhs),GSL_REAL(rhs),GSL_IMAG(rhs));
	exit(0);
      }
      
      /* Gain control case. Not used yet
      //h-sigma/taua=lambdan.sigma
      lhs=sigmanp;
      lhs=gsl_complex_mul_real(lhs,-unsurtaua);
      GSL_REAL(lhs)=p->h+GSL_REAL(lhs);
      
      rhs=gsl_complex_mul(sigmanp,lambda_th_n);
      test=(gsl_complex_abs(gsl_complex_sub(lhs,rhs))<resol);
      if (test==false) {printf("\nh-sigma/taua=lambdan.sigma failed at n=%d: lhs=%lg + i%lg != rhs=%lg+i%lg\n",n,GSL_REAL(lhs),GSL_IMAG(lhs),GSL_REAL(rhs),GSL_IMAG(rhs));
      exit(0);
      }
      */
    }//End of check lambdap eigenvalues

    //lambdam eigenvalues. 
    GSL_REAL(lambda_th_n)=re_lambda_th_m;//lambba-
    GSL_IMAG(lambda_th_n)=im_lambda_th_m;
    gsl_vector_complex_set(p->lambda_th,N+n,lambda_th_n);
    // printf("\nlambda%dm=%lg+i%lg\n",N+n,re_lambda_th_m,im_lambda_th_m);
   /* GSL_REAL(aux1)=unsurtaua+re_lambda_th_m;
    GSL_IMAG(aux1)=im_lambda_th_m;
    aux1=gsl_complex_inverse(aux1);
    sigmanm=gsl_complex_mul_real(aux1,p->h);
    */
    
    if (checks){//check lambdam eigenvalues
      //Check if rhom is a zero of the characteristic polynomial.
      polym=gsl_complex_add_real(gsl_complex_add(gsl_complex_mul_real(gsl_complex_pow_real(rhom,2),(-p->wab)*kappan),gsl_complex_mul_real(rhom,-p->unsurtau)),p->wba*sigman);
      if ((gsl_complex_abs(polym)>resol)) {
	printf("Solution rhom for n=%d is not OK\n kappan=%lg,rhom=%lg+i%lg,polym=%lg+i%lg\n",n,kappan,GSL_REAL(rhom),GSL_IMAG(rhom),GSL_REAL(polym),GSL_IMAG(polym));
	exit(0);
      }
      else {//printf("Solution rhom for n=%d is OK\n",n);
      }
      
      //Checking -1/tauB - wm rhonm kappan=lambdabn
      lhs=rhom;//printf("lhs=%lg + i %lg\n",GSL_REAL(lhs),GSL_IMAG(lhs)); printf("p->wm*kappan=%lg\n",(-p->wab)*kappan);
      lhs=gsl_complex_mul_real(lhs,(-p->wab)*kappan);//printf("lhs=%lg + i %lg\n",GSL_REAL(lhs),GSL_IMAG(lhs));
      GSL_REAL(lhs)=-p->unsurtauB-GSL_REAL(lhs);
      GSL_IMAG(lhs)=-GSL_IMAG(lhs);
      
      rhs=lambda_th_n;
      
      test=(gsl_complex_abs(gsl_complex_sub(lhs,rhs))<resol);
      if (test==false) {printf("\nEquation -1/tauB - w- rhonm kappan=lambdabn failed at n=%d: lhs=%lg + i%lg != rhs=%lg+i%lg\n",n,GSL_REAL(lhs),GSL_IMAG(lhs),GSL_REAL(rhs),GSL_IMAG(rhs));
	exit(0);
      }
      
      //Checking w^+*sigman - rhom tauA=rhom*lambdabn
      lhs=rhom;
      lhs=gsl_complex_mul_real(lhs,-p->unsurtauA);
      GSL_REAL(lhs)=p->wba*sigman+GSL_REAL(lhs);
      
      rhs=gsl_complex_mul(rhom,lambda_th_n);
      test=(gsl_complex_abs(gsl_complex_sub(lhs,rhs))<resol);
      if (test==false) {printf("\nEquation w+ - rhom tauA=rhom*lambdabn failed at n=%d: lhs=%lg + i%lg != rhs=%lg+i%lg\n",n,GSL_REAL(lhs),GSL_IMAG(lhs),GSL_REAL(rhs),GSL_IMAG(rhs));
	exit(0);
      }
      
      /* Gain control case. Not used yet
      //h-sigma/taua=lambdan.sigma
      lhs=sigmanp;
      lhs=gsl_complex_mul_real(lhs,-unsurtaua);
      GSL_REAL(lhs)=p->h+GSL_REAL(lhs);
      
      rhs=gsl_complex_mul(sigmanp,lambda_th_n);
      test=(gsl_complex_abs(gsl_complex_sub(lhs,rhs))<resol);
      if (test==false) {printf("\nh-sigma/taua=lambdan.sigma failed at n=%d: lhs=%lg + i%lg != rhs=%lg+i%lg\n",n,GSL_REAL(lhs),GSL_IMAG(lhs),GSL_REAL(rhs),GSL_IMAG(rhs));
      exit(0);
      }
      */
    }//End of check lambdam eigenvalues

    //Gain control. Not used yet
   /* GSL_REAL(lambda_th_n)=-1.0/p->taua;//-1/taua
    GSL_IMAG(lambda_th_n)=0;
    gsl_vector_complex_set(lambda_th,2*N+n,lambda_th_n);*/

    //GCells eigenvalues
    GSL_REAL(lambda_th_n)=-1.0/p->tauG;//-1/tauG
    GSL_IMAG(lambda_th_n)=0;
    gsl_vector_complex_set(p->lambda_th,2*N+n,lambda_th_n);

    //Computing P. Eigenvectors computation and normalization. 
    //The computation is considered without gain control
  
    //Computing the matrices J and K (actually, used for P^{-1})

    //1) Computing 1/(lambdan^{\pm}+1/tauG)
    sigmanp=gsl_complex_inverse({p->unsurtauG+re_lambda_th_p,im_lambda_th_p});
    sigmanm=gsl_complex_inverse({p->unsurtauG+re_lambda_th_m,im_lambda_th_m});

    //2) Computing the matrices M,Mp,J,K
    beta=n; //beta is used for P, P^{-1} to match theoretical computations
    gsl_complex  J_beta_ag={0,0},K_beta_ag={0,0},Mp_beta_ag={0,0},Mm_beta_ag={0,0},WBGscaled_p_ag={0,0},WBGscaled_m_ag={0,0},WAGscaled_p_ag={0,0},WAGscaled_m_ag={0,0},N_beta_ag={0,0},U_beta_ag={0,0},N_beta_ad={0,0},U_beta_ad={0,0},Pi_beta_dg={0,0},J_ag={0,0},K_ag={0,0};
    gsl_complex nu_beta=gsl_complex_sub(rhop,rhom);
    gsl_complex rhop_beta=rhop;
    gsl_complex rhop_betasurnu_beta=gsl_complex_div(rhop_beta,nu_beta);
    gsl_matrix_complex_set_zero(N_beta);
    gsl_matrix_complex_set_zero(U_beta);

    for (alpha=0;alpha<N;alpha++){//alpha loop
      for (int gama=0;gama<N;gama++){//gama loop
       	//WBG
	double WBG_ag=gsl_matrix_get(p->WBG,alpha,gama);
	WBGscaled_p_ag=gsl_complex_mul({WBG_ag,0},sigmanp);
	WBGscaled_m_ag=gsl_complex_mul({WBG_ag,0},sigmanm);
	
	//WAG
	double WAG_ag=gsl_matrix_get(p->WAG,alpha,gama);
	WAGscaled_p_ag=gsl_complex_mul({WAG_ag,0},sigmanp);
	WAGscaled_m_ag=gsl_complex_mul({WAG_ag,0},sigmanm);
	
	//Mp_beta
	Mp_beta_ag=gsl_complex_add(gsl_complex_mul(rhop,WAGscaled_p_ag),WBGscaled_p_ag);
	gsl_matrix_complex_set(Mp,alpha,gama,Mp_beta_ag);
	
	//Mm
	Mm_beta_ag=gsl_complex_add(gsl_complex_mul(rhom,WAGscaled_m_ag),WBGscaled_m_ag);
	gsl_matrix_complex_set(Mm,alpha,gama,Mm_beta_ag);
	
	//N_beta
	N_beta_ag=gsl_complex_add(gsl_complex_mul(gsl_complex_sub({1,0},rhop_betasurnu_beta),Mp_beta_ag),gsl_complex_mul(rhop_betasurnu_beta,Mm_beta_ag));
	gsl_matrix_complex_set(N_beta,alpha,gama,N_beta_ag);
	
	//Ubeta
	U_beta_ag=gsl_complex_div(gsl_complex_sub(Mp_beta_ag,Mm_beta_ag),nu_beta);
	gsl_matrix_complex_set(U_beta,alpha,gama,U_beta_ag);
      }//End of gama loop
    }//End of alpha loop
    
    //Computing J,K
    for (int gama=0;gama<N;gama++){//gama loop
      double Psi_gb=gsl_matrix_get(Psi,gama,beta);
      for (alpha=0;alpha<N;alpha++){//alpha loop
	J_ag=gsl_matrix_complex_get(J,alpha,gama);
	K_ag=gsl_matrix_complex_get(K,alpha,gama);
	J_beta_ag={0,0};
	K_beta_ag={0,0};
	
	for (int delta=0;delta<N;delta++){//delta loop
	  double Psi_db=gsl_matrix_get(Psi,delta,beta);
	  N_beta_ad=gsl_matrix_complex_get(N_beta,alpha,delta);
	  U_beta_ad=gsl_matrix_complex_get(U_beta,alpha,delta);
	  Pi_beta_dg={Psi_db*Psi_gb,0};
	  J_beta_ag=gsl_complex_add(J_beta_ag,gsl_complex_mul(N_beta_ad,Pi_beta_dg));
	  K_beta_ag=gsl_complex_add(K_beta_ag,gsl_complex_mul(U_beta_ad,Pi_beta_dg));
	}//End of delta loop
	gsl_matrix_complex_set(J,alpha,gama,gsl_complex_add(J_ag,J_beta_ag));
	gsl_matrix_complex_set(K,alpha,gama,gsl_complex_add(K_ag,K_beta_ag));
      }//End of alpha loop
    }//End of gama loop
    

    /*printf("\n------------\nbeta=%d\n",beta);
    printf("\nMp\n");ViewMatComp(Mp);
    printf("\nMm\n");ViewMatComp(Mm);
    printf("\nN_%d\n",beta);ViewMatComp(N_beta);
    printf("\nU_%d\n",beta);ViewMatComp(U_beta);
    printf("\nJ\n");ViewMatComp(J);
    printf("\nK\n");ViewMatComp(K);*/
    // printf("\nPsi\n");ViewMat(Psi);
    //    exit(0);

    //Computing phi^{\pm}_n=(WBG+rho^{\pm}WAG)Psi/(lambdan^{\pm}+1/tauG) = M^{\pm}.\psi_n
    double NormPhip=0,NormPhim=0;//Norme of Phi^{\pm}_n

    for (int i=0;i<N;i++){
      gsl_complex Phip_in={0,0},Phim_in={0,0}; //n th vector, i th line
      for (int j=0;j<N;j++){
	Phip_in=gsl_complex_add(Phip_in,gsl_complex_mul_real(gsl_matrix_complex_get(Mp,i,j),gsl_matrix_get(Psi,j,n)));
	Phim_in=gsl_complex_add(Phim_in,gsl_complex_mul_real(gsl_matrix_complex_get(Mm,i,j),gsl_matrix_get(Psi,j,n)));
      }
      gsl_vector_complex_set(Phip,i,Phip_in);
      gsl_vector_complex_set(Phim,i,Phim_in);      
      NormPhip+=gsl_complex_abs(Phip_in);
      NormPhim+=gsl_complex_abs(Phim_in);
    }

    //printf("Phip_%d\n",n);ViewVectComp(Phip);
    //printf("Phim_%d\n",n);ViewVectComp(Phim);

    //Computing the n \pm columns of P
    normePp_n=1;//sqrt(1+pow(gsl_complex_abs(rhop),2)+NormPhip);
    gsl_vector_set(normePp,n,normePp_n);
    //printf("normePp %d=%lg\n",n,normePp);
 
    //sigmanp=gsl_complex_mul_real(aux1,p.h);    //In the presence of gain control
    normePm_n=1;//sqrt(1+pow(gsl_complex_abs(rhom),2)+NormPhim);
    gsl_vector_set(normePm,n,normePm_n);
    //printf("normePm %d=%lg\n",n,normePm);
    
    //beta=n;
    
    for (alpha=0;alpha<N;alpha++){//Filling columns n and N+n of P. Scaning the 3N lines of the vector with an index ranging from 1 to N. We use indices alpha, beta to match formulas
      psi_ab=gsl_matrix_get(Psi,alpha,beta);
      
      //Mode + column n
      // GSL_REAL(Pab)=psi_ab;GSL_IMAG(Pab)=0;
      Pab=gsl_complex_div_real({psi_ab,0},normePp_n);
      gsl_matrix_complex_set(p->P,alpha,beta,Pab);//p.N first lines, n th column
      
      Pab=gsl_complex_mul_real(rhop,psi_ab);//p.N next lines, n th column
      Pab=gsl_complex_div_real(Pab,normePp_n);
      gsl_matrix_complex_set(p->P,alpha+N,beta,Pab);//p.N next lines, n th column
      
      Pab=gsl_vector_complex_get(Phip,alpha);//p.N last lines, n th column
      Pab=gsl_complex_div_real(Pab,normePp_n);
      gsl_matrix_complex_set(p->P,alpha+2*N,beta,Pab);//p.N next lines, n th column
      
      //Mode -, column N+n
      //GSL_REAL(Pab)=psi_ab;GSL_IMAG(Pab)=0;
      Pab=gsl_complex_div_real({psi_ab,0},normePm_n);
      gsl_matrix_complex_set(p->P,alpha,beta+N,Pab);//p.N first lines, n+N th column
      
      Pab=gsl_complex_mul_real(rhom,psi_ab);//p.N next lines, n+N th column
      Pab=gsl_complex_div_real(Pab,normePm_n);
      gsl_matrix_complex_set(p->P,alpha+N,beta+N,Pab);//p.N next lines, n+N th column
      
      Pab=gsl_vector_complex_get(Phim,alpha);//p.N last lines, n+N th column
      Pab=gsl_complex_div_real(Pab,normePm_n);
      gsl_matrix_complex_set(p->P,alpha+2*N,beta+N,Pab);//p.N next lines, n+N th column
    
    }//End of filling columns n and N+n of P. Scaning the 3N lines of the vector with an index ranging from 1 to N
    
    n++;
  }//End of scaning the eigenvalues and eigenmodes of WAB

  //Last block of P
  for (int i=2*N;i<dim;i++) gsl_matrix_complex_set(p->P,i,i,{1,0});

  //Computing the n \pm columns of P^{-1}

  beta=0;
  gsl_complex nu_alpha={0,0};
  gsl_complex rhop_alpha={0,0};//gsl_vector_complex_get(p->rhop,n);
  gsl_complex rhop_alphasurnu_alpha={0,0};//=gsl_complex_div(rhop,nu_n);
  gsl_complex Pinv_ab={0,0};
  double psi_transpose_ab=0;
  
  while (beta<N){//Loop on beta. beta is n. We use beta to match the formulas
    
    //Columns beta, beta+N
    for (alpha=0;alpha<N;alpha++){//Filling columns n and N+n of P^{-1}. Scaning the 3N lines of the vector with an index ranging from 1 to N. 
      psi_transpose_ab=gsl_matrix_get(Psi_transpose,alpha,beta);//printf("psi_%d%d/normeP=%lg\n",i+1,n+N,psi_ab);
      nu_alpha=gsl_vector_complex_get(nu,alpha);
      rhop_alpha=gsl_vector_complex_get(p->rhop,alpha);
      rhop_alphasurnu_alpha=gsl_complex_div(rhop_alpha,nu_alpha);
      normePp_n=gsl_vector_get(normePp,beta);
      normePm_n=gsl_vector_get(normePm,beta);
      
      //Column beta,line alpha
      Pinv_ab=gsl_complex_mul_real(gsl_complex_sub({1,0},rhop_alphasurnu_alpha),psi_transpose_ab);
      Pinv_ab=gsl_complex_mul_real(Pinv_ab,normePp_n);
      gsl_matrix_complex_set(p->Pinv,alpha,beta,Pinv_ab);//Column n, line alpha
      
      //Column n,line N+alpha
      Pinv_ab=gsl_complex_mul_real(rhop_alphasurnu_alpha,psi_transpose_ab);
      Pinv_ab=gsl_complex_mul_real(Pinv_ab,normePp_n);
      gsl_matrix_complex_set(p->Pinv,alpha+N,beta,Pinv_ab);//Column n,line N+i
      
      //Column beta, line 2N+alpha
      Pinv_ab=gsl_matrix_complex_get(J,alpha,beta);
      Pinv_ab=gsl_complex_mul_real(Pinv_ab,-normePp_n);//There is a minus sign in the inversion formula
      gsl_matrix_complex_set(p->Pinv,alpha+2*N,beta,Pinv_ab);
       
      //Column N+n
      
      //Column N+beta, line alpha
      
      Pinv_ab=gsl_complex_div({psi_transpose_ab,0},nu_alpha);
      Pinv_ab=gsl_complex_mul_real(Pinv_ab,normePm_n);
      gsl_matrix_complex_set(p->Pinv,alpha,beta+N,Pinv_ab);//Column n, line i
      
      //Column N+n,line N+alpha
      Pinv_ab=gsl_complex_div({-psi_transpose_ab,0},nu_alpha);
      Pinv_ab=gsl_complex_mul_real(Pinv_ab,normePm_n);
      gsl_matrix_complex_set(p->Pinv,alpha+N,beta+N,Pinv_ab);//Column n,line N+i
      
      //Column N+n, line 2N+alpha
      Pinv_ab= gsl_matrix_complex_get(K,alpha,beta);
      Pinv_ab=gsl_complex_mul_real(Pinv_ab,-normePm_n);//There is a minus sign in the inversion formula
      gsl_matrix_complex_set(p->Pinv,alpha+2*N,beta+N,Pinv_ab);//Column n,line N+i
      
    }//End of filling columns n and N+n of P^{-1}. Scaning the 3N lines of the vector with an index ranging from 1 to N
    
    beta++;
  }//End of loop on beta

  //Last block of P^{-1}
  for (int i=2*N;i<dim;i++) gsl_matrix_complex_set(p->Pinv,i,i,{1,0});

  //Freeing pointers
  My_gsl_matrix_complex_free(J);My_gsl_matrix_complex_free(K);
  My_gsl_matrix_complex_free(Mp);My_gsl_matrix_complex_free(Mm);
  My_gsl_matrix_complex_free(N_beta);My_gsl_matrix_complex_free(U_beta);
  My_gsl_vector_complex_free(Phip);My_gsl_vector_complex_free(Phim);
  //if (p->rhop !=NULL) My_gsl_vector_complex_free(p->rhop);
  //p->rhop=Rhop;
  //if (p->rhom !=NULL) My_gsl_vector_complex_free(p->rhom);
  //p->rhom=Rhom;
  My_gsl_vector_complex_free(nu);
  My_gsl_vector_free(normePp);My_gsl_vector_free(normePm);
  //printf("\t=>Done\n");

}

//Checks whether P and Pinv are inverse of each other
bool CheckInverse(gsl_matrix_complex* P,gsl_matrix_complex* Pinv){//Checks whether P and Pinv are inverse of each other

  bool check=true;
  gsl_complex un={1,0};

  if (P->size1!=P->size2) {printf("P is not a square matrix, aborting\n");exit(0);}
  if ((P->size1!=Pinv->size1)||(P->size1!=Pinv->size2)) {printf("P and Pinv do not have the same dimension, aborting\n");exit(0);}
  int dim=P->size1;
  //  int dim=3*p.N; //Dimension without activities

  //gsl_matrix_complex_set_identity(P);gsl_matrix_complex_set_identity(Pinv);//Used to check
  gsl_matrix_complex *prod=gsl_matrix_complex_calloc(dim,dim);
  
  for (int i=0; i<dim;i++){//i loop
    for (int j=0; j<dim;j++){//j loop
      gsl_complex prod_ij={0,0};
      for (int k=0; k<dim;k++){//k loop
	gsl_complex PikPinvkj=gsl_complex_mul(gsl_matrix_complex_get(P,i,k),gsl_matrix_complex_get(Pinv,k,j));
	prod_ij=gsl_complex_add(prod_ij,PikPinvkj);
      }//End of k loop

      if ((i==j)&&(gsl_complex_abs(gsl_complex_sub(un,prod_ij))>resol)){
	printf("The diagonal element %i of the product P.Pinv=(%g,%g) is not unity\n",i,GSL_REAL(prod_ij),GSL_IMAG(prod_ij)); return false;
      } 
      if ((i!=j)&&(gsl_complex_abs(prod_ij)>1E-7)){
	printf("The off-diagonal element (%d,%d) of the product P.Pinv=(%g,%g) is not zero\n",i,j,GSL_REAL(prod_ij),GSL_IMAG(prod_ij));return false;
      } 
      
      gsl_matrix_complex_set(prod,i,j,prod_ij);
    }//End of j loop
  }//End of i loop

  My_gsl_matrix_complex_free(prod);
  //printf("\nProduct Matrix\n"); ViewMatComp(prod);
  return check;
}  

#define epsilon 1E-3

bool TestSpectrum(gsl_matrix *cL,gsl_matrix_complex *P,gsl_vector_complex *lambda){//Tests a spectrum. L v_i= lambda_i v_i, i=1 ...N. Return TRUE if OK.
    
    bool test=true;
    if (cL->size1!=cL->size2) {printf("In TestSpectrum Matrix is not diagonal\n");exit(0);}
    //int L=cL->size1;
    int dim=P->size1;//Dimension of the phase space without activities
    gsl_vector_complex *v=gsl_vector_complex_calloc(dim);
    
    for (int i=0;i<dim;i++){//Loop on eigenvectors
        //printf("\nvector %d\n",i);
        gsl_complex lambdai=gsl_vector_complex_get(lambda,i);
        gsl_matrix_complex_get_col (v,P,i);//i th column of P, eigenvector vi
        //printf("Eigenvector %d=\n",i); for (int j=0;j<L;j++) printf("%lg+i%lg\n",GSL_REAL(gsl_vector_complex_get(v,j)),GSL_IMAG(gsl_vector_complex_get(v,j)));//exit(0);
        //printf("\n");
        
        for (int k=0;k<dim;k++){//Loop on eigenvector v entries
            //printf("k=%d\n",k);
            gsl_complex vk=gsl_vector_complex_get(v,k);//Entry vk of v
            gsl_complex lambdaivk=gsl_complex_mul(lambdai,vk);
            //printf("lambda%d=%lg+i%lg, lambda%dv%d=%lg+i%lg\n",i,GSL_REAL(lambdai),GSL_IMAG(lambdai),i,k,GSL_REAL(lambdaivk),GSL_IMAG(lambdaivk));
            
            gsl_complex Lvk={0,0};
            
            for (int j=0;j<dim;j++){//Product Lvk
                //printf("Lv%d=%lg+i%lg\n",k,GSL_REAL(Lvk),GSL_IMAG(Lvk));
                gsl_complex vj=gsl_vector_complex_get(v,j);//Entry vj of v
                //printf("v%d=%lg+i%lg\n",j,GSL_REAL(gsl_vector_complex_get(v,j)),GSL_IMAG(gsl_vector_complex_get(v,j)));
                double Lkj=gsl_matrix_get(cL,k,j);
                //printf("L%d%d=%lg\n",k,j,Lkj);
                gsl_complex Lkjvj=gsl_complex_mul_real(vj,Lkj);
                Lvk=gsl_complex_add(Lvk,Lkjvj);
            }
            //printf("Lv%d=%lg+i%lg\n",k,GSL_REAL(Lvk),GSL_IMAG(Lvk));
            test=(gsl_complex_abs(gsl_complex_sub(Lvk,lambdaivk))<epsilon);
            //printf("\n");
            
            if (test==false) {
                printf("Diagonalization failed at : (Lv_%d)_%d = %lg+i%lg != lambda%d v_%d=%lg+i%lg\n",i,k,GSL_REAL(Lvk),GSL_IMAG(Lvk),i,k,GSL_REAL(lambdaivk),GSL_IMAG(lambdaivk));
                return test;
            }
        }
    }
    My_gsl_vector_complex_free(v);
    return test;
}

void Check_Linear_Stability(params p){//Check if some eigenvalues of p.L are unstable or complex
  printf("\n\t\033[1;35mChecking if some eigenvalues of p.L are unstable or complex\n");
  printf("\033[0m");

  int dim=3*p.L;//Activities are not considered here
  double lambda_r= 0,lambda_i=0;//Real and imaginary part of eigenvalues
  double tau=0; //Characteristic decay time
  double omega=0; //Oscillation rate
  double dl=0; //Characteristic length of mode l
  double vl=0; //Characteristic speed of mode l

  for (int i=0;i<dim;i++){
      gsl_complex z=  gsl_vector_complex_get(p.lambda_th,i);
      if ((fabs(GSL_IMAG(z))>1E-6)&&(i<p.L/2)) {
	lambda_r= GSL_REAL(z);lambda_i=GSL_IMAG(z);
	tau=1.0/fabs(lambda_r); //Characteristic decay time
	omega=fabs(lambda_i); //Oscillation rate
	printf("\t\033[0;96mEigenvalue %d=(%lg,%lg) is complex\n",i,lambda_r,lambda_i);
	dl=p.L*p.dx/(i+1);
	vl=dl*omega;
	printf("\t\ttau=%lg ms, 1.0/omega=%lg ms, dl=%lg mm, vl=%lg mm/s\n",tau,1.0/omega,dl,1000*vl);
      }
      printf("\033[0m");

      if (GSL_REAL(z)>0){
	lambda_r= GSL_REAL(z);lambda_i=GSL_IMAG(z);
	tau=1.0/fabs(lambda_r); //Characteristic decay time
	printf( "\t\t\033[0;92mEigenvalue %d=(%lg,%lg) is unstable, tau=%lg\n",i,GSL_REAL(z),GSL_IMAG(z),tau);
     }
      printf("\033[0m");
    }

}
