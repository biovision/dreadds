/** Spectral computations **/

#ifndef Spectrum_h
#define Spectrum_h

#include "Defs.h"
#include "Utils.h"

//Compute the eigenmodes of a Laplacian in 1D with a lattice of size L, 0 boundary conditions
void ComputeTheoreticalModesLaplacian1D( params p,Spectrum *kappa_th,Spectrum *sigma_th,gsl_matrix *Psi,gsl_matrix *Psi_transpose);

/**Computing the theoretical spectrum and eigenvectors of cL. Eigenvalues are stored in p.lambda_th. Eigenvectors are stored in p.P. The inverse of P, p.Pinv, is computed too.
***/
void  ComputeTheoreticalSpectrumL(Spectrum kappa_th,Spectrum sigma_th,gsl_matrix *Psi,gsl_matrix *Psi_transpose,params *p);

//Checks whether P and Pinv are inverse of each other
bool CheckInverse(gsl_matrix_complex* P,gsl_matrix_complex* Pinv);//Checks whether P and Pinv are inverse of each other

//Tests a spectrum. L v_i= lambda_i v_i, i=1 ...N. Return TRUE if OK.
bool TestSpectrum(gsl_matrix *cL,gsl_matrix_complex *P,gsl_vector_complex *lambda);

//Check if some eigenvalues of p.L are unstable
void Check_Linear_Stability(params p);

#endif //Spectrum_h

