/** Utilities functions **/

#ifndef Utils_h
#define Utils_h

#include "Defs.h"
#include "Spectrum.h"
#include <gsl/gsl_errno.h>
#include <gsl/gsl_fft_complex.h>
#include <gsl/gsl_matrix_double.h>
#include <string.h>

/*** Parameters section ***/
//Save the parameters of p in SaveFile
void SaveParams(params p,std::string SaveFile);

//Write the parameters of p on the screen
void WriteParams(params p);

//Read the parameters from SaveFile and put them in p. Return false if the file couldn't be opened. 
bool ReadParams(params *p,std::string SaveFile);

//Copy the data in source to target. Note that pointers in target are not allocated. They are to be allocated by Init_Network_Laplacian 
void CopyParams(params source, params *target);

//Update the global parameters of p
void UpdateParams(params *p);

/*** Statistics section ***/
//Load value in the histo structure
void Update_Histo(Cell_id_type cell,Histo* histo);

//Compute probabilities and moments
void compute_Moments(Histo *histo, moments *moms);

/*** Matrices section ***/

//Free a gsl_vector. The gsl_vector_free method does not deallocate properly.
void My_gsl_vector_complex_free(gsl_vector_complex *M);

//Free a gsl_matrix. The gsl_matrix_free method does not deallocate properly.
void My_gsl_matrix_complex_free(gsl_matrix_complex *M);

//Free a gsl_vector. The gsl_vector_free method does not deallocate properly.
void My_gsl_vector_free(gsl_vector *M);

//Free a gsl_matrix. The gsl_matrix_free method does not deallocate properly.
void My_gsl_matrix_free(gsl_matrix *M);

//Initializes the connectivity matrices and the transport operator in Laplacian case. 
//p is the parameters vectors containing pointers to connectivity matrices
//dim is the lattice dimension
void init_Network_Laplacian(params *p, int dim);

//Free the pointers in p
void delete_params(params* p);

double GaussianPoolWeight(int i,int j, params p);//Bipolar pooling to GCells according to Chen et al 2013

void ViewMat(const gsl_matrix* M);//Viewing a real matrix on the terminal
void ViewMatComp(const gsl_matrix_complex* M);//Viewing a complex matrix on the terminal
void ViewVect(const gsl_vector* V);//Viewing a real matrix on the terminal
void ViewVectComp(const gsl_vector_complex* V);//Viewing a complex matrix on the terminal

void tuning(params *p);//Tuning wab from wba to keep the rest state constant. Holds in one d.

/*** Receptive fields section ***/

//Exponential kernel
double E(double t,params p);
//Exponential kernel complex used for convolution test
gsl_complex Ec(double t,params p,int beta);
//Gamma filter of order 1
double E1(double t, params p);
//Gamma filter of order 2
double E2(double t, params p);
//Gamma filter of order 2. Complex used for convolution test
gsl_complex E2c(double t, params p,int beta);
//Standard Gaussian 
double Gauss(double t,params p);
//Standard Gaussian complex used for convolution test
gsl_complex Gaussc(double t,params p,int beta);
//Function exp(lambdabeta*t) used in the numerical computation of GCell RF
// Gaussian random variable 
double Gauss(double moy, double var);
gsl_complex Eb(double t,params p,int beta);
//Lorentzian centered at xc, with width width.
double Lorentz(double x,double xc,double width);

/******** Receptive fields ****/

//Spatial part of the RF of BCell beta.
//Restricted to 1D for the moment
double KS(int beta,double x,double y,params p); 

//Temporal part of BCells RF. Does not depend on the cell index
double KT(double t,params p);

//Time derivative of the temporal part of BCells RF. Does not depend on the cell index
double dKTbydt(double t,params p);

//Temporal part of BCells RF. Complex form used for convolution test
gsl_complex KTc(double t,params p,int beta);

//Spatio-temporal RF of BCell beta.
double K(int beta,double x,double y,double t,params p);

/********* Stimuli and their derivatives ***********/

/**** Space uniform stimuli ****/

//Definition of a second order kernel starting at p.t0, defined from KT
double alpha2(double x,double t,params p);

//Time derivative of the second order kernel 
double dalpha2bydt(double x,double t,params p);

//Definition of a second order kernel starting at p.t0, defined from KT with periodic, alpha profile modulation, the frequency omega being in the parameter p. 
double alpha2_periodic(double x,double t,params p);

//Time derivative of the second order kernel with periodic modulation, alpha profile modulation, the frequency omega being in the parameter p. 
double dalpha2bydt_periodic(double x,double t,params p);

//cos stimulation, the frequency omega being in the parameter p. 
double Stim_cos(double x,double t,params p);

//Time derivative of the second order kernel with cos modulation, the frequency omega being in the parameter p. 
double dStim_cos_bydt(double x,double t,params p);

//sin stimulation, the frequency omega being in the parameter p. 
double Stim_sin(double x,double t,params p);

//Time derivative of the second order kernel with sin modulation, the frequency omega being in the parameter p. 
double dStim_sin_bydt(double x,double t,params p);

/*** Space dependent stimuli ****/

//Spatio temporal alpha kernel. The temporal part is KT=alpha2, the spatial part is KS(beta,x,p) where beta is the index of the BCell whose RF is computed and x is the point of space where the RF is computed.
double alpha2S(int beta,double x,double t,params p);

//Time derivative of the second order spatio-temporal kernel 
double dalpha2Sbydt(int beta,double x,double t,params p);

//Spatio temporal alpha kernel with periodic modulation, the frequency omega being in the parameter p. The temporal part is KT=alpha2, the spatial part is KS(beta,x,p) where beta is the index of the BCell whose RF is computed and x is the point of space where the RF is computed.
double alpha2S_periodic(int beta,double x,double t,params p);

//Time derivative of the second order spatio-temporal kernel with periodic modulation, the frequency omega being in the parameter p. 
double dalpha2Sbydt_periodic(int beta,double x,double t,params p);

//Spatio temporal alpha kernel with periodic modulation, the frequency omega being in the parameter p. The temporal part is KT=cos(omega*t), the spatial part is KS(beta,x,p) where beta is the index of the BCell whose RF is computed and x is the point of space where the RF is computed.
double Stim_S_cos(int beta,double x,double t,params p);

//Time derivative of the second order kernel with cos stimulation, the frequency omega being in the parameter p. 
double dStim_S_cos_bydt(int beta,double x,double t,params p);

//Spatio temporal alpha kernel with periodic modulation, the frequency omega being in the parameter p. The temporal part is KT=sin(omega*t), the spatial part is KS(beta,x,p) where beta is the index of the BCell whose RF is computed and x is the point of space where the RF is computed.
double Stim_S_sin(int beta,double x,double t,params p);

//Time derivative of the second order kernel with cos stimulation, the frequency omega being in the parameter p. 
double dStim_S_sin_bydt(int beta,double x,double t,params p);

/**** Bipolar inputs ****/

//Bipolar input (response to a alpha2 kernel at time t): ON Cell. This function is actually independent of x (it corresponds to a spatially uniform flash). The variable x is added to be compatible with the C definition of the vector field.
double Bip_input_alpha2_ON(double x,double t,params p);

//Bipolar input (response to a alpha2 kernel at time t): OFF Cell. This function is actually independent of x (it corresponds to a spatially uniform flash). The variable x is added to be compatible with the C definition of the vector field.
double Bip_input_alpha2_OFF(double x,double t,params p);

//Bipolar input (response to a alpha2 kernel at time t): ON cell with periodic modulation, the frequency omega being in the parameter p. . This function is actually independent of x (it corresponds to a spatially uniform flash). The variable x is added to be compatible with the C definition of the vector field.
double Bip_input_alpha2_ON_periodic(double x,double t,params p);

//Bipolar input (response to a alpha2S (spatial) kernel at time t   used in the dynamical system formulation): The temporal part is KT=alpha2, the spatial part is KS(beta,x,p) where beta is the index of the BCell whose RF is computed and x is the point of space where the RF is computed. 1D only.
double Bip_input_alpha2S_ON(int beta,double x,double t,params p);

//Bipolar input (response to a alpha2S (spatial) kernel at time t   used in the dynamical system formulation):ON Cell with periodic modulation, the frequency omega being in the parameter p. . The temporal part is KT=alpha2, the spatial part is KS(beta,x,p) where beta is the index of the BCell whose RF is computed and x is the point of space where the RF is computed. 1D only.
double Bip_input_alpha2S_ON_periodic(int beta,double x,double t,params p);

//Bipolar input (response to a cos stimulation at time t):the frequency omega being in the parameter p. . This function is actually independent of x (it corresponds to a spatially uniform flash). The variable x is added to be compatible with the C definition of the vector field.
double Bip_input_Stim_cos(double x,double t,params p);

//Bipolar input (response to a sin stimulation at time t):the frequency omega being in the parameter p. The minus sign is here to ease the computation of the complex susceptibility. This function is actually independent of x (it corresponds to a spatially uniform flash). The variable x is added to be compatible with the C definition of the vector field.
double Bip_input_Stim_sin(double x,double t,params p);

//Bipolar input (response to a alpha2S (spatial) kernel at time t   used in the dynamical system formulation):OFF Cell. The temporal part is KT=alpha2, the spatial part is KS(beta,x,p) where beta is the index of the BCell whose RF is computed and x is the point of space where the RF is computed. 1D only.
double Bip_input_alpha2S_OFF(int beta,double x,double t,params p);

//Spatio temporal alpha kernel with periodic modulation, the frequency omega being in the parameter p. The temporal part is KT=cos(omega*t), the spatial part is KS(beta,x,p) where beta is the index of the BCell whose RF is computed and x is the point of space where the RF is computed.
double Bip_input_Stim_S_cos(int beta,double x,double t,params p);

//Spatio temporal alpha kernel with periodic modulation, the frequency omega being in the parameter p. The temporal part is KT=sin(omega*t), the spatial part is KS(beta,x,p) where beta is the index of the BCell whose RF is computed and x is the point of space where the RF is computed.
double Bip_input_Stim_S_sin_bydt(double x,double t,params p);

//Bipolar null input. Used to simulate transients
double Bip_input_null(double x,double t,params p);

//Bipolar constant input. Used to simulate transients
double Bip_input_constant(double x,double t,params p);

/**** Theoretical RF ****/

//Temporal part of the contribution beta to the RF of a GCell. 
//lambda_beta is the eigenvalue beta of the transport operator L
//tau is the characteristic time of RF of BCells
//t is the time (>0)
//Calculation with complex. 
//This is the normalised, centered version (A0=1,b0=0).
//A0 and b0 are added when computing the RF, in the function KG
gsl_complex UT_c(gsl_complex lambda_beta, double t,params p);

//Temporal part of the contribution beta to the RF of a GCell computed numerically by FFT. 
//lambda_beta is the eigenvalue beta of the transport operator L
//tau is the characteristic time of RF of BCells
//t is the time (>0)
gsl_complex UTnum(gsl_complex lambda_beta,double tau,double t);

//Analytic Fourier transform of UT 
//lambda_beta is the eigenvalue beta of the transport operator L
//tau is the characteristic time of RF of BCells
//f is the real frequency (>0)
gsl_complex UT_hat(gsl_complex lambda_beta, double f,params p);

//Temporal part of the contribution beta to the RF of a GCell computed exactly. 
//beta is the eigenmode index
//tau is the characteristic time of RF of BCells
//t is the time (>0)
gsl_complex UT(gsl_complex lambda_beta, double t,params p);

//Computes the Spatial contribution of the RF from all eigenmodes 
//x, y are the spatial coordinates where the RF is to be computed
//p defines the parameters of cells and network. In particular it contains the addresses of eigenvectors (P and P^-1) and eigenvalues. These vectors must be allocated before calling this function
//KSG contains the value of the spatial RF, of each eigenmode beta, at point x,y.
//NB:This is useful only if the BCell RF is separable
void KSG(double x,double y,params p,gsl_vector_complex* SumPinvbgKSg);

//CNO contribution from ACells
void KG_CNOA(params p,gsl_vector_complex* SumPinvbgKSg_CNOA);

//Computes the RF of a GCell alpha embedded in the BCell, ACell, GCell network
//alpha is the cell index (must be between 2*N+1 and 3*N)
//x, y are the spatial coordinates where the RF is to be computed
//t is the time at which the RF is computed
//p defines the parameters of cells and network. In particular it contains the addresses of eigenvectors (P and P^-1) and eigenvalues. These vectors must be allocated before calling this function
//SumPinvbgKSg contains the value of the spatial RF, of each BCell gama, at point x,y.
//SumPinvbgKSg_CNOA contains the CNO ACells contribution.
//NB:This formula holds only if the BCell RF is separable
gsl_complex KG(int alpha,double x,double y,double t,params p,gsl_vector_complex* SumPinvbgKSg,gsl_vector_complex* SumPinvbgKSg_CNOA);

//Computes the Fourier transform of the RF of a GCell alpha embedded in the BCell, ACell, GCell network
//alpha is the cell index (must be between 2*N+1 and 3*N)
//x, y are the spatial coordinates where the RF is to be computed
//f is the frequency at which the RF is computed
//p defines the parameters of cells and network. In particular it contains the addresses of eigenvectors (P and P^-1) and eigenvalues. These vectors must be allocated before calling this function
//SumPinvbgKSg contains the value of the spatial RF, of each BCell gama, at point x,y.
//SumPinvbgKSg_CNOA contains the CNO ACells contribution.
//NB:This formula holds only if the BCell RF is separable
gsl_complex KG_hat(int alpha,double x,double y,double f,params p,gsl_vector_complex* SumPinvbgKSg,gsl_vector_complex* SumPinvbgKSg_CNOA);

//Computes the power spectrum of the RF of a GCell alpha embedded in the BCell, ACell, GCell network as a function of r,s (encoded in p). Also finds the peaks in the power spectrum, their location and their width return as vector
//alpha is the cell index (must be between 2*N+1 and 3*N)
//x, y are the spatial coordinates where the RF is to be computed
//p defines the parameters of cells and network. In particular it contains the addresses of eigenvectors (P and P^-1) and eigenvalues. These vectors must be allocated before calling this function
//NB:This formula holds only if the BCell RF is separable
std::vector<Peaks_fft> getPeriod(int alpha,double x,double y,params p);

/** Convolutions and FFT section **/

#define REAL(z,i) ((z)[2*(i)])
#define IMAG(z,i) ((z)[2*(i)+1])
#define  zero_PS 1E-3

//Gets the period of a real time signal contained in the vector experience.
//One first computes the FFT of the signal, sampled at the time rate sample_rate and extract the main peaks in the normalized power spectrum, i.e whose amplitude is larger than zero_PS . These peaks and their relative amplitude are returned on output. The power spectrum is stored in the file with name PowerSpectrum if the string  PowerSpectrum is non empty (different from ""). max_range is the maximal frequency range i.e. the maximal value where the amplitude of the normalized spectrum is larger than zero_PS.
std::vector<Peaks_fft> getPeriod(Experiment experience,double sample_rate,int *n_peaks,std::string PowerSpectrum,double* max_range);

//Gets the period of a real time signal contained in the file with name data. This file is a spatiotemporal file where the first coordinates (space) is xGC, the cell location.
//One first computes the FFT of the signal, sampled at the time rate sample_rate and extract the main peaks in the normalized power spectrum, i.e whose amplitude is larger than zero_PS . These peaks and their relative amplitude are returned on output. The power spectrum is stored in the file with name PowerSpectrum if the string  PowerSpectrum is non empty (different from ""). max_range is the maximal frequency range i.e. the maximal value where the amplitude of the normalized spectrum is larger than zero_PS.
std::vector<Peaks_fft> getPeriod(std::string Data,double xGC,double sample_rate,int *n_peaks,std::string PowerSpectrum,double* max_range);

//Gets the period of a signal contained in the Event vector with name data. 
//One first computes the FFT of the signal, sampled at the time rate sample_rate and extract the main peaks in the normalized power spectrum, i.e whose amplitude is larger than zero_PS . These peaks and their relative amplitude are returned on output. The power spectrum is stored in the file with name PowerSpectrum if the string  PowerSpectrum is non empty (different from ""). max_range is the maximal frequency range i.e. the maximal value where the amplitude of the normalized spectrum is larger than zero_PS.
std::vector<Peaks_fft> getPeriod(vector <Event> data,double sample_rate,int *n_peaks,std::string PowerSpectrum,double* max_range);

//Computes the numerical convolution of two real, causal (t>0) functions F1 and F2 of the variable t,  sampled with a time discretisation Delta with n samples where n has to be a power of 2, n=1<<PowTwo, over a time length T. Thus, T=n Delta. Returns an array, convolution, of size n, corresponding to the convolution F1*F2 sampled at time resolution Delta
void convolution(gsl_complex (*F1)(double t,params p,int beta),gsl_complex (*F2)(double t,params p,int beta), params p,int beta,double T, int n,double convolution[]);


/*** Dynamics utilities section ***/

//Selects dynamical type
//int dyn_type //0 if linear, 1 if rectified without gain control, 2 if rectified with gain control
//f is the space independent vector field
//fS is the space dependent vector field
string Select_dyn_type(int dyn_type,void (**f)(double *X,double *Xp,double t,params p,double (*input)(double x,double t, params p)),void (**fS)(double x,double *X,double *Xp,double t,params p,double (*input)(int beta,double x,double t, params p)));

/** Analytic utilities section ***/

analytic Compute_Rest_Analytic(params p, int d,double VB_rest,double VA_rest,double VG_rest,double dVB_rest,double dVA_rest,double dVG_rest,double dtauBdzetaA,double dtauAdzetaA,bool *test);//V*_rest are the voltages at equilibrium. dV*_rest are the derivatives of Vwith respect to zeta. Analytic computations.d is the space dimension. dtauB, dtauA, are the derivatives of characteristic times with respect to zetaA. Test is true if all formula have been checked.
double VB_an(params p,int d);//VB analytic. d is the space dimension
double VA_an(params p,int d);//VA analytic. d is the space dimension
double dVA_dzetaA_an(params p,int d);//dVA/dzetaA
double dVB_dzetaA_an(params p,int d);//dVB/dzetaA
double dVB_dtauA_an(params p,int d);//dVB/dtauA;
double dVB_dtauB_an(params p,int d);//dVB/dtauB
double dVA_dtauA_an(params p,int d);//dVA/dtauA
double dVA_dtauB_an(params p,int d);//dVA/dtauA
double dHA_dtauA_an(params p,int d);//dHA/dtauA
double dHB_dtauA_an(params p,int d);//dHB/dtauA
double dHA_dtauB_an(params p,int d);//dHA/dtauB
double dHB_dtauB_an(params p,int d);//dHB/dtauB
double dHA_dzetaA_an(params p,int d);//dHA/dzetaA
double dHB_dzetaA_an(params p,int d);//dHB/dzetaA
double VG_an(params p,int d);//VG analytic. d is the space dimension
double tauB_an(params p,int d);//tauB analytic.
double HB(params p,int d);//tauB implicit analytic eq. d is the space dimension.
double tauA_an(params p,int d);//tauA ianalytic eq. 
double HA(params p,int d);//tauA implicit analytic eq. d is the space dimension.
double detH(params p,int d);//Determinant of H
double zetaD(params p,int d);//Vanishing of the determinant
double num_dtauB_dzetaA_an(params p,int d);//Numerator of dtauB/dzetaA
double zetaNBA(params p, int d);//Vanishing of the numerator of dtauB/dzetaA
double num_dtauA_dzetaA_an(params p,int d);//Numerator of dtauA/dzetaA
double zetaNAA(params p, int d);//Vanishing of the numerator of dtauA/dzetaA#endif //Utils_h
double dtauB_dzetaA_an(params p, int d);//dtauB/dzetaA from the implicit function theorem
double dtauA_dzetaA_an(params p, int d);//dtauA/dzetaA from the implicit function theorem
#endif //Utils_h
