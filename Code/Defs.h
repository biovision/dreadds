/*** Definition of types and constants ***/
/***  Units are:
Voltage (mV)
Time (ms)
Conductance (nS)
Current (pA)
Capacity (pF) 
 ***/

#ifndef Defs_h
#define Defs_h

#include <math.h>
#include <map>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_complex.h>
#include <gsl/gsl_complex_math.h>
#include <vector>
#include <string>
#include <cstring>
#include <list>
#include <iostream>
#include <fstream>

using namespace std;

#define C 50.0 //Membrane capacity (pF).
#define ENa 55.0 //Sodium reversal potential (mV)
#define EK -90.0 //Potassium reversal potential (mV)
#define EL -70.0 //Leak potential (mV)
#define EAMPA 70 //Synaptic reversal potential of glutamate AMPA/NMDA referred to EB, ENMAD(=0)-EL(=-70)=70
#define EGlyc -5 //Synaptic reversal potential of glycine refered to EA, ECl-(=-75) - (EL=-70)=-5
#define gL 0.5 //Leak conductance (nS)
#define DeltaV 10 //Minimal amplitude in voltage variation to consider that there is a cell response (mV)
#define sr2 1.414213562373095145474621858738828450441360473633//sqrt(2)
#define usr2 0.70710678118654746171500846685 //1/sqrt(2)
#define u_s_spi 0.5641895835477562792803496449778322130441665649414 //1/sqrt(pi) 
#define u_s_s2pi 0.3989422804014327028632180827116826549172401428223 //1/sqrt(2pi)
#define us2pi 0.1591549430918953456082221009637578390538692474365 //1/2*pi
#define ps2 1.570796326794896557998981734272092580795288085938 //pi/2
#define dpi 6.28318530717958623199592693708837032318115234375 //2*M_PI
#define tol 1E-4 //Tolerance for test in analytical computation, at the bottom of Utils.cpp
#define eps_der 1E-6 //epsilon for derivatives

//Parameters of a moving bar
typedef struct bar_parameters{//Parameters used for the simulation
  double dt=0; //integration time step (ms)
  double dx=0; //Space scale (mm)
  double delta_t=0; //Duration of a frame (ms)
  double b=0; //Bar width (mm)
  double v=0; //Bar speed  (mm/ms)
  double tau=0; //Integration time of the Receptive Field (ms)
  double sigma_c=0; //Radius of the Receptive Field center (mm)
  double rho=0; //v*tau/sigma_c (dimensionless)
  double unsurrho=0; //1/rho (dimensionless)
  double expro=0; //Auxilary for analytic computations
  int n=0; //Order of the alpha kernel
} bar_params;

//Model's parameters all in one C-type
typedef struct parameters{
  //Lattice
  int N=0; //Number of visible neurons
  int offset=0; //Number of invisible neurons (on the left of the neuron 0 and on the right to neuron N-1) to avoid boundaries effects
  int L=0; //Total number of neurons
  double dt=0; //time step (ms)
  double dx=0,dy=0; //Cells spacing (mm) in abscissa and ordinate
  
  //Cells
  double tauL=0; //Leak

  //Bipolars
  double thetaB=0;//Threshold for bipolar rectification (mV).
  double tauBref=0;//Baseline bipolar voltage integration characteristic time (ms). This is the leak characteristic time.
  double tauB=0;//Bipolar voltage integration characteristic time with membrane depolarisation (ms). 
  double unsurtauB=0;//1/tauB (kHz)
  double CB=0; //Bipolar membrane capacity (pF)
  double gLB=0; //Bipolar leak conductance (nS) 
  double ELB=0; //Bipolar leak potential (mV) 
  double KB=0; //Constant fixed by the leak potential (mV/ms)
  //Gain control
  double h=0;//Activity rate
  double taua=0;//Bipolar activity characteristic time (ms)

  //Time RF of BCells(second order kernel)
  double A0=0; //Amplitude of the pulse - alpha profile (mV)
  double t0=0; //Starting time of the alpha profile (ms)
  double tauRF=0; //Characteristic time of the BCell RF (ms)
  double tauRF3=0; //Characteristic time of the RF to the power 3 used in the computation of RF (ms^3)
  double b0=0;  //Constant value in  the BCell response to a pulse (mV)

  //Spatial RF of BCells
  double Ac=0;//Amplitude of the spatial RF center (mV.mm)
  double sigma_c=0;//Variance of  the spatial RF center (mm)
  double As=0;//Amplitude of  the spatial RF surround (mV.mm)
  double sigma_s=0;//Variance of  the spatial RF surround (mm)
  
  //Amacrines
  double thetaA=0;//Threshold for amacrine rectification (mV). 
  double tauAref=0;//Baseline amacrine characteristic integration time (ms). This is the leak characteristic time.
  double lambdaA=0; //Amacrine CNO  ratio g/C (kHz)
  double ECNOA=0; //Amacrine CNO reversal potential (mV) 
  double gCNOA=0; //Amacrine CNO conductance (nS)
  double zetaA=0; //CNO influence for ACells (mV/ms)
  double tauA=0;//Amacrine characteristic integration time in the presence of CNO and membrane polarisation (ms)
  double unsurtauA=0;//1/tauA (kHz)
  double gLA=0; //Amacrine leak conductance (nS) 
  double CA=0; //Amacrine membrane capacity (pF)
  double ELA=0; //Amacrine leak potential (mV) 
  double KA=0; //Constant fixed by the leak potential (mV/ms)

  //Ganglions
   double lambdaG=0; //Amacrine CNO  ratio g/C (kHz)
  double ECNOG=0; //GCell CNO reversal potential (mV)
  double gCNOG=0; //GCell CNO conductance (nS)
  double zetaG=0; //CNO influence for GCells (mV/ms)
  double tauGref=0;//Baseline ganglion characteristic integration time (ms). This is the leak characteristic time/
  double tauG=0;//Ganglion characteristic integration time in the presence of CNO and membrane polarisation (ms)
  double unsurtauG=0;//1/tauG (kHz)
  double gLG=0; //Ganglion leak conductance (nS) 
  double CG=0; //Ganglion membrane capacity (pF)
  double ELG=0; //Ganglion leak potential (mV) 
   double KG=0; //Constant fixed by the leak potential (mV/ms)

  //Connections
  int BtoA=0;//If 0 one BCell connects a unique ACell, if 1 WAB=WBA
  bool IsPooling=true; //Pooling if true
  double wba=0; //Bipolar Amacrine  weight (kHz)
  gsl_matrix *WBA=NULL; //Connectivity matrix BA
  double wab=0; //Amacrine Bipolar weight (kHz)
  gsl_matrix *WAB=NULL; //Connectivity matrix AB
  double wbg=0; //Bipolar Ganglion weight in the Gaussian pooling (kHz)
  gsl_matrix *WBG=NULL; //Connectivity matrix BG
  double wag=0; //Amacrine Ganglion weight (kHz)
  gsl_matrix *WAG=NULL; //Connectivity matrix AG
  double sigmabg2=0;//Bipolar Ganglion variance in the Gaussian pooling (mm^2)
  gsl_matrix *cL=NULL;//Linear transport operator

  //Global
  double unsurtau=0;//1/tauA-1/tauB (kHz)
  double unsurtauAB=0;//1/tauA+1/tauB (kHz)
  double tau=0; //1/unsurtau (ms)
  double tau2=0;//tau**2 (ms^2)
  double tauAB=0;//1/(unsurtauA+unsurtauB) (ms)
  double mu=0;//Critical value for instability (dimensionless)

  //Eigenvalues and eigenvectors
  gsl_vector_complex* lambda_th=NULL;
  gsl_matrix_complex *P=NULL;
  gsl_matrix_complex *Pinv=NULL;
  gsl_vector_complex *rhop=NULL;
  gsl_vector_complex *rhom=NULL;

  //Pulse
  double AmpStim=0.0; //Amplitude of the pulse (mV)
  double speed=0; //Pulse' speed (mm/ms)
  double strength=0; //Strength of the stimulus
  double mean=0; //Pulse peak location (mm)
  double sigma=0; //Pulse width (mm)
  double tmin=0;//-1;//initial time (ms)
  double tmax=0; //Final time (ms)
  double T=0; //Period of a periodic forcing
  double omega=0; //Frequency of a periodic forcing
  int NPer=0; //Number of periods in the periodic forcing
  
  //Moving bar
  bar_params Bar_params;

  //Gnuplot variables
  int nbviewmax=0; //Maximum number of views in the gnuplot file
  int countmax=0; //Maximum count between time steps of views
  double wc=0; //Critical weight (kHz)
  double VBmin=0;//Bipolar voltage min (mV)
  double VBmax=0;//Bipolar voltage max (mV)
  double VAmin=0;//Amacrine voltage min (mV)
  double VAmax=0;//Amacrine voltage max (mV)
  double VGmin=0;//Ganglion voltage min (mV)
  double VGmax=0;//Ganglion voltage max (mV)
} params;

typedef vector<double> Spectrum; //Real spectrum

typedef vector< pair<double,double> > CSpectrum; //Complex spectrum

typedef struct event{//Contains points of type (t,f(t)).
  double t=0; 
  double Amp=0;//f(t)
} Event;

typedef struct peaks_fft{//Contains points of type (f,amp,width) where f is the frequency at the peak in a power spectrum, Amp its amplitude, and width the peak width.
  double f=0;//Frequency 
  double Amp=0;//Amplitude
  double width=0;//Width
} Peaks_fft;


typedef struct experiment{//Structure containing information on one experiment
  int Cell_id; //id of the cell 
  int Exp_id;// id of the experiment
  std::string Gene; //Gene
  std::vector <Event> data;//Data. Receptive field amplitude stored in a row
} Experiment;

typedef struct Celltype{//Cell type
  std::string type;//cell identifier: On, Off, OnOff,OffOn
  Event Mini={0,0};//time and amplitude of the minimum
  Event Maxi={0,0};//time and amplitude of the maximum
} CellType;

typedef struct Wtau{
  double w=0;
  double tau=0;
  std::string type="";
}wtau;

typedef struct Analytic{//Analytic computations
  double VB=0;//Rest BC
  double VA=0;//Rest AC
  double VG=0;//Rest GC
  double zeta_RectB=0; //Threshold for VB rectification
  double zeta_RectA=0;//Threshold for VA rectification
  double tauB=0;//Analytic tauB
  double tauA=0;//Analytic tauA
  double tauG=0;//Analytic tauG
  double dVBdzetaA=0;//dVB/dzetaA
  double dVAdzetaA=0;//dVA/dzetaA
  double dVBdtauB=0;//dVB/dtauB
  double dVBdtauA=0;//dVB/dtauA
  double dVAdtauB=0;//dVA/dtauB
  double dVAdtauA=0;//dVA/dtauA
  double dHBdzetaA=0;//dHB/dzetaA
  double dHAdzetaA=0;//dHA/dzetaA
  double dHBdtauB=0;//dHB/dtauB
  double dHBdtauA=0;//dHB/dtauA
  double dHAdtauB=0;//dHA/dtauB
  double dHAdtauA=0;//dHA/dtauA
  double dtauAdzetaA=0;//dtauA/dzetaA
  double dtauBdzetaA=0;//dtauB/dzetaA
  double det=0; //Determinant in the implicit function theorem
}analytic;


typedef struct Stats{//Statistics of RF parameters
  int label=0; //Cell label in the experiment
  string type="";//Cell type (ON or OFF)
  double field[11];/*tauB->0
		     tauA->1
		     tauRF->2
		     tauG->3
		     wba->4
		     wab->5
		     wbg->6
		     wag->7
		     mu->8
		     r->9
		     A0->10
		     b0->11*/
  /*double tauB=0;//Bipolar voltage integration characteristic time with membrane depolarisation (ms). 
  double tauA=0;//Amacrine characteristic integration time in the presence of CNO and membrane polarisation (ms)
  double tauRF=0; //Characteristic time of the BCell RF (ms)
  double tauG=0;//Ganglion characteristic integration time in the presence of CNO and membrane polarisation (ms)
  double wba=0; //Bipolar Amacrine  weight (kHz)
  double wab=0; //Amacrine Bipolar weight (kHz)
  double wbg=0; //Bipolar Ganglion weight in the Gaussian pooling (kHz)
  double wag=0; //Amacrine Ganglion weight (kHz)
  double mu=0;//Reduced control parameter (dimensionless)
  double r=0;//tauA/tauB; (dimensionless)
  double A0=0; //Amplitude of the pulse - alpha profile (mV)
  double b0=0;  //Constant value in  the BCell response to a pulse (mV)*/

}stats;

typedef std::pair<double,string> Cell_id_type;

struct compareCell_id_type{
    bool operator()(const Cell_id_type& a, const Cell_id_type& b) const {
        return a.first < b.first;
    }
} ;

typedef std::map<Cell_id_type,double>  Histo ;//Histogram: first field variable and cell type, second field probability
//typedef std::map<Cell_id_type,double,compareCell_id_type>  Histo ;//Histogram: first field variable and cell type, second field probability

typedef struct Moments{//Moments of a distribution
  double mean=0;//Global mean
  double std=0;//Global std
  double mean_ON=0;//ON mean
  double std_ON=0;//ON std
  double mean_OFF=0;//OFF mean
  double std_OFF=0;//OFF std
}moments;

#endif //Defs_h
