/*** Gnuplot routines **/

#ifndef Plot_h
#define Plot_h

#include "Defs.h"
#include <iostream>
#include <fstream>
#include <string>
#include <stdlib.h>
#include <unistd.h>

//Plot the experimental traces only (temporal and power spectrum)
//chaineExp contains the experimental trace. The time length of the plot is t_RF. It can differ from the sampling time in the FFT (Power spectrum defined by p.tmax). ExpCond contains the experimental condition (CTL or CNO). The gnuplot commands are copied in the file GnuName and 2 png files. Title contains the title to give to the figure. Cell_Type contains the type of the figure ON, or OFF. Used to display the legend properly. 
//chaineFFT is the file containing the spectrum; periods contains the periods. The gnuplot commands are copied in the file GnuName and the png  are generated. max_range is the maximal frequency range i.e. the maximal value where the amplitude of the normalized spectrum is larger than zero_PS.
void plot_Experimental_Traces(char* chaineExp,string ExpCond,char* chaineFFT,char* chaineFFT_Th,std::vector<Peaks_fft> periods,params p,string GnuName,char* PlotName,double max_range,double t_RF,string Cell_Type);

//Plot a summary panel which encompasses:
//1) The time response of BCs, ACs, OPL
//2) The time response of RGCs
//3) The spatio-temporal RF
//4) The power spectrum experimental (chaineFFT) and theoretical (chaineFFT_Th). If chaineFFT_Th="" the theoretical power spectrum is not computed.
//chaineT contains the temporal trace of the model, chaineS the spatiotemporal, chaineTh the theoretical (analytic) one, chaineExp contains the experimental trace. If this chain is equal to "void", then it is not plotted.  The time length of the plot is t_RF. It can differ from the sampling time in the FFT (Power spectrum defined by p.tmax). ExpCond contains the experimental condition (CTL or CNO). The gnuplot commands are copied in the file GnuName and 2 png file Space_RF_PlotName  and Time_RF_PlotName are generated. Title contains the title to give to the figure. Cell_Type contains the type of the figure ON, or OFF. Used to display the legend properly. 
//chaineFFT is the file containing the spectrum; periods contains the periods. The gnuplot commands are copied in the file GnuName and the png  PlotName is generated. max_range is the maximal frequency range i.e. the maximal value where the amplitude of the normalized spectrum is larger than zero_PS.
void plot_Summary_Panel(char* chaineT,char* chaineS,char* chaineTh,char* chaineExp,string ExpCond,char* chaineFFT,char* chaineFFT_Th,std::vector<Peaks_fft> periods,params p,string GnuName,char* PlotName,string Title,double max_range,double t_RF,string Cell_Type);

//Plot the power spectrum and places the peaks in frequency
//chaineFFT is the file containing the spectrum; periods contains the periods. The gnuplot commands are copied in the file GnuName and the png  PlotName is generated. Title contains the title to give to the figure. max_range is the maximal frequency range i.e. the maximal value where the amplitude of the normalized spectrum is larger than zero_PS.
void plot_Power_Spectrum(char* chaineFFT,std::vector<Peaks_fft> periods,string GnuName,char* PlotName,string Title,double max_range);

//Plot the 3D net activity - chainBCells, chainACells, chainGCells  - and the RF. chaineT contains the temporal trace of the model, chaineS the spatiotemporal, chaineTh the theoretical (analytic) one, chaineExp contains the experimental trace. If this chain is equal to "void", then it is not plotted. The time length of the plot is t_RF. ExpCond contains the experimental condition (CTL or CNO). One generates the time plots in one png file and the space plots in another png file. The gnuplot commands are copied in the file GnuName and a png file PlotName is generated. Title contains the title to give to the figure.
void plot_RFs_2Figs(char* chaineT,char* chaineS,char* chaineTh,char* chaineExp,string ExpCond,params p,string GnuName,char* PlotName,string Title,double t_RF);

void plot_Net_RF(char* RF_Time,char* RF_Full,char* chainBCells,char* chainACells,char* chainGCells,char* Plotted_RF,params p,char* GnuName,char* PlotName);//Plot the 3D net activity - files chainBCells, chainACells, chainGCells  - and the RF - file RF. BCells, ACells and RGCells are plotted in separated plots (old version). The gnuplot commands are copied in the file GnuName and a png file PlotName is generated. 

void plot_Mode_Evolution(int n, char* RF_Time,char* RF_Full,char* chainBCells,char* chainACells,char* chainGCells,char* Plotted_RF,params p,char* GnuName,char* PlotName,double rho_r,double rho_i,double pi_r,double pi_i);//Plot the 3D net activity when the initial state is prepared in the eigenmode n - files chainBCells, chainACells, chainGCells  - and the RF - file RF. The gnuplot commands are copied in the file GnuName and a png file PlotName is generated. 
 
void plot_Net_RF_Periodic(char* RF_Time,char* RF_Full,char* chainBCells,char* chainACells,char* chainGCells,char* Plotted_RF,params p,char* GnuName,char* PlotName,double tau,double tinf,double tsup);//Plot the 3D net activity - files chainBCells, chainACells, chainGCells  - and the RF - file RF. The gnuplot commands are copied in the file GnuName and a png file PlotName is generated. 

//Plot the stability map in the plane w,tau. a and b parametrize the path
void plot_Stability_Map(char* MapName,params p,char* GnuName,char* PlotName,double a, double b,int Pathway);

//Plot the resonance curve in the plane w,tau. a and b parametrize the path
void plot_Resonance_curve(char* Name,params p,char* GnuName,char* PlotName,double lambdai_min,double lambdar_slow,double max_BC,double max_AC,double max_GC);

//Plot statistics: Histo contains the histogram (must be initialized first with Update_Histo). It is normalized here and moments are computed. The histogram is stored in chaine_hist while a gnuplot script generates the plot and plot the file Name. The gnuplot commands are copied in the file GnuName and  png file PlotName is generated. Label contains the name of the variable. Unit the unit. Mode can be void or exp. In the last case one plots the exponential of the variable. OutMoments contains a file where the moments are loaded. 
void PlotStats(Histo histo,char *chaine_hist,string Name,string GnuName,char* PlotName,char* label,std::string Gene,std::string ExpCond,std::string unit,std::string mode,std::string OutMoments);

#endif //Plot_h
