/*** Library to fit model to data ***/

#include "Fitting.h"

/*** Gets the data.
     Each row corresponds to the temporal trace of each cell, where 
     the first column is the id of the cell and 
     the second column is the id of the experiment. 
     cell_id + experiment_id + gene.
     Data is the string naming the data file
     Gene is the name of the gene.
     sample_rate is the sample rate.
 **/

void GetData(std::string Data,std::string Gene, std::vector <Experiment> *experiences,double sample_rate){
  FILE * fp;
  char row[MAXCHAR];
  char *token;
  int size=100;
  double trace[size];
  Experiment exper;
  Event point;  
  int i=0,j=0;

  fp = fopen(Data.c_str(), "r");
  
  if (fp == NULL) {
    printf("In GetData. File %s can't be opened \n",Data.c_str());
    exit(1);
  }
  
  while (fgets(row, MAXCHAR, fp))
    {
      i=0;
       //split each line on the commas
      token = strtok(row, ",");
      
      while(token != NULL)
        {
	  trace[i] = atof(token);
	  token = strtok(NULL, ",");
	  i++;	  
	  if (i>=size) {
	    printf("\n\n\t\033[0;31mThe size %d of the data file exceeds the size =%d of the array trace. Aborting.\n",i,size);
	    printf("\033[0m");
	    exit(0);
	  }
        }
      
      //Storing data in the structure exper 
      exper.Cell_id=trace[0];
      exper.Exp_id=trace[1];
      exper.Gene=Gene;
      for (j = 2; j < i; j++ ) {
	point.t=(j-2)*1000*sample_rate;
	point.Amp=trace[j];
	exper.data.push_back(point);
      }
      experiences->push_back(exper);
      exper.data.clear();
    }
  
  fclose(fp);
 }

/*** Initializes the params p using a pre fit of the rising and decaying part of the RF. Also generates the power spectrum of the experimental trace having a sample rate sample_rate (expressed in ms, WARNING of the unit). Cell_Type returns an  estimation of the cell type (ON or OFF). ExpCond contains the experimental conditions CTL or CNO. chainDir contains the name of the directory where the power spectrum is stored**/
void preFit(Experiment exper,params *p,string *Cell_Type,string ExpCond,char* chaineDir,double sample_rate){

  //Finding the maximum A1 and storing the trace in the file DatScript for potential futher tests
  FILE *fdata=fopen("DataScript","w");
  double Amax=-1000000,Amin=1000000;
  double tAmax=-1000,tAmin=-1000;
  for (int i=0;i<exper.data.size();i++){
    fprintf(fdata,"%lg\t%lg\n",exper.data[i].t,exper.data[i].Amp);
    if  (Amax<exper.data[i].Amp){
      tAmax=exper.data[i].t;
      Amax=exper.data[i].Amp;
    }
    if (Amin>exper.data[i].Amp){
      tAmin=exper.data[i].t;
      Amin=exper.data[i].Amp;
    }
  }
  fclose(fdata);
  //printf("\t\t\ttAmax=%g, Amax=%g, tAmin=%g, Amin=%g\n",tAmax,Amax,tAmin,Amin);
  // string Cell_Type="";
  //Determinining the cell polarity

  if ((tAmax<tAmin)&&(Amax>0)) {//This is a ON Cell
    *Cell_Type="ON";
  }
  else  if ((tAmax>tAmin)&&(Amin<0)){//This is a OFF Cell
    *Cell_Type="OFF";
  }
  else {printf("\n\t\t\t\t\033[0;31mThis cell has no defined type. Aborting\033[0m\n");exit(0);}
  // printf("\t\t\t\tThis is a %s Cell\n",Cell_Type.c_str());    exit(0);

  //Computing the PowerSpectrum to check if the cell is mono or polyphasic and deduce a refined estimate of the parameters. Stores a plot of the power Spectrum
  char InFile[1000]="",PowerSpectrum[1000]="",GnuName[1000]="",PlotName[1000]="",LocExec[1000]="",Title[1000]="",chaine[1000]="";
  int n_peaks=0;
  double max_range=0;//max_range is the maximal frequency range i.e. the maximal value where the amplitude of the normalized spectrum is larger than zero_PS.
  
  sprintf(InFile,"_%d_%d_%s",exper.Cell_id,exper.Exp_id,ExpCond.c_str());//printf("%s\n",InFile);exit(0);
  sprintf(GnuName,"FFT%s.gnu",InFile);//printf("%s\n",GnuName);exit(0);
  sprintf(LocExec,"%sRFs/FFT%s.gnu",chaineDir,InFile);//printf("%s\n",LocExec);exit(0);    
  sprintf(PlotName,"PowerSpectrum%s.png",InFile);//printf("%s\n",PlotName);exit(0);
  sprintf(PowerSpectrum,"%sRFs/PowerSpectrum_RF%s",chaineDir,InFile);//printf("%s\n",PowerSpectrum);exit(0);
  
  //double sample_rate=0.033*1000/4.0;//Convert sample rate in ms
  std::vector<Peaks_fft> periods=getPeriod(exper,sample_rate,&n_peaks,PowerSpectrum,&max_range); 
  sprintf(PowerSpectrum,"PowerSpectrum_RF%s",InFile);//printf("%s\n",PowerSpectrum);//exit(0);//Label case
  
  printf("\n\t\tThe %d first peaks (periods) in the power spectrum are:\n",n_peaks);
  for (int k=0;k<n_peaks;k++) {
    if (periods[k].f>0) printf("\t\t\t(f=%lg (Hz), T=%lg (ms), Amp=%lg, width=%lg  => %lg (ms))\n",1000*periods[k].f,1.0/(dpi*periods[k].f),periods[k].Amp,1000*periods[k].width,1.0/(dpi*periods[k].width));
    else
      printf("\t\t\t(f=%lg (Hz), inf, Amp=%lg, width=%lg  => %lg (ms)) \n",1000*periods[k].f,periods[k].Amp,1000*periods[k].width,1.0/(dpi*periods[k].width));
  }
  
  plot_Power_Spectrum(PowerSpectrum,periods,LocExec,PlotName,Title,max_range);
  if (exper.Gene=="Grik4") chdir("Grik4/RFs/");
  if (exper.Gene=="Scnn1a") chdir("Scnn1a/RFs/");
  //system("pwd\n");
  sprintf(chaine,"gnuplot %s\n",GnuName);
  printf("\n\t\tExecuting %s",chaine);
  system(chaine);
  chdir("../..");
  sprintf(chaine,"rm -f %s\n",LocExec);
  printf("\n\t\tExecuting %s",chaine);
  system(chaine);
  sprintf(chaine,"rm -f %sRFs/%s\n",chaineDir,PowerSpectrum);
  printf("\n\t\tExecuting %s",chaine);
  system(chaine);
  
  //Generating a gnuplot script to use the gnuplot fitting procedure
  std::string str="";
  //char chaine[1000];
  sprintf(GnuName,"FitScript.gnu");
  char Command[1000]="";
  std::string cote="'";//Used in gnuplot to define the symbol '
  std::ofstream cout;//ofstream used to handle strings
  remove("FitParams");
  cout.open(GnuName);
  cout<<"#Generating a gnuplot script to use the gnuplot fitting procedure#\n\n";
  cout << "reset\n";
  cout<<"\n#Pre-fit of the BCs response#\n";
  //sprintf(chaine,"set yrange [%g:%g]\n",Amin,Amax);str=chaine;cout << str;
  sprintf(chaine,"f1(x,tauRF,A1,B1)=A1*x**3*exp(-x/tauRF)/(3*tauRF**2)+B1\n");str=chaine;cout << str;
  sprintf(chaine,"set fit quiet\n");str=chaine;cout << str;
  if (*Cell_Type=="ON") {
    sprintf(chaine,"tauRF=%g\nA1=%g\nB1=%g\n",tAmax,Amax,p->b0);str=chaine;cout << str;
    sprintf(chaine,"fit [0:%g] f1(x,tauRF,A1,B1) ",tAmax);
  }
  else if (*Cell_Type=="OFF"){
    sprintf(chaine,"tauRF=%g\nA1=%g\nB1=%g\n",p->tauRF,Amin,p->b0);str=chaine;cout << str;
    sprintf(chaine,"fit [0:%g] f1(x,tauRF,A1,B1) ",tAmin);
  }
  //str=chaine+cote+"DataScript"+cote+" u 1:2 via A1,B\n";cout<<str;
  //str=chaine+cote+"DataScript"+cote+" u 1:2 via B\n";cout<<str;
  //str=chaine+cote+"DataScript"+cote+" u 1:2 via tauRF\n";cout<<str;
  str=chaine+cote+"DataScript"+cote+" u 1:2 via A1,B1,tauRF\n";cout<<str;
  str="set print"+cote+"FitParams"+cote+"\n";cout<<str;

  cout<<"\n#Pre-fit of the ACs response#\n";
  sprintf(chaine,"f2(x,A2,tauA,trelax2,B2)=A2*exp(-(x-trelax2)/tauA)+B2\n");str=chaine;cout << str;
  if (*Cell_Type=="ON") {
    sprintf(chaine,"tauA=%g\ntrelax2=%g\nA2=%g\nB2=B1\n",p->tauA,tAmin,Amin);str=chaine;cout << str;
    sprintf(chaine,"fit [%g:%g] f2(x,A2,tauA,trelax2,B2)  ",tAmin+1,p->tmax);
  }
  else if (*Cell_Type=="OFF"){
    sprintf(chaine,"tauA=%g\ntrelax2=%g\nA2=%g\nB2=B1\n",p->tauA,tAmax,Amax);str=chaine;cout << str;
    sprintf(chaine,"fit [%g:%g] f2(x,A2,tauA,trelax2,B2)  ",tAmax,p->tmax);
  }
  str=chaine+cote+"DataScript"+cote+" u 1:2 via A2\n";cout<<str;
  str=chaine+cote+"DataScript"+cote+" u 1:2 via B2\n";cout<<str;
  str=chaine+cote+"DataScript"+cote+" u 1:2 via tauA\n";cout<<str;
  //str=chaine+cote+"DataScript"+cote+" u 1:2 via A2,B2,tauA\n";cout<<str;

  cout<<"\n#Pre-fit of the oscillations (if any)#\n";
  sprintf(chaine,"f3(x,A3,tau3,trelax3,B3)=A3*exp(-(x-trelax3)/tau3)*(1+cos(omega*(x-trelax3)))+B3\n");str=chaine;cout << str;
  double nu=0,tau3=0;
  if (n_peaks>=1){//There is a a non zero frequency in the spectrum
    nu=periods[1].f;//kHz
    tau3=1.0/(dpi*periods[1].width);
    
    if ((nu>1E-4)&&(tau3>1E-4)){ 
      sprintf(chaine,"nu=%g #kHz\nomega=%g\nA3=A2\ntau3=%g\nB3=B2\ntrelax3=trelax2/2\n",nu,dpi*nu,tau3);str=chaine;cout << str;
      if (*Cell_Type=="ON") {
	sprintf(chaine,"fit [%g:%g] f3(x,A3,tau3,trelax3,B3)  ",tAmin+1,p->tmax);
      }
      else if (*Cell_Type=="OFF"){
	sprintf(chaine,"fit [%g:%g] f3(x,A3,tau3,trelax3,B3)  ",tAmax,p->tmax);
      }
      str=chaine+cote+"DataScript"+cote+" u 1:2 via A3\n";cout<<str;
      str=chaine+cote+"DataScript"+cote+" u 1:2 via B3\n";cout<<str;
      str=chaine+cote+"DataScript"+cote+" u 1:2 via trelax3\n";cout<<str;
      str=chaine+cote+"DataScript"+cote+" u 1:2 via omega\n";cout<<str;
      str=chaine+cote+"DataScript"+cote+" u 1:2 via A3,B3,trelax3,omega\n";cout<<str;
      
      cout<<"\n#Print the results#\n";
      str="print sprintf("+cote+"%g %g %g %g %g %g"+cote+",A1,tauRF,B1,tauA,abs(2*pi/omega),tau3)"+"\n";cout<<str;
    }
    else{
      cout<<"\n#Print the results#\n";
      str="print sprintf("+cote+"%g %g %g %g %g %g"+cote+",A1,tauRF,B1,tauA,10000,10000)"+"\n";cout<<str;
    }
    //print sprintf("%g\n%g\n%g\n",A1,tauRF,B) 
    str="set print\n";cout<<str;
  }//End of there is a a non zero frequency in the spectrum
  else{
       cout<<"\n#Print the results#\n";
       str="print sprintf("+cote+"%g %g %g %g"+cote+",A1,tauRF,B,tauA)"+"\n";cout<<str;   
  }
  cout.close();
  sprintf(Command,"gnuplot %s\n",GnuName);
  system(Command);
 
  FILE *fparam=fopen("FitParams","r");
  double A0=0,tauRF=0,b0=0,tauA=0,period_seek=0,width_seek=0;
  fscanf(fparam,"%lg %lg %lg %lg %lg %lg",&A0,&tauRF,&b0,&tauA,&period_seek,&width_seek);
  fclose(fparam);
  
  //Seeking the value of s=wab/wba in the RFs map
  char chaineMap[1000];
  sprintf(InFile,"_N%d_An",p->N);
  sprintf(chaineMap,"%sMap/StabilityMap_LaplacianConnection_vs_r%s",chaineDir,InFile);
  FILE *fmap=fopen(chaineMap,"r");
  if (fmap==NULL) {printf("The file %s cannot be opened\n",chaineMap);exit(0);}
  double r=0,s=0,period1=0,width1=0,period2=0,width2=0,r_seek=tauA/tauRF;
  printf("\n\t\tSeeking r=%g, period=%g, width=%g\n",r_seek,period_seek,width_seek);//exit(0);
  int code=0,peaks=0;

  double dist_best_r=10000,dist_best_s=10000,dist=0,r_best=0,s_best=0,period_best1=0,width_best1=0,period_best2=0,width_best2=0;
  double period_seek1=1.0/periods[0].f,width_seek1=1.0/periods[0].width;//ms
  double period_seek2=1.0/periods[1].f,width_seek2=1.0/periods[1].width;//ms
  char comment[10000];
 
/*  while (fscanf(fmap,"%lg%lg%d%d",&r,&s,&code,&peaks)!=EOF){//Scanning the RF map to find the best r
    if (peaks==1){
      fscanf(fmap,"%lg%lg",&period1,&width1);
    }
    else if (peaks==2){
      fscanf(fmap,"%lg%lg%lg%lg",&period1,&width1,&period2,&width2);
      dist=sqrt(pow(period1-periods[0].f,2)+pow(period2-periods[1].f,2));
      if (dist<dist_best_s){
	dist_best_s=dist;
	s_best=s;
	r_best=r;
	period_best=period2;
	width_best=width2;
      }
    }
    else if (peaks>2){
      printf("The number of peaks is %d >2. Aborting\n",peaks);exit(0);
    }
  }
*/
  while (fscanf(fmap,"%lg%lg%d%d%[^\n]",&r,&s,&code,&peaks,&comment)!=EOF){//Scanning the RF map to find the best r
    if (peaks==1){
      fscanf(fmap,"%lg%lg%[^\n]",&period1,&width1,&comment);
    }
    else if (peaks==2){
      fscanf(fmap,"%lg%lg%lg%lg%[^\n]",&period1,&width1,&period2,&width2,&comment);
    }
    /*else if (peaks>2){
      printf("The number of peaks is %d >2. Abortig\n",peaks);exit(0);
      }*/
    //printf("r=%g, s=%g, code=%d, period=%g, width=%g\n",r,s,code,period,width);exit(0);
    dist=fabs(r-r_seek);
    if (dist<dist_best_r){
      r_best=r;
      dist_best_r=dist;
    }
  }//End of scanning the RF map  to find the best r
  
  rewind(fmap);
  dist_best_s=10000;
 while (fscanf(fmap,"%lg%lg%d%d%[^\n]",&r,&s,&code,&peaks,&comment)!=EOF){//Scanning the RF map to find the best s
    if (peaks==1){//Peak=1
      fscanf(fmap,"%lg%lg%[^\n]",&period1,&width1,&comment);
    }      
    else if (peaks==2){//peak=2
      fscanf(fmap,"%lg%lg%lg%lg%[^\n]",&period1,&width1,&period2,&width2,&comment);
      if (fabs(r-r_seek)==dist_best_r){//Scanning the file at the best r
	period1*=1000;period2*=1000;
	width1*=1000;width2*=1000;
	//dist=sqrt(pow(period2-period_seek2,2)+pow(width2-width_seek2,2));
	dist=sqrt(pow(period2-period_seek2,2));
	if (dist<dist_best_s){
	  dist_best_s=dist;
	  s_best=s;
	  period_best1=period1;
	  width_best1=width1;
	  period_best2=period2;
	  width_best2=width2;
	}
      }//End of scanning the file at the best r
    }//End of peak=2
  }//End of scanning the RF map to find the best s
  
  if (s_best==0) s_best=1;
  fclose(fmap);
  
  //Remapping
  double wba=0.0085,tauB=30;
  double renorm=(pow(p->wba*p->tauB,2)/pow(wba*tauB,2));
  s_best*=renorm;
  printf("\t\t\tThese values have been found: r=%g, s=%g, period_best1=%g, period_seek1=%g, width_best1=%g, width_seek1=%g, period_best2=%g, period_seek2=%g, width_best2=%g, width_seek2=%g\n",r_best,s_best,period_best1,period_seek1,width_best1,width_seek1,period_best2,period_seek2,width_best2,width_seek2);

      // exit(0);
  if (*Cell_Type=="ON") p->A0=Amax;
  else if (*Cell_Type=="OFF") p->A0=Amin;
  p->tauRF=tauRF;
  p->b0=b0;
  p->tauA=tauA;
  p->tauB=p->tauBref=p->tauRF;
  p->tauAref=p->tauA;
  p->tauG=p->tauGref=1.1*p->tauB;//GCell leak characteristic time (ms) 
  p->wab=-s_best*p->wba;
  
  UpdateParams(p);
}

/*** Fits a power spectrum in the r,s plane 
p contains the parameters;
PowerSpectrum is the name of the file where the power spectrum is stored
max_iter is the maximal number of iterations for the fit
precision is the precision required
sample_rate is the rate at which the power spectrum has been sampled.
On output r,s,p are modified 
***/
void Fit_FT(double *r,double* s,params *p,char* PowerSpectrum,int max_iter,double precision,double sample_rate){
  //Loading data
  vector<Event > data;
  FILE* fp=fopen(PowerSpectrum,"r");
  if (fp==NULL) {printf("The file %s could not be opened. Aborting.\n",PowerSpectrum);exit(0); }
  double f=0,Pf=0;
  while (fscanf(fp,"%lg%lg",&f,&Pf)!=EOF){
    Event point={f,Pf};
    data.push_back(point);
   }
  fclose(fp);
  int n_peaks=0;
  double max_range=0;
  std::vector<Peaks_fft> periods= getPeriod(data,sample_rate,&n_peaks,"",&max_range);
  double fmax_fit=data[data.size()-1].t,fmin_fit=data[0].t;//printf("fmax_fit=%g, fmin_fit=%g\n",fmax_fit,fmin_fit);exit(0);
  double dsurT=2.0/double(fmax_fit-fmin_fit);
  double norm=0;
  for (int ii=0;data[ii].t<fmax_fit;ii++){//Loop on data points
    for (int k=0;k<periods.size();k++){//Loop on peaks to weight the distance.
      //norm+=periods[k].Amp*Lorentz(data[ii].t,periods[k].f,periods[k].width);
      norm+=Lorentz(data[ii].t,periods[k].f,periods[k].width);
    }//End of loop on peaks to weight the distance.
  }//End of loop on data points

  //Setting parameters
  double weight_factor=1E-3;
  bool View=false,checks=true;
  int dim_ext=4*p->L;// Dimension of the phase space
  int dim=3*p->L;// Dimension of the phase space without activities
  
  int iGC=p->N/2;//Index of the GCell on the lattice
  int alphaBC=iGC;//Index of the BCell above this GCell in the extended phase space
  int alphaAC=iGC+p->L;//Index of the ACell above this GCell in the extended phase space
  int alphaGC=iGC+2*p->L;//Index of the GCell in the extended phase space
  int alphaAc=iGC+3*p->L;//Index of the BCell activity in the extended phase space
  double xGC=iGC*p->dx,yGC=0;//Physical coordinates of the GCell (mm)

  int iter=0;
  int nbview=0,strobo=10;

  fp=fopen("Evol_Distance_FT","w");fclose(fp);
  double d_iter=100,dist=100,dist_av=100,dd=10;
  gsl_matrix *Psi=gsl_matrix_calloc(p->L,p->L),*Psi_transpose=gsl_matrix_calloc(p->L,p->L);
  Spectrum kappa_th;//Theoretical eigenvalues of the connectivity matrix amacrine bipolar
  Spectrum sigma_th;//Theoretical eigenvalues of the connectivity matrix bipolar amacrine
  gsl_vector_complex* SumPinvbgKSg=gsl_vector_complex_calloc(dim);//Auxilary used for computation
  gsl_vector_complex* SumPinvbgKSg_CNOA=gsl_vector_complex_calloc(dim);//Auxilary used for computation
  
  double VB_rest=0, VA_rest=0,VG_rest=0,A_rest=0;
  double *X=(double*)calloc(dim_ext,sizeof(double));//State vector time t
  ComputeTheoreticalModesLaplacian1D(*p,&kappa_th,&sigma_th,Psi,Psi_transpose);
  double epsilonr=10; 
  double epsilons=10;
  
  while ((iter<=max_iter)&&(dist>precision)&&(d_iter>1E-7)){//Estimation loop
    if (!(nbview%strobo)) printf("\n\titer=%d\n",iter);

    init_Network_Laplacian(p,1);   
    ComputeTheoreticalSpectrumL(kappa_th,sigma_th,Psi,Psi_transpose,p);
    //if (!IsStable(*p)) {printf("\t\t\033[0;31mIn FitCell, at iteration %d, dynamics is linearly unstable \033[0m\n",iter); }
    if (checks){ //Beginning of comparing numerical and theoretical eigenvectors
      
      // printf("\n\tChecking inversion\n");
      if (CheckInverse(p->P,p->Pinv)) {
	//printf("\n\tPinv is the inverse of P\n");
      }
      else {
	printf("\n\tPinv is not the inverse of P, aborting\n");
	exit(0);
      }
      
      //printf("\n\tChecking theoretical eigenvalues and eigenvectors\n");
      if (TestSpectrum(p->cL,p->P,p->lambda_th)){//Tests eigenvalues and eigenvectors
	//  printf("\n\tThe spectrum is OK\n");
      }
      else {
	printf("\n\tP computation failed\n");
	exit(0);
      }
    }//End of comparing numerical and theoretical eigenvectors

    //printf("\n\t\t--------------------\n\t\tComputing the initial power spectrum\n\t\t--------------------\n");
    gsl_complex kg={0,0};
    std::vector <Event> ThRF; //Theoretical FT of a GCell alpha embedded in the BCell, ACell, GCell network
    Event pointTh;
     
    //double y=0;
    KSG(xGC,yGC,*p, SumPinvbgKSg);//KSG contains the value of the spatial R at point xGC,yGC.
    KG_CNOA(*p,SumPinvbgKSg_CNOA);//CNO A contribution

    double max_amp=0;
    for (int ii=0;(f=data[ii].t)<fmax_fit;ii++){//Frequency loop. .t corresponds here to a frequency.   
 	kg=gsl_complex_div_real(KG_hat(alphaGC,xGC,0,f,*p,SumPinvbgKSg,SumPinvbgKSg_CNOA),sample_rate);
	pointTh.t=f;pointTh.Amp=gsl_complex_abs(kg);
	max_amp=max(max_amp,pointTh.Amp);
	ThRF.push_back(pointTh);
    }//End of time loop
    
    for (int ii=0;ii<ThRF.size();ii++){//Normalisation
      ThRF[ii].Amp/=max_amp;
    }
	   
    /*   fp=fopen("Fit_Power_Spectrum","w");
    for (int ii=0;(f=data[ii].t)<fmax_fit;ii++) fprintf(fp,"%g\t%g\t%g\n",ThRF[ii].t,ThRF[ii].Amp,data[ii].Amp;
    fclose(fp);*/

    //Computes the distance between data and theoretical RF. The distance gives more weight on the peaks
    //dist=Distance(data,ThRF,fmax_fit);
    dist=0;
    for (int ii=0;data[ii].t<fmax_fit;ii++){//Loop on data points
      for (int k=0;k<periods.size();k++){//Loop on peaks to weight the distance with the Lorentzians.
	//dist+=pow(data[ii].Amp-ThRF[ii].Amp,2)*periods[k].Amp*Lorentz(data[ii].t,periods[k].f,periods[k].width);
	dist+=pow(data[ii].Amp-ThRF[ii].Amp,2)*Lorentz(data[ii].t,periods[k].f,periods[k].width);
      }//End of loop on peaks to weight the distance.
    }//End of loop on data points
    dist=dist/norm;

    d_iter=fabs(dd-dist);
    dd=dist;
    if (!(nbview%strobo)){
      printf("\t\tThe distance between experimental data and fit is %lg, increment=%lg, r=%lg, s=%lg\n",dist,d_iter,*r,*s);
      fp=fopen("Evol_Distance_FT","a");
      fprintf(fp,"%d\t%lg\t%lg\t%lg\t%lg\t%lg\t%lg\t%lg\t%lg\t%lg\t%lg\t%lg\t%lg\t%lg\n",iter,dist,p->wba,p->wab,p->wbg,p->wag,p->tauA,p->tauB,p->tauG,p->tauRF,p->zetaA,p->zetaG,p->A0,p->b0);
      fclose(fp);   
      fp=fopen("Fit_Power_Spectrum","w");
      for (int ii=0;(f=data[ii].t)<fmax_fit;ii++) fprintf(fp,"%g\t%g\t%g\n",data[ii].t,ThRF[ii].Amp,data[ii].Amp);
      fclose(fp);
    }
  /*  if (dist/dist_av>1){//The distance increases, which should not happen. Reducing epsilon
      printf("The distance increase dist=%g > dist_av=%g, which should not happen. Reducing epsilon.\n",dist,dist_av);
      epsilonr/=2.0;
      epsilons/=2.0;
    }*/
    dist_av=dist;

    //Computes the gradients
    std::vector <Event> dThRFdr,dThRFds;
    params pdp;
    double scaling=max_amp*sample_rate;//Takes into account that kg has been rescaled=>used in the linear response computation
    //Derivative in the direction r
    CopyParams(*p,&pdp);//Parallel computation. Update of parameters are made independently
    //WriteParams(pdp);
    pdp.tauA=(*r+epsilonr)*pdp.tauB;
    UpdateParams(&pdp);

    ComputeLinearResponse_hat(&pdp,fmax_fit,iGC,data,ThRF,&dThRFdr,kappa_th,sigma_th,Psi,Psi_transpose, SumPinvbgKSg,SumPinvbgKSg_CNOA,View,checks,scaling);
    delete_params(&pdp);//Freeing pointers in p. As ComputeLinearResponse generates new alloc
    
    //Derivative in the direction s
    CopyParams(*p,&pdp);//Parallel computation. Update of parameters are made independently
    //WriteParams(pdp);
    pdp.wab=-(*s+epsilons)*pdp.wba;
    UpdateParams(&pdp);
    
    ComputeLinearResponse_hat(&pdp,fmax_fit,iGC,data,ThRF,&dThRFds,kappa_th,sigma_th,Psi,Psi_transpose, SumPinvbgKSg,SumPinvbgKSg_CNOA,View,checks,scaling);
    delete_params(&pdp);//Freeing pointers in p. As ComputeLinearResponse generates new alloc
    
    //Update parameters   
    //printf("\n\t\tUpdating parameters\n");
    double dpdr=0,dpds=0;

    for (int ii=0;(f=data[ii].t)<fmax_fit;ii++){//Time loop where time is indexed on data
      double dif=0;
      for (int k=0;k<periods.size();k++){//Loop on peaks to weight the distance.
	//dif+=(data[ii].Amp-ThRF[ii].Amp)*periods[k].Amp*Lorentz(data[ii].t,periods[k].f,periods[k].width);
 	dif+=(data[ii].Amp-ThRF[ii].Amp)*Lorentz(data[ii].t,periods[k].f,periods[k].width);
      }
      dpdr+=dif*dThRFdr[ii].Amp;
      dpds+=dif*dThRFds[ii].Amp;
    }

    *r+=dpdr*dsurT/norm;
    *s+=dpds*dsurT/norm;
    p->tauA=(*r)*p->tauB;
    p->wab=-(*s)*p->wba;
    UpdateParams(p);
 
   //Updates iterations
    if (!(nbview%strobo)){
      // printf("\t\t(r=%lg, s=%lg)\n ",*r,*s);
      nbview=0;
    }
    nbview++;
    iter++;
  }//End of estimation loop

  My_gsl_matrix_free(Psi);My_gsl_matrix_free(Psi_transpose);
  My_gsl_vector_complex_free(SumPinvbgKSg);
  My_gsl_vector_complex_free(SumPinvbgKSg_CNOA);
  free(X);
}

/** Fit the model from a experimental cell by minimising the distance between the temporal RF and the model. Spatial RF is not taken into account.
    The cell data are stored in exp_file (format time (ms), RF)
    The model vector field is f
    tmin_fit,tmax_fit are the lower and upper bound for the time interval of fit
    fit_variables is a vector of booleans selecting the parameters to fit.
    max_iter is the maximum number of iterations 
    precision is the required precisions
**/
void FitCell(std::string exp_file,params *p,void (*f)(double *X,double *Xp,double t,params p,double (*input)(double x,double t, params p)),double tmin_fit,double tmax_fit,fit_switch fit_variables,int max_iter,double precision){
  
  double weight_factor=1E-3;
  bool View=false,checks=true;
  int dim_ext=4*p->L;// Dimension of the phase space
  int dim=3*p->L;// Dimension of the phase space without activities
  
  int iGC=p->N/2;//Index of the GCell on the lattice
  int alphaBC=iGC;//Index of the BCell above this GCell in the extended phase space
  int alphaAC=iGC+p->L;//Index of the ACell above this GCell in the extended phase space
  int alphaGC=iGC+2*p->L;//Index of the GCell in the extended phase space
  int alphaAc=iGC+3*p->L;//Index of the BCell activity in the extended phase space
  double xGC=iGC*p->dx,yGC=0;//Physical coordinates of the GCell (mm)
  
  printf("\n\t*************************\n\t\t\tLoading the experimental file %s\n\t*************************\n\n",exp_file.c_str());
  FILE *f_exp=fopen(exp_file.c_str(),"r");
  if (f_exp==NULL) {printf("File %s not found, aborting\n",exp_file);exit(0);}
  Event point;
  std::vector <Event> data;
  while (fscanf(f_exp,"%lg\t%lg",&(point.t),&(point.Amp))!=EOF){
    point.t*=1000; //Conversion to ms
    data.push_back(point);
  }
  fclose(f_exp);
  
  double T=data[data.size()-1].t;
  if (T<tmax_fit){printf("tmax_fit=%lg  is longer than data set T=%lg. Setting tmax_fit=T",tmax_fit,T);tmax_fit=T;};
  double dsurT=2.0/double(tmax_fit-tmin_fit);
  
  //printf("\n\t*************************\n\t\t\t Computing the RF for a predefined set of parameters values \n\t*************************\n");
  
  int iter=0;
  int nbview=0,strobo=10;

  FILE *fp=fopen("Evol_Distance","w");fclose(fp);
  double d_iter=100,d=100,dd=10;
  gsl_matrix *Psi=gsl_matrix_calloc(p->L,p->L),*Psi_transpose=gsl_matrix_calloc(p->L,p->L);
  Spectrum kappa_th;//Theoretical eigenvalues of the connectivity matrix amacrine bipolar
  Spectrum sigma_th;//Theoretical eigenvalues of the connectivity matrix bipolar amacrine
  gsl_vector_complex* SumPinvbgKSg=gsl_vector_complex_calloc(dim);//Auxilary used for computation
  gsl_vector_complex* SumPinvbgKSg_CNOA=gsl_vector_complex_calloc(dim);//Auxilary used for computation
  
  double VB_rest=0, VA_rest=0,VG_rest=0,A_rest=0;
  double *X=(double*)calloc(dim_ext,sizeof(double));//State vector time t

  ComputeTheoreticalModesLaplacian1D(*p,&kappa_th,&sigma_th,Psi,Psi_transpose);
  
  double epsilonwba=weight_factor; //kHz
  double epsilonwab=weight_factor; //kHz
  double epsilonwbg=weight_factor; //kHz
  double epsilonwag=weight_factor; //kHz
  double epsilonunsurtauA=10*weight_factor; //kHz
  double epsilonunsurtauB=10*weight_factor; //kHz
  double epsilonunsurtauG=10*weight_factor; //kHz
  double epsilonunsurtauRF=10*weight_factor; //kHz
  double epsilonA0=0.01;//mV
  double epsilonb0=0.01;//mV
  double epsilonzetaA=1E-6; //mV/ms
  double epsilonzetaG=1E-6;//mV/ms
  double epsilonlambdaA=0.01*weight_factor; //kHz
  double epsilonlambdaG=0.01*weight_factor;//kHz
  double epsilonECNOA=0.1; //mV
  double epsilonECNOG=1;//mV
  double d_min=1E-3,d_max=1E-1;

  int rescale=0; //Used to stop iterations when some derivative become too large
  bool Stop=false;

  while ((iter<=max_iter)&&(d>precision)&&(d_iter>1E-12)&&(!Stop)){//Estimation loop
    if (!(nbview%strobo)) printf("\n\titer=%d\n",iter);

    init_Network_Laplacian(p,1);   
    ComputeTheoreticalSpectrumL(kappa_th,sigma_th,Psi,Psi_transpose,p);
    //if (!IsStable(*p)) {printf("\t\t\033[0;31mIn FitCell, at iteration %d, dynamics is linearly unstable \033[0m\n",iter); }
    if (checks){ //Beginning of comparing numerical and theoretical eigenvectors
      
      // printf("\n\tChecking inversion\n");
      if (CheckInverse(p->P,p->Pinv)) {
	//printf("\n\tPinv is the inverse of P\n");
      }
      else {
	printf("\n\tPinv is not the inverse of P, aborting\n");
	exit(0);
      }
      
      //printf("\n\tChecking theoretical eigenvalues and eigenvectors\n");
      if (TestSpectrum(p->cL,p->P,p->lambda_th)){//Tests eigenvalues and eigenvectors
	//  printf("\n\tThe spectrum is OK\n");
      }
      else {
	printf("\n\tP computation failed\n");
	exit(0);
      }
    }//End of comparing numerical and theoretical eigenvectors
    
    //printf("\n\t\t--------------------\n\t\tComputing the initial RF\n\t\t--------------------\n");
    gsl_complex kg={0,0};//Computes the RF of a GCell alpha embedded in the BCell, ACell, GCell network   
    std::vector <Event> ThRF;
    Event pointTh;
    double t=0;
    
    //double y=0;
    KSG(xGC,yGC,*p, SumPinvbgKSg);//KSG contains the value of the spatial R at point xGC,yGC.
    KG_CNOA(*p,SumPinvbgKSg_CNOA);//CNO A contribution

    //for (double t=-0.1;t<=p->tmax_fit;t+=p->dt){
    for (int ii=0;(t=data[ii].t)<tmax_fit;ii++){//Time loop where time is indexed on data      
      kg=KG(alphaGC,xGC,0,t-p->t0,*p,SumPinvbgKSg,SumPinvbgKSg_CNOA);
      if (fabs(GSL_IMAG(kg)>1E-8)) {printf("Problem. The RF is not real. Imaginary part at (%lg,%lg,%lg)=%lg",xGC,yGC,t,kg);exit(0);}
      
      pointTh.t=t;pointTh.Amp=GSL_REAL(kg)+VG_rest;
      ThRF.push_back(pointTh);
    }//End of time loop
    
    
    //Computes the distance between data and theoretical RF
    d=Distance(data,ThRF,tmax_fit);
    d_iter=fabs(dd-d);
    dd=d;
    if (!(nbview%strobo)){
      printf("\t\tThe distance between experimental data and fit is %lg, increment=%lg\n",d,d_iter);
      fp=fopen("Evol_Distance","a");
      fprintf(fp,"%d\t%lg\t%lg\t%lg\t%lg\t%lg\t%lg\t%lg\t%lg\t%lg\t%lg\t%lg\t%lg\t%lg\n",iter,d,p->wba,p->wab,p->wbg,p->wag,p->tauA,p->tauB,p->tauG,p->tauRF,p->zetaA,p->zetaG,p->A0,p->b0);
      fclose(fp);   
    }

    //Computes the gradients
    //printf("\n--------------------\n \t\t\tComputing gradients\n--------------------\n");
    std::vector <Event> dThRFdwba,dThRFdwab,dThRFdwag,dThRFdwbg,dThRFdunsurtauA,dThRFdunsurtauB,dThRFdunsurtauG,dThRFdunsurtauRF,dThRFdA0,dThRFdb0,dThRFdlambdaA,dThRFdlambdaG,dThRFdzetaA,dThRFdzetaG,dThRFdECNOA,dThRFdECNOG;
    params pdp;

    if (fit_variables.fitwba){//Gradient with respect to wba
      //printf("\t\t\t=> dRF/dwba\n");
      CopyParams(*p,&pdp);//Parallel computation. Update of parameters are made independently
      pdp.wba+=epsilonwba;
      pdp.mu=-pdp.wab*pdp.wba*pdp.tau2;
       
      ComputeLinearResponse(&pdp,tmax_fit,iGC,data,ThRF,&dThRFdwba,kappa_th,sigma_th,Psi,Psi_transpose, SumPinvbgKSg,SumPinvbgKSg_CNOA,View,checks);
      delete_params(&pdp);//Freeing pointers in p. As ComputeLinearResponse generates new allocations, one has to free memory to avoid saturating it.
     }//End of gradient with respect to wba
    
    if (fit_variables.fitwab){//Gradient with respect to wab
      //printf("\n\t\t\t=> dRF/dwab\n");
      CopyParams(*p,&pdp);//Parallel computation. Update of parameters are made independently
      pdp.wab+=epsilonwab;
      pdp.mu=-pdp.wab*pdp.wba*pdp.tau2;
      
      ComputeLinearResponse(&pdp,tmax_fit,iGC,data,ThRF,&dThRFdwab,kappa_th,sigma_th,Psi,Psi_transpose, SumPinvbgKSg,SumPinvbgKSg_CNOA,View,checks);
      delete_params(&pdp);//Freeing pointers in p. As ComputeLinearResponse generates new allocations, one has to free memory to avoid saturating it.
    }//End of gradient with respect to wab

    if (fit_variables.fitwag){//Gradient with respect to wag
      //printf("\n\t\t\t=> dRF/dwag\n");
      CopyParams(*p,&pdp);//Parallel computation. Update of parameters are made independently
      pdp.wag+=epsilonwag;

      ComputeLinearResponse(&pdp,tmax_fit,iGC,data,ThRF,&dThRFdwag,kappa_th,sigma_th,Psi,Psi_transpose, SumPinvbgKSg,SumPinvbgKSg_CNOA,View,checks);
      delete_params(&pdp);//Freeing pointers in p. As ComputeLinearResponse generates new allocations, one has 
    }//End of gradient with respect to wag
    
    if (fit_variables.fitwbg){//Gradient with respect to wbg
      //printf("\n\t\t\t=> dRF/dwbg\n");
       CopyParams(*p,&pdp);//Parallel computation. Update of parameters are made independently
       pdp.wbg+=epsilonwbg;
     
      ComputeLinearResponse(&pdp,tmax_fit,iGC,data,ThRF,&dThRFdwbg,kappa_th,sigma_th,Psi,Psi_transpose, SumPinvbgKSg,SumPinvbgKSg_CNOA,View,checks);
      delete_params(&pdp);//Freeing pointers in p. As ComputeLinearResponse generates new allocations, one has 
    }//End of gradient with respect to wbg

    if (fit_variables.fittauA){//Gradient with respect to tauA
      //printf("\n\t\t\t=> dRF/dunsurtauA\n");
      CopyParams(*p,&pdp);//Parallel computation. Update of parameters are made independently
      pdp.unsurtauA+=epsilonunsurtauA;//1/tauA (kH)z
      pdp.tauA=1.0/pdp.unsurtauA;
      pdp.unsurtau=pdp.unsurtauA-pdp.unsurtauB;//1/tauA-1/tauB (kHz)
      pdp.unsurtauAB=pdp.unsurtauA+pdp.unsurtauB;//1/tauA+1/tauB (kHz)
      if ((fabs(pdp.unsurtau)>1E-10)) {
	pdp.tau=1.0/pdp.unsurtau;
      }
      else {
	printf("Case tau^-1=0 not implemented yet, exiting\n");
	exit(0);
      }
      pdp.tau2=pow(pdp.tau,2);
      pdp.tauAB=1.0/pdp.unsurtauAB;
      pdp.mu=-pdp.wab*pdp.wba*pdp.tau2;

      ComputeLinearResponse(&pdp,tmax_fit,iGC,data,ThRF,&dThRFdunsurtauA,kappa_th,sigma_th,Psi,Psi_transpose, SumPinvbgKSg,SumPinvbgKSg_CNOA,View,checks);
      delete_params(&pdp);//Freeing pointers in p. As ComputeLinearResponse generates new allocations, one has 
    }//End of gradient with respect to tauA

    if (fit_variables.fittauB){//Gradient with respect to tauB
      //printf("\n\t\t\t=> dRF/dunsurtauB\n");
      CopyParams(*p,&pdp);//Parallel computation. Update of parameters are made independently
      pdp.unsurtauB+=epsilonunsurtauB;//1/tauB (kHz)
      pdp.tauB=1.0/pdp.unsurtauB;
      //pdp.tauRF=pdp.tauB;
      pdp.unsurtau=pdp.unsurtauA-pdp.unsurtauB;//1/tauA-1/tauB (kHz)
      pdp.unsurtauAB=pdp.unsurtauA+pdp.unsurtauB;//1/tauA+1/tauB (kHz)
      if ((fabs(pdp.unsurtau)>1E-10)) {
	pdp.tau=1.0/pdp.unsurtau;
      }
      else {
	printf("Case tau^-1=0 not implemented yet, exiting\n");
	exit(0);
      }
      pdp.tau2=pow(pdp.tau,2);
      pdp.tauAB=1.0/pdp.unsurtauAB;
      pdp.mu=-pdp.wab*pdp.wba*pdp.tau2;

      ComputeLinearResponse(&pdp,tmax_fit,iGC,data,ThRF,&dThRFdunsurtauB,kappa_th,sigma_th,Psi,Psi_transpose, SumPinvbgKSg,SumPinvbgKSg_CNOA,View,checks);
      delete_params(&pdp);//Freeing pointers in p. As ComputeLinearResponse generates new allocations, one has 
    }//End of gradient with respect to tauB

    if (fit_variables.fittauG){//Gradient with respect to tauG
      //printf("\n\t\t\t=> dRF/dunsurtauG\n");
      CopyParams(*p,&pdp);//Parallel computation. Update of parameters are made independently
      pdp.unsurtauG+=epsilonunsurtauG;//1/tauG (kH)z
      pdp.tauG=1.0/pdp.unsurtauG;

      ComputeLinearResponse(&pdp,tmax_fit,iGC,data,ThRF,&dThRFdunsurtauG,kappa_th,sigma_th,Psi,Psi_transpose, SumPinvbgKSg,SumPinvbgKSg_CNOA,View,checks);
      delete_params(&pdp);//Freeing pointers in p. As ComputeLinearResponse generates new allocations, one has 
     }//End of gradient with respect to tauG

    if (fit_variables.fittauRF){//Gradient with respect to tauRF
      //printf("\n\t\t\t=> dRF/dunsurtauRF\n");
      CopyParams(*p,&pdp);//Parallel computation. Update of parameters are made independently
      pdp.tauRF=1.0/(1.0/pdp.tauRF+epsilonunsurtauRF);//1/tauRF (kH)z

      ComputeLinearResponse(&pdp,tmax_fit,iGC,data,ThRF,&dThRFdunsurtauRF,kappa_th,sigma_th,Psi,Psi_transpose, SumPinvbgKSg,SumPinvbgKSg_CNOA,View,checks);
      delete_params(&pdp);//Freeing pointers in p. As ComputeLinearResponse generates new allocations, one has 
    }//End of gradient with respect to tauRF

    if (fit_variables.fitA0){//Gradient with respect to A0
      //printf("\n\t\t\t=> dRF/dA0\n");
      CopyParams(*p,&pdp);//Parallel computation. Update of parameters are made independently
      pdp.A0+=epsilonA0;//Amplitude

      ComputeLinearResponse(&pdp,tmax_fit,iGC,data,ThRF,&dThRFdA0,kappa_th,sigma_th,Psi,Psi_transpose, SumPinvbgKSg,SumPinvbgKSg_CNOA,View,checks);
      delete_params(&pdp);//Freeing pointers in p. As ComputeLinearResponse generates new allocations, one has 
   }//End of gradient with respect to A0

    if (fit_variables.fitb0){//Gradient with respect to b0
      //printf("\n\t\t\t=> dRF/dA0\n");
      CopyParams(*p,&pdp);//Parallel computation. Update of parameters are made independently
      pdp.b0+=epsilonb0;//Amplitude

      ComputeLinearResponse(&pdp,tmax_fit,iGC,data,ThRF,&dThRFdb0,kappa_th,sigma_th,Psi,Psi_transpose, SumPinvbgKSg,SumPinvbgKSg_CNOA,View,checks);
      delete_params(&pdp);//Freeing pointers in p. As ComputeLinearResponse generates new allocations, one has 
    }//End of gradient with respect to b0

  
    if (fit_variables.fitgCNOA){//Gradient with respect to gCNOA
      CopyParams(*p,&pdp);//Parallel computation. Update of parameters are made independently
      ComputeRestState_FixedCharacteristicTimes(iGC,pdp,f,X,&VB_rest,&VA_rest,&VG_rest,&A_rest);
      ComputeReferenceTimes(p,VB_rest, VA_rest,VG_rest);

      pdp.lambdaA+=epsilonlambdaA;//printf("lambda apres=%lg\n",pdp.lambdaA);exit(0);
      pdp.gCNOA=pdp.lambdaA*pdp.CA;//printf("dgCNOA=%lg\t",pdp.gCNOA);
      pdp.unsurtauA=1.0/pdp.tauAref+pdp.lambdaA;
      pdp.tauA=1.0/pdp.unsurtauA;//Amacrine characteristic time in the presence of CNO (ms)
      pdp.unsurtau=pdp.unsurtauA-pdp.unsurtauB;
      if ((fabs(pdp.unsurtau)>1E-10)) pdp.tau=1.0/pdp.unsurtau;//printf("tau=%lg\n",p.tau);//exit(0);
      pdp.unsurtauAB=pdp.unsurtauA+pdp.unsurtauB;
      pdp.tau2=pow(pdp.tau,2);
      pdp.tauAB=1.0/pdp.unsurtauAB;//printf("tauAB=%lg\n",p.tauAB);//exit(0);
      pdp.mu=-pdp.wab*pdp.wba*pdp.tau2;
  
      pdp.zetaA=pdp.gCNOA*p->ECNOA;

      ComputeLinearResponse(&pdp,tmax_fit,iGC,data,ThRF,&dThRFdlambdaA,kappa_th,sigma_th,Psi,Psi_transpose, SumPinvbgKSg,SumPinvbgKSg_CNOA,View,checks);
      delete_params(&pdp);//Freeing pointers in p. As ComputeLinearResponse generates new allocations, one has 
     
   }//End of gradient with respect to gCNOA

    if (fit_variables.fitgCNOG){//Gradient with respect to gCNOG
      CopyParams(*p,&pdp);//Parallel computation. Update of parameters are made independently
      pdp.lambdaG+=epsilonlambdaG;
      pdp.gCNOG=pdp.lambdaG*pdp.CG;
      pdp.unsurtauG=1.0/pdp.tauGref+pdp.lambdaG;
      pdp.tauG=1.0/pdp.unsurtauG;//GCell characteristic time in the presence of CNO (ms)
      
      pdp.zetaG=pdp.gCNOG*p->ECNOG;
      
      ComputeLinearResponse(&pdp,tmax_fit,iGC,data,ThRF,&dThRFdlambdaG,kappa_th,sigma_th,Psi,Psi_transpose, SumPinvbgKSg,SumPinvbgKSg_CNOA,View,checks);
      delete_params(&pdp);//Freeing pointers in p. As ComputeLinearResponse generates new allocations, one has 
    }//End of gradient with respect to gCNOG
 
    if (fit_variables.fitzetaA){//Gradient with respect to zetaA
      CopyParams(*p,&pdp);//Parallel computation. Update of parameters are made independently
      pdp.zetaA+=epsilonzetaA;
      ComputeLinearResponse(&pdp,tmax_fit,iGC,data,ThRF,&dThRFdzetaA,kappa_th,sigma_th,Psi,Psi_transpose, SumPinvbgKSg,SumPinvbgKSg_CNOA,View,checks);
      delete_params(&pdp);//Freeing pointers in p. As ComputeLinearResponse generates new allocations, one has 
    }//End of gradient with respect to zetaA

    if (fit_variables.fitzetaG){//Gradient with respect to zetaG
      CopyParams(*p,&pdp);//Parallel computation. Update of parameters are made independently
      pdp.zetaG+=epsilonzetaG;
      ComputeLinearResponse(&pdp,tmax_fit,iGC,data,ThRF,&dThRFdzetaG,kappa_th,sigma_th,Psi,Psi_transpose, SumPinvbgKSg,SumPinvbgKSg_CNOA,View,checks);
      delete_params(&pdp);//Freeing pointers in p. As ComputeLinearResponse generates new allocations, one has 
    }//End of gradient with respect to zetaG

    if (fit_variables.fitECNOA){//Gradient with respect to ECNOA
      CopyParams(*p,&pdp);//Parallel computation. Update of parameters are made independently
      pdp.ECNOA+=epsilonECNOA;
      pdp.zetaA=p->gCNOA*pdp.ECNOA;
      
      ComputeLinearResponse(&pdp,tmax_fit,iGC,data,ThRF,&dThRFdECNOA,kappa_th,sigma_th,Psi,Psi_transpose, SumPinvbgKSg,SumPinvbgKSg_CNOA,View,checks);
      delete_params(&pdp);//Freeing pointers in p. As ComputeLinearResponse generates new allocations, one has
    }//End of gradient with respect to ECNOA
    
    if (fit_variables.fitECNOG){//Gradient with respect to ECNOG
      CopyParams(*p,&pdp);//Parallel computation. Update of parameters are made independently
      pdp.ECNOG+=epsilonECNOG; 
      pdp.zetaG=p->gCNOG*pdp.ECNOG;
      
      ComputeLinearResponse(&pdp,tmax_fit,iGC,data,ThRF,&dThRFdECNOG,kappa_th,sigma_th,Psi,Psi_transpose, SumPinvbgKSg,SumPinvbgKSg_CNOA,View,checks);
      delete_params(&pdp);//Freeing pointers in p. As ComputeLinearResponse generates new allocations, one has
    }//End of gradient with respect to ECNOG

    //printf("\n\t\tUpdating parameters\n");
    double dpdwba=0,dpdwab=0,dpdwag=0,dpdwbg=0,dpdunsurtauA=0,dpdunsurtauB=0,dpdunsurtauG=0,dpdunsurtauRF=0,dpdA0=0,dpdb0=0,dpdzetaA=0,dpdzetaG=0,dpdlambdaA=0,dpdlambdaG=0,dpdECNOA=0,dpdECNOG=0;

    if (!(nbview%strobo)) printf("\t\t\t\033[0;31mVB_rest=%lg, VA_rest=%lg, VG_rest=%lg, ECNOA=%lg, ECNOG=%lg, zetaA=%lg, zetaG=%lg\n\033[0m\n",VB_rest,VA_rest,VG_rest,p->ECNOA,p->ECNOG,p->zetaA,p->zetaG);    

    if (!(nbview%strobo)) printf("\t\t(wba=%lg, wab=%lg, wag=%lg, wbg=%lg, tauA=%lg, tauB=%lg, tauG=%lg, tauRF=%lg, A0=%lg, b0=%lg, gCNOA=%lg, gCNOG=%lg, ECNOA=%lg, ECNOG=%lg, mu=%lg)\n ",p->wba,p->wab,p->wag,p->wbg,p->tauA,p->tauB,p->tauG,p->tauRF,p->A0,p->b0,p->gCNOA,p->gCNOG,p->ECNOA,p->ECNOG,p->mu);

    for (int ii=0;(t=data[ii].t)<tmax_fit;ii++){//Time loop where time is indexed on data
      double dif=(ThRF[ii].Amp-data[ii].Amp);
      if (fit_variables.fitwba) dpdwba+=dif*dThRFdwba[ii].Amp;
      if (fit_variables.fitwab) dpdwab+=dif*dThRFdwab[ii].Amp;
      if (fit_variables.fitwag) dpdwag+=dif*dThRFdwag[ii].Amp;
      if (fit_variables.fitwbg) dpdwbg+=dif*dThRFdwbg[ii].Amp;
      if (fit_variables.fittauA) dpdunsurtauA+=dif*dThRFdunsurtauA[ii].Amp;
      if (fit_variables.fittauB) dpdunsurtauB+=dif*dThRFdunsurtauB[ii].Amp;
      if (fit_variables.fittauG) dpdunsurtauG+=dif*dThRFdunsurtauG[ii].Amp;
      if (fit_variables.fittauRF) dpdunsurtauRF+=dif*dThRFdunsurtauRF[ii].Amp;
      if (fit_variables.fitA0) dpdA0+=dif*dThRFdA0[ii].Amp;
      if (fit_variables.fitb0) dpdb0+=dif*dThRFdb0[ii].Amp;
      if (fit_variables.fitgCNOA) dpdlambdaA+=dif*dThRFdlambdaA[ii].Amp;
      if (fit_variables.fitgCNOG)dpdlambdaG+=dif*dThRFdlambdaG[ii].Amp;
      if (fit_variables.fitzetaA) dpdzetaA+=dif*dThRFdzetaA[ii].Amp;
      if (fit_variables.fitzetaG)dpdzetaG+=dif*dThRFdzetaG[ii].Amp;
      if (fit_variables.fitECNOA)dpdECNOA+=dif*dThRFdECNOA[ii].Amp;
      if (fit_variables.fitECNOG)dpdECNOG+=dif*dThRFdECNOG[ii].Amp;
    }//End of time loop where time is indexed on data
    
    if (fit_variables.fitgCNOA) {
      rescale=Rescale(dpdlambdaA,&epsilonlambdaA,d_min,d_max);
      if (rescale==1) 
	printf("\t\t\t\033[0;32mRescaling epsilonlambdaA=%lg\033[0m\n",epsilonlambdaA);
      if (rescale==2) Stop=true; 
    }

    if (fit_variables.fitgCNOG){
      rescale=Rescale(dpdlambdaG,&epsilonlambdaG,d_min,d_max);
      if (rescale==1) 
	printf("\t\t\t\033[0;32mRescaling epsilonlambdaG=%lg\033[0m\n",epsilonlambdaG);
      if (rescale==2) Stop=true; 
    }

    if (fit_variables.fitECNOA) {
      rescale=Rescale(dpdECNOA,&epsilonECNOA,d_min,d_max);
      if (rescale==1) 
	printf("\t\t\t\033[0;32mRescaling epsilonECNOA=%lg\033[0m\n",epsilonECNOA);
      if (rescale==2) Stop=true; 
    }
    if (fit_variables.fitECNOG){
      rescale=Rescale(dpdECNOG,&epsilonECNOG,d_min,d_max);
      if (rescale==1)
	printf("\t\t\t\033[0;32mRescaling epsilonECNOG=%lg\033[0m\n",epsilonECNOG);
      if (rescale==2) Stop=true; 
   }

    if (!(nbview%strobo)) printf("\t\t\tdpdlambdaA=%lg, dpdlambdaG=%lg, dpdECNOA=%lg, dpdECNOG=%lg\n",dpdlambdaA,dpdlambdaG,dpdECNOA,dpdECNOG);

    double delta=0;
    if (fit_variables.fitwba) {delta=p->wba-dpdwba*dsurT;if (delta>1E-4) p->wba=delta;}
    if (fit_variables.fitwab) {delta=p->wab-dpdwab*dsurT;if (delta<-1E-4) p->wab=delta;}
    if (fit_variables.fitwag) {delta=p->wag-dpdwag*dsurT;if (delta<-1E-4) p->wag=delta;}
    if (fit_variables.fitwbg) {delta=p->wbg-dpdwbg*dsurT;if (delta>1E-4) p->wbg=delta;}
    if (fit_variables.fittauA) {
      delta=p->unsurtauA-dpdunsurtauA*dsurT; 
      if (delta>0) {p->unsurtauA=delta;
	p->tauA=1.0/p->unsurtauA;}
    }
    if (fit_variables.fittauB){
      delta=p->unsurtauB-dpdunsurtauB*dsurT; 
      if (delta>0) {p->unsurtauB=delta;} 
      if (fit_variables.fittauB)
	p->tauB=1.0/p->unsurtauB;
    }
    if (fit_variables.fittauG){
      delta=p->unsurtauG-dpdunsurtauG*dsurT; if (delta>0) {p->unsurtauG=delta;} 
      p->tauG=1.0/p->unsurtauG;
    }
    if (fit_variables.fittauRF){
      delta=1.0/p->tauRF-dpdunsurtauRF*dsurT; if (delta>0) {p->tauRF=1.0/delta;} 
    }
    if (fit_variables.fittauRF){
      delta=p->A0-dpdA0*dsurT; p->A0=delta;
    }
    if (fit_variables.fitb0) {
      delta=p->b0-dpdb0*dsurT; p->b0=delta;
    }
    if (fit_variables.fitgCNOA) {
      delta=p->lambdaA-dpdlambdaA*dsurT;
      //printf("delta lambdaA=%lg\n",delta);exit(0);
 
      if (delta>=0){
	p->lambdaA=delta;
	p->gCNOA=p->lambdaA*p->CA;
	p->zetaA-=(dpdlambdaA*p->ECNOA+p->lambdaA*dpdECNOA)*dsurT; 
	p->unsurtauA=1.0/p->tauAref+p->lambdaA;
	p->tauA=1.0/p->unsurtauA;//Amacrine characteristic time in the presence of CNO (ms)
      }
    }
    if (fit_variables.fitgCNOG) {
      double deltagG=p->lambdaG-dpdlambdaG*dsurT; 
      if (deltagG>=0){
	p->lambdaG=deltagG;
	p->gCNOG=p->lambdaG*p->CG;
	p->unsurtauG=1.0/p->tauGref+p->lambdaG;
	p->tauG=1.0/p->unsurtauG;//GCell characteristic time in the presence of CNO (ms)
      }
      p->zetaG-=dpdlambdaG*p->ECNOG*dsurT; 
    }

    if (fit_variables.fitzetaA) {
      p->zetaA-=dpdzetaA*dsurT;
    }

    if (fit_variables.fitzetaG) {
      p->zetaG-=dpdzetaG*dsurT;
    }
    
    if (fit_variables.fitECNOG) {
      p->ECNOG-=dpdECNOG*dsurT;
      p->zetaG-=p->lambdaG*dpdECNOG*dsurT; 
    }

    if (fit_variables.fitECNOA) {
      delta=p->ECNOA-dpdECNOA*dsurT;
      p->zetaA-=(dpdlambdaA*p->ECNOA+p->lambdaA*dpdECNOA)*dsurT; 
      p->ECNOA=delta;
    }
   
    p->unsurtauAB=p->unsurtauA+p->unsurtauB;//1/tauA+1/tauB (kHz)
    p->unsurtau=p->unsurtauA-p->unsurtauB;//1/tauA-1/tauB (kHz)
    if ((fabs(p->unsurtau)>1E-10)) {
      p->tau=1.0/p->unsurtau;
    }
    else {
      printf("Case tau^-1=0 not implemented yet, exiting\n");
      exit(0);
    }
    p->tau2=pow(p->tau,2);
    p->tauAB=1.0/p->unsurtauAB;
    p->mu=-p->wab*p->wba*p->tau2;

    ComputeRestState_FixedCharacteristicTimes(iGC,*p,f,X,&VB_rest,&VA_rest,&VG_rest,&A_rest);
    ComputeReferenceTimes(p,VB_rest, VA_rest,VG_rest);
    if (!(nbview%strobo)) printf("\t\t\t\033[0;31mVB_rest=%lg, VA_rest=%lg, VG_rest=%lg, ECNOA=%lg, ECNOG=%lg, zetaA=%lg, zetaG=%lg, mu=%lg\n\033[0m\n",VB_rest,VA_rest,VG_rest,p->ECNOA,p->ECNOG,p->zetaA,p->zetaG,p->mu);
    if (!(nbview%strobo)) printf("\t\t\ttauB_ref=%lg, tauA_ref=%lg, tauG_ref=%lg\n", p->tauBref,p->tauAref,p->tauGref);
    if (!(nbview%strobo)) nbview=0;
    nbview++;
    iter++;

    //Clear events 
    ThRF.clear();
    dThRFdwba.clear();
    dThRFdwab.clear();
    dThRFdwag.clear();
    dThRFdwbg.clear();
    dThRFdunsurtauA.clear();
    dThRFdunsurtauB.clear();
    dThRFdunsurtauG.clear();
    dThRFdunsurtauRF.clear();
    dThRFdA0.clear();
    dThRFdb0.clear();
    dThRFdlambdaA.clear();
    dThRFdlambdaG.clear();
    dThRFdzetaA.clear();
    dThRFdzetaG.clear();
    dThRFdECNOA.clear();
    dThRFdECNOG.clear();
  }//End of estimation loop
  My_gsl_matrix_free(Psi);My_gsl_matrix_free(Psi_transpose);
  My_gsl_vector_complex_free(SumPinvbgKSg);
  My_gsl_vector_complex_free(SumPinvbgKSg_CNOA);
  free(X);
  //fclose(fp);
}

/** Fit the model from a experimental cell by minimising the distance between the temporal RF and the model. Spatial RF is not taken into account.
    The cell data are stored in the structure exper, of type Experiment
    The model vector field is f
    tmin_fit,tmax_fit are the lower and upper bound for the time interval of fit
    fit_variables is a vector of booleans selecting the parameters to fit.
    max_iter is the maximum number of iterations 
    precision is the required precisions
    return the final distance between the fit and data
**/
double FitCells(Experiment exper,params *p,void (*f)(double *X,double *Xp,double t,params p,double (*input)(double x,double t, params p)),double tmin_fit,double tmax_fit,fit_switch fit_variables,int max_iter,double precision){
  
  double weight_factor=1E-3;
  bool View=false,checks=true;
  int dim_ext=4*p->L;// Dimension of the phase space
  int dim=3*p->L;// Dimension of the phase space without activities
  
  int iGC=p->N/2;//Index of the GCell on the lattice
  int alphaBC=iGC;//Index of the BCell above this GCell in the extended phase space
  int alphaAC=iGC+p->L;//Index of the ACell above this GCell in the extended phase space
  int alphaGC=iGC+2*p->L;//Index of the GCell in the extended phase space
  int alphaAc=iGC+3*p->L;//Index of the BCell activity in the extended phase space
  double xGC=iGC*p->dx,yGC=0;//Physical coordinates of the GCell (mm)

  printf("\n\t*************************\n\t\t\tAnalysing the cell %d_%d_%s\n",exper.Cell_id,exper.Exp_id,exper.Gene.c_str());
  std::vector <Event> data=exper.data;
  
  double T=data[data.size()-1].t;
  if (T<tmax_fit){printf("tmax_fit=%lg  is longer than data set T=%lg. Setting tmax_fit=T",tmax_fit,T);tmax_fit=T;};
  double dsurT=2.0/double(tmax_fit-tmin_fit);
  
  //printf("\n\t*************************\n\t\t\t Computing the RF for a predefined set of parameters values \n\t*************************\n");
  
  int iter=0;
  int nbview=0,strobo=10;

  FILE *fp=fopen("Evol_Distance","w");fclose(fp);
  double d_iter=100,d=100,dd=10;
  gsl_matrix *Psi=gsl_matrix_calloc(p->L,p->L),*Psi_transpose=gsl_matrix_calloc(p->L,p->L);
  Spectrum kappa_th;//Theoretical eigenvalues of the connectivity matrix amacrine bipolar
  Spectrum sigma_th;//Theoretical eigenvalues of the connectivity matrix bipolar amacrine
  gsl_vector_complex* SumPinvbgKSg=gsl_vector_complex_calloc(dim);//Auxilary used for computation
  gsl_vector_complex* SumPinvbgKSg_CNOA=gsl_vector_complex_calloc(dim);//Auxilary used for computation
  
  double VB_rest=0, VA_rest=0,VG_rest=0,A_rest=0;
  double *X=(double*)calloc(dim_ext,sizeof(double));//State vector time t

  printf("\t\t\ttauB_ref=%lg, tauA_ref=%lg, tauG_ref=%lg\n", p->tauBref,p->tauAref,p->tauGref);

  ComputeTheoreticalModesLaplacian1D(*p,&kappa_th,&sigma_th,Psi,Psi_transpose);
  
  double epsilonwba=weight_factor; //kHz
  double epsilonwab=weight_factor; //kHz
  double epsilonwbg=weight_factor; //kHz
  double epsilonwag=weight_factor; //kHz
  double epsilonunsurtauA=0.01*weight_factor; //kHz
  double epsilonunsurtauB=0.1*weight_factor; //kHz
  double epsilonunsurtauG=0.1*weight_factor; //kHz
  double epsilonunsurtauRF=0.1*weight_factor; //kHz
  double epsilonA0=0.01;//mV
  double epsilonb0=0.01;//mV
  double epsilonzetaA=1E-6; //mV/ms
  double epsilonzetaG=1E-6;//mV/ms
  double epsilonlambdaA=0.01*weight_factor; //kHz
  double epsilonlambdaG=0.01*weight_factor;//kHz
  double epsilonECNOA=0.1; //mV
  double epsilonECNOG=1;//mV
  double d_min=1E-3,d_max=1E-1;

  int rescale=0; //Used to stop iterations when some derivative become too large
  bool Stop=false;

  while ((iter<=max_iter)&&(d>precision)&&(d_iter>1E-9)&&(!Stop)){//Estimation loop
    if (!(nbview%strobo)) printf("\n\titer=%d\n",iter);

    init_Network_Laplacian(p,1);   
    ComputeTheoreticalSpectrumL(kappa_th,sigma_th,Psi,Psi_transpose,p);
    //if (!IsStable(*p)) {printf("\t\t\033[0;31mIn FitCell, at iteration %d, dynamics is linearly unstable \033[0m\n",iter); }
    if (checks){ //Beginning of comparing numerical and theoretical eigenvectors
      
      // printf("\n\tChecking inversion\n");
      if (CheckInverse(p->P,p->Pinv)) {
	//printf("\n\tPinv is the inverse of P\n");
      }
      else {
	printf("\n\tPinv is not the inverse of P, aborting\n");
	exit(0);
      }
      
      //printf("\n\tChecking theoretical eigenvalues and eigenvectors\n");
      if (TestSpectrum(p->cL,p->P,p->lambda_th)){//Tests eigenvalues and eigenvectors
	//  printf("\n\tThe spectrum is OK\n");
      }
      else {
	printf("\n\tP computation failed\n");
	exit(0);
      }
    }//End of comparing numerical and theoretical eigenvectors
    
    //printf("\n\t\t--------------------\n\t\tComputing the initial RF\n\t\t--------------------\n");
    gsl_complex kg={0,0};//Computes the RF of a GCell alpha embedded in the BCell, ACell, GCell network   
    std::vector <Event> ThRF;
    Event pointTh;
    double t=0;
    
    //double y=0;
    KSG(xGC,yGC,*p, SumPinvbgKSg);//KSG contains the value of the spatial R at point xGC,yGC.
    KG_CNOA(*p,SumPinvbgKSg_CNOA);//CNO A contribution

    //for (double t=-0.1;t<=p->tmax_fit;t+=p->dt){
    for (int ii=0;(t=data[ii].t)<tmax_fit;ii++){//Time loop where time is indexed on data      
      kg=KG(alphaGC,xGC,0,t-p->t0,*p,SumPinvbgKSg,SumPinvbgKSg_CNOA);
      if (fabs(GSL_IMAG(kg)>1E-8)) {printf("Problem. The RF is not real. Imaginary part at (%lg,%lg,%lg)=%lg",xGC,yGC,t,kg);exit(0);}
      
      pointTh.t=t;pointTh.Amp=GSL_REAL(kg)+VG_rest;
      ThRF.push_back(pointTh);
    }//End of time loop
    
    
    //Computes the distance between data and theoretical RF
    d=Distance(data,ThRF,tmax_fit);
    d_iter=fabs(dd-d);
    dd=d;
    if (!(nbview%strobo)){
      printf("\t\tThe distance between experimental data and fit is %lg, increment=%lg\n",d,d_iter);
      fp=fopen("Evol_Distance","a");
      fprintf(fp,"%d\t%lg\t%lg\t%lg\t%lg\t%lg\t%lg\t%lg\t%lg\t%lg\t%lg\t%lg\t%lg\t%lg\n",iter,d,p->wba,p->wab,p->wbg,p->wag,p->tauA,p->tauB,p->tauG,p->tauRF,p->zetaA,p->zetaG,p->A0,p->b0);
      fclose(fp);   
    }
    double sigma_noise=d/3; //Adding noise in the algorithm

    //Computes the gradients
    //printf("\n--------------------\n \t\t\tComputing gradients\n--------------------\n");
    std::vector <Event> dThRFdwba,dThRFdwab,dThRFdwag,dThRFdwbg,dThRFdunsurtauA,dThRFdunsurtauB,dThRFdunsurtauG,dThRFdunsurtauRF,dThRFdA0,dThRFdb0,dThRFdlambdaA,dThRFdlambdaG,dThRFdzetaA,dThRFdzetaG,dThRFdECNOA,dThRFdECNOG;
    params pdp;

    if (fit_variables.fitwba){//Gradient with respect to wba
      //printf("\t\t\t=> dRF/dwba\n");
      CopyParams(*p,&pdp);//Parallel computation. Update of parameters are made independently
      pdp.wba+=epsilonwba*(1+sigma_noise*drand48());
      pdp.mu=-pdp.wab*pdp.wba*pdp.tau2;
       
      ComputeLinearResponse(&pdp,tmax_fit,iGC,data,ThRF,&dThRFdwba,kappa_th,sigma_th,Psi,Psi_transpose, SumPinvbgKSg,SumPinvbgKSg_CNOA,View,checks);
      delete_params(&pdp);//Freeing pointers in p. As ComputeLinearResponse generates new allocations, one has to free memory to avoid saturating it.
     }//End of gradient with respect to wba
    
    if (fit_variables.fitwab){//Gradient with respect to wab
      //printf("\n\t\t\t=> dRF/dwab\n");
      CopyParams(*p,&pdp);//Parallel computation. Update of parameters are made independently
      pdp.wab+=epsilonwab*(1+sigma_noise*drand48());
      pdp.mu=-pdp.wab*pdp.wba*pdp.tau2;
      
      ComputeLinearResponse(&pdp,tmax_fit,iGC,data,ThRF,&dThRFdwab,kappa_th,sigma_th,Psi,Psi_transpose, SumPinvbgKSg,SumPinvbgKSg_CNOA,View,checks);
      delete_params(&pdp);//Freeing pointers in p. As ComputeLinearResponse generates new allocations, one has to free memory to avoid saturating it.
    }//End of gradient with respect to wab

    if (fit_variables.fitwag){//Gradient with respect to wag
      //printf("\n\t\t\t=> dRF/dwag\n");
      CopyParams(*p,&pdp);//Parallel computation. Update of parameters are made independently
      pdp.wag+=epsilonwag*(1+sigma_noise*drand48());

      ComputeLinearResponse(&pdp,tmax_fit,iGC,data,ThRF,&dThRFdwag,kappa_th,sigma_th,Psi,Psi_transpose, SumPinvbgKSg,SumPinvbgKSg_CNOA,View,checks);
      delete_params(&pdp);//Freeing pointers in p. As ComputeLinearResponse generates new allocations, one has 
    }//End of gradient with respect to wag
    
    if (fit_variables.fitwbg){//Gradient with respect to wbg
      //printf("\n\t\t\t=> dRF/dwbg\n");
       CopyParams(*p,&pdp);//Parallel computation. Update of parameters are made independently
       pdp.wbg+=epsilonwbg*(1+sigma_noise*drand48());
     
      ComputeLinearResponse(&pdp,tmax_fit,iGC,data,ThRF,&dThRFdwbg,kappa_th,sigma_th,Psi,Psi_transpose, SumPinvbgKSg,SumPinvbgKSg_CNOA,View,checks);
      delete_params(&pdp);//Freeing pointers in p. As ComputeLinearResponse generates new allocations, one has 
    }//End of gradient with respect to wbg

    if (fit_variables.fittauA){//Gradient with respect to tauA
      //printf("\n\t\t\t=> dRF/dunsurtauA\n");
      CopyParams(*p,&pdp);//Parallel computation. Update of parameters are made independently
      pdp.unsurtauA+=epsilonunsurtauA*(1+sigma_noise*drand48());//1/tauA (kH)z
      pdp.tauA=1.0/pdp.unsurtauA;
      pdp.unsurtau=pdp.unsurtauA-pdp.unsurtauB;//1/tauA-1/tauB (kHz)
      pdp.unsurtauAB=pdp.unsurtauA+pdp.unsurtauB;//1/tauA+1/tauB (kHz)
      if ((fabs(pdp.unsurtau)>1E-10)) {
	pdp.tau=1.0/pdp.unsurtau;
      }
      else {
	printf("Case tau^-1=0 not implemented yet, exiting\n");
	exit(0);
      }
      pdp.tau2=pow(pdp.tau,2);
      pdp.tauAB=1.0/pdp.unsurtauAB;
      pdp.mu=-pdp.wab*pdp.wba*pdp.tau2;

      ComputeLinearResponse(&pdp,tmax_fit,iGC,data,ThRF,&dThRFdunsurtauA,kappa_th,sigma_th,Psi,Psi_transpose, SumPinvbgKSg,SumPinvbgKSg_CNOA,View,checks);
      delete_params(&pdp);//Freeing pointers in p. As ComputeLinearResponse generates new allocations, one has 
    }//End of gradient with respect to tauA

    if (fit_variables.fittauB){//Gradient with respect to tauB
      //printf("\n\t\t\t=> dRF/dunsurtauB\n");
      CopyParams(*p,&pdp);//Parallel computation. Update of parameters are made independently
      pdp.unsurtauB+=epsilonunsurtauB*(1+sigma_noise*drand48());//1/tauB (kHz)
      pdp.tauB=1.0/pdp.unsurtauB;
      //pdp.tauRF=pdp.tauB;
      pdp.unsurtau=pdp.unsurtauA-pdp.unsurtauB;//1/tauA-1/tauB (kHz)
      pdp.unsurtauAB=pdp.unsurtauA+pdp.unsurtauB;//1/tauA+1/tauB (kHz)
      if ((fabs(pdp.unsurtau)>1E-10)) {
	pdp.tau=1.0/pdp.unsurtau;
      }
      else {
	printf("Case tau^-1=0 not implemented yet, exiting\n");
	exit(0);
      }
      pdp.tau2=pow(pdp.tau,2);
      pdp.tauAB=1.0/pdp.unsurtauAB;
      pdp.mu=-pdp.wab*pdp.wba*pdp.tau2;

      ComputeLinearResponse(&pdp,tmax_fit,iGC,data,ThRF,&dThRFdunsurtauB,kappa_th,sigma_th,Psi,Psi_transpose, SumPinvbgKSg,SumPinvbgKSg_CNOA,View,checks);
      delete_params(&pdp);//Freeing pointers in p. As ComputeLinearResponse generates new allocations, one has 
    }//End of gradient with respect to tauB

    if (fit_variables.fittauG){//Gradient with respect to tauG
      //printf("\n\t\t\t=> dRF/dunsurtauG\n");
      CopyParams(*p,&pdp);//Parallel computation. Update of parameters are made independently
      pdp.unsurtauG+=epsilonunsurtauG*(1+sigma_noise*drand48());//1/tauG (kH)z
      pdp.tauG=1.0/pdp.unsurtauG;

      ComputeLinearResponse(&pdp,tmax_fit,iGC,data,ThRF,&dThRFdunsurtauG,kappa_th,sigma_th,Psi,Psi_transpose, SumPinvbgKSg,SumPinvbgKSg_CNOA,View,checks);
      delete_params(&pdp);//Freeing pointers in p. As ComputeLinearResponse generates new allocations, one has 
     }//End of gradient with respect to tauG

    if (fit_variables.fittauRF){//Gradient with respect to tauRF
      //printf("\n\t\t\t=> dRF/dunsurtauRF\n");
      CopyParams(*p,&pdp);//Parallel computation. Update of parameters are made independently
      pdp.tauRF=1.0/(1.0/pdp.tauRF+epsilonunsurtauRF*(1+sigma_noise*drand48()));//1/tauRF (kH)z

      ComputeLinearResponse(&pdp,tmax_fit,iGC,data,ThRF,&dThRFdunsurtauRF,kappa_th,sigma_th,Psi,Psi_transpose, SumPinvbgKSg,SumPinvbgKSg_CNOA,View,checks);
      delete_params(&pdp);//Freeing pointers in p. As ComputeLinearResponse generates new allocations, one has 
    }//End of gradient with respect to tauRF

    if (fit_variables.fitA0){//Gradient with respect to A0
      //printf("\n\t\t\t=> dRF/dA0\n");
      CopyParams(*p,&pdp);//Parallel computation. Update of parameters are made independently
      pdp.A0+=epsilonA0*(1+sigma_noise*drand48());//Amplitude

      ComputeLinearResponse(&pdp,tmax_fit,iGC,data,ThRF,&dThRFdA0,kappa_th,sigma_th,Psi,Psi_transpose, SumPinvbgKSg,SumPinvbgKSg_CNOA,View,checks);
      delete_params(&pdp);//Freeing pointers in p. As ComputeLinearResponse generates new allocations, one has to remove the previous ones.
   }//End of gradient with respect to A0

    if (fit_variables.fitb0){//Gradient with respect to b0
      //printf("\n\t\t\t=> dRF/dA0\n");
      CopyParams(*p,&pdp);//Parallel computation. Update of parameters are made independently
      pdp.b0+=epsilonb0*(1+sigma_noise*drand48());//Amplitude

      ComputeLinearResponse(&pdp,tmax_fit,iGC,data,ThRF,&dThRFdb0,kappa_th,sigma_th,Psi,Psi_transpose, SumPinvbgKSg,SumPinvbgKSg_CNOA,View,checks);
      delete_params(&pdp);//Freeing pointers in p. As ComputeLinearResponse generates new allocations, one has to remove the previous ones. 
    }//End of gradient with respect to b0

  
    if (fit_variables.fitgCNOA){//Gradient with respect to gCNOA
      CopyParams(*p,&pdp);//Parallel computation. Update of parameters are made independently
      ComputeRestState_FixedCharacteristicTimes(iGC,pdp,f,X,&VB_rest,&VA_rest,&VG_rest,&A_rest);
      ComputeReferenceTimes(p,VB_rest, VA_rest,VG_rest);

      pdp.lambdaA+=epsilonlambdaA*(1+sigma_noise*drand48());//printf("lambda apres=%lg\n",pdp.lambdaA);exit(0);
      pdp.gCNOA=pdp.lambdaA*pdp.CA;//printf("dgCNOA=%lg\t",pdp.gCNOA);
      pdp.unsurtauA=1.0/pdp.tauAref+pdp.lambdaA;
      pdp.tauA=1.0/pdp.unsurtauA;//Amacrine characteristic time in the presence of CNO (ms)
      pdp.unsurtau=pdp.unsurtauA-pdp.unsurtauB;
      if ((fabs(pdp.unsurtau)>1E-10)) pdp.tau=1.0/pdp.unsurtau;//printf("tau=%lg\n",p.tau);//exit(0);
      pdp.unsurtauAB=pdp.unsurtauA+pdp.unsurtauB;
      pdp.tau2=pow(pdp.tau,2);
      pdp.tauAB=1.0/pdp.unsurtauAB;//printf("tauAB=%lg\n",p.tauAB);//exit(0);
      pdp.mu=-pdp.wab*pdp.wba*pdp.tau2;
  
      pdp.zetaA=pdp.gCNOA*p->ECNOA;

      ComputeLinearResponse(&pdp,tmax_fit,iGC,data,ThRF,&dThRFdlambdaA,kappa_th,sigma_th,Psi,Psi_transpose, SumPinvbgKSg,SumPinvbgKSg_CNOA,View,checks);
      delete_params(&pdp);//Freeing pointers in p. As ComputeLinearResponse generates new allocations, one has to remove the previous ones. 
     
   }//End of gradient with respect to gCNOA

    if (fit_variables.fitgCNOG){//Gradient with respect to gCNOG
      CopyParams(*p,&pdp);//Parallel computation. Update of parameters are made independently
      pdp.lambdaG+=epsilonlambdaG*(1+sigma_noise*drand48());
      pdp.gCNOG=pdp.lambdaG*pdp.CG;
      pdp.unsurtauG=1.0/pdp.tauGref+pdp.lambdaG;
      pdp.tauG=1.0/pdp.unsurtauG;//GCell characteristic time in the presence of CNO (ms)
      
      pdp.zetaG=pdp.gCNOG*p->ECNOG;
      
      ComputeLinearResponse(&pdp,tmax_fit,iGC,data,ThRF,&dThRFdlambdaG,kappa_th,sigma_th,Psi,Psi_transpose, SumPinvbgKSg,SumPinvbgKSg_CNOA,View,checks);
      delete_params(&pdp);//Freeing pointers in p. As ComputeLinearResponse generates new allocations, one has to remove the previous ones. 
    }//End of gradient with respect to gCNOG
 
    if (fit_variables.fitzetaA){//Gradient with respect to zetaA
      CopyParams(*p,&pdp);//Parallel computation. Update of parameters are made independently
      pdp.zetaA+=epsilonzetaA*(1+sigma_noise*drand48());
      ComputeLinearResponse(&pdp,tmax_fit,iGC,data,ThRF,&dThRFdzetaA,kappa_th,sigma_th,Psi,Psi_transpose, SumPinvbgKSg,SumPinvbgKSg_CNOA,View,checks);
      delete_params(&pdp);//Freeing pointers in p. As ComputeLinearResponse generates new allocations, one has to remove the previous ones. 
    }//End of gradient with respect to zetaA

    if (fit_variables.fitzetaG){//Gradient with respect to zetaG
      CopyParams(*p,&pdp);//Parallel computation. Update of parameters are made independently
      pdp.zetaG+=epsilonzetaG*(1+sigma_noise*drand48());
      ComputeLinearResponse(&pdp,tmax_fit,iGC,data,ThRF,&dThRFdzetaG,kappa_th,sigma_th,Psi,Psi_transpose, SumPinvbgKSg,SumPinvbgKSg_CNOA,View,checks);
      delete_params(&pdp);//Freeing pointers in p. As ComputeLinearResponse generates new allocations, one has to remove the previous ones. 
    }//End of gradient with respect to zetaG

    if (fit_variables.fitECNOA){//Gradient with respect to ECNOA
      CopyParams(*p,&pdp);//Parallel computation. Update of parameters are made independently
      pdp.ECNOA+=epsilonECNOA*(1+sigma_noise*drand48());
      pdp.zetaA=p->gCNOA*pdp.ECNOA;
      
      ComputeLinearResponse(&pdp,tmax_fit,iGC,data,ThRF,&dThRFdECNOA,kappa_th,sigma_th,Psi,Psi_transpose, SumPinvbgKSg,SumPinvbgKSg_CNOA,View,checks);
      delete_params(&pdp);//Freeing pointers in p. As ComputeLinearResponse generates new allocations, one has to remove the previous ones.
    }//End of gradient with respect to ECNOA
    
    if (fit_variables.fitECNOG){//Gradient with respect to ECNOG
      CopyParams(*p,&pdp);//Parallel computation. Update of parameters are made independently
      pdp.ECNOG+=epsilonECNOG*(1+sigma_noise*drand48()); 
      pdp.zetaG=p->gCNOG*pdp.ECNOG;
      
      ComputeLinearResponse(&pdp,tmax_fit,iGC,data,ThRF,&dThRFdECNOG,kappa_th,sigma_th,Psi,Psi_transpose, SumPinvbgKSg,SumPinvbgKSg_CNOA,View,checks);
      delete_params(&pdp);//Freeing pointers in p. As ComputeLinearResponse generates new allocations, one has to remove the previous ones.
    }//End of gradient with respect to ECNOG

    //printf("\n\t\tUpdating parameters\n");
    double dpdwba=0,dpdwab=0,dpdwag=0,dpdwbg=0,dpdunsurtauA=0,dpdunsurtauB=0,dpdunsurtauG=0,dpdunsurtauRF=0,dpdA0=0,dpdb0=0,dpdzetaA=0,dpdzetaG=0,dpdlambdaA=0,dpdlambdaG=0,dpdECNOA=0,dpdECNOG=0;

    if (!(nbview%strobo)) printf("\t\t\t\033[0;31mVB_rest=%lg, VA_rest=%lg, VG_rest=%lg, ECNOA=%lg, ECNOG=%lg, zetaA=%lg, zetaG=%lg\n\033[0m\n",VB_rest,VA_rest,VG_rest,p->ECNOA,p->ECNOG,p->zetaA,p->zetaG);    

    if (!(nbview%strobo)) printf("\t\t(wba=%lg, wab=%lg, wag=%lg, wbg=%lg, tauA=%lg, tauB=%lg, tauG=%lg, tauRF=%lg, A0=%lg, b0=%lg, gCNOA=%lg, gCNOG=%lg, ECNOA=%lg, ECNOG=%lg, mu=%lg, r=%lg, s=%lg)\n ",p->wba,p->wab,p->wag,p->wbg,p->tauA,p->tauB,p->tauG,p->tauRF,p->A0,p->b0,p->gCNOA,p->gCNOG,p->ECNOA,p->ECNOG,p->mu,p->tauA/p->tauB,-p->wab/p->wba);

    for (int ii=0;(t=data[ii].t)<tmax_fit;ii++){//Time loop where time is indexed on data
      double dif=(ThRF[ii].Amp-data[ii].Amp);
      if (fit_variables.fitwba) dpdwba+=dif*dThRFdwba[ii].Amp;
      if (fit_variables.fitwab) dpdwab+=dif*dThRFdwab[ii].Amp;
      if (fit_variables.fitwag) dpdwag+=dif*dThRFdwag[ii].Amp;
      if (fit_variables.fitwbg) dpdwbg+=dif*dThRFdwbg[ii].Amp;
      if (fit_variables.fittauA) dpdunsurtauA+=dif*dThRFdunsurtauA[ii].Amp;
      if (fit_variables.fittauB) dpdunsurtauB+=dif*dThRFdunsurtauB[ii].Amp;
      if (fit_variables.fittauG) dpdunsurtauG+=dif*dThRFdunsurtauG[ii].Amp;
      if (fit_variables.fittauRF) dpdunsurtauRF+=dif*dThRFdunsurtauRF[ii].Amp;
      if (fit_variables.fitA0) dpdA0+=dif*dThRFdA0[ii].Amp;
      if (fit_variables.fitb0) dpdb0+=dif*dThRFdb0[ii].Amp;
      if (fit_variables.fitgCNOA) dpdlambdaA+=dif*dThRFdlambdaA[ii].Amp;
      if (fit_variables.fitgCNOG)dpdlambdaG+=dif*dThRFdlambdaG[ii].Amp;
      if (fit_variables.fitzetaA) dpdzetaA+=dif*dThRFdzetaA[ii].Amp;
      if (fit_variables.fitzetaG)dpdzetaG+=dif*dThRFdzetaG[ii].Amp;
      if (fit_variables.fitECNOA)dpdECNOA+=dif*dThRFdECNOA[ii].Amp;
      if (fit_variables.fitECNOG)dpdECNOG+=dif*dThRFdECNOG[ii].Amp;
    }//End of time loop where time is indexed on data
    
    if (fit_variables.fitgCNOA) {
      rescale=Rescale(dpdlambdaA,&epsilonlambdaA,d_min,d_max);
      if (rescale==1) 
	printf("\t\t\t\033[0;32mRescaling epsilonlambdaA=%lg\033[0m\n",epsilonlambdaA);
      if (rescale==2) Stop=true; 
    }

    if (fit_variables.fitgCNOG){
      rescale=Rescale(dpdlambdaG,&epsilonlambdaG,d_min,d_max);
      if (rescale==1) 
	printf("\t\t\t\033[0;32mRescaling epsilonlambdaG=%lg\033[0m\n",epsilonlambdaG);
      if (rescale==2) Stop=true; 
    }

    if (fit_variables.fitECNOA) {
      rescale=Rescale(dpdECNOA,&epsilonECNOA,d_min,d_max);
      if (rescale==1) 
	printf("\t\t\t\033[0;32mRescaling epsilonECNOA=%lg\033[0m\n",epsilonECNOA);
      if (rescale==2) Stop=true; 
    }
    if (fit_variables.fitECNOG){
      rescale=Rescale(dpdECNOG,&epsilonECNOG,d_min,d_max);
      if (rescale==1)
	printf("\t\t\t\033[0;32mRescaling epsilonECNOG=%lg\033[0m\n",epsilonECNOG);
      if (rescale==2) Stop=true; 
   }

    if (!(nbview%strobo)) printf("\t\t\tdpdlambdaA=%lg, dpdlambdaG=%lg, dpdECNOA=%lg, dpdECNOG=%lg\n",dpdlambdaA,dpdlambdaG,dpdECNOA,dpdECNOG);

    double delta=0;
    if (fit_variables.fitwba) {delta=p->wba-dpdwba*dsurT;if (delta>1E-4) p->wba=delta;}
    if (fit_variables.fitwab) {delta=p->wab-dpdwab*dsurT;if (delta<-1E-4) p->wab=delta;}
    if (fit_variables.fitwag) {delta=p->wag-dpdwag*dsurT;if (delta<-1E-4) p->wag=delta;}
    if (fit_variables.fitwbg) {delta=p->wbg-dpdwbg*dsurT;if (delta>1E-4) p->wbg=delta;}
    if (fit_variables.fittauA) {
      delta=p->unsurtauA-dpdunsurtauA*dsurT; 
      if (delta>0) {p->unsurtauA=delta;
	p->tauA=1.0/p->unsurtauA;}
    }
    if (fit_variables.fittauB){
      delta=p->unsurtauB-dpdunsurtauB*dsurT; 
      if (delta>0) {p->unsurtauB=delta;} 
      if (fit_variables.fittauB)
	p->tauB=1.0/p->unsurtauB;
    }
    if (fit_variables.fittauG){
      delta=p->unsurtauG-dpdunsurtauG*dsurT; if (delta>0) {p->unsurtauG=delta;} 
      p->tauG=1.0/p->unsurtauG;
    }
    if (fit_variables.fittauRF){
      delta=1.0/p->tauRF-dpdunsurtauRF*dsurT; if (delta>0) {p->tauRF=1.0/delta;} 
    }
    if (fit_variables.fittauRF){
      delta=p->A0-dpdA0*dsurT; p->A0=delta;
    }
    if (fit_variables.fitb0) {
      delta=p->b0-dpdb0*dsurT; p->b0=delta;
    }
    if (fit_variables.fitgCNOA) {
      delta=p->lambdaA-dpdlambdaA*dsurT;
      //printf("delta lambdaA=%lg\n",delta);exit(0);
 
      if (delta>=0){
	p->lambdaA=delta;
	p->gCNOA=p->lambdaA*p->CA;
	p->zetaA-=(dpdlambdaA*p->ECNOA+p->lambdaA*dpdECNOA)*dsurT; 
	p->unsurtauA=1.0/p->tauAref+p->lambdaA;
	p->tauA=1.0/p->unsurtauA;//Amacrine characteristic time in the presence of CNO (ms)
      }
    }
    if (fit_variables.fitgCNOG) {
      double deltagG=p->lambdaG-dpdlambdaG*dsurT; 
      if (deltagG>=0){
	p->lambdaG=deltagG;
	p->gCNOG=p->lambdaG*p->CG;
	p->unsurtauG=1.0/p->tauGref+p->lambdaG;
	p->tauG=1.0/p->unsurtauG;//GCell characteristic time in the presence of CNO (ms)
      }
      p->zetaG-=dpdlambdaG*p->ECNOG*dsurT; 
    }

    if (fit_variables.fitzetaA) {
      p->zetaA-=dpdzetaA*dsurT;
    }

    if (fit_variables.fitzetaG) {
      p->zetaG-=dpdzetaG*dsurT;
    }
    
    if (fit_variables.fitECNOG) {
      p->ECNOG-=dpdECNOG*dsurT;
      p->zetaG-=p->lambdaG*dpdECNOG*dsurT; 
    }

    if (fit_variables.fitECNOA) {
      delta=p->ECNOA-dpdECNOA*dsurT;
      p->zetaA-=(dpdlambdaA*p->ECNOA+p->lambdaA*dpdECNOA)*dsurT; 
      p->ECNOA=delta;
    }
   
    if ((p->tauA>2000)||(p->tauB>2000)||(p->tauRF>2000)||(p->tauG>2000)){  
      printf("Characteristic times are becoming too large: tauB=%g, tauA=%g, tauRF=%g, tauG=%g. Aborting.\n",p->tauB,p->tauA,p->tauRF,p->tauG);
      exit(0);
    }
    
    p->unsurtauAB=p->unsurtauA+p->unsurtauB;//1/tauA+1/tauB (kHz)
    p->unsurtau=p->unsurtauA-p->unsurtauB;//1/tauA-1/tauB (kHz)
    if ((fabs(p->unsurtau)>1E-10)) {
      p->tau=1.0/p->unsurtau;
    }
    else {
      printf("Case tau^-1=0 not implemented yet, exiting\n");
      exit(0);
    }
    p->tau2=pow(p->tau,2);
    p->tauAB=1.0/p->unsurtauAB;
    p->mu=-p->wab*p->wba*p->tau2;

    ComputeRestState_FixedCharacteristicTimes(iGC,*p,f,X,&VB_rest,&VA_rest,&VG_rest,&A_rest);
    ComputeReferenceTimes(p,VB_rest, VA_rest,VG_rest);
    if (!(nbview%strobo)) printf("\t\t\t\033[0;31mVB_rest=%lg, VA_rest=%lg, VG_rest=%lg, ECNOA=%lg, ECNOG=%lg, zetaA=%lg, zetaG=%lg, mu=%lg\n\033[0m\n",VB_rest,VA_rest,VG_rest,p->ECNOA,p->ECNOG,p->zetaA,p->zetaG,p->mu);
    if (!(nbview%strobo)) printf("\t\t\ttauB_ref=%lg, tauA_ref=%lg, tauG_ref=%lg\n", p->tauBref,p->tauAref,p->tauGref);
    if (!(nbview%strobo)) nbview=0;
    nbview++;
    iter++;

    //Clear events 
    ThRF.clear();
    dThRFdwba.clear();
    dThRFdwab.clear();
    dThRFdwag.clear();
    dThRFdwbg.clear();
    dThRFdunsurtauA.clear();
    dThRFdunsurtauB.clear();
    dThRFdunsurtauG.clear();
    dThRFdunsurtauRF.clear();
    dThRFdA0.clear();
    dThRFdb0.clear();
    dThRFdlambdaA.clear();
    dThRFdlambdaG.clear();
    dThRFdzetaA.clear();
    dThRFdzetaG.clear();
    dThRFdECNOA.clear();
    dThRFdECNOG.clear();
  }//End of estimation loop
  My_gsl_matrix_free(Psi);My_gsl_matrix_free(Psi_transpose);
  My_gsl_vector_complex_free(SumPinvbgKSg);
  My_gsl_vector_complex_free(SumPinvbgKSg_CNOA);
  free(X);
  //fclose(fp);
  return d;
}

double Distance(std::vector <Event> data1,std::vector <Event> data2,double tmax_fit){//Computes the L2 distance between two sets of data over a time horizon tmax_fit
  
  // if ((data1.size()<tmax_fit)||(data2.size()<tmax_fit)) {printf("In the function Distance, tmax_fit is larger than the vectors dimension,aborting \n");exit(0);}
  
  double d=0;
  double t=0;
   
  for (int ii=0;(t=data1[ii].t)<tmax_fit;ii++){
    if (t!=data2[ii].t) {printf("In the function Distance, time mistatch, aborting\n");exit(0);}       
    d+=pow(data1[ii].Amp-data2[ii].Amp,2);
  }

  return sqrt(d/(double)tmax_fit);
}

//Perturbs the parameters in one direction and computes the linear response
//On output the params pdp is modified and the linear response vector  dThRFd is computed
//pdp: params to be modified
//tmax_fit: maximal time for computing the response
//iGC: index of the GCell 
//data: vector of data
//ThRF: theoretical RF without perturbation
//kappa_th, sigma_th,Psi, Psi_transpose, SumPinvbgKSg, SumPinvbgKSg_CNOA: auxilary vectors. Must be allocated first
//View, checks: used to debug.
void ComputeLinearResponse(params *pdp,double tmax_fit,int iGC,std::vector <Event> data,std::vector <Event> ThRF,std::vector <Event> *dThRFd,Spectrum kappa_th,Spectrum sigma_th,gsl_matrix *Psi,gsl_matrix *Psi_transpose,gsl_vector_complex* SumPinvbgKSg,gsl_vector_complex* SumPinvbgKSg_CNOA,bool View,bool checks){
 
  int alphaGC=iGC+2*pdp->L;//Index of the GCell in the extended phase space
  double xGC=iGC*pdp->dx,yGC=0;//Physical coordinates of the GCell (mm)

  //printf("\t\t\t=> dRF/dwba\n");
 // pdp->wba+=epsilonwba;
  //pdp->mu=-pdp->wab*pdp->wba*pdp->tau2;
  
  init_Network_Laplacian(pdp,1);   
  
  ComputeTheoreticalSpectrumL(kappa_th,sigma_th,Psi,Psi_transpose,pdp);
  
  if (checks){ //Beginning of comparing numerical and theoretical eigenvectors  
    //  printf("\n\tChecking inversion\n");
    if (CheckInverse(pdp->P,pdp->Pinv)) {
      //printf("\n\tPinv is the inverse of P\n");
    }
    else {
      printf("\n\tPinv is not the inverse of P, aborting\n");
      exit(0);
    }
    
    //printf("\n\tChecking theoretical eigenvalues and eigenvectors\n");
    if (TestSpectrum(pdp->cL,pdp->P,pdp->lambda_th)){//Tests eigenvalues and eigenvectors
      //printf("\n\tThe spectrum is OK\n");
    }
    else {
      printf("\n\tP computation failed\n");
      exit(0);
    }
  }//End of comparing numerical and theoretical eigenvectors
  
  KSG(xGC,yGC,*pdp, SumPinvbgKSg);//KSG contains the value of the spatial R at point xGC,yGC.
  KG_CNOA(*pdp,SumPinvbgKSg_CNOA);//CNO A contribution
  
  double t=0;
  gsl_complex kg={0,0};
  Event point;

  for (int ii=0;(t=data[ii].t)<tmax_fit;ii++){//Time loop where time is indexed on data
    kg=KG(alphaGC,xGC,0,t-pdp->t0,*pdp,SumPinvbgKSg,SumPinvbgKSg_CNOA);
    if (fabs(GSL_IMAG(kg)>1E-8)) {printf("Problem. The RF is not real. Imaginary part at (%lg,%lg,%lg)=%lg",xGC,yGC,t,kg);exit(0);}
    
    point.t=t;point.Amp=(GSL_REAL(kg)-ThRF[ii].Amp);
    dThRFd->push_back(point);
  }//End of time loop
  
}

//Perturbs the parameters in one direction and computes the normalized power spectrum of the linear response
//On output the params pdp is modified and the linear response vector  dThRFd is computed
//pdp: params to be modified
//tmax_fit: maximal time for computing the response
//iGC: index of the GCell 
//data: vector of data
//ThRF: theoretical RF without perturbation
//kappa_th, sigma_th,Psi, Psi_transpose, SumPinvbgKSg, SumPinvbgKSg_CNOA: auxilary vectors. Must be allocated first
//View, checks: used to debug.
//scaling. Scaling factor to normalise kg
void ComputeLinearResponse_hat(params *pdp,double tmax_fit,int iGC,std::vector <Event> data,std::vector <Event> ThRF,std::vector <Event> *dThRFd,Spectrum kappa_th,Spectrum sigma_th,gsl_matrix *Psi,gsl_matrix *Psi_transpose,gsl_vector_complex* SumPinvbgKSg,gsl_vector_complex* SumPinvbgKSg_CNOA,bool View,bool checks,double scaling){
 
  int alphaGC=iGC+2*pdp->L;//Index of the GCell in the extended phase space
  double xGC=iGC*pdp->dx,yGC=0;//Physical coordinates of the GCell (mm)

  //printf("\t\t\t=> dRF/dwba\n");
 // pdp->wba+=epsilonwba;
  //pdp->mu=-pdp->wab*pdp->wba*pdp->tau2;
  
  init_Network_Laplacian(pdp,1);   
  
  ComputeTheoreticalSpectrumL(kappa_th,sigma_th,Psi,Psi_transpose,pdp);
  
  if (checks){ //Beginning of comparing numerical and theoretical eigenvectors  
    //  printf("\n\tChecking inversion\n");
    if (CheckInverse(pdp->P,pdp->Pinv)) {
      //printf("\n\tPinv is the inverse of P\n");
    }
    else {
      printf("\n\tPinv is not the inverse of P, aborting\n");
      exit(0);
    }
    
    //printf("\n\tChecking theoretical eigenvalues and eigenvectors\n");
    if (TestSpectrum(pdp->cL,pdp->P,pdp->lambda_th)){//Tests eigenvalues and eigenvectors
      //printf("\n\tThe spectrum is OK\n");
    }
    else {
      printf("\n\tP computation failed\n");
      exit(0);
    }
  }//End of comparing numerical and theoretical eigenvectors
  
  KSG(xGC,yGC,*pdp, SumPinvbgKSg);//KSG contains the value of the spatial R at point xGC,yGC.
  KG_CNOA(*pdp,SumPinvbgKSg_CNOA);//CNO A contribution
  
  double t=0;
  gsl_complex kg={0,0};
  Event point;

  for (int ii=0;(t=data[ii].t)<tmax_fit;ii++){//Time loop where time is indexed on data
    kg=gsl_complex_div_real(KG_hat(alphaGC,xGC,0,t-pdp->t0,*pdp,SumPinvbgKSg,SumPinvbgKSg_CNOA),scaling);
      
    point.t=t;point.Amp=(gsl_complex_abs(kg)-ThRF[ii].Amp);
    dThRFd->push_back(point);
  }//End of time loop
  
}

//Compute and store numerical and theoretical temporal RF of GCell with index iGC
//chaineT is the name of the file where OPL,BCell,ACell,GCell voltage and BCell activity vs time are stored
//chainTh contains the theoretical spatio temporal RF
//f is the space independent vector field
//fS is the space dependent vector field
//SumPinvbgKSg is an auxilary vector used for computations (must be allocated first)
void ComputeAndStoreRF(int iGC,params p,char *chaineT,char* chaineTh,void (*f)(double *X,double *Xp,double t,params p,double (*input)(double x,double t, params p)),void (*fS)(double x,double *X,double *Xp,double t,params p,double (*input)(int beta,double x,double t, params p)),gsl_vector_complex* SumPinvbgKSg,gsl_vector_complex* SumPinvbgKSg_CNOA){
   
  int alphaBC=iGC;//Index of the BCell above this GCell in the extended phase space
  int alphaAC=iGC+p.L;//Index of the ACell above this GCell in the extended phase space
  int alphaGC=iGC+2*p.L;//Index of the GCell in the extended phase space
  int alphaAc=iGC+3*p.L;//Index of the BCell activity in the extended phase space
  double xGC=iGC*p.dx,yGC=0;//Physical coordinates of the GCell (mm)

  int dim_ext=4*p.L;// Dimension of the phase space
  int dim=3*p.L;// Dimension of the phase space without activities
  double *X=(double*)calloc(dim_ext,sizeof(double));//State vector time t: Variables 0... N-1 are bipolar voltages,Variables N... 2N-1 are amacrine voltages, Variables 2N... 3N-1 are ganglion voltages
  double *Xp=(double*)calloc(dim_ext,sizeof(double));//State vector time t+dt
  double* temp;//swap variable
  
  //Computation of GCells Receptive Field     
  printf("\n\tSimulating the Gcell %d response to a step stimulus\n",alphaBC);
  
  printf("\n\t\t=>Time empirical RF will be stored in %s\n",chaineT);
  FILE* fp1=fopen(chaineT,"w");
  double t=p.tmin;    
  //int count=p.countmax;//State is saved every p.countmax
 
  double VB_rest=0; //Bipolar rest voltage 
  double VA_rest=0; //Amacrine rest voltage 
  double VG_rest=0; //Ganglion rest voltage 
  double A_rest=0; //Ganglion rest voltage */

  t=0;
  ComputeRestState_FixedCharacteristicTimes(iGC,p,f,X,&VB_rest,&VA_rest,&VG_rest,&A_rest);
  //ComputeRestState_Full(iGC,&p,f,X,&VB_rest,&VA_rest,&VG_rest,&A_rest);
	
  /*while (t<=p.tmax){//Time loop (t is in ms)   
    double vbi=X[alphaBC]+VB_rest; //Bipolar voltage 
    double vai=X[alphaAC]+VA_rest; //Amacrine voltage 
    double vgi=X[alphaGC]+VG_rest+p.b0; //Ganglion voltage 
    fprintf(fp1,"%lg\t%lg\t%lg\t%lg\t%lg\n",t,alpha2S(alphaBC,xGC,t,p),vbi,vai,vgi);

    //Updating the vector field
    fS(xGC,X,Xp,t,p,Bip_input_alpha2S_ON);
    
    temp=X;
    X=Xp;
    Xp=temp;
    
    t+=p.dt;
  }
  for (int i=0;i<dim_ext;i++) X[i]=Xx[i];//Reinitialising the initial state 
  fclose(fp1); */
  
  /*** Computing eigenmodes and eigenvalues **/
  printf("\n\tComputing the theoretical RF of cell %d \n",alphaBC);
  printf("\n\t\t--------------------\n\t\tComputing eigenmodes and eigenvalues\n\t\t--------------------\n");
  
  gsl_matrix *Psi=gsl_matrix_calloc(p.L,p.L),*Psi_transpose=gsl_matrix_calloc(p.L,p.L);
  Spectrum kappa_th;//Theoretical eigenvalues of the connectivity matrix amacrine bipolar
  Spectrum sigma_th;//Theoretical eigenvalues of the connectivity matrix bipolar amacrine
  bool View=false; //If true print the matrices involved in the model
  bool checks=true; //If true checks spectrum and P^-1

  ComputeTheoreticalModesLaplacian1D(p,&kappa_th,&sigma_th,Psi,Psi_transpose);
  ComputeTheoreticalSpectrumL(kappa_th,sigma_th,Psi,Psi_transpose,&p);
  if (View){
    printf("\n\tEigenvalues\n\n");  ViewVectComp(p.lambda_th);
    printf("\n\tMatrix P\n\n");  ViewMatComp(p.P);
    printf("\tMatrix Pinv\n\n");  ViewMatComp(p.Pinv);
	//    exit(0);
  }
  
  if (checks){ //Beginning of comparing numerical and theoretical eigenvectors
    
    printf("\n\tChecking inversion\n");
    if (CheckInverse(p.P,p.Pinv)) {printf("\n\tPinv is the inverse of P\n");}
    else {
      printf("\n\tPinv is not the inverse of P, aborting\n");
      exit(0);
    }
    
    printf("\n\tChecking theoretical eigenvalues and eigenvectors\n");
    if (TestSpectrum(p.cL,p.P,p.lambda_th)){//Tests eigenvalues and eigenvectors
      printf("\n\tThe spectrum is OK\n");
    }
    else {
      printf("\n\tP computation failed\n");
      exit(0);
    }
  }//End of comparing numerical and theoretical eigenvectors
  
  gsl_complex kg={0,0};//Computes the RF of a GCell alpha embedded in the BCell, ACell, GCell network
  
  // sprintf(chaineTh,"Trajectories/Theoretical_Cell_%s%s",InFile,ISCNO);
  printf("\n\t\t=>Theoretical RF will be stored in %s\n",chaineTh);
  //sprintf(chaine2,"Trajectories/SpatialResponsealpha2%s%s",InFile,ISCNO);
  //printf("\t\t=>Spatial kernel will be stored in %s\n",chaine2);
  
  SumPinvbgKSg=gsl_vector_complex_calloc(dim);//Auxilary used for computation
  SumPinvbgKSg_CNOA=gsl_vector_complex_calloc(dim);//Auxilary used for computation
  KG_CNOA(p,SumPinvbgKSg_CNOA);//CNO A contribution

  fp1=fopen(chaineTh,"w");	
      
  KSG(xGC,yGC,p, SumPinvbgKSg);//KSG contains the value of the spatial RF, of each eigenmode beta, at point x,y.
  
  for (double t=p.tmin;t<=p.tmax;t+=p.dt){//Time loop
    kg=KG(alphaGC,xGC,0,t-p.t0,p,SumPinvbgKSg,SumPinvbgKSg_CNOA);
    if (fabs(GSL_IMAG(kg)>1E-8)) {printf("Problem. The RF is not real. Imaginary part at (%lg,%lg,%lg)=%lg",xGC,yGC,t,kg);exit(0);}
    //fprintf(fp1,"%lg\t%lg\t%lg\n",x,t,p.A0*GSL_REAL(kg)+p.b0+VG_rest);
    fprintf(fp1,"%lg\t%lg\n",t,GSL_REAL(kg)+VG_rest);
  }//End of time loop
  fclose(fp1);
  
  My_gsl_matrix_free(Psi);My_gsl_matrix_free(Psi_transpose);
 
}

//Compute and store numerical and theoretical spatio-temporal RF of GCell with index iGC
//string Cell_Type distinguishes between a ON and OFF Cell
//chaineT is the name of the file where OPL,BCell,ACell,GCell voltage and BCell activity vs time are stored
//chaineS contains the spatial receptive field
//chainTh contains the theoretical spatio temporal RF
//f is the space independent vector field
//fS is the space dependent vector field
//SumPinvbgKSg is an auxilary vector used for computations (must be allocated first)
void ComputeAndStoreSpaceRF(int iGC,params p,char *chaineT,char* chaineS,char* chaineTh,void (*f)(double *X,double *Xp,double t,params p,double (*input)(double x,double t, params p)),void (*fS)(double x,double *X,double *Xp,double t,params p,double (*input)(int beta,double x,double t, params p)),gsl_vector_complex* SumPinvbgKSg,gsl_vector_complex* SumPinvbgKSg_CNOA){
  //sprintf(ISCNO,"%s","_CNO");     
  
  /*double (*input)(int beta,double x,double t, params p);//Useless. If the cell is OFF it is sufficient to let A0 be negative
  if (Cell_Type=="ON") input=Bip_input_alpha2S_ON;
  else if (Cell_Type=="OFF") input=Bip_input_alpha2S_ON;*/

  int alphaBC=iGC;//Index of the BCell above this GCell in the extended phase space
  int alphaAC=iGC+p.L;//Index of the ACell above this GCell in the extended phase space
  int alphaGC=iGC+2*p.L;//Index of the GCell in the extended phase space
  int alphaAc=iGC+3*p.L;//Index of the BCell activity in the extended phase space
  double xGC=iGC*p.dx,yGC=0;//Physical coordinates of the GCell (mm)
  
  int dim_ext=4*p.L;// Dimension of the phase space
  int dim=3*p.L;// Dimension of the phase space without activities
  double *X=(double*)calloc(dim_ext,sizeof(double));//State vector time t: Variables 0... N-1 are bipolar voltages,Variables N... 2N-1 are amacrine voltages, Variables 2N... 3N-1 are ganglion voltages
  double *Xp=(double*)calloc(dim_ext,sizeof(double));//State vector time t+dt
  double *Xx=(double*)calloc(dim_ext,sizeof(double)); //X(x,t)
  double* temp;//swap variable
  
  //Computation of GCells Receptive Field     
  printf("\n\tSimulating the Gcell %d response to a step stimulus\n",alphaBC);
  
  printf("\n\t\t=>Time empirical RF will be stored in %s\n",chaineT);
  printf("\n\t\t=>Full Empirical RF will be stored in %s\n",chaineS);
  FILE* fpT=fopen(chaineT,"w");
  FILE* fpS=fopen(chaineS,"w");
  double t=p.tmin;    
  //int count=p.countmax;//State is saved every p.countmax
  
  /* while (t<0){//Transient time loop (t is in ms) to reach the rest state 
    f(X,Xp,t,p,Bip_input_null);
    temp=X;
    X=Xp;
    Xp=temp;
    t+=p.dt;
  }//End of transient time loop*/
 
  t=0;
  double VB_rest=0; //Bipolar rest voltage 
  double VA_rest=0; //Amacrine rest voltage 
  double VG_rest=0; //Ganglion rest voltage 
  double A_rest=0; //Ganglion rest voltage 
  ComputeRestState_FixedCharacteristicTimes(iGC,p,f,X,&VB_rest,&VA_rest,&VG_rest,&A_rest); 
  for (int i=0;i<dim_ext;i++) Xx[i]=X[i]; //Storing values at the end of the transient  

  //Test to be removed
  /*double CB=0,CA=0,CG=0;
  for (int j=0;j<p.L;j++){//Loop cells
    int beta=j;//Bipolars
    double Lalphabeta=gsl_matrix_get(p.cL,alphaAC,beta);
    CA-=Lalphabeta*p.thetaB;
    Lalphabeta=gsl_matrix_get(p.cL,alphaGC,beta);
    CG-=Lalphabeta*p.thetaB;
    beta=p.L+j;//Amacrines
    Lalphabeta=gsl_matrix_get(p.cL,alphaBC,beta);
    CB-=Lalphabeta*p.thetaA;
    Lalphabeta=gsl_matrix_get(p.cL,alphaGC,beta);
    CG-=Lalphabeta*p.thetaA;
  }//End of loop cells
  */
  //printf("\t\t\t\tIn ComputeRF: VBRest=%g, VARest=%g, VGRest=%g, CB=%g, CA=%g, CG=%g\n",VB_rest,VA_rest,VG_rest,CB,CA,CG);//Test to be removed
  
  bool store=true;
  for (double x=xGC-p.L/2*p.dx;x<=xGC+p.L/2*p.dx;x+=p.dx){//x loop
    //printf("\t\tx=%lg\n",x);
    t=0;
    if (fabs(x-xGC)<1E-10) store=true;
    else store=false;
    
    while (t<=p.tmax){//Time loop (t is in ms)   
      /*if (count==p.countmax){//State is saved every p.countmax
      //printf("t=%lg\n",t);
      count=1;
      }//End of state is saved every p.countmax
      else count++;*/
      
      //Storing the response of the cells located at alphaBC (Bipolar), alphaAC (Amacrine), alphAGC (Ganglion) 
      double vgi=X[alphaGC]+p.b0; //Ganglion voltage 
      if (store==true) {
	double vbi=X[alphaBC]; //Bipolar voltage 
	double vai=X[alphaAC]; //Amacrine voltage 
	//double vbi=X[alphaBC]+VB_rest; //Bipolar voltage 
	//double vai=X[alphaAC]+VA_rest; //Amacrine voltage 
	//double vgi=X[alphaGC]+VG_rest+p.b0; //Ganglion voltage 
	//double Acti=X[alphaAc]; //Bipolar activity
 	//fprintf(fpT,"%lg\t%lg\t%lg\t%lg\t%lg\n",t,alpha2S(alphaBC,x,t,p)+VB_rest,vbi,vai,vgi);
	fprintf(fpT,"%lg\t%lg\t%lg\t%lg\t%lg\n",t,alpha2S(alphaBC,x,t,p),vbi,vai,vgi);
      }
      fprintf(fpS,"%lg\t%lg\t%lg\n",x,t,vgi);

      //Updating the vector field
       fS(x,X,Xp,t,p,Bip_input_alpha2S_ON);//If the cell is OFF it is sufficient to let A0 be negative
       
       temp=X;
       X=Xp;
       Xp=temp;
      /* if (fabs(x-xGC)<1E-10){
	printf("BC central Voltage=%g,Activity=%g\n",X[alphaBC],X[alphaAC]);
	}*/
      
      t+=p.dt;
    }//End of time loop
    for (int i=0;i<dim_ext;i++) X[i]=Xx[i];//Reinitialising the initial state 
  }//End of xloop
  fclose(fpT); fclose(fpS);
  
  /*** Computing eigenmodes and eigenvalues **/
  printf("\n\tComputing the theoretical RF of cell %d \n",alphaBC);
  printf("\n\t\t--------------------\n\t\tComputing eigenmodes and eigenvalues\n\t\t--------------------\n");
  
  gsl_matrix *Psi=gsl_matrix_calloc(p.L,p.L),*Psi_transpose=gsl_matrix_calloc(p.L,p.L);
  Spectrum kappa_th;//Theoretical eigenvalues of the connectivity matrix amacrine bipolar
  Spectrum sigma_th;//Theoretical eigenvalues of the connectivity matrix bipolar amacrine
  bool View=false; //If true print the matrices involved in the model
  bool checks=true; //If true checks spectrum and P^-1

  ComputeTheoreticalModesLaplacian1D(p,&kappa_th,&sigma_th,Psi,Psi_transpose);
  ComputeTheoreticalSpectrumL(kappa_th,sigma_th,Psi,Psi_transpose,&p);
  if (View){
    printf("\n\tEigenvalues\n\n");  ViewVectComp(p.lambda_th);
    printf("\n\tMatrix P\n\n");  ViewMatComp(p.P);
    printf("\tMatrix Pinv\n\n");  ViewMatComp(p.Pinv);
	//    exit(0);
  }
  
  if (checks){ //Beginning of comparing numerical and theoretical eigenvectors
    
    printf("\n\tChecking inversion\n");
    if (CheckInverse(p.P,p.Pinv)) {printf("\n\tPinv is the inverse of P\n");}
    else {
      printf("\n\tPinv is not the inverse of P, aborting\n");
      exit(0);
    }
    
    printf("\n\tChecking theoretical eigenvalues and eigenvectors\n");
    if (TestSpectrum(p.cL,p.P,p.lambda_th)){//Tests eigenvalues and eigenvectors
      printf("\n\tThe spectrum is OK\n");
    }
    else {
      printf("\n\tP computation failed\n");
      exit(0);
    }
  }//End of comparing numerical and theoretical eigenvectors
  
  gsl_complex kg={0,0};//Computes the RF of a GCell alpha embedded in the BCell, ACell, GCell network
  
  // sprintf(chaineTh,"Trajectories/Theoretical_Cell_%s%s",InFile,ISCNO);
  printf("\n\t\t=>Theoretical RF will be stored in %s\n",chaineTh);
  //sprintf(chaine2,"Trajectories/SpatialResponsealpha2%s%s",InFile,ISCNO);
  //printf("\t\t=>Spatial kernel will be stored in %s\n",chaine2);
  
  SumPinvbgKSg=gsl_vector_complex_calloc(dim);//Auxilary used for computation
  SumPinvbgKSg_CNOA=gsl_vector_complex_calloc(dim);//Auxilary used for computation
  KG_CNOA(p,SumPinvbgKSg_CNOA);//CNO A contribution

  fpT=fopen(chaineTh,"w");	
  
  //double y=0;
  for (double x=xGC-p.L/2*p.dx;x<=xGC+p.L/2*p.dx;x+=p.dx){//x loop
    //	for (double y=-3*p.dy;y<=3*p.dy;y+=p.dy/4){
    
    KSG(x,yGC,p, SumPinvbgKSg);//KSG contains the value of the spatial RF, of each eigenmode beta, at point x,y.
    
    for (double t=p.tmin;t<=p.tmax;t+=p.dt){//Time loop
      kg=KG(alphaGC,x,0,t-p.t0,p,SumPinvbgKSg,SumPinvbgKSg_CNOA);
      if (fabs(GSL_IMAG(kg)>1E-8)) {printf("Problem. The RF is not real. Imaginary part at (%lg,%lg,%lg)=%lg",xGC,yGC,t,kg);exit(0);}
      //fprintf(fpT,"%lg\t%lg\t%lg\n",x,t,p.A0*GSL_REAL(kg)+p.b0+VG_rest);
      fprintf(fpT,"%lg\t%lg\t%lg\n",x,t,GSL_REAL(kg)+VG_rest);
    }//End of time loop
    //}//End of y loop
  }//End of x loop
  fclose(fpT);
  
  My_gsl_matrix_free(Psi);My_gsl_matrix_free(Psi_transpose);
 
}


//Rescale the increment epsilon used in FitCell whenever the computed derivative is outside the range [d_min,d_max].
//Returns true when rescaling,
int Rescale(double derivative,double *epsilon,double d_min,double d_max){

  int IsRescaled=0;
  
  if (fabs(derivative)>10*d_max){
    printf("\t\t\t\033[0;32mDerivative =%lg is too big, epsilon=%lg\033[0m",fabs(derivative),*epsilon);
    *epsilon/=10;
    IsRescaled=1;
  }
  else if (fabs(derivative)>d_max){
    printf("\t\t\t\033[0;32mDerivative =%lg is too big, epsilon=%lg\033[0m",fabs(derivative),*epsilon);
    *epsilon/=2;
    IsRescaled=1;
  }
  else if (fabs(derivative)<d_min) {
    printf("\t\t\t\033[0;32mDerivative =%lg is too small. epsilon=%lg\033[0m",fabs(derivative),*epsilon);
    *epsilon*=2;
    IsRescaled=1;
  }
  
  if ((*epsilon>100)||(*epsilon<1E-10)) {
    printf("\n\t\t\t\033[0;35mepsilon=%lg as become out of reasonable bounds. Aborting. \n\033[0m",*epsilon);
    IsRescaled=2;
    //exit(0);
    }
  return IsRescaled;
}
