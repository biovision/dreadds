/** Functions for dynamic evolution **/

#define bA 0
#define bB 0

#include "Dynamics.h"
double NA(double V,params p){//Amacrine Rectification
  //return V-p.thetaA;
  //return (V>p.thetaA ? V - p.thetaA:0); 
  return (V>p.thetaA ? V - p.thetaA+bA:bA); 
  //return (V>p.thetaA ? (V< 10 ? V - p.thetaA:10):0); 
}

double NB(double V,params p){//Bipolar Rectification
  // return V-p.thetaB;
  //return (V>p.thetaB ? V - p.thetaB:0); 
  //return (V>p.thetaB ? V : p.thetaB); 
  return (V>p.thetaB ? V - p.thetaB+bB:bB); 
 //return (V>p.thetaB ? (V< 10 ? V - p.thetaB:10):0); 
}

double G(double A){//Bipolar Gain control
  //return 1.0;
  return (A<0 ? 0.0: 1.0/(1+pow(A,6)));
}

double R(double V,double A,params p){//Non Linear Bipolar correction
  return NB(V,p)*G(A);
}

#define theta_act 2.0/3.0

//Vector field with rectification and gain control
//Variables 0... N-1 are bipolar voltages
//Variables N... 2N-1 are amacrine voltages
//Variables 2N... 3N-1 are ganglion voltages
//Variables 3N... 4N-1 are bipolar activities
//X is the vector state at time t
//Xp is the vector state at time t+dt
//t is the current time
//p contains the model's parameters
//cL is the matrix defining the linear part of the vector field. Included in p (pointer)
//input is a pointer of function pointing the time dependent input, a function of space coordinate (x), time (t) and parameters (p).
//This corresponds to a case where the input changes with space but does not depend on the Bcell index
void vector_field(double *X,double *Xp,double t,params p,double (*input)(double x,double t, params p)){//Vector field
  double Lalphabeta=0,dVBalpha=0,dVAalpha=0,dVGalpha=0,dVActalpha=0;
  int alpha=0,beta=0;

  /*for (int i=0;i<p.L;i++){//Test to be removed
    alpha=i;
    if (X[alpha]<p.thetaB) {printf("\t\t\033[0;35mBC %d has voltage %g < thetaB=%g, rectified\n",beta,X[alpha],p.thetaA);} 
    if (X[alpha+3*p.L]>theta_act) {printf("\t\t\033[0;35mBC %d has gain control A= %g > %g\n",beta,X[alpha+3*p.L],p.thetaA,theta_act);} 
    alpha=i+p.L;
    if (X[alpha]<p.thetaA) {printf("\t\t\033[0;35mAC %d has voltage %g < thetaA=%g, rectified\n",beta,X[alpha],p.thetaA);;} 
    printf("\033[0m");
  }
  */
  for (int i=0;i<p.L;i++){//Updating cells' voltage
    //Updating bipolar voltages
    alpha=i;
    dVBalpha=-X[alpha]/p.tauB+input(i*p.dx,t,p)+p.KB;
    // printf("input=%lg\n",input(i*p.dx,t,p));//exit(0);
    for (int j=0;j<p.L;j++){//Loop on amacrine voltages
      beta=p.L+j;
      Lalphabeta=gsl_matrix_get(p.cL,alpha,beta);
      dVBalpha+=Lalphabeta*NA(X[beta],p); //Rectified amacrine input 
    }//End of loop on amacrine voltages
    Xp[alpha]=X[alpha]+dVBalpha*p.dt;
    
    //Updating amacrine voltages
    alpha=i+p.L;
    dVAalpha=-X[alpha]/p.tauA+p.zetaA+p.KA;
    for (int j=0;j<p.L;j++){//Loop on bipolar voltages
      beta=j;
      Lalphabeta=gsl_matrix_get(p.cL,alpha,beta);
      dVAalpha+=Lalphabeta*R(X[beta],X[beta+3*p.L],p);//Rectified bipolar input
    }//End of loop on bipolar voltages
    Xp[alpha]=X[alpha]+dVAalpha*p.dt;
    
    //Updating ganglion voltages
    alpha=i+2*p.L;
    dVGalpha=-X[alpha]/p.tauG+p.zetaG+p.KG;
    for (int j=0;j<p.L;j++){//Loop on bipolar and amacrines voltages
      beta=j;//Bipolar contribution
      Lalphabeta=gsl_matrix_get(p.cL,alpha,beta);
      dVGalpha+=Lalphabeta*R(X[beta],X[beta+3*p.L],p);//Rectified bipolar input
      //dVGalpha+=Lalphabeta*X[beta];//Non rectified bipolar input
      
      beta=p.L+j;//Amacrine contribution
      Lalphabeta=gsl_matrix_get(p.cL,alpha,beta);
      dVGalpha+=Lalphabeta*NA(X[beta],p);//Rectified amacrine input
      //dVGalpha+=Lalphabeta*X[beta];//Non rectified amacrine input
    }//End of loop on bipolar and amacrine voltages
    Xp[alpha]=X[alpha]+dVGalpha*p.dt;
    
    //Updating BCells activities
    alpha=i+3*p.L;
    dVActalpha=-X[alpha]/p.taua+gsl_matrix_get(p.cL,alpha,i)*NB(X[i],p);
    Xp[alpha]=X[alpha]+dVActalpha*p.dt;
  }//End of updating cells' voltage    
}

//Vector field with rectification without gain control
//Variables 0... N-1 are bipolar voltages
//Variables N... 2N-1 are amacrine voltages
//Variables 2N... 3N-1 are ganglion voltages
//Variables 3N... 4N-1 are bipolar activities
//X is the vector state at time t
//Xp is the vector state at time t+dt
//t is the current time
//p contains the model's parameters
//cL is the matrix defining the linear part of the vector field. Included in p (pointer)
//input is a pointer of function pointing the time dependent input, a function of space coordinate (x), time (t) and parameters (p).
//This corresponds to a case where the input changes with space but does not depend on the Bcell index
void vector_field_Rect(double *X,double *Xp,double t,params p,double (*input)(double x,double t, params p)){//Vector field
  double Lalphabeta=0,dVBalpha=0,dVAalpha=0,dVGalpha=0,dVActalpha=0;
  int alpha=0,beta=0;

  /*
  for (int i=0;i<p.L;i++){//Test to be removed
    alpha=i;
    if (X[alpha]<p.thetaB) {printf("\t\t\033[0;35mBC %d has voltage %g > thetaA=%g, rectified\n",beta,X[alpha],p.thetaA);} 
    alpha=i+p.L;
    if (X[alpha]<p.thetaA) {printf("\t\t\033[0;35mAC %d has voltage %g > thetaA=%g, rectified\n",beta,X[alpha],p.thetaA);;} 
    printf("\033[0m");
  }
  */

  for (int i=0;i<p.L;i++){//Updating cells' voltage
    //Updating bipolar voltages
    alpha=i;
    dVBalpha=-X[alpha]/p.tauB+input(i*p.dx,t,p)+p.KB;
    // printf("input=%lg\n",input(i*p.dx,t,p));//exit(0);
    for (int j=0;j<p.L;j++){//Loop on amacrine voltages
      beta=p.L+j;
      Lalphabeta=gsl_matrix_get(p.cL,alpha,beta);
      dVBalpha+=Lalphabeta*NA(X[beta],p); //Rectified amacrine input 
    }//End of loop on amacrine voltages
    Xp[alpha]=X[alpha]+dVBalpha*p.dt;
    
    //Updating amacrine voltages
    alpha=i+p.L;
    dVAalpha=-X[alpha]/p.tauA+p.zetaA+p.KA;
    for (int j=0;j<p.L;j++){//Loop on bipolar voltages
      beta=j;
      Lalphabeta=gsl_matrix_get(p.cL,alpha,beta);
      dVAalpha+=Lalphabeta*NB(X[beta],p);//Rectified bipolar input
    }//End of loop on bipolar voltages
    Xp[alpha]=X[alpha]+dVAalpha*p.dt;
    
    //Updating ganglion voltages
    alpha=i+2*p.L;
    dVGalpha=-X[alpha]/p.tauG+p.zetaG+p.KG;
    for (int j=0;j<p.L;j++){//Loop on bipolar and amacrines voltages
      beta=j;//Bipolar contribution
      Lalphabeta=gsl_matrix_get(p.cL,alpha,beta);
      dVGalpha+=Lalphabeta*NB(X[beta],p);//Rectified bipolar input
      //dVGalpha+=Lalphabeta*X[beta];//Non rectified bipolar input
      
      beta=p.L+j;//Amacrine contribution
      Lalphabeta=gsl_matrix_get(p.cL,alpha,beta);
      dVGalpha+=Lalphabeta*NA(X[beta],p);//Rectified amacrine input
      //dVGalpha+=Lalphabeta*X[beta];//Non rectified amacrine input
    }//End of loop on bipolar and amacrine voltages
    Xp[alpha]=X[alpha]+dVGalpha*p.dt;
    
  }//End of updating cells' voltage    
}

//Linear vector field, without rectification, without gain control
void vector_field_lin(double *X,double *Xp,double t,params p,double (*input)(double x,double t, params p)){//Vector field
  double Lalphabeta=0,dVBalpha=0,dVAalpha=0,dVGalpha=0;
  int alpha=0,beta=0;
  //  double tauV=0;

  for (int i=0;i<p.L;i++){//Updating cells' voltage
    //Updating bipolar voltages
    alpha=i;
    dVBalpha=-X[alpha]/p.tauB+input(i*p.dx,t,p)+p.KB;
    // printf("input=%lg\n",input(i*p.dx,t,p));//exit(0);
    for (int j=0;j<p.L;j++){//Loop on amacrine voltages
      beta=p.L+j;
      Lalphabeta=gsl_matrix_get(p.cL,alpha,beta);
      //dVBalpha+=Lalphabeta*X[beta]; //Non-rectified amacrine input         }
      dVBalpha+=Lalphabeta*(X[beta]-p.thetaA); //Non-rectified amacrine input         }
      }//End of loop on amacrine voltages
    Xp[alpha]=X[alpha]+dVBalpha*p.dt;
 
    //Updating amacrine voltages
    alpha=i+p.L;
    dVAalpha=-X[alpha]/p.tauA+p.zetaA+p.KA;
    for (int j=0;j<p.L;j++){//Loop on bipolar voltages
      beta=j;
      Lalphabeta=gsl_matrix_get(p.cL,alpha,beta);
     // dVAalpha+=Lalphabeta*X[beta];//Non-rectified bipolar input
      dVAalpha+=Lalphabeta*(X[beta]-p.thetaB);//Non-rectified bipolar input
    }//End of loop on bipolar voltages
    Xp[alpha]=X[alpha]+dVAalpha*p.dt;

    //Updating ganglion voltages
    alpha=i+2*p.L;
    dVGalpha=-X[alpha]/p.tauG+p.zetaG+p.KG;
    for (int j=0;j<p.L;j++){//Loop on bipolar and amacrines voltages
      beta=j;//Bipolar contribution
      Lalphabeta=gsl_matrix_get(p.cL,alpha,beta);
      //dVGalpha+=Lalphabeta*X[beta];//Non-rectified bipolar input
      dVGalpha+=Lalphabeta*(X[beta]-p.thetaB);//Non rectified bipolar input
 
      beta=p.L+j;//Amacrine contribution
      Lalphabeta=gsl_matrix_get(p.cL,alpha,beta);
      //dVGalpha+=Lalphabeta*X[beta];//Non-rectified amacrine input
      dVGalpha+=Lalphabeta*(X[beta]-p.thetaA);//Non rectified amacrine input
    }//End of loop on bipolar and amacrine voltages
    Xp[alpha]=X[alpha]+dVGalpha*p.dt;
    
  }//End of updating cells' voltage    
}

//Vector field, similar as above,  with an additional variable, the space location x , the point of space where GCells RFs are computed. Rectified and Gain control
void vector_field_Space(double x,double *X,double *Xp,double t,params p,double (*input)(int ic,double x,double t, params p)){//Vector field
  double Lalphabeta=0,dVBalpha=0,dVAalpha=0,dVGalpha=0,dVActalpha=0;
  int alpha=0,beta=0;
  //  double tauV=0;

  /*for (int i=0;i<p.L;i++){//Test to be removed
    alpha=i;
     if (X[alpha]<p.thetaB) {printf("\t\t\033[0;35mBC %d has voltage %g < thetaB=%g, rectified\n",beta,X[alpha],p.thetaA);} 
    if (X[alpha+3*p.L]>theta_act) {printf("\t\t\033[0;34mBC %d has gain control A= %g > %g\n",beta,X[alpha+3*p.L],p.thetaA,theta_act);} 
    alpha=i+p.L;
    if (X[alpha]<p.thetaA) {printf("\t\t\033[0;35mAC %d has voltage %g < thetaA=%g, rectified\n",beta,X[alpha],p.thetaA);;} 
    printf("\033[0m");
    }*/

  for (int i=0;i<p.L;i++){//Updating cells' voltage
    //Updating bipolar voltages
    alpha=i;
    dVBalpha=-X[alpha]/p.tauB+input(alpha,x,t,p)+p.KB;//Each BCell alpha receives the input of its spatio-temporal RF computed at spatial point x and time t
    // printf("input=%lg\n",input(i*p.dx,t,p));//exit(0);
    for (int j=0;j<p.L;j++){//Loop on amacrine voltages
      beta=p.L+j;
      Lalphabeta=gsl_matrix_get(p.cL,alpha,beta);
      dVBalpha+=Lalphabeta*NA(X[beta],p); //Rectified amacrine input         }
      }//End of loop on amacrine voltages
    Xp[alpha]=X[alpha]+dVBalpha*p.dt;
 
    //Updating amacrine voltages
    alpha=i+p.L;
    dVAalpha=-X[alpha]/p.tauA+p.zetaA+p.KA;
    for (int j=0;j<p.L;j++){//Loop on bipolar voltages
      beta=j;
      Lalphabeta=gsl_matrix_get(p.cL,alpha,beta);
      dVAalpha+=Lalphabeta*R(X[beta],X[beta+3*p.L],p);//Rectified and gain control bipolar input
    }//End of loop on bipolar voltages
    Xp[alpha]=X[alpha]+dVAalpha*p.dt;

    //Updating ganglion voltages
    alpha=i+2*p.L;
    dVGalpha=-X[alpha]/p.tauG+p.zetaG+p.KG;
    for (int j=0;j<p.L;j++){//Loop on bipolar and amacrines voltages
      beta=j;//Bipolar
      Lalphabeta=gsl_matrix_get(p.cL,alpha,beta);
      dVGalpha+=Lalphabeta*R(X[beta],X[beta+3*p.L],p);//Rectified and gain control bipolar input
      //dVGalpha+=Lalphabeta*X[beta];//Non rectified bipolar input
 
      beta=p.L+j;//Amacrine
      Lalphabeta=gsl_matrix_get(p.cL,alpha,beta);
      dVGalpha+=Lalphabeta*NA(X[beta],p);//Rectified amacrine input
      //dVGalpha+=Lalphabeta*X[beta];//Non rectified amacrine input
    }//End of loop on bipolar and amacrine voltages
    Xp[alpha]=X[alpha]+dVGalpha*p.dt;
    
    //Updating BCells activities
    alpha=i+3*p.L;
    dVActalpha=-X[alpha]/p.taua+gsl_matrix_get(p.cL,alpha,i)*NB(X[i],p);
    Xp[alpha]=X[alpha]+dVActalpha*p.dt;
    //if (X[i]>0) printf("i=%d, G=%g, VB=%g, NB=%g, a=%g\n",i,G(X[alpha]),X[i],NB(X[i],p),X[alpha]);
  }//End of updating cells' voltage    
}

//Vector field, similar as above,  with an additional variable, the space location x , the point of space where GCells RFs are computed. Rectified without Gain control
void vector_field_Space_Rect(double x,double *X,double *Xp,double t,params p,double (*input)(int ic,double x,double t, params p)){//Vector field
  double Lalphabeta=0,dVBalpha=0,dVAalpha=0,dVGalpha=0,dVActalpha=0;
  int alpha=0,beta=0;
  //  double tauV=0;

  /*for (int i=0;i<p.L;i++){//Test to be removed
    alpha=i;
    if (X[alpha]<p.thetaB) {printf("\t\t\033[0;35mBC %d has voltage %g > thetaA=%g, rectified\n",beta,X[alpha],p.thetaA);} 
    alpha=i+p.L;
    if (X[alpha]<p.thetaA) {printf("\t\t\033[0;35mAC %d has voltage %g > thetaA=%g, rectified\n",beta,X[alpha],p.thetaA);;} 
    printf("\033[0m");
    }*/

  for (int i=0;i<p.L;i++){//Updating cells' voltage
    //Updating bipolar voltages
    alpha=i;
    dVBalpha=-X[alpha]/p.tauB+input(alpha,x,t,p)+p.KB;//Each BCell alpha receives the input of its spatio-temporal RF computed at spatial point x and time t
    // printf("input=%lg\n",input(i*p.dx,t,p));//exit(0);
    for (int j=0;j<p.L;j++){//Loop on amacrine voltages
      beta=p.L+j;
      Lalphabeta=gsl_matrix_get(p.cL,alpha,beta);
      dVBalpha+=Lalphabeta*NA(X[beta],p); //Rectified amacrine input         }
      }//End of loop on amacrine voltages
    Xp[alpha]=X[alpha]+dVBalpha*p.dt;
 
    //Updating amacrine voltages
    alpha=i+p.L;
    dVAalpha=-X[alpha]/p.tauA+p.zetaA+p.KA;
    for (int j=0;j<p.L;j++){//Loop on bipolar voltages
      beta=j;
      Lalphabeta=gsl_matrix_get(p.cL,alpha,beta);
      dVAalpha+=Lalphabeta*NB(X[beta],p);//Rectified and gain control bipolar input
    }//End of loop on bipolar voltages
    Xp[alpha]=X[alpha]+dVAalpha*p.dt;

    //Updating ganglion voltages
    alpha=i+2*p.L;
    dVGalpha=-X[alpha]/p.tauG+p.zetaG+p.KG;
    for (int j=0;j<p.L;j++){//Loop on bipolar and amacrines voltages
      beta=j;//Bipolar
      Lalphabeta=gsl_matrix_get(p.cL,alpha,beta);
      dVGalpha+=Lalphabeta*NB(X[beta],p);//Rectified and gain control bipolar input
      //dVGalpha+=Lalphabeta*X[beta];//Non rectified bipolar input
 
      beta=p.L+j;//Amacrine
      Lalphabeta=gsl_matrix_get(p.cL,alpha,beta);
      dVGalpha+=Lalphabeta*NA(X[beta],p);//Rectified amacrine input
      //dVGalpha+=Lalphabeta*X[beta];//Non rectified amacrine input
    }//End of loop on bipolar and amacrine voltages
    Xp[alpha]=X[alpha]+dVGalpha*p.dt;
    
  }//End of updating cells' voltage    
}

//Vector field, similar as above,  but without rectification,without gain control. 
void vector_field_Space_lin(double x,double *X,double *Xp,double t,params p,double (*input)(int ic,double x,double t, params p)){//Vector field
  double Lalphabeta=0,dVBalpha=0,dVAalpha=0,dVGalpha=0;
  int alpha=0,beta=0;
  //  double tauV=0;

  for (int i=0;i<p.L;i++){//Updating cells' voltage
    //Updating bipolar voltages
    alpha=i;
    dVBalpha=-X[alpha]/p.tauB+input(alpha,x,t,p)+p.KB;//Each BCell alpha receives the input of its spatio-temporal RF computed at spatial point x and time t
    // printf("input=%lg\n",input(i*p.dx,t,p));//exit(0);
    for (int j=0;j<p.L;j++){//Loop on amacrine voltages
      beta=p.L+j;
      Lalphabeta=gsl_matrix_get(p.cL,alpha,beta);
      //dVBalpha+=Lalphabeta*X[beta]; //Non-rectified amacrine input         }
      dVBalpha+=Lalphabeta*(X[beta]-p.thetaA); //Non-rectified amacrine input         }
      }//End of loop on amacrine voltages
    Xp[alpha]=X[alpha]+dVBalpha*p.dt;
 
    //Updating amacrine voltages
    alpha=i+p.L;
    dVAalpha=-X[alpha]/p.tauA+p.zetaA+p.KA;
    for (int j=0;j<p.L;j++){//Loop on bipolar voltages
      beta=j;
      Lalphabeta=gsl_matrix_get(p.cL,alpha,beta);
      //dVAalpha+=Lalphabeta*X[beta];//Non-rectified bipolar input
      dVAalpha+=Lalphabeta*(X[beta]-p.thetaB);//Non-rectified bipolar input
    }//End of loop on bipolar voltages
    Xp[alpha]=X[alpha]+dVAalpha*p.dt;

    //Updating ganglion voltages
    alpha=i+2*p.L;
    dVGalpha=-X[alpha]/p.tauG+p.zetaG+p.KG;
    for (int j=0;j<p.L;j++){//Loop on bipolar and amacrines voltages
      beta=j;//Bipolar
      Lalphabeta=gsl_matrix_get(p.cL,alpha,beta);
      //dVGalpha+=Lalphabeta*X[beta];//Non-rectified bipolar input
      dVGalpha+=Lalphabeta*(X[beta]-p.thetaB);//Non rectified bipolar input
 
      beta=p.L+j;//Amacrine
      Lalphabeta=gsl_matrix_get(p.cL,alpha,beta);
      //dVGalpha+=Lalphabeta*X[beta];//Non-rectified amacrine input
      dVGalpha+=Lalphabeta*(X[beta]-p.thetaA);//Non rectified amacrine input
    }//End of loop on bipolar and amacrine voltages
    Xp[alpha]=X[alpha]+dVGalpha*p.dt;
    
  }//End of updating cells' voltage    
}

//Computes the reference times tauBref,tauAref, tauGref, from the prescribed value of tauB, tauA, tauG. Used to initialise data from experiments.
//These times are computed for the center cell
//params contains the parameters and the prescribed value of  tauB, tauA, tauG.
//V* rest are the rest states (to be computed before)
void ComputeReferenceTimes(params*p,double VB_rest, double VA_rest,double VG_rest){
  //printf("The function ComputeReferenceTimes must be revisited\n");exit(0);
  double sum_AtoB=0;//Network effect of ACells to BCells characteristic time tauB
  double  sum_BtoA=p->lambdaA;//Network effect of BCells to ACells characteristic time tauA
  double sum_G=p->lambdaG;//Network effect of ACells and BCells on GCells characteristic time tauG
  
  int iGC=p->N/2;//Index of the GCell on the lattice
  double Lalphabeta=0;
  
  //Bipolar time
  int alpha=iGC,beta=0;
  for (int j=0;j<p->L;j++){//Loop on amacrine voltages
    beta=p->L+j;
    Lalphabeta=gsl_matrix_get(p->cL,alpha,beta);    
    sum_AtoB+=Lalphabeta*NA(VA_rest,*p)/EGlyc;//ACell to BCell synapse is Glycinergic
  }
  
  //Amacrine time
  alpha=iGC+p->L;
  for (int j=0;j<p->L;j++){//Loop on bipolar voltages
    beta=j;
    Lalphabeta=gsl_matrix_get(p->cL,alpha,beta);    
    sum_BtoA+=Lalphabeta*NB(VB_rest,*p)/EAMPA;//BCell to ACell synapse is Glutamate-AMPA,NMDA
  }
  
  //Ganglion time
  alpha=iGC+2*p->L;
  for (int j=0;j<p->L;j++){//Loop on bipolar voltages and amacrines
    beta=j;
    Lalphabeta=gsl_matrix_get(p->cL,alpha,beta);    
    sum_G+=Lalphabeta*NB(VB_rest,*p)/EAMPA;//BCell to GCell synapse is Glutamate-AMPA,NMDA
    beta=p->L+j;
    Lalphabeta=gsl_matrix_get(p->cL,alpha,beta);    
    sum_G+=Lalphabeta*NA(VA_rest,*p)/EGlyc;//ACell to GCell synapse is Glycinergic
  }
  
  //Updating tauBref
  sum_AtoB=1-p->tauB*sum_AtoB;//Network effect of ACells to BCells characteristic time tauB
  p->tauBref=p->tauB/sum_AtoB;
  
  //Updating tauAref
  sum_BtoA=1-p->tauA*sum_BtoA;//Network effect of BCells to ACells characteristic time tauA
  p->tauAref=p->tauA/sum_BtoA;
  
  //Updating tauGr
  sum_G=1-p->tauG*sum_G;//Network effect of ACells and BCells on GCells characteristic time tauG
  p->tauGref=p->tauG/sum_G;
}

//Computes the characteristic times as a function of weights for a fixed value of the rest state.
//These times are computed for the center cell. WARNING. Here one assumes therefore that the weights and rest states are uniform and that one neglects boundaries effects.
//params contains the parameters
//V* rest are the rest states (to be computed before)
void ComputeCharacteristicTimes_FixedRestState(params*p,double VB_rest, double VA_rest,double VG_rest){
  
  double sum_AtoB=1;//This variables computes the CNO and network effect of ACells to BCells characteristic time tauB. It is initialised at the value holding when there is no network effect. No CNO for BCells.
  double  sum_BtoA=1+p->tauL*p->zetaA/p->ECNOA;//Same for tauA
  double sum_G=1+p->tauL*p->zetaG/p->ECNOG;//Same for tauG
  
  int iGC=p->N/2;//Index of the GCell on the lattice
  double Lalphabeta=0;
  
  //Bipolar time
  int alpha=iGC,beta=0;
  for (int j=0;j<p->L;j++){//Loop on amacrine voltages
    beta=p->L+j;
    Lalphabeta=gsl_matrix_get(p->cL,alpha,beta);    
    sum_AtoB+=Lalphabeta*NA(VA_rest,*p)*p->tauL/EGlyc;//ACell to BCell synapse is Glycinergic
    //sum_AtoB+=Lalphabeta*(VA_rest-p->thetaA)*p->tauL/EGlyc;//Linear form. 
  }
  
  //Amacrine time
  alpha=iGC+p->L;
  for (int j=0;j<p->L;j++){//Loop on bipolar voltages
    beta=j;
    Lalphabeta=gsl_matrix_get(p->cL,alpha,beta);    
    sum_BtoA+=Lalphabeta*NB(VB_rest,*p)*p->tauL/EAMPA;//BCell to ACell synapse is Glutamate-AMPA,NMDA
    //sum_BtoA+=Lalphabeta*(VB_rest-p->thetaB)*p->tauL/EAMPA;//Linear form. 
  }
  
  //Ganglion time
  alpha=iGC+2*p->L;
  for (int j=0;j<p->L;j++){//Loop on bipolar voltages and amacrines
    beta=j;
    Lalphabeta=gsl_matrix_get(p->cL,alpha,beta);    
    sum_G+=Lalphabeta*NB(VB_rest,*p)*p->tauL/EAMPA;//BCell to GCell synapse is Glutamate-AMPA,NMDA
   // sum_G+=Lalphabeta*(VB_rest-p->thetaB)*p->tauL/EAMPA;//Linear form
    beta=p->L+j;
    Lalphabeta=gsl_matrix_get(p->cL,alpha,beta);    
    sum_G+=Lalphabeta*NA(VA_rest,*p)*p->tauL/EGlyc;//ACell to GCell synapse is Glycinergic
    //sum_G+=Lalphabeta*(VA_rest-p->thetaA)*p->tauL/EGlyc;//Linear form
  }
  
  //Updating tauB
  if (fabs(sum_AtoB)<1E-6) {printf("\t\tWarning tauB is going to become very large. Denominator=%g\n",sum_AtoB);}
  p->tauB=p->tauL/sum_AtoB;
  if (fabs(p->tauB)>1E-6) {p->unsurtauB=1.0/p->tauB;}else {printf("In ComputeCharacteristicTimes , tauB=%lg is too small, aborting\n",p->tauB);exit(0);}
  
  //Updating tauA
  if (fabs(sum_BtoA)<1E-6) {printf("\t\tWarning tauA is going to become very large. Denominator=%g\n",sum_BtoA);}  
  p->tauA=p->tauL/sum_BtoA;
  if (fabs(p->tauA)>1E-6) {p->unsurtauA=1.0/p->tauA;}else {printf("In ComputeCharacteristicTimes , tauA=%lg is too small, aborting\n",p->tauA);exit(0);}
  
  //Updating tauG
  if (fabs(sum_G)<1E-6) {printf("\t\tWarning tauG is going to become very large. Denominator=%g\n",sum_G);}  
  p->tauG=p->tauL/sum_G;
  if (fabs(p->tauG)>1E-6) {p->unsurtauG=1.0/p->tauG;}else {printf("In ComputeCharacteristicTimes , tauG=%lg is too small, aborting\n",p->tauG);exit(0);}
  
  //Updating other parameters depending on tauA, tauB, tauG
  //p->lambdaA=p->gCNOA/p->CA;
  //p->zetaA=p->lambdaA*p->ECNOA; //CNO influence for Amacrines (mV/ms)
  //p->lambdaG=p->gCNOG/p->CG;//Same effect on ACell and GCell
  //p->zetaG=p->lambdaG*p->ECNOG; //CNO influence for GCells (mV/ms)
  p->unsurtauAB=p->unsurtauA+p->unsurtauB;//1/tauA+1/tauB (kHz)
  p->unsurtau=p->unsurtauA-p->unsurtauB;//1/tauA-1/tauB (kHz)
  if ((fabs(p->unsurtau)>1E-10)) {
    p->tau=1.0/p->unsurtau;
  }
  else {
    printf("Case tau^-1=0 not implemented yet, exiting\n");
    exit(0);
  }
  p->tau2=pow(p->tau,2);
  p->tauAB=1.0/p->unsurtauAB;
  p->mu=-p->wab*p->wba*p->tau2;  
}

//Compute the rest state of the model for a fixed value of characteristic times
//p is the set of parameters
//f is the vector field
//X is the network rest state
//VB_rest, VA_rest, VG_rest are the rest state of BCells, ACells, GCells used later to compute characteristic times
//Arest is the rest activity for a model with gain control 
void ComputeRestState_FixedCharacteristicTimes(int iGC,params p,void (*f)(double *X,double *Xp,double t,params p,double (*input)(double x,double t, params p)),double *Xrest,double *VB_rest,double *VA_rest,double *VG_rest,double *A_rest){

  //printf("\tComputing the rest state\n");

  int dim_ext=4*p.L;// Dimension of the phase space

  double t=-1000;
  double *X=(double*)calloc(dim_ext,sizeof(double));//State vector time t+dt
  double *Xp=(double*)calloc(dim_ext,sizeof(double));//State vector time t+dt
  double* temp;//swap variable
  int alphaBC=iGC;//Index of the BCell above this GCell in the extended phase space
  int alphaAC=iGC+p.L;//Index of the ACell above this GCell in the extended phase space
  int alphaGC=iGC+2*p.L;//Index of the GCell in the extended phase space
  int alphaAc=iGC+3*p.L;//Index of the BCell activity in the extended phase space

  while (t<0){//Transient time loop (t is in ms) to reach the rest state 
    f(X,Xp,t,p,Bip_input_null);
    temp=X;
    X=Xp;
    Xp=temp;
    t+=p.dt;
  }//End of transient time loop
    
  *VB_rest=X[alphaBC]; //Bipolar rest voltage 
  *VA_rest=X[alphaAC]; //Amacrine rest voltage 
  *VG_rest=X[alphaGC]; //Ganglion rest voltage 
  *A_rest=X[alphaAc]; //Ganglion rest voltage 

  for (int i=0;i<dim_ext;i++) Xrest[i]=X[i];

  free(X);free(Xp);

 // printf("\t\t\tdone\n");
}

//Checks the linear stability 
bool IsStable(params p){

  bool stable=true;
  
  int dim_ext=3*p.N;
  gsl_complex lambda={0,0};
  for (int n=0;((n<dim_ext)&&stable);n++){
    lambda=gsl_vector_complex_get(p.lambda_th,n);
    if (GSL_REAL(lambda)>0) stable=false;
  }

  return stable;
}


//Computes the rest state characteristics, Vrest and times
//p is the set of parameters
//f is the vector field
//X is the network rest state
//VB_rest, VA_rest, VG_rest are the rest state of BCells, ACells, GCells used later to compute characteristic times
//Arest is the rest activity for a model with gain control 
void ComputeRestState_Full(int iGC,params *p,void (*f)(double *X,double *Xp,double t,params p,double (*input)(double x,double t, params p)),double *Xrest,double *VB_rest,double *VA_rest,double *VG_rest,double *A_rest){
  //printf("\n\t\t\tComputing the rest state\n");

  int dim_ext=4*p->L;// Dimension of the phase space
  double *X=(double*)calloc(dim_ext,sizeof(double));//State vector time t: Variables 0... N-1 are bipolar voltages,Variables N... 2N-1 are amacrine voltages, Variables 2N... 3N-1 are ganglion voltages

  // Iteration loop to compute Vrest and characteristic times 
  *VB_rest=0;*VA_rest=0;*VG_rest=0;*A_rest=0;
  double VBp_rest=100,VAp_rest=100,VGp_rest=100,Ap_rest=100;
  double tauBp=100,tauAp=100,tauGp=100;
  double dist=100;
  int n_it=1;
  init_Network_Laplacian(p,1);   
	
  while ((dist >1E-15)&&(n_it<=100)){//Convergence to the rest state with a non linear dependence to characteristic times
    //printf("\t\tit=%d\n",n_it);
    ComputeRestState_FixedCharacteristicTimes(iGC,*p,f,X,VB_rest,VA_rest,VG_rest,A_rest);
    //printf("\t\t\tRest state: VB_rest=%lg, VA_rest=%lg, VG_rest=%lg, Characteristice times; tauB=%lg, tauA=%lg, tauG=%lg\n", *VB_rest, *VA_rest,*VG_rest,p->tauB,p->tauA,p->tauG);
    ComputeCharacteristicTimes_FixedRestState(p,*VB_rest, *VA_rest,*VG_rest);
    //printf("\t\t\tRest state: VB_rest=%lg, VA_rest=%lg, VG_rest=%lg, Characteristice times; tauB=%lg, tauA=%lg, tauG=%lg\n", *VB_rest, *VA_rest,*VG_rest,p->tauB,p->tauA,p->tauG);

    dist=sqrt(pow(*VB_rest-VBp_rest,2)+pow(*VA_rest-VAp_rest,2)+pow(*VG_rest-VGp_rest,2)+pow(p->tauB-tauBp,2)+pow(p->tauA-tauAp,2)+pow(p->tauG-tauGp,2));
    VBp_rest=*VB_rest;VAp_rest=*VA_rest;VGp_rest=*VG_rest;
    tauBp=p->tauB;
    tauAp=p->tauA;
    tauGp=p->tauG;
    init_Network_Laplacian(p,1);      //Reinitialising eigenvalues and eigenvectors  
    n_it++;
  }//End of convergence to the rest state with a non linear dependence to characteristic times

  //printf("\t\t\tn_it=%d, dist=%lg\n",n_it,dist);
  free(X);
}
