/** Utilities functions **/

#include "Utils.h"
#include "Dynamics.h"

/*** Parameters section ***/
//Save the parameters of p in SaveFile
void SaveParams(params p,std::string SaveFile){
  FILE *fp=fopen(SaveFile.c_str(),"w");
  if (fp==NULL) {printf("The file %s could not be open\n",SaveFile.c_str());exit(0);}

  printf("\t\tSaving parameters in the file %s\n",SaveFile.c_str());
  //Lattice
  fprintf(fp,"%d\n",p.N); //Number of visible neurons
  fprintf(fp,"%d\n",p.offset);  //Number of invisible neurons (on the left of the neuron 0 and on the right to neuron N-1) to avoid boundaries effects
  fprintf(fp,"%d\n",p.L); //Total number of neurons
  fprintf(fp,"%lg\n",p.dt); //time step (ms)
  fprintf(fp,"%lg\n",p.dx); //Cells spacing (mm) in abscissa and ordinate
  fprintf(fp,"%lg\n",p.dy); //Cells spacing (mm) in abscissa and ordinate

  //Cells
  fprintf(fp,"%lg\n",p.tauL);//Leak time (ms)

  //Bipolars
  fprintf(fp,"%lg\n",p.thetaB);//Threshold for bipolar rectification (mV) From Berry 1999
  fprintf(fp,"%lg\n",p.tauBref);//Baseline Bipolar voltage integration characteristic time (ms)
  fprintf(fp,"%lg\n",p.tauB);//Bipolar voltage integration characteristic time (ms)
  fprintf(fp,"%lg\n",p.unsurtauB);//1/tauB (kHz)
  fprintf(fp,"%lg\n",p.CB); //Bipolar membrane capacity (pF)
  fprintf(fp,"%lg\n",p.gLB); //Bipolar leak conductance (nS) 
  fprintf(fp,"%lg\n",p.ELB); //Bipolar leak potential (mV) 
  fprintf(fp,"%lg\n",p.KB); //Constant fixed by the leak potential (mV/ms)
  //Gain control
  fprintf(fp,"%lg\n",p.h);//Activity rate
  fprintf(fp,"%lg\n",p.taua);//Bipolar activity characteristic time (ms)
  
  //Time RF of BCells(second order kernel)
  fprintf(fp,"%lg\n",p.A0); //Amplitude of the BCell response to a pulse - alpha profile (mV)
  fprintf(fp,"%lg\n",p.t0); //Starting time of the alpha profile (ms)
  fprintf(fp,"%lg\n",p.tauRF); //Characteristic time of the BCell RF (ms)
  fprintf(fp,"%lg\n",p.tauRF3); //Characteristic time of the RF to the power 3 used in the computation of RF (ms^3)
  fprintf(fp,"%lg\n",p.b0);  //Constant value in  the BCell response to a pulse (mV)
  
  //Spatial RF of BCells
  fprintf(fp,"%lg\n",p.Ac);//Amplitude of the spatial RF center (mV.mm)
  fprintf(fp,"%lg\n",p.sigma_c);//Variance of  the spatial RF center (mm)
  fprintf(fp,"%lg\n",p.As);//Amplitude of  the spatial RF surround (mV.mm)
  fprintf(fp,"%lg\n",p.sigma_s);//Variance of  the spatial RF surround (mm)
  
  //Amacrines
  fprintf(fp,"%lg\n",p.thetaA);//Threshold for amacrine rectification (mV). From Berry 1999
  fprintf(fp,"%lg\n",p.tauAref);//Baseline amacrine characteristic integration time (ms)
  fprintf(fp,"%lg\n",p.tauA);//Amacrine characteristic integration time in the presence of CNO (ms)
  fprintf(fp,"%lg\n",p.lambdaA); //Amacrine CNO  ratio g/C (kHz)
  fprintf(fp,"%lg\n",p.ECNOA); //Amacrine CNO reversal potential (mV)
  fprintf(fp,"%lg\n",p.gCNOA); //Amacrine CNO conductance (nS)
  fprintf(fp,"%lg\n",p.zetaA); //CNO influence for ACells (mV/ms)
  fprintf(fp,"%lg\n",p.unsurtauA);//1/tauA (kHz)
  fprintf(fp,"%lg\n",p.gLA); //Amacrine leak conductance (nS) 
  fprintf(fp,"%lg\n",p.CA); //Amacrine membrane capacity (pF)
  fprintf(fp,"%lg\n",p.ELA); //Amacrine leak potential (mV) 
  fprintf(fp,"%lg\n",p.KA); //Constant fixed by the leak potential (mV/ms)
  
  //Ganglions
  fprintf(fp,"%lg\n",p.lambdaG); //Amacrine CNO  ratio g/C (kHz)
  fprintf(fp,"%lg\n",p.ECNOG); //GCell CNO reversal potential (mV)
  fprintf(fp,"%lg\n",p.gCNOG); //GCell CNO conductance (nS)
  fprintf(fp,"%lg\n",p.zetaG); //CNO influence for GCells (mV/ms)
  fprintf(fp,"%lg\n",p.tauGref);//Baseline ganglion characteristic integration time (ms)
  fprintf(fp,"%lg\n",p.tauG);//Ganglion characteristic integration time in the presence of CNO (ms)
  fprintf(fp,"%lg\n",p.unsurtauG);//1/tauG (kHz)
  fprintf(fp,"%lg\n",p.gLG); //Ganglion leak conductance (nS) 
  fprintf(fp,"%lg\n",p.CG); //Ganglion membrane capacity (pF)
  fprintf(fp,"%lg\n",p.ELG); //Ganglion leak potential (mV) 
  fprintf(fp,"%lg\n",p.KG); //Constant fixed by the leak potential (mV/ms)
  
  //Connections
  fprintf(fp,"%d\n",p.BtoA);//If 0 one BCell connects a unique ACell, if 1 WAB=WBA
  fprintf(fp,"%d\n",p.IsPooling); //Pooling if true
  fprintf(fp,"%lg\n",p.wba); //Bipolar Amacrine  weight (kHz)
  fprintf(fp,"%lg\n",p.wab); //Amacrine Bipolar weight (kHz)
  fprintf(fp,"%lg\n",p.wbg); //Bipolar Ganglion weight in the Gaussian pooling (kHz)
  fprintf(fp,"%lg\n",p.wag); //Amacrine Ganglion weight (kHz)
  fprintf(fp,"%lg\n",p.sigmabg2);//Bipolar Ganglion variance in the Gaussian pooling (mm^2)
  
  //Global
  fprintf(fp,"%lg\n",p.unsurtau);//1/tauA-1/tauB (kHz)
  fprintf(fp,"%lg\n",p.unsurtauAB);//1/tauA+1/tauB (kHz)
  fprintf(fp,"%lg\n",p.tau); //1/unsurtau (ms)
  fprintf(fp,"%lg\n",p.tau2);//tau**2 (ms^2)
  fprintf(fp,"%lg\n",p.tauAB);//1/(unsurtauA+unsurtauB) (ms)
  fprintf(fp,"%lg\n",p.mu);//Critical value for instability (dimensionless)
  
  //Pulse
  fprintf(fp,"%lg\n",p.AmpStim); //Amplitude of the pulse (mV)
  fprintf(fp,"%lg\n",p.speed); //Pulse' speed (mm/ms)
  fprintf(fp,"%lg\n",p.strength); //Strength of the stimulus
  fprintf(fp,"%lg\n",p.mean); //Pulse peak location (mm)
  fprintf(fp,"%lg\n",p.sigma); //Pulse width (mm)
  fprintf(fp,"%lg\n",p.tmin);//-1;//initial time (ms)
  fprintf(fp,"%lg\n",p.tmax); //Final time (ms)
  fprintf(fp,"%lg\n",p.T); //Period of a periodic forcing
  fprintf(fp,"%lg\n",p.omega); //Frequency of a periodic forcing
  fprintf(fp,"%d\n",p.NPer); //Number of periods in the periodic forcing
  
  //Gnuplot variables
  fprintf(fp,"%d\n",p.nbviewmax); //Maximum number of views in the gnuplot file
  fprintf(fp,"%d\n",p.countmax); //Maximum count between time steps of views
  fprintf(fp,"%lg\n",p.wc); //Critical weight (kHz)
  fprintf(fp,"%lg\n",p.VBmin);//Bipolar voltage min (mV)
  fprintf(fp,"%lg\n",p.VBmax);//Bipolar voltage max (mV)
  fprintf(fp,"%lg\n",p.VAmin);//Amacrine voltage min (mV)
  fprintf(fp,"%lg\n",p.VAmax);//Amacrine voltage max (mV)
  fprintf(fp,"%lg\n",p.VGmin);//Ganglion voltage min (mV)
  fprintf(fp,"%lg\n",p.VGmax);//Ganglion voltage max (mV)
  
  fclose(fp);
}

//Write the parameters of p on the screen
void WriteParams(params p){
  
  printf("\n\t************\n\tparameters\n\t************\n\n");

  //Lattice
  printf("\n\t\tLattice\n");
  printf("\tN=%d\n",p.N); //Number of visible neurons
  printf("\toffset=%d\n",p.offset);  //Number of invisible neurons (on the left of the neuron 0 and on the right to neuron N-1) to avoid boundaries effects
  printf("\tL=%d\n",p.L); //Total number of neurons
  printf("\tdt=%lg ms\n",p.dt); //time step (ms)
  printf("\tdx=%lg mm\n",p.dx); //Cells spacing (mm) in abscissa and ordinate
  printf("\tdy=%lg mm\n",p.dy); //Cells spacing (mm) in abscissa and ordinate

  //Cells  
  printf("\t\n\t\tCells\n");
  printf("\tLeak=%lg ms\n",p.tauL);//Leak time
  printf("\n\n\t\tBipolars\n");

  //Bipolar
  printf("\tthetaB=%lg mV\n",p.thetaB);//Threshold for bipolar rectification (mV) From Berry 1999
  printf("\ttauBref=%lg ms\n",p.tauBref);//Baseline Bipolar voltage integration characteristic time (ms)
  printf("\ttauB=%lg ms\n",p.tauB);//Bipolar voltage integration characteristic time (ms)
  printf("\tunsurtauB=%lg kHz\n",p.unsurtauB);//1/tauB (kHz)
  printf("\tCB=%lg pF\n",p.CB); //Bipolar membrane capacity (pF)
  printf("\tgLB=%lg nS\n",p.gLB); //Bipolar leak conductance (nS) 
  printf("\tELB=%lg mV\n",p.ELB); //Bipolar leak potential (mV) 
  printf("\tKB=%lg mV/ms\n",p.KB); //Constant fixed by the leak potential (mV/ms)

  //Gain control
  printf("\t\n\t\tGain control\n");
  printf("\th=%lg\n",p.h);//Activity rate
  printf("\ttaua=%lg ms\n",p.taua);//Bipolar activity characteristic time (ms)
  
  //Time RF of BCells(second order kernel)
  printf("\t\n\t\tTime RF of BCells(second order kernel)\n");
  printf("\tA0=%lg mV\n",p.A0); //Amplitude of the BCell response to a pulse - alpha profile (mV)
  printf("\tt0=%lg ms\n",p.t0); //Starting time of the alpha profile (ms)
  printf("\ttauRF=%lg ms\n",p.tauRF); //Characteristic time of the BCell RF (ms)
  printf("\ttauRF3=%lg ms^3\n",p.tauRF3); //Characteristic time of the RF to the power 3 used in the computation of RF (ms^3)
  printf("\tb0=%lg mV\n",p.b0);  //Constant value in  the BCell response to a pulse (mV)
  
  //Spatial RF of BCells
  printf("\t\n\t\tSpatial RF of BCells\n");
  printf("\tAc=%lg mV.mm\n",p.Ac);//Amplitude of the spatial RF center (mV.mm)
  printf("\tsigmac=%lg mm\n",p.sigma_c);//Variance of  the spatial RF center (mm)
  printf("\tAs=%lg mV.mm\n",p.As);//Amplitude of  the spatial RF surround (mV.mm)
  printf("\tsigmas=%lg mm\n",p.sigma_s);//Variance of  the spatial RF surround (mm)
  
  //Amacrines
  printf("\t\n\t\tAmacrines\n");
  printf("\tthetaA=%lg mV\n",p.thetaA);//Threshold for amacrine rectification (mV). From Berry 1999
  printf("\ttauAref=%lg ms\n",p.tauAref);//Baseline amacrine characteristic integration time (ms)
  printf("\ttauA=%lg ms\n",p.tauA);//Amacrine characteristic integration time in the presence of CNO (ms)
  printf("\tlambadA=%lg kHz\n",p.lambdaA); //Amacrine CNO  ratio g/C (kHz)
  printf("\tECNOA=%lg lV\n",p.ECNOA); //Amacrine CNO reversal potential (mV)
  printf("\tgCNOA=%lg nS\n",p.gCNOA); //Amacrine CNO conductance (nS)
  printf("\tzetaA=%lg mV/ms\n",p.zetaA); //CNO influence for ACells (mV/ms)
  printf("\tunsurtauA=%lg kHz\n",p.unsurtauA);//1/tauA (kHz)
  printf("\tgLA=%lg nS\n",p.gLA); //Amacrine leak conductance (nS) 
  printf("\tCA=%lg pF\n",p.CA); //Amacrine membrane capacity (pF)
  printf("\tELA=%lg mV\n",p.ELA); //Amacrine leak potential (mV) 
  printf("\tKA=%lg mV/ms\n",p.KA); //Constant fixed by the leak potential (mV/ms)
  
  //Ganglions
  printf("\t\n\t\tGanglions\n");
  printf("\tlambdaG=%lg kHz\n",p.lambdaG); //Amacrine CNO  ratio g/C (kHz)
  printf("\tECNOG=%lg mV\n",p.ECNOG); //GCell CNO reversal potential (mV)
  printf("\tgCNOG=%lg nS\n",p.gCNOG); //GCell CNO conductance (nS)
  printf("\tzetaG%lg mV/ms\n",p.zetaG); //CNO influence for GCells (mV/ms)
  printf("\ttauGref=%lg ms\n",p.tauGref);//Baseline ganglion characteristic integration time (ms)
  printf("\ttauG=%lg ms\n",p.tauG);//Ganglion characteristic integration time in the presence of CNO (ms)
  printf("\tunsurtauG=%lg kHz\n",p.unsurtauG);//1/tauG (kHz)
  printf("\tgLG=%lg nS\n",p.gLG); //Ganglion leak conductance (nS) 
  printf("\tCG=%lg pF\n",p.CG); //Ganglion membrane capacity (pF)
  printf("\tELG=%lg mV\n",p.ELG); //Ganglion leak potential (mV) 
  printf("\tKG=%lg mV/ms\n",p.KG); //Constant fixed by the leak potential (mV/ms)
  
  //Connections
  printf("\t\n\t\tConnections\n");
  printf("\tBtoA=%d\n",p.BtoA);//If 0 one BCell connects a unique ACell, if 1 WAB=WBA
  printf("\tIsPooling=%d\n",p.IsPooling); //Pooling if true
  printf("\twba=%lg kHz\n",p.wba); //Bipolar Amacrine  weight (kHz)
  printf("\twab=%lg kHz\n",p.wab); //Amacrine Bipolar weight (kHz)
  printf("\twbg=%lg kHz\n",p.wbg); //Bipolar Ganglion weight in the Gaussian pooling (kHz)
  printf("\twag=%lg kHz\n",p.wag); //Amacrine Ganglion weight (kHz)
  printf("\tsigmabg2=%lg mm^2\n",p.sigmabg2);//Bipolar Ganglion variance in the Gaussian pooling (mm^2)
  
  //Global
  printf("\t\n\t\tGlobal\n");
  printf("\tunsurtau=%lg kHz\n",p.unsurtau);//1/tauA-1/tauB (kHz)
  printf("\tunsurtauAB=%lg kHz\n",p.unsurtauAB);//1/tauA+1/tauB (kHz)
  printf("\ttau=%lg ms\n",p.tau); //1/unsurtau (ms)
  printf("\ttau2=%lg ms^2\n",p.tau2);//tau**2 (ms^2)
  printf("\ttauAB=%lg ms\n",p.tauAB);//1/(unsurtauA+unsurtauB) (ms)
  printf("\tmu=%lg\n",p.mu);//Critical value for instability (dimensionless)
  
  //Pulse
  printf("\t\n\t\tPulse\n");
  printf("\tAmpStim=%lg mV\n",p.AmpStim); //Amplitude of the pulse (mV)
  printf("\tspeed=%lg mm/ms\n",p.speed); //Pulse' speed (mm/ms)
  printf("\tstrength=%lg\n",p.strength); //Strength of the stimulus
  printf("\tPeak=%lg mm\n",p.mean); //Pulse peak location (mm)
  printf("\tWidth=%lg mm\n",p.sigma); //Pulse width (mm)
  printf("\ttmin=%lg ms\n",p.tmin);//-1;//initial time (ms)
  printf("\ttmax=%lg ms\n",p.tmax); //Final time (ms)
  printf("\tT=%lg ms\n",p.T); //Period of a periodic forcing
  printf("\tomega=%lg  kHz\n",p.omega); //Frequency of a periodic forcing
  printf("\tNPer=%d\n",p.NPer); //Number of periods in the periodic forcing
  
  //Gnuplot variables
  printf("\t\n\t\tGnuplot variables\n");
  printf("\tnbviewmax=%d\n",p.nbviewmax); //Maximum number of views in the gnuplot file
  printf("\tcountmax=%d\n",p.countmax); //Maximum count between time steps of views
  printf("\twc=%lg\n",p.wc); //Critical weight (kHz)
  printf("\tVBmin=%lg\n",p.VBmin);//Bipolar voltage min (mV)
  printf("\tVBmax=%lg\n",p.VBmax);//Bipolar voltage max (mV)
  printf("\tVAmin=%lg\n",p.VAmin);//Amacrine voltage min (mV)
  printf("\tVAmax=%lg\n",p.VAmax);//Amacrine voltage max (mV)
  printf("\tVGmin=%lg\n",p.VGmin);//Ganglion voltage min (mV)
  printf("\tVGmax=%lg\n",p.VGmax);//Ganglion voltage max (mV) 
}

//Read the parameters from SaveFile and put them in p. Return false if the file couldn't be opened. 
bool ReadParams(params *p,std::string SaveFile){
  FILE *fp=fopen(SaveFile.c_str(),"r");
  if (fp==NULL) {printf("The file %s could not be open\n",SaveFile.c_str());return false;}

  //Lattice
  fscanf(fp,"%d\n",&(p->N)); //Number of visible neurons
  fscanf(fp,"%d\n",&(p->offset));  //Number of invisible neurons (on the left of the neuron 0 and on the right to neuron N-1) to avoid boundaries effects
  fscanf(fp,"%d\n",&(p->L)); //Total number of neurons
  fscanf(fp,"%lg\n",&(p->dt)); //time step (ms)
  fscanf(fp,"%lg\n",&(p->dx)); //Cells spacing (mm) in abscissa and ordinate
  fscanf(fp,"%lg\n",&(p->dy)); //Cells spacing (mm) in abscissa and ordinate
 
  //Cells
  fscanf(fp,"%lg\n",&(p->tauL));//Leak time

  //Bipolars
  fscanf(fp,"%lg\n",&(p->thetaB));//Threshold for bipolar rectification (mV) From Berry 1999
  fscanf(fp,"%lg\n",&(p->tauBref));//Baseline Bipolar voltage integration characteristic time (ms)
  fscanf(fp,"%lg\n",&(p->tauB));//Bipolar voltage integration characteristic time (ms)
  fscanf(fp,"%lg\n",&(p->unsurtauB));//1/tauB (kHz)
  fscanf(fp,"%lg\n",&(p->CB)); //Bipolar membrane capacity (pF)
  fscanf(fp,"%lg\n",&(p->gLB)); //Bipolar leak conductance (nS) 
  fscanf(fp,"%lg\n",&(p->ELB)); //Bipolar leak potential (mV) 
  fscanf(fp,"%lg\n",&(p->KB)); //Constant fixed by the leak potential (mV/ms)
  //Gain control
  fscanf(fp,"%lg\n",&(p->h));//Activity rate
  fscanf(fp,"%lg\n",&(p->taua));//Bipolar activity characteristic time (ms)
  
  //Time RF of BCells(second order kernel)
  fscanf(fp,"%lg\n",&(p->A0)); //Amplitude of the BCell response to a pulse - alpha profile (mV)
  fscanf(fp,"%lg\n",&(p->t0)); //Starting time of the alpha profile (ms)
  fscanf(fp,"%lg\n",&(p->tauRF)); //Characteristic time of the BCell RF (ms)
  fscanf(fp,"%lg\n",&(p->tauRF3)); //Characteristic time of the RF to the power 3 used in the computation of RF (ms^3)
  fscanf(fp,"%lg\n",&(p->b0));  //Constant value in  the BCell response to a pulse (mV)
  
  //Spatial RF of BCells
  fscanf(fp,"%lg\n",&(p->Ac));//Amplitude of the spatial RF center (mV.mm)
  fscanf(fp,"%lg\n",&(p->sigma_c));//Variance of  the spatial RF center (mm)
  fscanf(fp,"%lg\n",&(p->As));//Amplitude of  the spatial RF surround (mV.mm)
  fscanf(fp,"%lg\n",&(p->sigma_s));//Variance of  the spatial RF surround (mm)
  
  //Amacrines
  fscanf(fp,"%lg\n",&(p->thetaA));//Threshold for amacrine rectification (mV). From Berry 1999
  fscanf(fp,"%lg\n",&(p->tauAref));//Baseline amacrine characteristic integration time (ms)
  fscanf(fp,"%lg\n",&(p->tauA));//Amacrine characteristic integration time in the presence of CNO (ms)
  fscanf(fp,"%lg\n",&(p->lambdaA)); //Amacrine CNO  ratio g/C (kHz)
  fscanf(fp,"%lg\n",&(p->ECNOA)); //Amacrine CNO reversal potential (mV)
  fscanf(fp,"%lg\n",&(p->gCNOA)); //Amacrine CNO conductance (nS)
  fscanf(fp,"%lg\n",&(p->zetaA)); //CNO influence for ACells (mV/ms)
  fscanf(fp,"%lg\n",&(p->unsurtauA));//1/tauA (kHz)
  fscanf(fp,"%lg\n",&(p->gLA)); //Amacrine leak conductance (nS) 
  fscanf(fp,"%lg\n",&(p->CA)); //Amacrine membrane capacity (pF)
  fscanf(fp,"%lg\n",&(p->ELA)); //Amacrine leak potential (mV) 
  fscanf(fp,"%lg\n",&(p->KA)); //Constant fixed by the leak potential (mV/ms)
  
  //Ganglions
  fscanf(fp,"%lg\n",&(p->lambdaG)); //Amacrine CNO  ratio g/C (kHz)
  fscanf(fp,"%lg\n",&(p->ECNOG)); //GCell CNO reversal potential (mV)
  fscanf(fp,"%lg\n",&(p->gCNOG)); //GCell CNO conductance (nS)
  fscanf(fp,"%lg\n",&(p->zetaG)); //CNO influence for GCells (mV/ms)
  fscanf(fp,"%lg\n",&(p->tauGref));//Baseline ganglion characteristic integration time (ms)
  fscanf(fp,"%lg\n",&(p->tauG));//Ganglion characteristic integration time in the presence of CNO (ms)
  fscanf(fp,"%lg\n",&(p->unsurtauG));//1/tauG (kHz)
  fscanf(fp,"%lg\n",&(p->gLG)); //Ganglion leak conductance (nS) 
  fscanf(fp,"%lg\n",&(p->CG)); //Ganglion membrane capacity (pF)
  fscanf(fp,"%lg\n",&(p->ELG)); //Ganglion leak potential (mV) 
  fscanf(fp,"%lg\n",&(p->KG)); //Constant fixed by the leak potential (mV/ms)
  
  //Connections
  fscanf(fp,"%d\n",&(p->BtoA));//If 0 one BCell connects a unique ACell, if 1 WAB=WBA
  fscanf(fp,"%d\n",&(p->IsPooling)); //Pooling if true
  fscanf(fp,"%lg\n",&(p->wba)); //Bipolar Amacrine  weight (kHz)
  fscanf(fp,"%lg\n",&(p->wab)); //Amacrine Bipolar weight (kHz)
  fscanf(fp,"%lg\n",&(p->wbg)); //Bipolar Ganglion weight in the Gaussian pooling (kHz)
  fscanf(fp,"%lg\n",&(p->wag)); //Amacrine Ganglion weight (kHz)
  fscanf(fp,"%lg\n",&(p->sigmabg2));//Bipolar Ganglion variance in the Gaussian pooling (mm^2)
  
  //Global
  fscanf(fp,"%lg\n",&(p->unsurtau));//1/tauA-1/tauB (kHz)
  fscanf(fp,"%lg\n",&(p->unsurtauAB));//1/tauA+1/tauB (kHz)
  fscanf(fp,"%lg\n",&(p->tau)); //1/unsurtau (ms)
  fscanf(fp,"%lg\n",&(p->tau2));//tau**2 (ms^2)
  fscanf(fp,"%lg\n",&(p->tauAB));//1/(unsurtauA+unsurtauB) (ms)
  fscanf(fp,"%lg\n",&(p->mu));//Critical value for instability (dimensionless)
  
  //Pulse
  fscanf(fp,"%lg\n",&(p->AmpStim)); //Amplitude of the pulse (mV)
  fscanf(fp,"%lg\n",&(p->speed)); //Pulse' speed (mm/ms)
  fscanf(fp,"%lg\n",&(p->strength)); //Strength of the stimulus
  fscanf(fp,"%lg\n",&(p->mean)); //Pulse peak location (mm)
  fscanf(fp,"%lg\n",&(p->sigma)); //Pulse width (mm)
  fscanf(fp,"%lg\n",&(p->tmin));//-1;//initial time (ms)
  fscanf(fp,"%lg\n",&(p->tmax)); //Final time (ms)
  fscanf(fp,"%lg\n",&(p->T)); //Period of a periodic forcing
  fscanf(fp,"%lg\n",&(p->omega)); //Frequency of a periodic forcing
  fscanf(fp,"%d\n",&(p->NPer)); //Number of periods in the periodic forcing
  
  //Gnuplot variables
  fscanf(fp,"%d\n",&(p->nbviewmax)); //Maximum number of views in the gnuplot file
  fscanf(fp,"%d\n",&(p->countmax)); //Maximum count between time steps of views
  fscanf(fp,"%lg\n",&(p->wc)); //Critical weight (kHz)
  fscanf(fp,"%lg\n",&(p->VBmin));//Bipolar voltage min (mV)
  fscanf(fp,"%lg\n",&(p->VBmax));//Bipolar voltage max (mV)
  fscanf(fp,"%lg\n",&(p->VAmin));//Amacrine voltage min (mV)
  fscanf(fp,"%lg\n",&(p->VAmax));//Amacrine voltage max (mV)
  fscanf(fp,"%lg\n",&(p->VGmin));//Ganglion voltage min (mV)
  fscanf(fp,"%lg\n",&(p->VGmax));//Ganglion voltage max (mV)
  
 fclose(fp);

 p->unsurtau=p->unsurtauA-p->unsurtauB;
 p->unsurtauAB=p->unsurtauA+p->unsurtauB;
 
 if ((fabs(p->unsurtau)>1E-10)) p->tau=1.0/p->unsurtau;//printf("tau=%lg\n",p->tau);//exit(0);
 p->tau2=pow(p->tau,2);
 p->tauAB=1.0/p->unsurtauAB;//printf("tauAB=%lg\n",p->tauAB);//exit(0);
 p->mu=-p->wab*p->wba*p->tau2;//printf("mu=%lg\n",p->mu);//exit(0);

 return true;
}

void CopyParams(params source, params *target){//Copy the data in source to target. Note that pointers in target are not allocated. They are to be allocated by Init_Network_Laplacian 

  //Lattice
  target->N=source.N; //Number of visible neurons
  target->offset=source.offset; //Number of invisible neurons (on the left of the neuron source. and on the right to neuron N-1) to avoid boundaries effects
  target->L=source.L; //Total number of neurons
  target->dt=source.dt; //time step (ms)
  target->dx=source.dx,target->dy=source.dy; //Cells spacing (mm) in abscissa and ordinate
  
  //Cells
  target->tauL=source.tauL; //Leak

  //Bipolars
  target->thetaB=source.thetaB;//Threshold for bipolar rectification (mV).
  target->tauBref=source.tauBref;//Baseline bipolar voltage integration characteristic time (ms). This is the leak characteristic time.
  target->tauB=source.tauB;//Bipolar voltage integration characteristic time with membrane depolarisation (ms). 
  target->unsurtauB=source.unsurtauB;//1/tauB (kHz)
  target->CB=source.CB; //Bipolar membrane capacity (pF)
  target->gLB=source.gLB; //Bipolar leak conductance (nS) 
  target->ELB=source.ELB; //Bipolar leak potential (mV) 
  target->KB=source.KB; //Constant fixed by the leak potential (mV/ms)
  //Gain control
  target->h=source.h;//Activity rate
  target->taua=source.taua;//Bipolar activity characteristic time (ms)

  //Time RF of BCells(second order kernel)
  target->A0=source.A0; //Amplitude of the pulse - alpha profile (mV)
  target->t0=source.t0; //Starting time of the alpha profile (ms)
  target->tauRF=source.tauRF; //Characteristic time of the BCell RF (ms)
  target->tauRF3=source.tauRF3; //Characteristic time of the RF to the power 3 used in the computation of RF (ms^3)
  target->b0=source.b0;  //Constant value in  the BCell response to a pulse (mV)

  //Spatial RF of BCells
  target->Ac=source.Ac;//Amplitude of the spatial RF center (mV.mm)
  target->sigma_c=source.sigma_c;//Variance of  the spatial RF center (mm)
  target->As=source.As;//Amplitude of  the spatial RF surround (mV.mm)
  target->sigma_s=source.sigma_s;//Variance of  the spatial RF surround (mm)
  
  //Amacrines
  target->thetaA=source.thetaA;//Threshold for amacrine rectification (mV). 
  target->tauAref=source.tauAref;//Baseline amacrine characteristic integration time (ms). This is the leak characteristic time.
  target->lambdaA=source.lambdaA; //Amacrine CNO  ratio g/C (kHz)
  target->ECNOA=source.ECNOA; //Amacrine CNO reversal potential (mV) 
  target->gCNOA=source.gCNOA; //Amacrine CNO conductance (nS)
  target->zetaA=source.zetaA; //CNO influence for ACells (mV/ms)
  target->tauA=source.tauA;//Amacrine characteristic integration time in the presence of CNO and membrane polarisation (ms)
  target->unsurtauA=source.unsurtauA;//1/tauA (kHz)
  target->gLA=source.gLA; //Amacrine leak conductance (nS) 
  target->CA=source.CA; //Amacrine membrane capacity (pF)
  target->ELA=source.ELA; //Amacrine leak potential (mV) 
  target->KA=source.KA; //Constant fixed by the leak potential (mV/ms)

  //Ganglions
  target->lambdaG=source.lambdaG; //Amacrine CNO  ratio g/C (kHz)
  target->ECNOG=source.ECNOG; //GCell CNO reversal potential (mV)
  target->gCNOG=source.gCNOG; //GCell CNO conductance (nS)
  target->zetaG=source.zetaG; //CNO influence for GCells (mV/ms)
  target->tauGref=source.tauGref;//Baseline ganglion characteristic integration time (ms). This is the leak characteristic time/
  target->tauG=source.tauG;//Ganglion characteristic integration time in the presence of CNO and membrane polarisation (ms)
  target->unsurtauG=source.unsurtauG;//1/tauG (kHz)
  target->gLG=source.gLG; //Ganglion leak conductance (nS) 
  target->CG=source.CG; //Ganglion membrane capacity (pF)
  target->ELG=source.ELG; //Ganglion leak potential (mV) 
  target->KG=source.KG; //Constant fixed by the leak potential (mV/ms)

  //Connections
  target->BtoA=source.BtoA;//If source. one BCell connects a unique ACell, if 1 WAB=WBA
  target->IsPooling=source.IsPooling; //Pooling if true
  target->wba=source.wba; //Bipolar Amacrine  weight (kHz)
  target->WBA=NULL; //Connectivity matrix BA
  target->wab=source.wab; //Amacrine Bipolar weight (kHz)
  target->WAB=NULL; //Connectivity matrix AB
  target->wbg=source.wbg; //Bipolar Ganglion weight in the Gaussian pooling (kHz)
  target->WBG=NULL; //Connectivity matrix BG
  target->wag=source.wag; //Amacrine Ganglion weight (kHz)
  target->WAG=NULL; //Connectivity matrix AG
  target->sigmabg2=source.sigmabg2;//Bipolar Ganglion variance in the Gaussian pooling (mm^2)
  target->cL=NULL;//Linear transport operator

  //Global
  target->unsurtau=source.unsurtau;//1/tauA-1/tauB (kHz)
  target->unsurtauAB=source.unsurtauAB;//1/tauA+1/tauB (kHz)
  target->tau=source.tau; //1/unsurtau (ms)
  target->tau2=source.tau2;//tau**2 (ms^2)
  target->tauAB=source.tauAB;//1/(unsurtauA+unsurtauB) (ms)
  target->mu=source.mu;//Critical value for instability (dimensionless)

  //Eigenvalues and eigenvectors
  target->lambda_th=NULL;
  target->P=NULL;
  target->Pinv=NULL;
  target->rhop=NULL;
  target->rhom=NULL;

  //Pulse
  target->AmpStim=source.AmpStim; //Amplitude of the pulse (mV)
  target->speed=source.speed; //Pulse' speed (mm/ms)
  target->strength=source.strength; //Strength of the stimulus
  target->mean=source.mean; //Pulse peak location (mm)
  target->sigma=source.sigma; //Pulse width (mm)
  target->tmin=source.tmin;//-1;//initial time (ms)
  target->tmax=source.tmax; //Final time (ms)
  target->T=source.T; //Period of a periodic forcing
  target->omega=source.omega; //Frequency of a periodic forcing
  target->NPer=source.NPer; //Number of periods in the periodic forcing
  
  //Moving bar
  target->Bar_params=source.Bar_params;

  //Gnuplot variables
  target->nbviewmax=source.nbviewmax; //Maximum number of views in the gnuplot file
  target->countmax=source.countmax; //Maximum count between time steps of views
  target->wc=source.wc; //Critical weight (kHz)
  target->VBmin=source.VBmin;//Bipolar voltage min (mV)
  target->VBmax=source.VBmax;//Bipolar voltage max (mV)
  target->VAmin=source.VAmin;//Amacrine voltage min (mV)
  target->VAmax=source.VAmax;//Amacrine voltage max (mV)
  target->VGmin=source.VGmin;//Ganglion voltage min (mV)
  target->VGmax=source.VGmax;//Ganglion voltage max (mV)
}

void UpdateParams(params *p){//Update the global parameters of p
  p->unsurtauB=1.0/p->tauB;//,unsurtaua=1.0/p.taua;
  p->tauRF3= pow(p->tauRF,3); 
  p->unsurtauA=1.0/p->tauA;//1/tauA (kHz)
  p->unsurtauG=1.0/p->tauG;
  p->unsurtau=p->unsurtauA-p->unsurtauB;//1/tauA-1/tauB (kHz)
  p->unsurtauAB=p->unsurtauA+p->unsurtauB;//1/tauA+1/tauB (kHz)
  if ((fabs(p->unsurtau)>1E-10)) {
    p->tau=1.0/p->unsurtau;
  }
  else {
    printf("Case tau^-1=0 not implemented yet, exiting\n");
    exit(0);
  }
  p->tau2=pow(p->tau,2);
  p->tauAB=1.0/p->unsurtauAB;
  p->mu=-p->wab*p->wba*p->tau2;

}

/*** Statistics section  ***/
//Load value in the histo structure
void Update_Histo(Cell_id_type cell, Histo* histo){
  Histo::iterator it=histo->find(cell);
  if (it==histo->end()){//value was not found before. Creating the corresponding bin
    histo->insert (std::pair<Cell_id_type ,double>(cell,1) );
  }
  else{//value was found before. Updating the probability
    it->second++;
  }
}

//Compute probabilities and moments conditionally upon the cell type
void compute_Moments(Histo *histo, moments *moms){
  double norm_ON=0,norm_OFF=0,norm=0;

  for (Histo::iterator it=histo->begin();it!=histo->end();++it){//Normalisation
    norm+=it->second;//Full probability
    if (it->first.second=="ON") norm_ON+=it->second;//Conditional ON probability
    else if (it->first.second=="OFF") norm_OFF+=it->second;//Conditional OFF probability
  }

  moms->mean=moms->std=moms->mean_ON=moms->std_ON=moms->mean_OFF=moms->std_OFF=0;
  for (Histo::iterator it=histo->begin();it!=histo->end();++it){//Moments
    double value=it->first.first;string type=it->first.second;
    if (type=="ON") {//Conditional ON moments
      moms->mean_ON+=value*it->second;//Here it->second counts the number of occurences
      moms->std_ON+=pow(value,2)*it->second;
    }
    else if (type=="OFF"){//Conditional OFF moments
      moms->mean_OFF+=value*it->second;//Here it->second counts the number of occurences
      moms->std_OFF+=pow(value,2)*it->second;
    }
 
    it->second/=norm;//Normalising the full probability
    moms->mean+=value*it->second;//Full moments. //Here it->second is a full probability
    moms->std+=pow(value,2)*it->second;
  }//End of moments

  //Finalising moments
  moms->std=moms->std-pow(moms->mean,2);
  if (moms->std>=0) moms->std=sqrt(moms->std);
  else  {printf("\t\t\t\tProblem in compute_Moments: std =%lg is negative. Aborting\n",moms->std);exit(0);}

  //ON
  moms->mean_ON/=norm_ON;
  moms->std_ON=(moms->std_ON-pow(moms->mean_ON,2)*norm_ON)/(norm_ON-1); //We use here the unbiased estimator of the variance.
  if (moms->std_ON>=0) moms->std_ON=sqrt(moms->std_ON);
  else  {printf("\t\t\t\tProblem in compute_Moments: std ON=%lg is negative. Aborting\n",moms->std_ON);exit(0);}

  //OFF
  moms->mean_OFF/=norm_OFF;
  moms->std_OFF=(moms->std_OFF-pow(moms->mean_OFF,2)*norm_OFF)/(norm_OFF-1); //We use here the unbiased estimator of the variance.
  if (moms->std_OFF>=0) moms->std_OFF=sqrt(moms->std_OFF);
  else  {printf("\t\t\t\tProblem in compute_Moments: std OFF=%lg is negative. Aborting\n",moms->std_OFF);exit(0);}
}

/*** Matrices section ***/

//Free a gsl_vector_complex. The gsl_vector_free method does not deallocate properly.
void My_gsl_vector_complex_free(gsl_vector_complex *M){
  //if (M->owner==0) free(M->block);
  //gsl_vector_complex_free(M);
  //free(M->data);
  free(M->block);
  free(M->data);
  free(M);
}

//Free a gsl_matrix. The gsl_matrix_free method does not deallocate properly.
void My_gsl_matrix_complex_free(gsl_matrix_complex *M){
  //  if (M->owner==0) free(M->block);
  //gsl_matrix_complex_free(M);
  //free(M->block);
  //free(M->data);
  free(M->block);
  free(M->data);
  free(M);
}

//Free a gsl_vector. The gsl_vector_free method does not deallocate properly.
void My_gsl_vector_free(gsl_vector *M){
  //  if (M->owner==0) free(M->block);
  //gsl_vector_free(M);
  //free(M->block);
  //free(M->data);
  free(M->block);
  free(M->data);
  free(M);
}

//Free a gsl_matrix. The gsl_matrix_free method does not deallocate properly.
void My_gsl_matrix_free(gsl_matrix *M){
  //if (M->owner==0)     free(M->block);
  //gsl_matrix_free(M);
  free(M->block);
  free(M->data);
  free(M);
}

//Initializes the connectivity matrices and the transport operator in Laplacian case. 
//p is the parameters vectors containing pointers to connectivity matrices
//Space_dim is the lattice dimension
void init_Network_Laplacian(params *p, int Space_dim){
  //Initialization of the network
  //WriteParams(*p);exit(0);
  //delete_params(p);
  int L=Space_dim*p->L;
  
  //Generating auxilaries matrices including null boundary conditions
  gsl_matrix *Wab= gsl_matrix_calloc(L+2*Space_dim,L+2*Space_dim); //Connectivity Amacrine to Bipolar with null boundaries
  gsl_matrix *Wba= gsl_matrix_calloc(L+2*Space_dim,L+2*Space_dim); //Connectivity Bipolar to  Amacrine with null boundaries
  
  switch(Space_dim){
  case 1:
    //Initializing Wab 
    for (int i=1;i<L+1;i++){
      for (int j=1;j<L+1;j++){
	if ((j==i+1)&&(j<L+2)) {
	  gsl_matrix_set (Wab,i,j,1) ;//Laplacian connectivity for ACell-> BCell 
	}
	if ((j==i-1)&&(j >0)) {
	  gsl_matrix_set (Wab,i,j,1);
	}
      }
    }
    
    switch(p->BtoA){
    case(0)://If 0 one BCell connects a unique ACell
      for (int i=1;i<L+1;i++)
	gsl_matrix_set (Wba,i,i,1); 
      break;
    case (1)://If 1 WBA=WAB
      gsl_matrix_memcpy(Wba,Wab);
      break;
    default:
      printf("BtoA=%d not implemented, aborting\n");
      exit(0);
      break;
    }
    break;
  case 2:
    printf("Dimension 2 not implemented yet\n");
    exit(0);
    break;
  default:
    printf("Space_dim=%d not implemented, aborting\n",Space_dim);
    exit(0);
    break;
  }

  //Connectivity matrices without boundaries
  p->WAB= gsl_matrix_calloc(L,L);//Connectivity Amacrine to Bipolar without boundaries
  p->WBA= gsl_matrix_calloc(L,L);//Connectivity Bipolar to Amacrine without boundaries
  p->WBG= gsl_matrix_calloc(L,L);//Connectivity Bipolar to Ganglion
  p->WAG= gsl_matrix_calloc(L,L);//Connectivity Amacrine to Ganglion
  
  //Bipolar Amacrine connectivity
  for (int i=0;i<L;i++){
    for (int j=0;j<L;j++){
      gsl_matrix_set (p->WAB,i,j,gsl_matrix_get(Wab,i+1,j+1));
      gsl_matrix_set (p->WBA,i,j,gsl_matrix_get(Wba,i+1,j+1));
    }
  }
  
  //Bipolar Ganglion connectivity. Gaussian pooling

  //The matrix WBG is multiplied by wbg
  if (p->IsPooling){//Gaussian pooling
    for (int i=0;i<L;i++){
      for (int j=0;j<L;j++){
	gsl_matrix_set (p->WBG,i,j,p->wbg*GaussianPoolWeight(i,j,*p));
      }
    }
  }
  else{//Identity
    for (int i=0;i<L;i++) gsl_matrix_set (p->WBG,i,i,p->wbg);
  }

  //The matrix WAG is multiplied by wag
  if (p->IsPooling){//Gaussian pooling
    for (int i=0;i<L;i++){
      for (int j=0;j<L;j++){
	gsl_matrix_set (p->WAG,i,j,p->wag*GaussianPoolWeight(i,j,*p));
      }
    }
  }
  else{//Identity
    for (int i=0;i<L;i++) gsl_matrix_set (p->WAG,i,i,p->wag);
  }

  //Generating the propagation operator cL
 
  int dim=3*p->L;//Dimension of the phase space without activities 
  int dim_ext=4*p->L;// Dimension of the extended phase space (including activities). N first lines => BCells; N+1 ... 2N, ACells; 2N+1..3N, GCells; 3N+1..4N BCell activity (gain control)
  p->cL= gsl_matrix_calloc(dim_ext,dim_ext);//Propagation operator
  p->P=gsl_matrix_complex_calloc(dim,dim);//Theoretical eigenvectors (in the subspace without activities).
  p->Pinv=gsl_matrix_complex_calloc(dim,dim);//Inverse of P
  p->lambda_th=gsl_vector_complex_calloc(dim); //Theoretical eigenvalues of the full linear operator cL (in the subspace without activities).

  for (int i=0;i<p->L;i++){
    int alpha=i;//Bipolar lines
    gsl_matrix_set (p->cL,alpha,alpha,-1.0/p->tauB);//Diagonal
    for (int j=0;j<p->L;j++){//Amacrine to bipolar
      int beta=p->L+j;
      gsl_matrix_set (p->cL,alpha,beta,p->wab*gsl_matrix_get(p->WAB,i,j));
    }
    
    alpha=i+p->L;//Amacrine lines
    gsl_matrix_set (p->cL,alpha,alpha,-1.0/p->tauA);//Diagonal
    for (int j=0;j<p->L;j++){//Bipolar to Amacrine
      int beta=j;
      gsl_matrix_set (p->cL,alpha,beta,p->wba*gsl_matrix_get(p->WBA,i,j));
    }
    
    alpha=i+2*p->L;//Ganglion lines
    gsl_matrix_set (p->cL,alpha,alpha,-1.0/p->tauG);//Diagonal
    for (int j=0;j<p->L;j++){//Bipolar to Ganglion
      int beta=j;
      gsl_matrix_set (p->cL,alpha,beta,gsl_matrix_get(p->WBG,i,j));
    }
    for (int j=0;j<p->L;j++){//Amacrine to Ganglion
      int beta=p->L+j;
      gsl_matrix_set (p->cL,alpha,beta,gsl_matrix_get(p->WAG,i,j));
    }
    
    alpha=i+3*p->L;//Activity line
    gsl_matrix_set (p->cL,alpha,alpha,-1.0/p->taua);
    gsl_matrix_set (p->cL,alpha,i,p->h);
    //printf("p.h=%g, L%d%d=%g\n",p->h,alpha,i,gsl_matrix_get(p->cL,alpha,i));

  }
  p->rhop=gsl_vector_complex_calloc(p->N);//Vector of $\rho^+_n$
  p->rhom=gsl_vector_complex_calloc(p->N);//Vector of $\rho^+_n$
  My_gsl_matrix_free(Wab);My_gsl_matrix_free(Wba);
}

void delete_params(params* p){//Free the pointers in p
  if (p->WBA!=NULL) {My_gsl_matrix_free(p->WBA);p->WBA=NULL;}
  if (p->WAB!=NULL){ My_gsl_matrix_free(p->WAB);p->WAB=NULL;}
  if (p->WBG!=NULL){ My_gsl_matrix_free(p->WBG);p->WBG=NULL;}
  if (p->WAG!=NULL){ My_gsl_matrix_free(p->WAG);p->WAG=NULL;}
  if (p->cL!=NULL){ My_gsl_matrix_free(p->cL);p->cL=NULL;}
  if (p->lambda_th!=NULL){ My_gsl_vector_complex_free(p->lambda_th);p->lambda_th=NULL;}
  if (p->P!=NULL){ My_gsl_matrix_complex_free(p->P);p->P=NULL;}
  if (p->Pinv!=NULL){ My_gsl_matrix_complex_free(p->Pinv);p->Pinv=NULL;}
  if (p->rhop!=NULL){ My_gsl_vector_complex_free(p->rhop);p->rhop=NULL;}
  if (p->rhom!=NULL){ My_gsl_vector_complex_free(p->rhom);p->rhom=NULL;}

  /*if (p->WBA!=NULL) {free(p->WBA->data);p->WBA=NULL;}
  if (p->WAB!=NULL) {free(p->WAB->data);p->WAB=NULL;}
  if (p->WBG!=NULL) {free(p->WBG->data);p->WBG=NULL;}
  if (p->WAG!=NULL) {free(p->WAG->data);p->WAG=NULL;}
  if (p->cL!=NULL) {free(p->cL->data);p->cL=NULL;}
  if (p->lambda_th!=NULL) {free(p->lambda_th->data);p->lambda_th=NULL;}
  if (p->P!=NULL) {free(p->P->data);p->P=NULL;}
  if (p->Pinv!=NULL) {free(p->Pinv->data);p->Pinv=NULL;}
  if (p->rhop!=NULL) {free(p->rhop->data);p->rhop=NULL;}
  if (p->rhom!=NULL) {free(p->rhom->data);p->rhom=NULL;}*/

}

double GaussianPoolWeight(int i,int j, params p){//Bipolar pooling to GCells according to Chen et al 2013
  // printf("%lg\n",p.wbg*exp(-pow((i-j)*p.dx,2)/(2*p.sigmabg2)));
  return exp(-pow((i-j)*p.dx,2)/(2*p.sigmabg2))*u_s_s2pi;
}

void ViewMat(const gsl_matrix* M){//Viewing a matrix on the terminal. JNon zero entries are colored in red.
    for (int i=0;i<M->size1;i++){
        for (int j=0;j<M->size2;j++){
	  double Mij=gsl_matrix_get(M,i,j);
	  if (fabs(Mij)>1e-5) printf(" \033[0;31m%5.3f ",Mij);
	  else printf(" \033[0m%5.3f ",Mij);
        }
        printf("\n");
    }
 printf(" \033[0m");
}

void ViewMatComp(const gsl_matrix_complex* M){//Viewing a matrix on the terminal
    gsl_complex Mij;
    
    for (int i=0;i<M->size1;i++){
      for (int j=0;j<M->size2;j++){
	Mij=gsl_matrix_complex_get(M,i,j);
	if (gsl_complex_abs(Mij)>1e-5) printf(" \033[0;31m%5.2f +i%5.2f\t",GSL_REAL(Mij),GSL_IMAG(Mij));
	  else printf(" \033[0m%5.2f +i%5.2f\t",GSL_REAL(Mij),GSL_IMAG(Mij));
         }
        printf("\n");
    }
 printf(" \033[0m");
}

void ViewVect(const gsl_vector* V){//Viewing a vector on the terminal. Non zero entries are colored in red.
    for (int i=0;i<V->size;i++){
	  double Vi=gsl_vector_get(V,i);
	  if (fabs(Vi)>1e-5) printf(" \033[0;31m%5.3f ",Vi);
	  else printf(" \033[0m%5.3f ",Vi);
	  printf("\n");
    }
    printf(" \033[0m");
}

void ViewVectComp(const gsl_vector_complex* V){//Viewing a vector on the terminal
    gsl_complex Vi;
    
    for (int i=0;i<V->size;i++){
      Vi=gsl_vector_complex_get(V,i);
      if (gsl_complex_abs(Vi)>1e-5) printf(" \033[0;31m%5.2f +i%5.2f\t",GSL_REAL(Vi),GSL_IMAG(Vi));
      else printf(" \033[0m%5.2f +i%5.2f\t",GSL_REAL(Vi),GSL_IMAG(Vi));
      printf("\n");
    }

    printf(" \033[0m");
}

//Tuning wab from wba to keep the rest state constant. Holds in one d.
void tuning(params *p){
p->wab=-0.5*(p->wba-p->unsurtau);
if (p->wab>0) {printf("wab=%lg must be negative\n",p->wab);exit(0);}
}
 
/*** Receptive fields section ***/

//Exponential kernel
double E(double t,params p){
  return (t < 0 ? 0.0: exp(-t/p.tauRF)/p.tauRF);
}

//Exponential kernel complex used for convolution test
gsl_complex Ec(double t,params p,int beta){
  return {(t < 0 ? 0.0: exp(-t/p.tauRF)/p.tauRF),0};
}

//Gamma filter of order 1
double E1(double t, params p){
  return (t < 0 ? 0.0:t*exp(-t/p.tauRF)/pow(p.tauRF,2));
 }

//Gamma filter of order 2
double E2(double t, params p){
  return (t < 0 ? 0.0: pow(2*t,2)*exp(-2*t/p.tauRF)/p.tauRF3);
}

//Gamma filter of order 2. Complex used for convolution test
gsl_complex E2c(double t, params p,int beta){
  return  {E2(t,p),0};
}

//Standard Gaussian 
double Gauss(double t,params p){
  return (t < 0 ? 0.0: exp(-pow(t-5,2)/2.0)*u_s_s2pi);
}

//Standard Gaussian complex used for convolution test
gsl_complex Gaussc(double t,params p,int beta){
  return {(t < 0 ? 0.0: exp(-pow(t-5,2)/2.0)*u_s_s2pi),0};
}

// Gaussian random variable 
double Gauss(double moy, double var){
  return sqrt(-2.0*var*log(drand48()))*cos(dpi*drand48())+ moy;
}

//Function exp(lambdabeta*t) used in the numerical computation of GCell RF
gsl_complex Eb(double t,params p,int beta){
  if (t<0) return {0,0};
  gsl_complex lambdat=gsl_complex_mul_real(gsl_vector_complex_get(p.lambda_th,beta),t);
  return gsl_complex_exp(lambdat);
}

//Lorentzian centered at xc, with width width.
double Lorentz(double x,double xc,double width){
  return width/sqrt(pow(x-xc,2)+pow(width,2));
}

//RF Kernels. 
//RF K have norm 1. Response is of the form p.A0*K

//Spatial part of the RF of BCell beta.
double KS(int beta,double x,double y,params p){ 
  //return 1.0;////Check to be removed

  //Restricted to 1D for the moment
  double xbeta=beta*p.dx;
  //double ybeta=0; //Restricted to 1D for the moment
  //double d2=(pow(x-xbeta,2)+pow(y-ybeta,2))/2.0;
  double d2=pow(x-xbeta,2);
  double d2c=d2/(2.0*pow(p.sigma_c,2));
  double d2s=d2/(2.0*pow(p.sigma_s,2));

  return u_s_s2pi*(p.Ac*exp(-d2c)/p.sigma_c-p.As*exp(-d2s)/p.sigma_s); //Careful the normalisation is done for 1D
}

//Temporal part of BCells RF. Does not depend on the cell index
double KT(double t,params p){
 // return (t < 0 ? 0.0: pow(t,2)*exp(-t/p.tauRF)/(2*p.tauRF3));
  return (t < 0 ? 0.0: pow(t,2)*exp(-t/p.tauRF)/pow(p.tauRF,2));
}

//Time derivative of the temporal part of BCells RF. Does not depend on the cell index
double dKTbydt(double t,params p){
//  return (t < 0 ? 0.0: (2*t-pow(t,2)/p.tauRF)*exp(-t/p.tauRF)/(2*p.tauRF3));
  return (t < 0 ? 0.0: (2*t-pow(t,2)/p.tauRF)*exp(-t/p.tauRF)/pow(p.tauRF,2));
}

//Temporal part of BCells RF. Complex form used for convolution test
gsl_complex KTc(double t,params p,int beta){
  //return (t < 0 ? 0.0: pow(t,2)*exp(-t/p.tauRF)/p.tauRF3);
  return {KT(t,p),0};
}

//Spatio-temporal RF of BCell beta.
double K(int beta,double x,double y,double t,params p){
  return KS(beta,x,y,p)*KT(t,p);
}

/********* Stimuli and their derivatives ***********/

/**** Space uniform stimuli ****/

//Definition of a second order kernel starting at p.t0, defined from KT
double alpha2(double x,double t,params p){ 
  return  p.A0*KT(t-p.t0,p);
}

//Time derivative of the second order kernel 
double dalpha2bydt(double x,double t,params p){ 
  return p.A0*dKTbydt(t-p.t0,p);
}

//Definition of a second order kernel starting at p.t0, defined from KT with periodic modulation, the frequency omega being in the parameter p. 
double alpha2_periodic(double x,double t,params p){ 
  double sum=0;
  for (int n=-p.NPer;n<p.NPer;n++)
    sum+=KT(t-p.t0-n*p.T,p);
  
  return p.A0*sum;
}

//Time derivative of the second order kernel with periodic modulation, the frequency omega being in the parameter p. 
double dalpha2bydt_periodic(double x,double t,params p){ 

  double sum=0;
  for (int n=-p.NPer;n<p.NPer;n++)
    sum+=dKTbydt(t-p.t0-n*p.T,p);
    return p.A0*sum;
}

//cos stimulation, the frequency omega being in the parameter p. 
double Stim_cos(double x,double t,params p){

return  p.A0*cos(p.omega*t);
}

//Time derivative of the cos modulation, the frequency omega being in the parameter p. 
double dStim_cos_bydt(double x,double t,params p){

return  -p.A0*p.omega*sin(p.omega*t);
}

//sin stimulation, the frequency omega being in the parameter p. The minus sign is here to ease the computation of the complex susceptibility. 
double Stim_sin(double x,double t,params p){

return  -p.A0*sin(p.omega*t);
}

//Time derivative of the cos modulation, the frequency omega being in the parameter p. 
double dStim_sin_bydt(double x,double t,params p){

return  -p.A0*p.omega*cos(p.omega*t);
}

/*** Space dependent stimuli ****/

//Spatio temporal alpha kernel. The temporal part is KT=alpha2, the spatial part is KS(beta,x,p) where beta is the index of the BCell whose RF is computed and x is the point of space where the RF is computed.
double alpha2S(int beta,double x,double t,params p){ 
  return  p.A0*K(beta,x,0,t-p.t0,p);
}

//Time derivative of the second order spatio-temporal kernel 
double dalpha2Sbydt(int beta,double x,double t,params p){ 
  return p.A0*dKTbydt(t-p.t0,p)*KS(beta,x,0,p);  
}

//Spatio temporal alpha kernel with periodic modulation, the frequency omega being in the parameter p. The temporal part is KT=alpha2, the spatial part is KS(beta,x,p) where beta is the index of the BCell whose RF is computed and x is the point of space where the RF is computed.
double alpha2S_periodic(int beta,double x,double t,params p){ 
 
  double sum=0;
  for (int n=-p.NPer;n<p.NPer;n++)
    sum+=K(beta,x,0,t-p.t0-n*p.T,p);
 
   return p.A0*sum; 
}

//Time derivative of the second order spatio-temporal kernel with periodic modulation, the frequency omega being in the parameter p. 
double dalpha2Sbydt_periodic(int beta,double x,double t,params p){ 
    double sum=0;
  for (int n=-p.NPer;n<p.NPer;n++)
    sum+=dKTbydt(t-p.t0-n*p.T,p);

    return p.A0*sum*KS(beta,x,0,p);
}

//Spatio temporal alpha kernel with periodic modulation, the frequency omega being in the parameter p. The temporal part is KT=cos(omega*t), the spatial part is KS(beta,x,p) where beta is the index of the BCell whose RF is computed and x is the point of space where the RF is computed.
double Stim_S_cos(int beta,double x,double t,params p){
   
  return p.A0*KS(beta,x,0,p)*cos(p.omega*t);
}

//Time derivative of the second order kernel with cos stimulation, the frequency omega being in the parameter p. 
double dStim_S_cos_bydt(int beta,double x,double t,params p){

  return -p.A0*KS(beta,x,0,p)*p.omega*sin(p.omega*t);  
}

//Spatio temporal alpha kernel with periodic modulation, the frequency omega being in the parameter p. The temporal part is KT=sin(omega*t), the spatial part is KS(beta,x,p) where beta is the index of the BCell whose RF is computed and x is the point of space where the RF is computed.
double Stim_S_sin(int beta,double x,double t,params p){

  return -p.A0*KS(beta,x,0,p)*sin(p.omega*t);
}

//Time derivative of the second order kernel with cos stimulation, the frequency omega being in the parameter p. 
double dStim_S_sin_bydt(int beta,double x,double t,params p){

  return -p.A0*KS(beta,x,0,p)*p.omega*cos(p.omega*t);
}

/**** Bipolar inputs ****/

//Bipolar input (response to a alpha2 kernel at time t): ON cell. This function is actually independent of x (it corresponds to a spatially uniform flash). The variable x is added to be compatible with the C definition of the vector field.
double Bip_input_alpha2_ON(double x,double t,params p){
  return alpha2(x,t,p)/p.tauB + dalpha2bydt(x,t,p); 
}

//Bipolar input (response to a alpha2 kernel at time t): OFF cell. This function is actually independent of x (it corresponds to a spatially uniform flash). The variable x is added to be compatible with the C definition of the vector field.
double Bip_input_alpha2_OFF(double x,double t,params p){
  return -alpha2(x,t,p)/p.tauB - dalpha2bydt(x,t,p); 
}

//Bipolar input (response to a alpha2 kernel at time t): ON cell with periodic modulation, the frequency omega being in the parameter p. . This function is actually independent of x (it corresponds to a spatially uniform flash). The variable x is added to be compatible with the C definition of the vector field.
double Bip_input_alpha2_ON_periodic(double x,double t,params p){
  return alpha2_periodic(x,t,p)/p.tauB + dalpha2bydt_periodic(x,t,p); 
}

//Bipolar input (response to a cos stimulation at time t):the frequency omega being in the parameter p. . This function is actually independent of x (it corresponds to a spatially uniform flash). The variable x is added to be compatible with the C definition of the vector field.
double Bip_input_Stim_cos(double x,double t,params p){
  return Stim_cos(x,t,p); 
  //return Stim_cos(x,t,p)/p.tauB + dStim_cos_bydt(x,t,p); 
}

//Bipolar input (response to a sin stimulation at time t):the frequency omega being in the parameter p. This function is actually independent of x (it corresponds to a spatially uniform flash). The variable x is added to be compatible with the C definition of the vector field.
double Bip_input_Stim_sin(double x,double t,params p){
  return Stim_sin(x,t,p); 
  //return -Stim_sin(x,t,p)/p.tauB - dStim_sin_bydt(x,t,p); 
}

//Bipolar null input. Used to simulate transients
double Bip_input_null(double x,double t,params p){
  return 0.0; 
}

//Bipolar constant input. Used to simulate transients
double Bip_input_constant(double x,double t,params p){
  return p.b0; 
}


//Bipolar input (response to a alpha2S (spatial) kernel at time t   used in the dynamical system formulation):ON Cell. The temporal part is KT=alpha2, the spatial part is KS(beta,x,p) where beta is the index of the BCell whose RF is computed and x is the point of space where the RF is computed. 1D only.
double Bip_input_alpha2S_ON(int beta,double x,double t,params p){
  return alpha2S(beta,x,t,p)/p.tauB + dalpha2Sbydt(beta,x,t,p); 
}

//Bipolar input (response to a alpha2S (spatial) kernel at time t   used in the dynamical system formulation):ON Cell with periodic modulation, the frequency omega being in the parameter p. . The temporal part is KT=alpha2, the spatial part is KS(beta,x,p) where beta is the index of the BCell whose RF is computed and x is the point of space where the RF is computed. 1D only.
double Bip_input_alpha2S_ON_periodic(int beta,double x,double t,params p){
  return alpha2S_periodic(beta,x,t,p)/p.tauB + dalpha2Sbydt_periodic(beta,x,t,p); 
}

//Bipolar input (response to a alpha2S (spatial) kernel at time t   used in the dynamical system formulation):OFF Cell. The temporal part is KT=alpha2, the spatial part is KS(beta,x,p) where beta is the index of the BCell whose RF is computed and x is the point of space where the RF is computed. 1D only.
double Bip_input_alpha2S_OFF(int beta,double x,double t,params p){
  return -alpha2S(beta,x,t,p)/p.tauB - dalpha2Sbydt(beta,x,t,p); 
}

//Spatio temporal alpha kernel with periodic modulation, the frequency omega being in the parameter p. The temporal part is KT=cos(omega*t), the spatial part is KS(beta,x,p) where beta is the index of the BCell whose RF is computed and x is the point of space where the RF is computed.
double Bip_input_Stim_S_cos(int beta,double x,double t,params p){
  //return Stim_S_cos(beta,x,t,p)/p.tauB + dStim_S_cos_bydt(beta,x,t,p);
  return Stim_S_cos(beta,x,t,p);
}

//Spatio temporal alpha kernel with periodic modulation, the frequency omega being in the parameter p. The temporal part is KT=sin(omega*t), the spatial part is KS(beta,x,p) where beta is the index of the BCell whose RF is computed and x is the point of space where the RF is computed.
double Bip_input_Stim_S_sin_bydt(int beta,double x,double t,params p){
  //return Stim_S_sin(beta,x,t,p)/p.tauB + dStim_S_sin_bydt(beta,x,t,p);
  return Stim_S_sin(beta,x,t,p);
}


/**** Theoretical RF ****/

//Temporal part of the contribution beta to the RF of a GCell computed numerically by FFT. 
//lambda_beta is the eigenvalue beta of the transport operator L
//tau is the characteristic time of RF of BCells
//t is the time (>0)
gsl_complex UTnum(gsl_complex lambda_beta,double tau,double t){
  gsl_complex ut={0,0};
  exit(0);//Not implemented yet
  return ut;
}

//Temporal part of the contribution beta to the RF of a GCell. 
//lambda_beta is the eigenvalue beta of the transport operator L
//tau is the characteristic time of RF of BCells
//t is the time (>0)
//Calculation with complex. 
//This is the normalised, centered version (A0=1,b0=0).
//A0 and b0 are added when computing the RF, in the function KG
gsl_complex UT_c(gsl_complex lambda_beta, double t,params p){
  if (t<0) return {0,0};
  double tau2=pow(p.tauRF,2);//tauRF^2
  gsl_complex lambdatau_plus_un=gsl_complex_add({1.0},gsl_complex_mul_real(lambda_beta,p.tauRF));//1+\lambda_\beta\tau_RF//To match the normalisation of UT
  //gsl_complex denomA=gsl_complex_mul_real(gsl_complex_pow_real(lambdatau_plus_un,3),2*tau2);//\frac{1}{2\tau_RF^2(1+\lambda_\beta\tau_RF)^3}
 gsl_complex denomA=gsl_complex_mul_real(gsl_complex_pow_real(lambdatau_plus_un,3),p.tauRF);//\frac{1}{2\tau_RF^2(1+\lambda_\beta\tau_RF)^3}
  gsl_complex numA1=gsl_complex_mul_real(gsl_complex_exp(gsl_complex_mul_real(lambda_beta,t)),2*tau2);//2\tauRF^2 e^{\lambda_beta t}
  gsl_complex numA2=gsl_complex_mul_real(gsl_complex_pow_real(lambdatau_plus_un,2),pow(t,2)); //t^2(1+\lambda_\beta\tau_RF)^2
  gsl_complex numA3=gsl_complex_mul_real(lambdatau_plus_un,2*t*p.tauRF);//2t\tau_RF(1+\lambda_\beta\tau_RF)
  gsl_complex numA4={2*tau2,0};//2\tau_RF^2
  gsl_complex numA5=gsl_complex_mul_real(gsl_complex_add(gsl_complex_add(numA2,numA3),numA4),exp(-t/p.tauRF));//(t^2(1+\lambda_\beta\tau_RF)^2+2t\tau_RF(1+\lambda_\beta\tau_RF)+/2\tau_RF^2)e^{-t/tau_RF}
  gsl_complex numA=gsl_complex_sub(numA1,numA5);//\pare{2\tauRF^2 e^{\lambda_beta t}-(t^2(1+\lambda_\beta\tau_RF)^2+2t\tau_RF(1+\lambda_\beta\tau_RF)+/2\tau_RF^2)e^{-t/tau_RF}}
  gsl_complex A=gsl_complex_div(numA,denomA);
  //A=gsl_complex_mul_real(A,2*tau2);//To match UT whose integral is not normalized
  //  gsl_complex B=gsl_complex_div(gsl_complex_mul_real(gsl_complex_sub(gsl_complex_exp(gsl_complex_mul_real(lambda_beta,t)),{1,0}),p.b0),lambda_beta);//b0=0

  //return gsl_complex_add(A,B);

  return A;
}

//Temporal part of the contribution beta to the RF of a GCell. 
//lambda_beta is the eigenvalue beta of the transport operator L
//tau is the characteristic time of RF of BCells
//t is the time (>0)
//Calculation with real and imaginary part
//This is the normalised, centered version (A0=1,b0=0).
//A0 and b0 are added when computing the RF, in the function KG

gsl_complex UT(gsl_complex lambda_beta, double t,params p){

  if (t<0) return {0,0};

  //Analytical definition: real and imaginary part 
  double lambdar=GSL_REAL(lambda_beta),lambdai=GSL_IMAG(lambda_beta);
  double lambdartaup1=lambdar*p.tauRF+1;
  double lambdartaup1sq=pow(lambdartaup1,2);
  double tausq=pow(p.tauRF,2);   
  double lambdaisq=pow(lambdai,2); 
  double tsq=pow(t,2);
  double Cs=p.tauRF*pow(pow(lambdartaup1,2)+tausq*lambdaisq,3);
  //double Cs=2*tausq*pow(pow(lambdartaup1,2)+tausq*lambdaisq,3);//This normalisation is necessary to fit the computation with complex
  double epsurtau=exp(-t/p.tauRF);
  if (fabs(Cs)<1E-10) {//lambda = -1/tau
    //printf("Cs=%lg is small\n",Cs);
    return {pow(t,3)*epsurtau/(3*tausq),0};
  }

  double Ar=(lambdartaup1sq-lambdaisq*tausq)*tsq+2*p.tauRF*lambdartaup1*t+2*tausq;
  double Ai=2*lambdai*p.tauRF*t*(lambdartaup1*t+p.tauRF);
  double Br=exp(lambdar*t)*cos(lambdai*t);
  double Bi=exp(lambdar*t)*sin(lambdai*t);

  double Dr=pow(lambdartaup1,3)-3*tausq*lambdaisq*lambdartaup1;
  double Di=pow(p.tauRF*lambdai,3)-3*p.tauRF*lambdai*lambdartaup1sq;
  double fr1=2*tausq*(Br*Dr-Bi*Di);
  double fr2=(Ar*Dr-Ai*Di)*epsurtau;
  double UTr=(fr1-fr2)/Cs;
  double UTi= (2*tausq*(Bi*Dr+Br*Di)-(Ar*Di+Ai*Dr)*epsurtau)/Cs;
 
  return {UTr,UTi};
}

//Analytic Fourier transform of UT 
//lambda_beta is the eigenvalue beta of the transport operator L
//tau is the characteristic time of RF of BCells
//omega is the real frequency (>0)
gsl_complex UT_hat(gsl_complex lambda_beta, double f,params p){
  double omega=dpi*f;

  //Drive part
  gsl_complex Vhat=gsl_complex_inverse(gsl_complex_pow_real({1,omega*p.tauRF},3)); 
  Vhat=gsl_complex_mul_real(Vhat,2*p.tauRF);//To match the normalisation of UT.

  //lambda part
  gsl_complex chi=gsl_complex_inverse(gsl_complex_add(gsl_complex_negative(lambda_beta),{0,omega}));

  return gsl_complex_mul(Vhat,chi);
}

//Computes the Spatial contribution of the RF from all eigenmodes 
//x, y are the spatial coordinates where the RF is to be computed
//p defines the parameters of cells and network. In particular it contains the addresses of eigenvectors (P and P^-1) and eigenvalues. These vectors must be allocated before calling this function
//KSG contains the value of the spatial RF, of each eigenmode beta, at point x,y.
//NB:This is useful only if the BCell RF is separable
void KSG(double x,double y,params p,gsl_vector_complex* SumPinvbgKSg){
  int dim=3*p.N;//Dimension without activation
  
  for (int beta=0;beta<dim;beta++){//beta loop
    gsl_complex kg={0,0};
    for (int gama=0;gama<p.N;gama++){//gama loop on BCells
      double ksg=KS(gama,x,y,p);//printf("gama=%d\tksg=%lg\n",gama,ksg);
      gsl_complex Pinvb_ksg=gsl_complex_mul_real(gsl_matrix_complex_get(p.Pinv,beta,gama),ksg);
      kg=gsl_complex_add(kg,Pinvb_ksg);
    }//End of gama loop
    
    gsl_vector_complex_set(SumPinvbgKSg,beta,kg);
  }//End of beta loop
}

//CNO contribution from ACells
void KG_CNOA(params p,gsl_vector_complex* SumPinvbgKSg_CNOA){
  int dim=3*p.N;//Dimension without activation
  
  for (int beta=0;beta<dim;beta++){//beta loop
    gsl_complex kg={0,0};
    for (int gama=p.N;gama<2*p.N;gama++){//gama loop on ACells
      kg=gsl_complex_add(kg,gsl_matrix_complex_get(p.Pinv,beta,gama));
    }//End of gama loop
    
    gsl_vector_complex_set(SumPinvbgKSg_CNOA,beta,kg);
  }//End of beta loop
}

//Computes the RF of a GCell alpha embedded in the BCell, ACell, GCell network
//alpha is the cell index (must be between 2*N+1 and 3*N)
//x, y are the spatial coordinates where the RF is to be computed
//t is the time at which the RF is computed
//p defines the parameters of cells and network. In particular it contains the addresses of eigenvectors (P and P^-1) and eigenvalues. These vectors must be allocated before calling this function
//SumPinvbgKSg contains the value of the spatial RF, of each BCell gama, at point x,y.
//SumPinvbgKSg_CNOA contains the CNO ACells contribution.
//NB:This formula holds only if the BCell RF is separable
gsl_complex KG(int alpha,double x,double y,double t,params p,gsl_vector_complex* SumPinvbgKSg,gsl_vector_complex* SumPinvbgKSg_CNOA){
  
  //Drive contribution
  gsl_complex  kg={0,0};
  if ((alpha<2*p.N)||(alpha>3*p.N)) {printf("In KG the GCell index alpha=%d is not in the good range [%d,%d]\n",alpha,2*p.N+1,3*p.N);exit(0); }
  if ((p.lambda_th==NULL)||(p.P==NULL)||(p.Pinv==NULL)){printf("In KG, you must first diagonalize cL and initialize p.lambda_th,p.P,p.Pinv\n");exit(0);}
  
  //Drive contribution
  for (int beta=0;beta<3*p.N;beta++){//Mode beta loop
    gsl_complex lambda_beta=gsl_vector_complex_get(p.lambda_th,beta);
    if (GSL_REAL(lambda_beta)<0){
      gsl_complex Pab=gsl_matrix_complex_get(p.P,alpha,beta);
      gsl_complex factor=gsl_complex_mul(Pab,gsl_complex_add_real(lambda_beta,p.unsurtauB));
      gsl_complex Pab_factor_UT=gsl_complex_mul(gsl_complex_mul(factor,UT(lambda_beta,t,p)),gsl_vector_complex_get(SumPinvbgKSg,beta));
      kg=gsl_complex_add(kg,Pab_factor_UT);
    }
  }//End of mode beta loop

  kg=gsl_complex_mul_real(kg,p.A0);
  kg=gsl_complex_add_real(kg,p.b0);

  //CNO contribution
  gsl_complex kg_CNOA={0,0};
  
  if (p.zetaA!=0){//CNO Amacrine contribution
    for (int beta=0;beta<3*p.N;beta++){//Mode beta loop
      gsl_complex lambda_beta=gsl_vector_complex_get(p.lambda_th,beta);
      gsl_complex exp_lambda_beta_t=gsl_complex_exp(gsl_complex_mul_real(lambda_beta,t));
      gsl_complex xiCNOA=gsl_complex_div(gsl_complex_add_real(exp_lambda_beta_t,-1.0),lambda_beta);

      if (GSL_REAL(lambda_beta)<0){
	gsl_complex Pab=gsl_matrix_complex_get(p.P,alpha,beta);
	gsl_complex Pab_factor_CNOA=gsl_complex_mul(gsl_complex_mul(Pab,xiCNOA),gsl_vector_complex_get(SumPinvbgKSg_CNOA,beta));
	kg_CNOA=gsl_complex_add(kg_CNOA,Pab_factor_CNOA);
      }
    }//End of mode beta loop
    kg=  gsl_complex_add(kg,gsl_complex_mul_real(kg_CNOA,p.zetaA));
  }//End of CNO Amacrines contribution

  if (p.zetaG!=0){//CNO GCell contribution
    if (t>=0) kg=  gsl_complex_add_real(kg,p.tauG*p.zetaG*(1-exp(-t/p.tauG)));
  }

  /*  double modlambda2=pow(lambdar,2)+pow(lambdai,2);
  double xiCNOAr=p.zetaA*(lambdar*(1-Br)+lambdai*Bi)/modlambda2;
  double xiCNOAi=p.zetaA*(lambdar*Bi-lambdai*(1-Br))/modlambda2;
  double xiCNOGr=p.zetaG*(1-exp(-t/p.tauG));

  return {UTr+xiCNOAr,UTi+xiCNOAi};
  */

  return kg;
}


//Computes the Fourier transform of the RF of a GCell alpha embedded in the BCell, ACell, GCell network
//alpha is the cell index (must be between 2*N+1 and 3*N)
//x, y are the spatial coordinates where the RF is to be computed
//f is the frequency at which the RF is computed
//p defines the parameters of cells and network. In particular it contains the addresses of eigenvectors (P and P^-1) and eigenvalues. These vectors must be allocated before calling this function
//SumPinvbgKSg contains the value of the spatial RF, of each BCell gama, at point x,y.
//SumPinvbgKSg_CNOA contains the CNO ACells contribution.
//NB:This formula holds only if the BCell RF is separable
gsl_complex KG_hat(int alpha,double x,double y,double f,params p,gsl_vector_complex* SumPinvbgKSg,gsl_vector_complex* SumPinvbgKSg_CNOA){
  
  //Drive contribution
  gsl_complex  kg={0,0};
  if ((alpha<2*p.N)||(alpha>3*p.N)) {printf("In KG the GCell index alpha=%d is not in the good range [%d,%d]\n",alpha,2*p.N+1,3*p.N);exit(0); }
  if ((p.lambda_th==NULL)||(p.P==NULL)||(p.Pinv==NULL)){printf("In KG, you must first diagonalize cL and initialize p.lambda_th,p.P,p.Pinv\n");exit(0);}
  
  //Drive contribution
  for (int beta=0;beta<3*p.N;beta++){//Mode beta loop
    gsl_complex lambda_beta=gsl_vector_complex_get(p.lambda_th,beta);
    if (GSL_REAL(lambda_beta)<0){
      gsl_complex Pab=gsl_matrix_complex_get(p.P,alpha,beta);
      gsl_complex factor=gsl_complex_mul(Pab,gsl_complex_add_real(lambda_beta,p.unsurtauB));
      gsl_complex Pab_factor_UT=gsl_complex_mul(gsl_complex_mul(factor,UT_hat(lambda_beta,f,p)),gsl_vector_complex_get(SumPinvbgKSg,beta));
      kg=gsl_complex_add(kg,Pab_factor_UT);
    }
  }//End of mode beta loop

  kg=gsl_complex_mul_real(kg,p.A0);
  if (fabs(f)<=1E-6) kg=gsl_complex_add_real(kg,p.b0);//Add Dirac in the power spectrum

  //CNO contribution
  gsl_complex kg_CNOA={0,0};
  
  if (p.zetaA!=0){//CNO Amacrine contribution
    printf("In KG_hat: the CNO contribution must be implemented\n");
    exit(0);
    /*
    for (int beta=0;beta<3*p.N;beta++){//Mode beta loop
      gsl_complex lambda_beta=gsl_vector_complex_get(p.lambda_th,beta);
      gsl_complex exp_lambda_beta_t=gsl_complex_exp(gsl_complex_mul_real(lambda_beta,t));
      gsl_complex xiCNOA=gsl_complex_div(gsl_complex_add_real(exp_lambda_beta_t,-1.0),lambda_beta);

      if (GSL_REAL(lambda_beta)<0){
	gsl_complex Pab=gsl_matrix_complex_get(p.P,alpha,beta);
	gsl_complex Pab_factor_CNOA=gsl_complex_mul(gsl_complex_mul(Pab,xiCNOA),gsl_vector_complex_get(SumPinvbgKSg_CNOA,beta));
	kg_CNOA=gsl_complex_add(kg_CNOA,Pab_factor_CNOA);
      }
    }//End of mode beta loop
    kg=  gsl_complex_add(kg,gsl_complex_mul_real(kg_CNOA,p.zetaA));
    */
  }//End of CNO Amacrines contribution
  /*
  if (p.zetaG!=0){//CNO GCell contribution
    if (t>=0) kg=  gsl_complex_add_real(kg,p.tauG*p.zetaG*(1-exp(-t/p.tauG)));
  }
  */

  /*  double modlambda2=pow(lambdar,2)+pow(lambdai,2);
  double xiCNOAr=p.zetaA*(lambdar*(1-Br)+lambdai*Bi)/modlambda2;
  double xiCNOAi=p.zetaA*(lambdar*Bi-lambdai*(1-Br))/modlambda2;
  double xiCNOGr=p.zetaG*(1-exp(-t/p.tauG));

  return {UTr+xiCNOAr,UTi+xiCNOAi};
  */

  return kg;
}

//Computes the power spectrum of the RF of a GCell alpha embedded in the BCell, ACell, GCell network as a function of r,s (encoded in p). Also finds the peaks in the power spectrum, their location and their width return as vector
//alpha is the cell index (must be between 2*N+1 and 3*N)
//x, y are the spatial coordinates where the RF is to be computed
//p defines the parameters of cells and network. In particular it contains the addresses of eigenvectors (P and P^-1) and eigenvalues. These vectors must be allocated before calling this function
//NB:This formula holds only if the BCell RF is separable
std::vector<Peaks_fft> getPeriod(int alpha,double x,double y,params p){
  std::vector<Peaks_fft> periods;
  double r=p.tauA/p.tauB;
  double s=-p.wab/p.wba;
  int dim_ext=4*p.L;// Dimension of the phase space
  int dim=3*p.L;// Dimension of the phase space without activities

  init_Network_Laplacian(&p,1);
  
  gsl_matrix *Psi=gsl_matrix_calloc(p.L,p.L),*Psi_transpose=gsl_matrix_calloc(p.L,p.L);
  Spectrum kappa_th;//Theoretical eigenvalues of the connectivity matrix amacrine bipolar
  Spectrum sigma_th;//Theoretical eigenvalues of the connectivity matrix bipolar amacrine
  bool View=false; //If true print the matrices involved in the model
  bool checks=true; //If true checks spectrum and P^-1
  
  ComputeTheoreticalModesLaplacian1D(p,&kappa_th,&sigma_th,Psi,Psi_transpose);
  ComputeTheoreticalSpectrumL(kappa_th,sigma_th,Psi,Psi_transpose,&p);
  
  gsl_vector_complex* SumPinvbgKSg=gsl_vector_complex_calloc(dim);//Auxilary used for computation
  gsl_vector_complex* SumPinvbgKSg_CNOA=gsl_vector_complex_calloc(dim);//Auxilary used for computation
  KG_CNOA(p,SumPinvbgKSg_CNOA);//CNO A contribution
  
  KSG(x,y,p, SumPinvbgKSg);//KSG contains the value of the spatial RF, of each eigenmode beta, at point x,y.
  gsl_complex kg={0,0};//Computes the RF of a GCell alpha embedded in the BCell, ACell, GCell network

  //Computes, stores the analytic curve of the power spectrum, and locates the peaks
  std::vector<Event> Curve;
  double df=0.00001;
  for (double f=0;f<0.2;f+=df){//kHz
    Event point;
    gsl_complex z=KG_hat(alpha,x,y,f,p,SumPinvbgKSg,SumPinvbgKSg_CNOA);
    point.t=f;point.Amp=gsl_complex_abs(z);
    Curve.push_back(point);
  }
  
  //Locates the peaks
  //FILE *fp=fopen("PowerSpectrum_Test_rs","w");
  double chim=Curve[1].Amp,chi=Curve[2].Amp,chip=0,dchim=0,dchip=0,dchi2=0;
  for (int i=3;i<Curve.size()-1;i++){
    chip=Curve[i].Amp;
    dchip=(chip-chim)/(2*df);
    dchi2=(chip+chim-2*chi)/(df*df);
    //printf("%g\t%g\t%g\n",Curve[i].t,dchi,dchi2);
    if (((dchim*dchip)<0)&&(dchi2<0)){//The derivative changes its sign. Finding the place where it vanishes by a linear interpolation
      double a=(dchip-dchim)/(Curve[i].t-Curve[i-1].t);
      double b=dchip-a*Curve[i].t;
      double f_peak=-b/a;
      gsl_complex z=KG_hat(alpha,x,y,f_peak,p,SumPinvbgKSg,SumPinvbgKSg_CNOA);
      double maxi=gsl_complex_abs(z);
      double width=sqrt((sr2-2)*maxi/dchi2);
      //printf("Peak found at %g Hz, value=%g, width=%g Hz\n",1000*f_peak,maxi,1000*width); 
      Peaks_fft point={1000*f_peak,maxi,1000*width};//Hz
      periods.push_back(point);
    }//End of the derivative changes its sign. 
    chim=Curve[i-1].Amp;
    chi=Curve[i].Amp;
    dchim=dchip;
    //fprintf(fp,"%g\t%g\t%g\t%g\n",Curve[i].t,Curve[i].Amp,dchip,dchi2);
  }
  //fclose(fp);
  
  My_gsl_vector_complex_free(SumPinvbgKSg);
  My_gsl_vector_complex_free(SumPinvbgKSg_CNOA);
  My_gsl_matrix_free(Psi);My_gsl_matrix_free(Psi_transpose);
  
  return periods;
}

/** Convolutions and FFT section **/

//Gets the period of a real time signal contained in the vector experience.
//One first computes the FFT of the signal, sampled at the time rate sample_rate (in ms or s, depending on the user's choice) and extract the main peaks in the normalized power spectrum, i.e whose amplitude is larger than zero_PS . These peaks , their relative amplitude, and their width are returned on output. The power spectrum is stored in the file with name PowerSpectrum if the string  PowerSpectrum is non empty (different from ""). max_range is the maximal frequency range i.e. the maximal value where the amplitude of the normalized spectrum is larger than zero_PS.
std::vector<Peaks_fft> getPeriod(Experiment experience,double sample_rate,int *n_peaks,std::string PowerSpectrum,double* max_range){
  std::vector<Peaks_fft> periods;
  int data_size=experience.data.size();

  if (data_size==0) {printf("In getPeriod, the data vector is void. Aborting.\n");exit(0);}
  //Seeking n=inf{k; 2^k>=data_size} => data_size=2^{n-1}+r => data_size >= 2^{n-1} =>  log(data_size)/log(2) >= n-1 => n <= 1+log(data_size)/log(2)

  //int k=floor(1.0+log(data_size)/log(2.0));
  int k=floor(log(data_size)/log(2.0));// largest power of 2 containing the data
  int n=1<<k;
  printf("\t\t\tdata_size=%d, k=%d, 2^k=%g, n=%d\n",data_size,k,pow(2,k),n);
  
  double ArrayF1[2*n],PowSpect[2*n];
   int index=0;
   for (index=0;index<n;index++){//Fill sample arrays for F1 until the largest power of 2 containing the data
     REAL(ArrayF1,index) = experience.data[index].Amp; IMAG(ArrayF1,index) = 0;
   }

  //Computes the Fourier transform and stores it in a complex vector
   int err1=gsl_fft_complex_radix2_forward(ArrayF1,1,n);   
   if (err1==GSL_EDOM) {printf("In getPeriod, n=%d must be a power of 2\n",n);exit(0);}
   if (err1!=GSL_SUCCESS) {printf("In getPeriod, GSL error in fft computation\n",n);exit(0);}

   double delta_f=1.0/(n*sample_rate);//Frequency sampling in Hz

   //Computes the power spectrum
   gsl_complex z1={0,0},z2={0,0},Prod={0,0};
   double max_pow=0;
   
   for (index=0;index<n;index++){
     gsl_complex z1={REAL(ArrayF1,index),IMAG(ArrayF1,index)};
     gsl_complex z2={REAL(ArrayF1,index),-IMAG(ArrayF1,index)};
     Prod=gsl_complex_mul(z1,z2);
     REAL(PowSpect,index)=GSL_REAL(Prod)/n;
     max_pow=max(max_pow,REAL(PowSpect,index));
     IMAG(PowSpect,index)=GSL_IMAG(Prod)/n;
   }

   //Normalizes the spectrum
   for (index=0;index<n;index++) {
     REAL(PowSpect,index)=sqrt(REAL(PowSpect,index)/max_pow);
   }
   for (index=0;index<=n/2;index++)
     if (REAL(PowSpect,index)>zero_PS) *max_range=index*delta_f;

   *max_range*=1000; //kHz to Hz
   //printf("max_range=%g\n",*max_range);// exit(0);
   //Store the power spectrum. 
   if (strcmp(PowerSpectrum.c_str(),"")==0){
     //printf("FFT will not be stored\n");
     }
   else
     {
       printf("\t\t\tFFT will be stored in %s\n",PowerSpectrum.c_str());
       FILE *fp=fopen(PowerSpectrum.c_str(),"w");
       
       //Filling the file. Two loops are required to have the right order when gnuplotting with lines
       for (index=1;index<n/2;index++)  fprintf(fp,"%lg\t%lg\n",-(n/2-index)*delta_f,REAL(PowSpect,n/2+index));//left
       for (index=0;index<n/2;index++) fprintf(fp,"%lg\t%lg\n",index*delta_f,REAL(PowSpect,index));//right. 
       fclose(fp);
   }
 
   //Get the peaks   
   double maxi=-10000,max_abs=10E20,fmax=0,val=0;
   int idx_max=0;
   int max_peaks=5;
   bool IsNonZeroWidth=true;

   for (int k=0;(k<max_peaks)&&IsNonZeroWidth;k++){//Loop on peaks. Stops when the last peak has a width zero.
     for (index=0;index<n/2;index++){//Finding the max
       val=REAL(PowSpect,index);
       if ((maxi<max_abs)&&(maxi<val)) {
	 maxi=val;
	 fmax=index*delta_f;
	 idx_max=index;
       }
     }
     
     //Finding the width and removing the peak from the power spectrum
     double width=0;//witdh of the peak
     double val_ref=maxi/sr2; //Half height of the Lorenzian peak
     double val_p_left=0,val_m_left=0,val_p_right=0,val_m_right=0;//Auxilaries to find the peak width
     double PS_p_left=0,PS_m_left=0,PS_p_right=0,PS_m_right=0;//Auxilaries to find the peak width
     int i_p_right=0,i_m_right=0,i_p_left=0,i_m_left=0;//Auxilaries to find the peak width
     double a_left=0,b_left=0,a_right=0,b_right=0,w_left=0,w_right=0;//Auxilaries to find the peak width
     bool found_left=false,found_right=false;

     val_m_left=maxi;
     index=idx_max;

     if (index>0){//Descending on the left until a minimum
       do{
	 val_p_left=val_m_left;
	 REAL(PowSpect,index)=0;
	 index--;
	 val_m_left=REAL(PowSpect,index);
	 if ((val_p_left>=val_ref)&&(val_m_left)<val_ref) {//The half-width point is in between i_m_left and i_p_left
	   i_m_left=index;
	   PS_m_left=val_m_left;
	   i_p_left=index+1;
	   PS_p_left=val_p_left;
	   found_left=true;
	 }
       }while ((val_p_left>val_m_left)&&(index>0));//Stops when the gradient increases
     }//End of descending on the left

     val_m_right=maxi;
     index=idx_max;
     if (REAL(PowSpect,index+1)>0){//The peak has a non zero width on the right
       if (index<=n/2){//Descending on the right until a minimum
	 do{
	   val_p_right=val_m_right;
	   REAL(PowSpect,index)=0;
	   index++;
	   val_m_right=REAL(PowSpect,index);
	   
	   if ((val_p_right>=val_ref)&&(val_m_right<val_ref)){//The half-width point is in between i_m_right and i_p_right
	     i_m_right=index;
	     PS_m_right=val_m_right;
	     i_p_right=index-1;
	     PS_p_right=val_p_right;
	     found_right=true;
	   }
	 } while ((val_p_right>val_m_right)&&(index<=n/2));//Stops when the gradient increases
       }//End of descending on the right    
     }//End of the peak has a non zero width on the right
     else IsNonZeroWidth=false;

     //Resymetrize the spectrum
     for (index=0;index<n/2;index++) REAL(PowSpect,n/2+index)=REAL(PowSpect,n/2);

     if (IsNonZeroWidth==true){//The peak has a positive width
       //Linear interpolation of the width
       if (found_left==true){//Half-width point has been found on the left
	 a_left=(PS_m_left-PS_p_left)/((i_m_left-i_p_left)*delta_f);
	 b_left=PS_p_left-a_left*i_p_left*delta_f;
	 if (a_left>0) w_left=(maxi*usr2-b_left)/a_left;
	 else w_left=0;//The tangent is horizontal so the width is infinite
       }
       if (found_right==true){//Half-width point has been found on the right
  	 a_right=(PS_m_right-PS_p_right)/((i_m_right-i_p_right)*delta_f);
	 b_right=PS_p_right-a_right*i_p_right*delta_f;
	 if (a_right<0) w_right=(maxi*usr2-b_right)/a_right;
	 else w_right=0;//The tangent is horizontal so the width is infinite
       }
       if (found_left==true){//Half-width point has been found on the left
	 if (found_right==true){//Half-width point has been found on the right
	   width=fabs((w_right-w_left)/2.0);//Two sided case
	 }
	 else width=fabs(w_left-idx_max*delta_f);//Left sided case
       }
       else{//Half-width point has not been found on the left
	 if (found_right==true){//Half-width point has been found on the right
	   width=fabs(w_right-idx_max*delta_f);//Right sided case
	 }
	 else {//The peak is not significant. Ignoring it and stopping.
	   width=0;
	   break;
	 }
       }
     }//End of the peak has the positive width
     else//The peak is not significant. Ignoring it and stopping.
       break;

     Peaks_fft point={fmax,maxi,width*dpi};
     if (k==0){
       periods.push_back(point);
       REAL(PowSpect,0)=0;//Enleve le pic central en 0. Pas sur que ce soit correct.
     }
     else if (point.Amp >=0.05*periods[0].Amp) //Keeps only the relevant peaks
       periods.push_back(point);

     max_abs=maxi;
     maxi=-10000;

     //Test to be removed
     /*FILE* fp=fopen(PowerSpectrum.c_str(),"w");
       //Filling the file. Two loops are required to have the right order when plotting with lines
       for (index=1;index<n/2;index++)  fprintf(fp,"%lg\t%lg\n",-(n/2-index)*delta_f,REAL(PowSpect,n/2+index));//left
       for (index=0;index<n/2;index++) fprintf(fp,"%lg\t%lg\n",index*delta_f,REAL(PowSpect,index));//right. 
       fclose(fp);*/
     //End of test to be removed

   }
 
   *n_peaks=periods.size();

   //Normalising peaks to the maximum maximorum
   maxi=periods[0].Amp;
   for (int k=0;k<*n_peaks;k++) 
     periods[k].Amp/=maxi; 

   return periods;
}

//Gets the period of a real time signal contained in the file with name data. This file is a spatiotemporal file where the first coordinates (space) is xGC, the cell location.
//One first computes the FFT of the signal, sampled at the time rate sample_rate (in ms or s, depending on the user's choice) and extract the main peaks in the normalized power spectrum, i.e whose amplitude is larger than zero_PS . These peaks and their relative amplitude are returned on output. The power spectrum is stored in the file with name PowerSpectrum if the string  PowerSpectrum is non empty (different from ""). max_range is the maximal frequency range i.e. the maximal value where the amplitude of the normalized spectrum is larger than zero_PS.
std::vector<Peaks_fft> getPeriod(std::string Data,double xGC,double sample_rate,int *n_peaks,std::string PowerSpectrum,double* max_range){
  FILE* fp=fopen(Data.c_str(),"r");
  if (fp == NULL) {
    printf("In getPeriod. File %s can't be opened \n",Data.c_str());
    exit(1);
  }

  Experiment experience;
  Event point;
  double intpart=0,x=0,dx=1E-5;
  while ((fscanf(fp,"%lg%lg%lg\n",&x,&point.t,&point.Amp))!=EOF){
    //if ((fabs(x-xGC)<dx)&&(modf(point.t/sample_rate,&intpart)==0)) 
    if ((fabs(x-xGC)<dx)){
      if (modf(point.t/sample_rate,&intpart)==0){
	experience.data.push_back(point);
      }
    }
  }
  fclose(fp);
  
  std::vector<Peaks_fft> periods=getPeriod(experience,sample_rate,n_peaks,PowerSpectrum,max_range); 

  return periods;
}

//Gets the period of a signal contained in the Event vector with name data. 
//One first computes the FFT of the signal, sampled at the time rate sample_rate  (in ms or s, depending on the user's choice) and extract the main peaks in the normalized power spectrum, i.e whose amplitude is larger than zero_PS . These peaks and their relative amplitude are returned on output. The power spectrum is stored in the file with name PowerSpectrum if the string  PowerSpectrum is non empty (different from ""). max_range is the maximal frequency range i.e. the maximal value where the amplitude of the normalized spectrum is larger than zero_PS.
std::vector<Peaks_fft> getPeriod(vector <Event> data,double sample_rate,int *n_peaks,std::string PowerSpectrum,double* max_range){

  Experiment experience;
  Event point;
  for (int i=0;i<data.size();i++){
    experience.data.push_back(data[i]);
  }
  
  std::vector<Peaks_fft> periods=getPeriod(experience,sample_rate,n_peaks,PowerSpectrum,max_range); 

  return periods;
}

//Computes the numerical convolution of two real, causal (t>0) functions F1 and F2 of the variable t,  sampled with a time discretisation Delta with n samples where n has to be a power of 2, n=1<<PowTwo, over a time length T. Thus, Delta=T/n. Returns an array, convolution, of size n, corresponding to the convolution F1*F2 sampled at time resolution Delta
void convolution(gsl_complex (*F1)(double t,params p,int beta),gsl_complex (*F2)(double t,params p,int beta), params p,int beta,double T, int n,double convolution[]){
  double t=0;
  int index=0;
  double ArrayF1[2*n],ArrayF2[2*n];
  double Delta=T/(double)n;
  double norm=1/Delta;

  for (index=0;index<n;index++){//Fill sample arrays for F1,F2
    t=index*Delta;
    REAL(ArrayF1,index) = GSL_REAL(F1(t,p,beta)); IMAG(ArrayF1,index) = GSL_IMAG(F1(t,p,beta));
    REAL(ArrayF2,index) = GSL_REAL(F2(t,p,beta)); IMAG(ArrayF2,index) = GSL_IMAG(F2(t,p,beta));
  }

  //Computes the Fourier transforms and store them in a complex vector
   int err1=gsl_fft_complex_radix2_forward(ArrayF1,1,n);   
   if (err1==GSL_EDOM) {printf("In convolution, n=%d must be a power of 2\n",n);exit(0);}
   if (err1!=GSL_SUCCESS) {printf("In convolution, GSL error in fft computation\n",n);exit(0);}

   err1=gsl_fft_complex_radix2_forward(ArrayF2,1,n);  
   if (err1!=GSL_SUCCESS) {printf("In convolution, GSL error in fft computation\n",n);exit(0);}

   //Computes the product of Fourier transforms
   
   gsl_complex z1={0,0},z2={0,0},Prod={0,0};

   for (index=0;index<n;index++){
     gsl_complex z1={REAL(ArrayF1,index),IMAG(ArrayF1,index)};
     gsl_complex z2={REAL(ArrayF2,index),IMAG(ArrayF2,index)};
     Prod=gsl_complex_mul(z1,z2);
     REAL(convolution,index)=GSL_REAL(Prod)/norm;
     IMAG(convolution,index)=GSL_IMAG(Prod)/norm;
   }

   err1=gsl_fft_complex_radix2_inverse(convolution,1,n);
  if (err1!=GSL_SUCCESS) {printf("In convolution, GSL error in fft computation\n",n);exit(0);}
 
   return;
}

/*** Dynamics utilities section ***/

//Selects dynamical type
//int dyn_type //0 if linear, 1 if rectified without gain control, 2 if rectified with gain control
//f is the space independent vector field
//fS is the space dependent vector field
string Select_dyn_type(int dyn_type,void (**f)(double *X,double *Xp,double t,params p,double (*input)(double x,double t, params p)),void (**fS)(double x,double *X,double *Xp,double t,params p,double (*input)(int beta,double x,double t, params p))){
    string chain_type="";
    
    switch(dyn_type){//Definition of dynamical types
    case 0://Linear
      printf("\n\t***********************\n\tLinear dynamics\n\t***********************\n");
      chain_type="_Linear";
      *f=vector_field_lin;
      *fS=vector_field_Space_lin;
      break;
    case 1://Rectified without gain control
      printf("\n\t***********************\n\tRectified without gain control\n\t***********************\n");
      chain_type="_Rectified";
      *f=vector_field_Rect;
      *fS= vector_field_Space_Rect;
      break;
    case 2://Rectified with gain control
      printf("\n\t***********************\n\tRectified with gain control\n\t***********************\n");
      chain_type="_GainControl";
      *f=vector_field;
      *fS= vector_field_Space;
      printf("\n\t\t\033[0;31mGain control case? Check you have correctly initialized h\n\n");
      printf("\033[0m");
      break;
    }//End of definition of dynamical types

    return chain_type;
}

/** Analytic utilities section ***/

analytic Compute_Rest_Analytic(params p, int d,double VB_rest,double VA_rest,double VG_rest,double dVB_rest,double dVA_rest,double dVG_rest,double dtauBdzetaA,double dtauAdzetaA,bool *test){//V*_rest are the voltages at equilibrium. dV*_rest are the derivatives of Vwith respect to zeta. Analytic computations.d is the space dimension. dtauB, dtauA, are the derivatives of characteristic times with respect to zetaA. Test is true if all formula have been checked
  printf("\n\t\t\tChecking analytics\n");
  
  analytic ana;
  double tauB=p.tauB,tauA=p.tauA,tauG=p.tauG,wm=-p.wab,wp=p.wba,thetaA=p.thetaA,thetaB=p.thetaB,zetaA=p.zetaA,zetaG=p.zetaG,EA=EGlyc,EB=EAMPA,ECNOA=p.ECNOA;
  double num=1+4*d*d*tauA*tauB*wm*wp,nums=num*num;
  
  //VB
  ana.VB=VB_an(p,d);
  if (fabs(ana.VB-VB_rest)>=tol) {
    printf("\t\t\t\033[0;35mTest failed for VB_rest=%10.9lg != VB_an=%10.9lg.\n",ana.VB,VB_rest);
    printf("\033[0m");
    *test=false;
  }
  else printf("\t\t\t\tVB_Rest OK: VB_rest (%g)=VB_an (%g)\n",ana.VB,VB_rest);

  //VA
  ana.VA=VA_an(p,d);
  if (fabs(ana.VA-VA_rest)>=tol) {
    printf("\t\t\t\033[0;35mTest failed for VA_rest=%10.9lg != VA_an=%10.9lg. \n",ana.VA,VA_rest);
    printf("\033[0m");
    *test=false;
  }
  else printf("\t\t\t\tVA_Rest OK: VA_rest (%g)=VA_an (%g)\n",ana.VA,VA_rest);

  //VG
  ana.VG=VG_an(p,d);
  if (fabs(ana.VG-VG_rest)>=tol) {
    printf("\t\t\t\033[0;35mTest failed for VG_rest=%10.9lg != VG_an=%10.9lg.\n",VG_rest,ana.VG);
    printf("\033[0m");
    *test=false;
  }
  else printf("\t\t\t\tVG_Rest OK: VG_rest (%g)=VG_an (%g)\n",ana.VG,VG_rest);

  //Rectificationcondition B
  ana.zeta_RectB=-(thetaB/(2*d*tauB*wm)-thetaA)/tauA;
  params pp=p,pm=p;
  pp.zetaA=ana.zeta_RectB;
  if (fabs(VB_an(pp,d)-thetaB)>=tol){
    printf("\t\t\t\033[0;35mTest failed for zeta_BA=%10.9lg: VB_an=%10.9lg>tol=%lg. \n",ana.zeta_RectB,VB_an(pp,d),tol);
    printf("\033[0m");
    *test=false;
    }
  else printf("\t\t\t\tRectB OK: zeta_RectB=%g;  VB_an(zeta_RectB) (%g)= thetaB(%g)\n",ana.zeta_RectB,VB_an(pp,d),thetaB);

   pp.zetaA=ana.zeta_RectB-0.1;
   if (VB_an(pp,d)<thetaB){
     printf("\t\t\t\033[0;35mTest failed for zeta_RectB=%10.9lg: VB_an(zeta_RectB-0.1)=%10.9lg < thetaB=%lg. \n",ana.zeta_RectB,VB_an(pp,d),thetaB);
    printf("\033[0m");
    *test=false;
    }
     else printf("\t\t\t\tSign RectB OK: VB_an(zeta_RectB-0.1)=%10.9lg > thetaB=%lg. \n",VB_an(pp,d),thetaB);

   //Rectificationcondition A
   ana.zeta_RectA=(thetaA+2*d*tauA*wp*thetaB)/tauA;
   pp.zetaA=ana.zeta_RectA;
   if (fabs(VA_an(pp,d)-thetaA)>=tol){
     printf("\t\t\t\033[0;35mTest failed for zeta_RectA=%10.9lg: VA_an=%10.9lg>tol=%lg. \n",ana.zeta_RectA,VA_an(pp,d),tol);
     printf("\033[0m");
     *test=false;
   }
  else printf("\t\t\t\tRectA OK: zeta_RectA=%g;  VA_an(zeta_RectA) (%g)= thetaA(%g)\n",ana.zeta_RectA,VA_an(pp,d),thetaA);

   pp.zetaA=ana.zeta_RectA+0.1;
   if (VA_an(pp,d)<thetaA){
     printf("\t\t\t\033[0;35mTest failed for zeta_RectA=%10.9lg: VA_an(zeta_RectA+0.1)=%10.9lg < thetaA=%lg. \n",ana.zeta_RectA,VA_an(pp,d),thetaA);
     printf("\033[0m");
     *test=false;
   }
   else printf("\t\t\t\tSign RectA OK: VA_an(zeta_RectA+0.1)=%10.9lg > thetaA=%lg. \n",VA_an(pp,d),thetaA);
 
  //tauB
   ana.tauB=tauB_an(p,d);
  if (fabs(ana.tauB-p.tauB)>=tol) {
    printf("\t\t\t\033[0;35mTest failed for tauB=%10.9lg != tauB_an=%10.9lg.\n",ana.tauB,p.tauB);
    printf("\033[0m");
    *test=false;
  }
  else printf("\t\t\t\ttauB OK: tauB (%g)=tauB_an (%g)\n",p.tauB,ana.tauB);
   
  //tauA
   ana.tauA=tauA_an(p,d);
  if (fabs(ana.tauA-p.tauA)>=tol) {
    printf("\t\t\t\033[0;35mTest failed for tauA=%10.9lg != tauA_an=%10.9lg.\n",ana.tauA,p.tauA);
    printf("\033[0m");
    *test=false;
  }
  else printf("\t\t\t\ttauA OK: tauA (%g)=tauA_an (%g)\n",p.tauA,ana.tauA);

   //HB
   if (fabs(HB(p,d))>tol){
     printf("\t\t\t\033[0;35mTest failed for |HB|=%10.9lg>tol=%lg. \n",HB(p,d),tol);
     printf("\033[0m");
     *test=false;
   }
   else printf("\t\t\t\tHB OK: HB (%g)> tol (%g)\n",HB(p,d),tol);

   //HA
   if (fabs(HA(p,d))>tol){
     printf("\t\t\t\033[0;35mTest failed for |HA|=%10.9lg>tol=%lg. \n",HA(p,d),tol);
     printf("\033[0m");
     *test=false;
   }
   else printf("\t\t\t\tHA OK: HA (%g)> tol (%g)\n",HA(p,d),tol);

   //Derivatives

   //zetaA
   pp=p; pp.zetaA+=eps_der;
   pm=p;pm.zetaA-=eps_der;
   
   //dVB/dzetaA
   double dVBdzetaA=(VB_an(pp,d)-VB_an(pm,d))/(2*eps_der);
   ana.dVBdzetaA=dVB_dzetaA_an(p,d);
   if (fabs(dVBdzetaA-ana.dVBdzetaA)>=tol) {
     printf("\t\t\t\033[0;35mTest failed for dVBdzetaA=%10.9lg != dVBdzetaA_an=%10.9lg.\n",dVBdzetaA,ana.dVBdzetaA);
     printf("\033[0m");
     *test=false;
  }
   else printf("\t\t\t\tdVB/dzetaA OK: dVB/dzetaA_num (%g) = dVB/dzetaA_ana (%g)\n",dVBdzetaA,ana.dVBdzetaA);

   //dVA/dzetaA
   double dVAdzetaA=(VA_an(pp,d)-VA_an(pm,d))/(2*eps_der);
   ana.dVAdzetaA=dVA_dzetaA_an(p,d);
   if (fabs(dVAdzetaA-ana.dVAdzetaA)>=tol) {
     printf("\t\t\t\033[0;35mTest failed for dVAdzetaA=%10.9lg != dVAdzetaA_an=%10.9lg.\n",dVAdzetaA,ana.dVAdzetaA);
     printf("\033[0m");
     *test=false;
  }
   else printf("\t\t\t\tdVA/dzetaA OK: dVA/dzetaA_num (%g) = dVA/dzetaA_ana (%g)\n",dVAdzetaA,ana.dVAdzetaA);

   //dHB/dzetaA
   double dHBdzetaA=(HB(pp,d)-HB(pm,d))/(2*eps_der);
   ana.dHBdzetaA=dHB_dzetaA_an(p,d);
   if (fabs(dHBdzetaA-ana.dHBdzetaA)>=tol) {
     printf("\t\t\t\033[0;35mTest failed for dHBdzetaA=%10.9lg != dHBdzetaA_an=%10.9lg.\n",dHBdzetaA,ana.dHBdzetaA);
     printf("\033[0m");
     *test=false;
  }
   else printf("\t\t\t\tdHB/dzetaA OK: dHB/dzetaA_num (%g) = dHB/dzetaA_ana (%g)\n",dHBdzetaA,ana.dHBdzetaA);

   //dHA/dzetaA
   double dHAdzetaA=(HA(pp,d)-HA(pm,d))/(2*eps_der);
   ana.dHAdzetaA=dHA_dzetaA_an(p,d);
   if (fabs(dHAdzetaA-ana.dHAdzetaA)>=tol) {
     printf("\t\t\t\033[0;35mTest failed for dHAdzetaA=%10.9lg != dHAdzetaA_an=%10.9lg.\n",dHAdzetaA,ana.dHAdzetaA);
     printf("\033[0m");
     *test=false;
   }
   else printf("\t\t\t\tdHA/dzetaA OK: dHA/dzetaA_num (%g) = dHA/dzetaA_ana (%g)\n",dHAdzetaA,ana.dHAdzetaA);

   //tauB
   pp=p; pp.tauB+=eps_der;
   pm=p;pm.tauB-=eps_der;

   //dVA/dtauB
   double dVAdtauB=(VA_an(pp,d)-VA_an(pm,d))/(2*eps_der);
   ana.dVAdtauB=dVA_dtauB_an(p,d);
   if (fabs(dVAdtauB-ana.dVAdtauB)>=tol) {
     printf("\t\t\t\033[0;35mTest failed for dVAdtauB=%10.9lg != dVAdtauB_an=%10.9lg.\n",dVAdtauB,ana.dVAdtauB);
     printf("\033[0m");
     *test=false;
  }
   else printf("\t\t\t\tdVA/dtauB OK: dVA/dtauB_num (%g) = dVA/dtauB_ana (%g)\n",dVAdtauB,ana.dVAdtauB);

   //dVB/dtauB
   double dVBdtauB=(VB_an(pp,d)-VB_an(pm,d))/(2*eps_der);
   ana.dVBdtauB=dVB_dtauB_an(p,d);
   if (fabs(dVBdtauB-ana.dVBdtauB)>=tol) {
     printf("\t\t\t\033[0;35mTest failed for dVBdtauB=%10.9lg != dVBdtauB_an=%10.9lg.\n",dVBdtauB,ana.dVBdtauB);
     printf("\033[0m");
     *test=false;
  }
   else printf("\t\t\t\tdVB/dtauB OK: dVB/dtauB_num (%g) = dVB/dtauB_ana (%g)\n",dVBdtauB,ana.dVBdtauB);

   //dHA/dtauB
   double dHAdtauB=(HA(pp,d)-HA(pm,d))/(2*eps_der);
   ana.dHAdtauB=dHA_dtauB_an(p,d);
   if (fabs(dHAdtauB-ana.dHAdtauB)>=tol) {
     printf("\t\t\t\033[0;35mTest failed for dHAdtauB=%10.9lg != dHAdtauB_an=%10.9lg.\n",dHAdtauB,ana.dHAdtauB);
     printf("\033[0m");
     *test=false;
  }
   else printf("\t\t\t\tdHA/dtauB OK: dHA/dtauB_num (%g) = dHA/dtauB_ana (%g)\n",dHAdtauB,ana.dHAdtauB);

   //dHB/dtauB
   double dHBdtauB=(HB(pp,d)-HB(pm,d))/(2*eps_der);
   ana.dHBdtauB=dHB_dtauB_an(p,d);
   if (fabs(dHBdtauB-ana.dHBdtauB)>=tol) {
     printf("\t\t\t\033[0;35mTest failed for dHBdtauB=%10.9lg != dHBdtauB_an=%10.9lg.\n",dHBdtauB,ana.dHBdtauB);
     printf("\033[0m");
     *test=false;
  }
   else printf("\t\t\t\tdHB/dtauB OK: dHB/dtauB_num (%g) = dHB/dtauB_ana (%g)\n",dHBdtauB,ana.dHBdtauB);

   //tauA
   pp=p; pp.tauA+=eps_der;
   pm=p;pm.tauA-=eps_der;
   
   //dVA/dtauA
   double dVAdtauA=(VA_an(pp,d)-VA_an(pm,d))/(2*eps_der);
   ana.dVAdtauA=dVA_dtauA_an(p,d);
   if (fabs(dVAdtauA-ana.dVAdtauA)>=tol) {
     printf("\t\t\t\033[0;35mTest failed for dVAdtauA=%10.9lg != dVAdtauA_an=%10.9lg.\n",dVAdtauA,ana.dVAdtauA);
     printf("\033[0m");
     *test=false;
  }
   else printf("\t\t\t\tdVA/dtauA OK: dVA/dtauA_num (%g) = dVA/dtauA_ana (%g)\n",dVAdtauA,ana.dVAdtauA);
   
   //dVB/dtauA
   double dVBdtauA=(VB_an(pp,d)-VB_an(pm,d))/(2*eps_der);
   ana.dVBdtauA=dVB_dtauA_an(p,d);
   if (fabs(dVBdtauA-ana.dVBdtauA)>=tol) {
     printf("\t\t\t\033[0;35mTest failed for dVBdtauA=%10.9lg != dVBdtauA_an=%10.9lg.\n",dVBdtauA,ana.dVBdtauA);
     printf("\033[0m");
     *test=false;
  }
   else printf("\t\t\t\tdVB/dtauA OK: dVB/dtauA_num (%g) = dVB/dtauA_ana (%g)\n",dVBdtauA,ana.dVBdtauA);

   //dHA/dtauA
   double dHAdtauA=(HA(pp,d)-HA(pm,d))/(2*eps_der);
   ana.dHAdtauA=1+2*d*wp*tauA*tauA*ana.dVBdtauA/EB;
   if (fabs(dHAdtauA-ana.dHAdtauA)>=tol) {
     printf("\t\t\t\033[0;35mTest failed for dHAdtauA=%10.9lg != dHAdtauA_an=%10.9lg.\n",dHAdtauA,ana.dHAdtauA);
     printf("\033[0m");
     *test=false;
  }
   else printf("\t\t\t\tdHA/dtauA OK: dHA/dtauA_num (%g) = dHA/dtauA_ana (%g)\n",dHAdtauA,ana.dHAdtauA);

   //dHB/dtauA
   double dHBdtauA=(HB(pp,d)-HB(pm,d))/(2*eps_der);
   ana.dHBdtauA=-2*d*wm*tauB*tauB*ana.dVAdtauA/EA;
   if (fabs(dHBdtauA-ana.dHBdtauA)>=tol) {
     printf("\t\t\t\033[0;35mTest failed for dHBdtauA=%10.9lg != dHBdtauA_an=%10.9lg.\n",dHBdtauA,ana.dHBdtauA);
     printf("\033[0m");
     *test=false;
  }
   else printf("\t\t\t\tdHB/dtauA OK: dHB/dtauA_num (%g) = dHB/dtauA_ana (%g)\n",dHBdtauA,ana.dHBdtauA);

   //Implicit function theorem
   //Determinant
   double det=ana.dHBdtauB*ana.dHAdtauA-ana.dHBdtauA*ana.dHAdtauB;
   ana.det=detH(p,d);
   if (fabs(det-ana.det)>=tol) {
     printf("\t\t\t\033[0;35mTest failed for det=%10.9lg != det_an=%10.9lg.\n",det,ana.det);
     printf("\033[0m");
     *test=false;
  }
   else printf("\t\t\t\tdet OK: det_num (%g) = det_ana (%g)\n",det,ana.det);

   //Vanishing of the determinant
   if (zetaD(p,d)>0) {
     pp=p;pp.zetaA=zetaD(p,d);
     if (fabs(detH(pp,d))>=tol) {
       printf("\t\t\t\033[0;35mTest failed for zetaD. DetH=%10.9lg !=0.\n",detH(pp,d));
       printf("\033[0m");
       *test=false;
     }
     else printf("\t\t\t\t Test OK for zetaD. DetH=%10.9lg.\n",detH(pp,d));
 }

   //Numerator of dtauB/dzetaA
   double num_dtauB_dzetaA=-ana.dHAdtauA*ana.dHBdzetaA+ana.dHBdtauA*ana.dHAdzetaA;
   if (fabs(num_dtauB_dzetaA-num_dtauB_dzetaA_an(p,d))>=tol) {
     printf("\t\t\t\033[0;35mTest failed for numerator of dtauB/dzetaA=%10.9lg != Num an =%10.9lg.\n",num_dtauB_dzetaA,num_dtauB_dzetaA_an(p,d));
     printf("\033[0m");
     *test=false;
  }
  else printf("\t\t\t\tTest OK for numerator of dtauB/dzetaA=%10.9lg = Num an =%10.9lg.\n",num_dtauB_dzetaA,num_dtauB_dzetaA_an(p,d));

   //Vanishing of the numerator of dtauB/dzetaA
   pp=p;pp.zetaA=zetaNBA(p,d);
   if (fabs(num_dtauB_dzetaA_an(pp,d))>=tol) {
     printf("\t\t\t\033[0;35mTest failed for zetaNBA. num_dtauB_dzetaA=%10.9lg !=0.\n",num_dtauB_dzetaA_an(pp,d));
     printf("\033[0m");
     *test=false;
  }
   else printf("\t\t\t\tTest OK for zetaNBA=%g corresponding to a \033[0;36mconductance g=%g nS]\033[0m. num_dtauB_dzetaA=%10.9lg. The rectification threshold is zeta <=%g\n",pp.zetaA,pp.zetaA*C/ECNOA,num_dtauB_dzetaA_an(pp,d),ana.zeta_RectB);

   //Sign of the numerator of dtauB/dzetaA
   pp.zetaA+=0.1; 
   if (num_dtauB_dzetaA_an(pp,d)<0) {
     printf("\t\t\t\033[0;35mTest failed for zetaNBA+0.1. num=%10.9lg<0 ought to be positive.\n",num_dtauB_dzetaA_an(pp,d));
     printf("\033[0m");
     *test=false;
  }
   else printf("\t\t\t\tTest OK for zetaNBA+0.1. num=%10.9lg>0.\n",num_dtauB_dzetaA_an(pp,d));

   //Numerator of dtauA/dzetaA
   double num_dtauA_dzetaA=ana.dHAdtauB*ana.dHBdzetaA-ana.dHBdtauB*ana.dHAdzetaA;
   if (fabs(num_dtauA_dzetaA-num_dtauA_dzetaA_an(p,d))>=tol) {
     printf("\t\t\t\033[0;35mTest failed for numerator of dtauA/dzetaA=%10.9lg != Num an =%10.9lg.\n",num_dtauA_dzetaA,num_dtauA_dzetaA_an(p,d));
     printf("\033[0m");
     *test=false;
  }
   else printf("\t\t\t\tTest OK for numerator of dtauA/dzetaA=%10.9lg = Num an =%10.9lg.\n",num_dtauA_dzetaA,num_dtauA_dzetaA_an(p,d));

   //Vanishing of the numerator of dtauA/dzetaA
   pp=p;pp.zetaA=zetaNAA(p,d);

   if (fabs(num_dtauA_dzetaA_an(pp,d))>=tol) {
     printf("\t\t\t\033[0;35mTest failed for zetaNAA. num_dtauA_dzetaA=%10.9lg !=0.\n",num_dtauA_dzetaA_an(pp,d));
     printf("\033[0m");
     *test=false;
   }
   else printf("\t\t\t\tTest OK for zetaNAA=%g corresponding to a \033[0;36mconductance g=%g nS]\033[0m. num_dtauA_dzetaA=%10.9lg. The rectification threshold is zeta >= %g\n",pp.zetaA,pp.zetaA*C/ECNOA,num_dtauA_dzetaA_an(pp,d),ana.zeta_RectA);

   //Sign of the numerator of dtauB/dzetaA
   pp.zetaA+=0.1; 
   if (pp.zetaA<0){
     printf("\t\t\t\033[0;35mzetaNAA=%10.9lg is negative.\n",pp.zetaA);
     printf("\033[0m");
   }
   else{
     if (num_dtauA_dzetaA_an(pp,d)<0) {
       printf("\t\t\t\033[0;35mTest failed for zetaNAA. num_dtauB_dzetaA=%10.9lg>0 ought to be positive.\n",num_dtauA_dzetaA_an(pp,d));
     printf("\033[0m");
     *test=false;
     }
     else printf("\t\t\t\tTest OK for zetaNAA+0.1. num=%10.9lg>0.\n",num_dtauA_dzetaA_an(pp,d));
     }

   //dtauB/dzetaA
   //pp=p; pp.zetaA+=eps_der;
   //pm=p;pm.zetaA-=eps_der;
   
   //double dtauBdzetaA=(tauB_an(pp,d)-tauB_an(pm,d))/(2*eps_der);
   ana.dtauBdzetaA=dtauB_dzetaA_an(p,d);
   //ana.dtauBdzetaA=(-ana.dHAdtauA*ana.dHBdzetaA+ana.dHBdtauA*ana.dHAdzetaA)/(ana.dHBdtauB*ana.dHAdtauA-ana.dHBdtauA*ana.dHAdtauB);
   if (fabs(dtauBdzetaA-ana.dtauBdzetaA)>=tol) {
     printf("\t\t\t\033[0;35mTest failed for dtauBdzetaA=%10.9lg != dtauBdzetaA_an=%10.9lg.\n",dtauBdzetaA,ana.dtauBdzetaA);
     printf("\033[0m");
     *test=false;
  }
   else printf("\t\t\t\tTest OK for dtauBdzetaA=%10.9lg != dtauBdzetaA_an=%10.9lg.\n",dtauBdzetaA,ana.dtauBdzetaA);

   //dtauA/dzetaA
   //double dtauAdzetaA=(tauA_an(pp,d)-tauA_an(pm,d))/(2*eps_der);
   ana.dtauAdzetaA=dtauA_dzetaA_an(p,d);
   if (fabs(dtauAdzetaA-ana.dtauAdzetaA)>=tol) {
     printf("\t\t\t\033[0;35mTest failed for dtauAdzetaA=%10.9lg != dtauAdzetaA_an=%10.9lg.\n",dtauAdzetaA,ana.dtauAdzetaA);
     printf("\033[0m");
     *test=false;
  }

   return ana;
}

double VB_an(params p,int d){//VB analytic. d is the space dimension.
  double tauB=p.tauB,tauA=p.tauA,tauG=p.tauG,wm=-p.wab,wp=p.wba,thetaA=p.thetaA,thetaB=p.thetaB,zetaA=p.zetaA,zetaG=p.zetaG;
  double num=1+4*d*d*tauA*tauB*wm*wp;
  return 2*d*tauB*wm*(2*d*tauA*wp*thetaB-tauA*zetaA+thetaA)/num;
}

double VA_an(params p,int d){//VA analytic. d is the space dimension.
  double tauB=p.tauB,tauA=p.tauA,tauG=p.tauG,wm=-p.wab,wp=p.wba,thetaA=p.thetaA,thetaB=p.thetaB,zetaA=p.zetaA,zetaG=p.zetaG;
  double num=1+4*d*d*tauA*tauB*wm*wp;
  return (2*d*tauA*wp*(2*d*tauB*wm*thetaA-thetaB)+tauA*zetaA)/num;
}

double dVA_dzetaA_an(params p,int d){//dVA/dzetaA
  double tauB=p.tauB,tauA=p.tauA,tauG=p.tauG,wm=-p.wab,wp=p.wba,thetaA=p.thetaA,thetaB=p.thetaB,zetaA=p.zetaA,zetaG=p.zetaG;
  double num=1+4*d*d*tauA*tauB*wm*wp;
  return tauA/num;
}

double dVB_dzetaA_an(params p,int d){//dVB/dzetaA
  double tauB=p.tauB,tauA=p.tauA,tauG=p.tauG,wm=-p.wab,wp=p.wba,thetaA=p.thetaA,thetaB=p.thetaB,zetaA=p.zetaA,zetaG=p.zetaG;
  double num=1+4*d*d*tauA*tauB*wm*wp;
  return -2*d*tauA*tauB*wm/num;
}

double dVB_dtauA_an(params p,int d){//dVB/dtauA
  double tauB=p.tauB,tauA=p.tauA,tauG=p.tauG,wm=-p.wab,wp=p.wba,thetaA=p.thetaA,thetaB=p.thetaB,zetaA=p.zetaA,zetaG=p.zetaG;
  double nums=pow(1+4*d*d*tauA*tauB*wm*wp,2);
  return -2*d*tauB*wm*(4*d*d*tauB*wm*wp*thetaA-2*d*wp*thetaB+zetaA)/nums;
}

double dVA_dtauA_an(params p,int d){//dVA/dtauA
  double tauB=p.tauB,tauA=p.tauA,tauG=p.tauG,wm=-p.wab,wp=p.wba,thetaA=p.thetaA,thetaB=p.thetaB,zetaA=p.zetaA,zetaG=p.zetaG;
  double nums=pow(1+4*d*d*tauA*tauB*wm*wp,2);
  return (4*d*d*tauB*wm*wp*thetaA-2*d*wp*thetaB+zetaA)/nums;
}

double dVA_dtauB_an(params p,int d){//dVA/dtauA
  double tauB=p.tauB,tauA=p.tauA,tauG=p.tauG,wm=-p.wab,wp=p.wba,thetaA=p.thetaA,thetaB=p.thetaB,zetaA=p.zetaA,zetaG=p.zetaG;
  double nums=pow(1+4*d*d*tauA*tauB*wm*wp,2);
  return 4*d*d*tauA*wm*wp*(thetaA+2*tauA*wp*thetaB-tauA*zetaA)/nums;
}

double dVB_dtauB_an(params p,int d){//dVB/dtauB
  double tauB=p.tauB,tauA=p.tauA,tauG=p.tauG,wm=-p.wab,wp=p.wba,thetaA=p.thetaA,thetaB=p.thetaB,zetaA=p.zetaA,zetaG=p.zetaG;
  double nums=pow(1+4*d*d*tauA*tauB*wm*wp,2);

  return 2*d*wm*(thetaA+2*tauA*wp*thetaB-tauA*zetaA)/nums;
}

double VG_an(params p,int d){//VG analytic. d is the space dimension.
  double tauG=p.tauG,thetaA=p.thetaA,thetaB=p.thetaB,zetaG=p.zetaG;
  return tauG*(p.wbg*(VB_an(p,d)-thetaB)+p.wag*(VA_an(p,d)-thetaA)+zetaG);
}

double tauB_an(params p,int d){//tauB analytic.
  double wm=-p.wab,EA=EGlyc;

  return  p.tauL/(1-2*d*p.tauL*wm*NA(VA_an(p,d),p)/EA);
}

double HB(params p,int d){//tauB implicit analytic eq. d is the space dimension.
  double wm=-p.wab,EA=EGlyc;

  return p.tauB- p.tauL/(1-2*d*p.tauL*wm*NA(VA_an(p,d),p)/EA);
}

double tauA_an(params p,int d){//tauA implicit analytic eq. d is the space dimension.
  double wp=p.wba,EB=EAMPA;

  return p.tauL/(1+2*d*p.tauL*wp*NB(VB_an(p,d),p)/EB+p.tauL*p.zetaA/p.ECNOA);
}

double HA(params p,int d){//tauA implicit analytic eq. d is the space dimension.
  double wp=p.wba,EB=EAMPA;

  return p.tauA- p.tauL/(1+2*d*p.tauL*wp*NB(VB_an(p,d),p)/EB+p.tauL*p.zetaA/p.ECNOA);
}

double dHA_dtauA_an(params p,int d){//dHA/dtauA
  double tauB=p.tauB,tauA=p.tauA,tauG=p.tauG,wm=-p.wab,wp=p.wba,thetaA=p.thetaA,thetaB=p.thetaB,EA=EGlyc,EB=EAMPA,zetaA=p.zetaA;

  return 1+2*d*wp*tauA*tauA*dVB_dtauA_an(p,d)/EB;
}

double dHB_dtauA_an(params p,int d){//dHB/dtauA
  double tauB=p.tauB,tauA=p.tauA,tauG=p.tauG,wm=-p.wab,wp=p.wba,thetaA=p.thetaA,thetaB=p.thetaB,EA=EGlyc,EB=EAMPA,zetaA=p.zetaA;

return -2*d*wm*tauB*tauB*dVA_dtauA_an(p,d)/EA;
}

double dHA_dtauB_an(params p,int d){//dHA/dtauA
  double tauB=p.tauB,tauA=p.tauA,tauG=p.tauG,wm=-p.wab,wp=p.wba,thetaA=p.thetaA,thetaB=p.thetaB,EA=EGlyc,EB=EAMPA,zetaA=p.zetaA;
  return 2*d*wp*tauA*tauA*dVB_dtauB_an(p,d)/EB;
}

double dHB_dtauB_an(params p,int d){//dHB/dtauB
  double tauB=p.tauB,tauA=p.tauA,tauG=p.tauG,wm=-p.wab,wp=p.wba,thetaA=p.thetaA,thetaB=p.thetaB,EA=EGlyc,EB=EAMPA,zetaA=p.zetaA;

  return 1-2*d*wm*tauB*tauB*dVA_dtauB_an(p,d)/EA;
}
double dHB_dzetaA_an(params p,int d){//dHB/dzetaA
  double tauB=p.tauB,tauA=p.tauA,tauG=p.tauG,wm=-p.wab,wp=p.wba,thetaA=p.thetaA,thetaB=p.thetaB,EA=EGlyc,EB=EAMPA,zetaA=p.zetaA;

  return -2*d*wm*tauB*tauB*dVA_dzetaA_an(p,d)/EA;
}

double dHA_dzetaA_an(params p,int d){//dHA/dzetaA
  double tauB=p.tauB,tauA=p.tauA,tauG=p.tauG,wm=-p.wab,wp=p.wba,thetaA=p.thetaA,thetaB=p.thetaB,EA=EGlyc,EB=EAMPA,zetaA=p.zetaA,ECNOA=p.ECNOA;

  return tauA*tauA*(1.0/ECNOA+2*d*wp*dVB_dzetaA_an(p,d)/EB);
}

double detH(params p,int d){//Determinant of H
  double tauB=p.tauB,tauA=p.tauA,tauG=p.tauG,wm=-p.wab,wp=p.wba,thetaA=p.thetaA,thetaB=p.thetaB,EA=EGlyc,EB=EAMPA,zetaA=p.zetaA;
    double num=1+4*d*d*tauA*tauB*wm*wp,nums=pow(num,2);
    
    double dHBdtauB=1-2*d*wm*tauB*tauB*dVA_dtauB_an(p,d)/EA,dHAdtauA=1+2*d*wp*tauA*tauA*dVB_dtauA_an(p,d)/EB,dHBdtauA=-2*d*wm*tauB*tauB*dVA_dtauA_an(p,d)/EA,dHAdtauB=2*d*wp*tauA*tauA*dVB_dtauB_an(p,d)/EB;
    //return dHBdtauB*dHAdtauA-dHBdtauA*dHAdtauB;
    //return 1-2*d*wm*pow(tauB,2)*dVA_dtauB_an(p,d)/EA+2*d*wp*pow(tauA,2)*dVB_dtauA_an(p,d)/EB + 4*pow(d,2)*pow(tauA,2)*pow(tauB,2)*wm*wp*(dVA_dtauA_an(p,d)*dVB_dtauB_an(p,d)-dVA_dtauB_an(p,d)*dVB_dtauA_an(p,d))/(EA*EB);
    //return 1-2*d*wm*pow(tauB,2)*dVA_dtauB_an(p,d)/EA+2*d*wp*pow(tauA,2)*dVB_dtauA_an(p,d)/EB + 4*pow(d,2)*pow(tauA,2)*pow(tauB,2)*wm*wp*((1+4*d*d*tauA*tauB*wm*wp)*dVA_dtauA_an(p,d)*dVB_dtauB_an(p,d))/(EA*EB);
    //return 1-2*d*wm*pow(tauB,2)*2*d*tauA*wp*dVB_dtauB_an(p,d)/EA-2*d*wp*pow(tauA,2)*2*d*tauB*wm*dVA_dtauA_an(p,d)/EB + 4*pow(d,2)*pow(tauA,2)*pow(tauB,2)*wm*wp*((1+4*d*d*tauA*tauB*wm*wp)*dVA_dtauA_an(p,d)*dVB_dtauB_an(p,d))/(EA*EB);

    //return 1-4*pow(d,2)*tauA*pow(tauB,2)*wm*wp*dVB_dtauB_an(p,d)/EA-4*pow(d,2)*tauB*pow(tauA,2)*wm*wp*dVA_dtauA_an(p,d)/EB+4*pow(d,2)*pow(tauA,2)*pow(tauB,2)*wm*wp*(1+4*d*d*tauA*tauB*wm*wp)*dVA_dtauA_an(p,d)*dVB_dtauB_an(p,d)/(EA*EB);
    //return (1-4*pow(d,2)*tauA*pow(tauB,2)*wm*wp*dVB_dtauB_an(p,d)/EA)*(1-4*pow(d,2)*tauB*pow(tauA,2)*wm*wp*dVA_dtauA_an(p,d)/EB)+4*pow(d,2)*pow(tauA,2)*pow(tauB,2)*wm*wp*dVA_dtauA_an(p,d)*dVB_dtauB_an(p,d)/(EA*EB);

double u=(zetaA-2*wp*thetaB)/nums;

 return 1+4*pow(d,2)*tauB*pow(tauA,2)*wm*wp*(2*d*wm*tauB/EA-1/EB)*u-8*pow(d,3)*pow(tauA,3)*pow(tauB,2)*pow(wm,2)*wp*num*pow(u,2)/(EA*EB);
}

double zetaD(params p,int d){//Vanishing of the determinant
  double tauB=p.tauB,tauA=p.tauA,tauG=p.tauG,wm=-p.wab,wp=p.wba,thetaA=p.thetaA,thetaB=p.thetaB,EA=EGlyc,EB=EAMPA,zetaA=p.zetaA;
  double num=1+4*pow(d,2)*tauA*tauB*wm*wp;
  double Delta=16*pow(d,4)*pow(tauB,2)*pow(tauA,4)*pow(wm,2)*pow(wp,2)*pow((2*d*wm*tauB/EA-1/EB),2)+32*pow(d,3)*pow(tauA,3)*pow(tauB,2)*pow(wm,2)*wp*num/(EA*EB);
  if (Delta<0){
    printf("\t\t\t\t\033[0;31mDelta=%g<0. No solution for zetaD\n",Delta);printf("\033[0m");
    return 0;
    }
  else{
    double coef=-2*pow(d,2)*tauB*pow(tauA,2)*wm*wp*(2*d*wm*tauB/EA-1/EB);
    double u1=coef-sqrt(Delta)/2;
    double u2=coef+sqrt(Delta)/2;
    printf("\t\t\t\tu1=%g, u2=%g\n",u1,u2);
    return u2;
  }
}

double num_dtauB_dzetaA_an(params p,int d){//Numerator of dtauB/dzetaA
  double tauB=p.tauB,tauA=p.tauA,tauG=p.tauG,wm=-p.wab,wp=p.wba,thetaA=p.thetaA,thetaB=p.thetaB,EA=EGlyc,EB=EAMPA,zetaA=p.zetaA,ECNOA=p.ECNOA;

  return -dHA_dtauA_an(p,d)*dHB_dzetaA_an(p,d)+dHB_dtauA_an(p,d)*dHA_dzetaA_an(p,d);
  return 2*d*wm*pow(tauB,2)*(dVA_dzetaA_an(p,d)-pow(tauA,2)*dVA_dtauA_an(p,d)/ECNOA)/EA;
}

double zetaNBA(params p, int d){//Vanishing of the numerator of dtauB/dzetaA
  double tauB=p.tauB,tauA=p.tauA,tauG=p.tauG,wm=-p.wab,wp=p.wba,thetaA=p.thetaA,thetaB=p.thetaB,EA=EGlyc,EB=EAMPA,zetaA=p.zetaA,ECNOA=p.ECNOA;

  //double XiBm=2*d*tauB*wm,XiAp=2*d*tauA*wp;
  //printf("\n\t\t\t\tInit%lg, with Xi %lg\n",ECNOA/tauA+4*pow(d,2)*tauB*wm*wp*(ECNOA-thetaA)+2*d*wp*thetaB,ECNOA*(1+(1-thetaA/ECNOA)*XiBm*XiAp+thetaB*XiAp/ECNOA)/tauA); 
  return ECNOA/tauA+4*pow(d,2)*tauB*wm*wp*(ECNOA-thetaA)+2*d*wp*thetaB; 
}

double num_dtauA_dzetaA_an(params p,int d){//Numerator of dtauA/dzetaA
  double tauB=p.tauB,tauA=p.tauA,tauG=p.tauG,wm=-p.wab,wp=p.wba,thetaA=p.thetaA,thetaB=p.thetaB,EA=EGlyc,EB=EAMPA,zetaA=p.zetaA,ECNOA=p.ECNOA;
  double num=1+4*d*d*tauA*tauB*wm*wp,nums=num*num;
  double u=(zetaA-2*wp*thetaB)/nums;

  //return dHA_dtauB_an(p,d)*dHB_dzetaA_an(p,d)-dHB_dtauB_an(p,d)*dHA_dzetaA_an(p,d);
  //return (2*d*wp*tauA*tauA*dVB_dtauB_an(p,d)/EB)*( -2*d*wm*tauB*tauB*dVA_dzetaA_an(p,d)/EA)-(1-2*d*wm*tauB*tauB*dVA_dtauB_an(p,d)/EA)*(tauA*tauA*(1.0/ECNOA+2*d*wp*dVB_dzetaA_an(p,d)/EB));
  //return -4*pow(d,2)*pow(tauA,2)*pow(tauB,2)*wm*wp*dVB_dtauB_an(p,d)*dVA_dzetaA_an(p,d)/(EA*EB)-pow(tauA,2)*(1.0/ECNOA-2*d*wm*tauB*tauB*dVA_dtauB_an(p,d)/(EA*ECNOA)+2*d*wp*dVB_dzetaA_an(p,d)/EB-4*pow(d,2)*pow(tauB,2)*wm*wp*dVA_dtauB_an(p,d)*dVB_dzetaA_an(p,d)/(EA*EB));
   // return pow(tauA,2)*(-1/ECNOA+2*d*wm*pow(tauB,2)*dVA_dtauB_an(p,d)/(EA*ECNOA)-2*d*wp*dVB_dzetaA_an(p,d)/EB+4*pow(d,2)*pow(tauB,2)*wm*wp*(dVA_dtauB_an(p,d)*dVB_dzetaA_an(p,d)-dVB_dtauB_an(p,d)*dVA_dzetaA_an(p,d))/(EA*EB));
  // return -2*d
  //return pow(tauA,2)*(-1/ECNOA+2*d*wm*pow(tauB,2)*(-4*pow(d,2)*pow(tauA,2)*wm*wp*u)/(EA*ECNOA)-2*d*wp*( -2*d*tauA*tauB*wm/num)/EB+4*pow(d,2)*pow(tauB,2)*wm*wp*((-4*pow(d,2)*pow(tauA,2)*wm*wp*u)*( -2*d*tauA*tauB*wm/num)-((-2*d*wm*tauA*u))*(tauA/num))/(EA*EB));
  // return pow(tauA,2)*(-1/ECNOA-(8*pow(d,3)*pow(tauA,2)*pow(tauB,2)*pow(wm,2)*wp*u)/(EA*ECNOA)+4*pow(d,2)*tauA*tauB*wm*wp/(num*EB)+4*pow(d,2)*pow(tauB,2)*wm*wp*(8*pow(d,3)*pow(tauA,3)*tauB*pow(wm,2)*wp*u/num+2*d*wm*pow(tauA,2)*u/num)/(EA*EB));
  //return pow(tauA,2)*(-1/ECNOA-(8*pow(d,3)*pow(tauA,2)*pow(tauB,2)*pow(wm,2)*wp*u)/(EA*ECNOA)+4*pow(d,2)*tauA*tauB*wm*wp/(num*EB)+8*pow(d,3)*pow(tauA,2)*pow(tauB,2)*pow(wm,2)*wp*(4*pow(d,2)*tauA*tauB*wm*wp+1)*u/(EA*EB*num));

  // return pow(tauA,2)*(-1/ECNOA  +4*pow(d,2)*tauA*tauB*wm*wp/(EB*num) +8*pow(d,3)*pow(tauA,2)*pow(tauB,2)*pow(wm,2)*wp*((1+4*pow(d,2)*tauA*tauB*wm*wp)/(num*EB)-1/ECNOA)*(u/EA));
  return pow(tauA,2)*(-1/ECNOA  +4*pow(d,2)*tauA*tauB*wm*wp/(EB*num) +8*pow(d,3)*pow(tauA,2)*pow(tauB,2)*pow(wm,2)*wp*(1/EB-1/ECNOA)*(u/EA));
}

double zetaNAA(params p, int d){//Vanishing of the numerator of dtauA/dzetaA
  double tauB=p.tauB,tauA=p.tauA,tauG=p.tauG,wm=-p.wab,wp=p.wba,thetaA=p.thetaA,thetaB=p.thetaB,EA=EGlyc,EB=EAMPA,zetaA=p.zetaA,ECNOA=p.ECNOA;
  double num=1+4*d*d*tauA*tauB*wm*wp,nums=num*num;

  double uA=-EA*(4*pow(d,2)*tauA*tauB*wm*wp/(EB*num)-1/ECNOA)/(8*pow(d,3)*pow(tauA,2)*pow(tauB,2)*pow(wm,2)*wp*(1/EB-1/ECNOA));

  double XiBm=2*d*tauB*wm,XiAp=2*d*tauA*wp;
  //printf("\n\t\t\t\tInit=%lg, with Xi=%lg\n",nums*uA+ 2*wp*thetaB,ECNOA*(XiAp*thetaB/ECNOA-(EA/ECNOA)*pow((1+XiAp*XiBm),2)*(((ECNOA/EB)*XiAp*XiBm/((1+XiAp*XiBm))-1)/(XiAp*pow(XiBm,2)*(ECNOA/EB-1))))/tauA); 

  //return 2*d*wp*thetaB-EA*nums*((4*pow(d,2)*tauA*tauB*wm*wp/(EB*num)-1/ECNOA)/(8*pow(d,3)*pow(tauA,2)*pow(tauB,2)*pow(wm,2)*wp*(1/EB-1/ECNOA)));
  
//return ECNOA*(2*d*wp*tauA*thetaB/ECNOA-(EA/ECNOA)*nums*tauA*((4*pow(d,2)*tauA*tauB*wm*wp/(EB*num)-1/ECNOA)/(8*pow(d,3)*pow(tauA,2)*pow(tauB,2)*pow(wm,2)*wp*(1/EB-1/ECNOA))))/tauA;

//return ECNOA*(XiAp*thetaB/ECNOA-(EA/ECNOA)*nums*((XiAp*XiBm/(EB*num)-1/ECNOA)/(XiAp*pow(XiBm,2)*(1/EB-1/ECNOA))))/tauA;

//return ECNOA*(XiAp*thetaB/ECNOA-(EA/ECNOA)*pow((1+XiAp*XiBm),2)*((XiAp*XiBm/(EB*(1+XiAp*XiBm))-1/ECNOA)/(XiAp*pow(XiBm,2)*(1/EB-1/ECNOA))))/tauA;

//return ECNOA*(XiAp*thetaB/ECNOA-(EA/ECNOA)*pow((1+XiAp*XiBm),2)*(((ECNOA/EB)*XiAp*XiBm/((1+XiAp*XiBm))-1)/(XiAp*pow(XiBm,2)*(ECNOA/EB-1))))/tauA;

  return 2*d*wp*thetaB+nums*uA ; 
}

double dtauB_dzetaA_an(params p, int d){//dtauB/dzetaA from the implicit function theorem
  return num_dtauB_dzetaA_an(p,d)/detH(p,d);
}

double dtauA_dzetaA_an(params p, int d){//dtauA/dzetaA from the implicit function theorem
  return num_dtauA_dzetaA_an(p,d)/detH(p,d);
}
